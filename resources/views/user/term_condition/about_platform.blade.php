@extends('user.layout.master',['menu'=>'support'])
@section('title',__('About Platform'))
@section('style')
@endsection
@section('name',Auth::user()->name)
@section('content')
<div class="main content-wrapper">
    <div class="account-area">
        <div class="container">
            <h3><strong>{{__('About Platform')}}</strong></h3>
            <br>
            <br>
            {!! $about_platform !!}
        </div>
    </div>
</div>
@endsection

@section('script')


@endsection
