@extends('user.layout.master',['menu'=>'support'])
@section('title',__('FAQS'))
@section('style')
    <style>
        .faqHeader {
            font-size: 27px;
            margin: 20px;
        }

        .panel-heading [data-toggle="collapse"]:after {
            font-family: 'Glyphicons Halflings';
            content: "\e072"; /* "play" icon */
            float: right;
            color: #0f7079;
            font-size: 18px;
            line-height: 22px;
            /* rotate "play" icon from > (right arrow) to down arrow */
            -webkit-transform: rotate(-90deg);
            -moz-transform: rotate(-90deg);
            -ms-transform: rotate(-90deg);
            -o-transform: rotate(-90deg);
            transform: rotate(-90deg);
        }

        .panel-heading [data-toggle="collapse"].collapsed:after {
            /* rotate "play" icon from > (right arrow) to ^ (up arrow) */
            -webkit-transform: rotate(90deg);
            -moz-transform: rotate(90deg);
            -ms-transform: rotate(90deg);
            -o-transform: rotate(90deg);
            transform: rotate(90deg);
            color: #454444;
        }
    </style>
@endsection
@section('name',Auth::user()->name)
@section('content')
<div class="main content-wrapper">
    <div class="main content-wrapper">
        <div class="account-area">
            <div class="container">
                <div class="page-header">
                    <h3><strong>{{__('MegaBlock FAQs')}}</strong></h3>
                </div>

                <div class="panel-group" id="accordion">
                    @if(isset($faqs))
                        @php ($i = 0)
                        @foreach($faqs as $faq)
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a class="accordion-toggle {{$i == 1 ? 'collapsed':''}} " data-toggle="collapse" data-parent="#accordion" href="#collapse{{$faq->id}}">{{$faq->title}}</a>
                                    </h4>
                                </div>
                                <div id="collapse{{$faq->id}}"  class="panel-collapse collapse {{$i == 0 ? 'in':''}}">
                                    <div class="panel-body">
                                        {{$faq->description}}
                                    </div>
                                </div>
                            </div>
                            @php ($i = 1)
                        @endforeach
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')


@endsection
