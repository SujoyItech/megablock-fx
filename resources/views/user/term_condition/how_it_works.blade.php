@extends('user.layout.master',['menu'=>'support'])
@section('title',__('How it works'))
@section('style')
@endsection
@section('name',Auth::user()->name)
@section('content')
<div class="main content-wrapper">
    <div class="main content-wrapper">
        <div class="account-area">
            <div class="container">
                <h3><strong>{{__('How it works?')}}</strong></h3>
                <br>
                <br>
                {!! $how_works_text !!}
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')


@endsection
