@extends('user.layout.master',['menu'=>'support'])
@section('title',__('Terms & Condition'))
@section('style')
@endsection
@section('name',Auth::user()->name)
@section('content')
<div class="main content-wrapper">
    <div class="main content-wrapper">
        <div class="account-area">
            <div class="container">
                <h3><strong>{{__('Terms & Conditions')}}</strong></h3>
                <br>
                <br>
                {!! $term_condition_text !!}
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')


@endsection
