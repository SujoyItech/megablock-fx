<div class="col-md-12 col-lg-12">
    <div class="dash-item" style="padding: 0;">
        <div class="col-md-4 col-lg-4 pips_block pips_block_left">
            <div class="col-md-5 text-right">
                <span class="pips_segments_num">{{isset($pips) ? $pips[0]->last_30 : ''}}</span>
            </div>
            <div class="col-md-7 pips_segments">
                <span class="pips_segments_pips">PIPS</span><br>
                <span class="pips_segments_text">{{__('LAST 30 DAYS')}}</span>
            </div>
        </div>
        <div class="col-md-4 col-lg-4 pips_block pips_block_left">
            <div class="col-md-5 text-right">
                <span class="pips_segments_num">{{isset($pips) ? $pips[0]->total_pips : ''}}</span>
            </div>
            <div class="col-md-7 pips_segments">
                <span class="pips_segments_pips">PIPS</span><br>
                <span class="pips_segments_text">{{__('PIPS LIFETIME')}}</span>
            </div>
        </div>
        <div class="col-md-4 col-lg-4 pips_block pips_block_right">
            <div class="col-md-4 text-right">
                <span class="pips_segments_num">{{isset($pips) ? $pips[0]->after_registration : ''}}</span>
            </div>
            <div class="col-md-8 pips_segments">
                <span class="pips_segments_pips">PIPS</span><br>
                <span class="pips_segments_text">{{__('SINCE REGISTRATION')}}</span>
            </div>
        </div>
        <div class="row"></div>
    </div>
    <div class="row"></div>
</div>
