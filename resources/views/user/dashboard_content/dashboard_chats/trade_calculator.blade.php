<div id="trading_calculator_map"></div>
<script type="text/javascript" src="https://s3.tradingview.com/tv.js"></script>
<script type="text/javascript">
    new TradingView.widget({
        "width": "100%",
        "height": "365",
        "symbol": "FX:EURGBP",
        "interval": "5",
        "timezone": "Etc/UTC",
        "theme": "Light",
        "style": "1",
        "locale": "en",
        "toolbar_bg": "#f1f3f6",
        "enable_publishing": false,
        "allow_symbol_change": true,
        "container_id": "trading_calculator_map"
    });
</script>
