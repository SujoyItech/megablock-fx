<div class="dash-item" style="padding: 15px">
    <h3 class="pb-10 pl-15 pr-15 pt-10" style="border-bottom: 1px solid #efefef; margin: 0 -15px">{{__('Trading Tools')}}</h3>
    <div class="pt-10 pb-10 mb-10">
        <style>
            .custom-btn-group button{
                margin: 5px 10px;
            }
            .dropdown-dashboard li > a:hover {
                background-image: none;
                background-color: #0F7079;
            }
            .map_scripts{
                padding-bottom: 5%;
            }
        </style>
        <div class="btn-group custom-btn-group" role="group" style="display: flex;flex-wrap: wrap;">
            <button type="button" id="economic_calendar" class="btn btn-sm btn-primary show_button active">{{__('Economic calendar')}}</button>
            <div class="btn-group">
                <button type="button" id="trading_calculator" class="btn btn btn-sm btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    {{__('Trading calculators')}} <span class="caret"></span>
                </button>
                <ul class="dropdown-menu dropdown-dashboard" style="top: 40px !important; left: initial !important;">
                    <li><a href="#"  id="position_size_calculator" class="btn btn-sm btn-primary show_button" style="color: #fff">Position Size Calculator</a></li>
                    <li><a href="#"  id="pip_calculator" class="btn btn-sm btn-primary show_button" style="color: #fff">Pip Calculator Widget</a></li>
                    <li><a href="#"  id="profit_calculator" class="btn btn-sm btn-primary show_button" style="color: #fff">Profit Calculator</a></li>
                    <li><a href="#"  id="margin_calculator" class="btn btn-sm btn-primary show_button" style="color: #fff">Margin Calculator</a></li>
                    <li><a href="#"  id="forex_pip_calculator" class="btn btn-sm btn-primary show_button" style="color: #fff">Forex Pip Calculator</a></li>
                    <li><a href="#"  id="pivot_point_calculator" class="btn btn-sm btn-primary show_button" style="color: #fff">Pivot Point Calculator</a></li>
                    <li><a href="#"  id="fibonacci_ratio_calculator" class="btn btn-sm btn-primary show_button" style="color: #fff">Fibonacci Ratios Calculator</a></li>
                </ul>
            </div>
            <button type="button" id="market_overview" class="btn btn-sm btn-primary show_button">{{__('Market Overview')}}</button>
            <button type="button" id="news_market" class="btn btn-sm btn-primary show_button">{{__('News')}}</button>
            <button type="button" id="cot_report" class="btn btn-sm btn-primary show_button">{{__('COT Report')}}</button>
            <button type="button" id="trader_sentiment" class="btn btn-sm btn-primary show_button">{{__('Trader sentiment')}}</button>
        </div>
    </div>
    <div class="economic_calendar map_scripts">
        @include('user.dashboard_content.dashboard_chats.economic_calendar')
    </div>
    <div class="trading_calculator map_scripts">
        @include('user.dashboard_content.dashboard_chats.trade_calculator')
    </div>
    <div class="news_market map_scripts">
        @include('user.dashboard_content.dashboard_chats.news_feed')
    </div>
    <div class="market_overview map_scripts">
        @include('user.dashboard_content.dashboard_chats.market_overview')
    </div>
    <div class="cot_report map_scripts">
        @include('user.dashboard_content.dashboard_chats.cot_reports')
    </div>
    <div class="trader_sentiment map_scripts">
        @include('user.dashboard_content.dashboard_chats.trader_sentiment')
    </div>
    <div class="position_size_calculator map_scripts">
        @include('user.dashboard_content.calculator.position_size')
    </div>
    <div class="pip_calculator map_scripts">
        @include('user.dashboard_content.calculator.pip_calculator_widget')
    </div>
    <div class="profit_calculator map_scripts">
        @include('user.dashboard_content.calculator.profit_calculator')
    </div>
    <div class="margin_calculator map_scripts">
        @include('user.dashboard_content.calculator.margin_calculator')
    </div>
    <div class="forex_pip_calculator map_scripts">
        @include('user.dashboard_content.calculator.forex_pip_calculator')
    </div>
    <div class="pivot_point_calculator map_scripts">
        @include('user.dashboard_content.calculator.pivot_point_calculator')
    </div>
    <div class="fibonacci_ratio_calculator map_scripts">
        @include('user.dashboard_content.calculator.fibonacci_calculator')
    </div>
</div>

<script>
    $(document).ready(function () {
        $('.map_scripts').hide();
        $('.economic_calendar').show();
        $('.economic_calendar').addClass('active');
    });
    $('.show_button').on('click', function () {
        $('.show_button').removeClass('active');
         $(this).addClass('active');
        var idval = $(this).attr('id');
        $('.map_scripts').hide();
        $('.' + idval).show();
    });
</script>

