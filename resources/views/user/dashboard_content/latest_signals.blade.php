<div class="dash-item" style="padding: 15px">
    <h3 class="pb-10 pl-15 pr-15 pt-10" style="border-bottom: 1px solid #efefef; margin: 0 -15px">
        Latest Signals</h3>
    @if(isset($signals) && !empty($signals))
        <table class="table-responsive mt-15">
            <thead>
            <tr class="">
                <th width="20%"></th>
                <th width="20%" class="pb-10 signal_tbl_td text-left">{{__('Action')}}</th>
                <th width="20%" class="pb-10 signal_tbl_td text-left">{{__('Entry Price')}}</th>
                <th width="20%" class="pb-10 signal_tbl_td text-left">{{__('Take Profit')}}</th>
                <th width="20%" class="pb-10 signal_tbl_td text-left">{{__('Stop Loss')}}</th>
                <th width="" class="pb-10 signal_tbl_td text-left">{{__('Status')}}</th>
            </tr>
            </thead>
            <tbody>
            @foreach($signals as $signal)
                <tr class="dashboard-signal custom-grid" data-signal="{{$signal->id}}" style="cursor: pointer;">
                    <td class="text-left pb-10 pt-10 pl-5 pr-5">{{$signal->financial_instrument_code}}</td>
                    <td class="text-left pb-10 pt-10 pl-5 pr-5">
                        @if($signal->action == SIGNAL_ACTION_BUY)
                            <label class="label label-primary pl-10 pr-10" style="border-radius: 10px">{{__('Buy')}}</label>
                        @else
                            <label class="label label-danger pl-10 pr-10" style="border-radius: 10px">{{__('Sell')}}</label>
                        @endif
                    </td>
                    <td class="text-left pb-10 pt-10 pl-5 pr-5">{{number_format($signal->entry_price,3)}}</td>
                    <td class="text-left pb-10 pt-10 pl-5 pr-5">{{number_format($signal->take_profit_1,3)}}</td>
                    <td class="text-left pb-10 pt-10">{{number_format($signal->stop_loss_1,3)}}</td>
                    <td class="text-center pb-10 pt-10 pl-5 pr-5">
                        <span>
                            @if($signal->status == SIGNAL_ACTIVE) <label
                                class="label label-success pl-10 pr-10"
                                style="border-radius: 10px">{{__('Active')}}</label>
                            @elseif($signal->status == SIGNAL_PENDING ) <label
                                class="label label-warning pl-10 pr-10"
                                style="border-radius: 10px">{{__('Pending')}}</label>
                            @elseif($signal->status == SIGNAL_CLOSED) <label
                                class="label label-danger pl-10 pr-10"
                                style="border-radius: 10px">{{__('Closed')}}</label>
                            @endif
                        </span>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    @else
        <h4>{{__('No signal found.')}}</h4>
    @endif
</div>
