<style>
    .modal-backdrop {
        display: none;
    }

    .modal-maps {
        position: fixed;
        top: 0;
        left: 200px;
        bottom: 0;
        z-index: 1050;
        display: none;
        overflow: hidden!important;
        -webkit-overflow-scrolling: touch;
        outline: 0;
        margin: 0;
        width: 500px!important;
        min-width: 500px!important;
        max-width: 500px!important;
        /*display: inline-block!important;*/
    }
    .modal-full{
        width: 100%!important;
    }
</style>
<div class="btn-group" role="group" aria-label="Basic example" style="">
    <button  type="button" id="cross_rate" class="btn btn-md btn-primary modal_button"><i class="fa fa-compress"></i> {{__('Cross rates')}}</button>
    <button  type="button" id="screener" class="btn btn-md btn-primary modal_button"><i class="fa fa-bar-chart"></i> {{__('Screener ')}}</button>
    <button  type="button" id="forex_heat_map" class="btn btn-md btn-primary modal_button"><i class="fa fa-thermometer-full" aria-hidden="true"></i> {{__('Forex Heat Map')}}</button>
    <button  type="button" id="tv" class="btn btn-md btn-primary modal_button"><i class="fa fa-television" aria-hidden="true"></i> {{__('TV')}}</button>
</div>
<div class="modal modal-maps inmodal fade" id="cross_rate_modal" role="dialog" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-full">
        <div class="modal-content animated">
            <div class="modal-header" style="cursor: move;">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                @include('user.dashboard_content.dashboard_maps.cross_rate')
            </div>
        </div>
    </div>
</div>
<div class="modal modal-maps fade" id="screener_modal" role="dialog" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-full">
        <div class="modal-content animated">
            <div class="modal-header" style="cursor: move;">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class="screener_data">
                    @include('user.dashboard_content.dashboard_maps.screener')
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal modal-maps fade" id="forex_heat_map_modal" role="dialog" data-backdrop="static">
    <div class="modal-dialog modal-full">
        <div class="modal-content animated">
            <div class="modal-header" style="cursor: move;">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                @include('user.dashboard_content.dashboard_maps.forex_heat_map')
            </div>
        </div>
    </div>
</div>
<div class="modal modal-maps fade" id="tv_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
    <div class="modal-dialog modal-full">
        <div class="modal-content animated">
            <div class="modal-header" style="cursor: move;">
                <button type="button" class="close" id="tv_button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class="tv_data">
                </div>
                @include('user.dashboard_content.dashboard_maps.tv')
            </div>
        </div>
    </div>
</div>

<script>

    $('#tv_button').on('click',function () {

        var player = videojs("hls-example");
        player.pause();
    });

    $('.modal_button').on('click', function () {
        var idval = $(this).attr('id');
        if(idval == 'tv'){
            var player = videojs("hls-example");
            player.play();
        }
        $('#'+$(this).attr('id')+'_modal').modal('show');
    });

    $(".modal-header").on("mousedown", function (mousedownEvt) {
        var $draggable = $(this);
        var x = mousedownEvt.pageX - $draggable.offset().left,
            y = mousedownEvt.pageY - $draggable.offset().top;
        $("body").on("mousemove.draggable", function (mousemoveEvt) {
            $draggable.closest(".modal").offset({
                "left": mousemoveEvt.pageX - x,
                "top": mousemoveEvt.pageY - y
            });
        });
        $("body").one("mouseup", function () {
            $("body").off("mousemove.draggable");
        });
        $draggable.closest(".modal").one("bs.modal.hide", function () {
            $("body").off("mousemove.draggable");
        });
    });

</script>
<script>
    $(document).ready(function() {

        $('.modal').on('hidden.bs.modal', function(event) {
            $(this).removeClass( 'fv-modal-stack' );
            $('body').data( 'fv_open_modals', $('body').data( 'fv_open_modals' ) - 1 );
        });

        $('.modal').on('shown.bs.modal', function (event) {
            // keep track of the number of open modals
            if ( typeof( $('body').data( 'fv_open_modals' ) ) == 'undefined' ) {
                $('body').data( 'fv_open_modals', 0 );
            }

            // if the z-index of this modal has been set, ignore.
            if ($(this).hasClass('fv-modal-stack')) {
                return;
            }

            $(this).addClass('fv-modal-stack');
            $('body').data('fv_open_modals', $('body').data('fv_open_modals' ) + 1 );
            $(this).css('z-index', 1040 + (10 * $('body').data('fv_open_modals' )));
            $('.modal-backdrop').not('.fv-modal-stack').css('z-index', 1039 + (10 * $('body').data('fv_open_modals')));
            $('.modal-backdrop').not('fv-modal-stack').addClass('fv-modal-stack');

        });
    });
</script>
<script>

</script>




