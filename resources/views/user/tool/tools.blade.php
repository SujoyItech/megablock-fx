@extends('user.layout.master',['menu'=>'tools','submenu'=>$code])
@section('title',__('Tools'))
@section('style')
    <style>
        body {
            background: #FFF url({{asset('user/images/icon-mega/loader.gif')}}) top left repeat-x;
        }
        .page { display: none; padding: 0 0.5em; }
        #loading {
            display: block;
            position: absolute;
            top: 0;
            left: 120px;
            z-index: 100;
            width: 100vw;
            height: 100vh;
            background-color: #ffffff;
            background-image: url({{asset('user/images/icon-mega/loader.gif')}});
            background-repeat: no-repeat;
            background-position: center;
        }
    </style>
@endsection
@section('name',Auth::user()->name)
@section('content')
    <div class="main content-wrapper">
        <div class="container">
            <div class="account-area">
                <div class="container">
                    <div class="row page">
                        <div class="col-sm-12">
                            <!-- TradingView Widget BEGIN -->
                            @if($code == 'chart')


                            @elseif($code == 'crossRates')
                                <div class="tradingview-widget-container">
                                    <div class="tradingview-widget-container__widget"></div>
                                    <div class="tradingview-widget-copyright"><a href="https://www.tradingview.com/markets/currencies/forex-cross-rates/" rel="noopener" target="_blank"><span class="blue-text">Forex Rates</span></a> by TradingView</div>
                                    <script type="text/javascript" src="https://s3.tradingview.com/external-embedding/embed-widget-forex-cross-rates.js" async>
                                        {
                                            "width": "100%",
                                            "height": "610",
                                            "currencies": [
                                            "EUR",
                                            "USD",
                                            "JPY",
                                            "GBP",
                                            "CHF",
                                            "AUD",
                                            "CAD",
                                            "NZD",
                                            "CNY"
                                        ],
                                            "locale": "en"
                                        }
                                    </script>
                                </div>

                            @elseif($code == 'economicCalender')
                                <div class="tradingview-widget-container">
                                    <div class="tradingview-widget-container__widget"></div>
                                    <div class="tradingview-widget-copyright"><a href="https://www.tradingview.com/markets/currencies/economic-calendar/" rel="noopener" target="_blank"><span class="blue-text">Economic Calendar</span></a> by TradingView</div>
                                    <script type="text/javascript" src="https://s3.tradingview.com/external-embedding/embed-widget-events.js" async>
                                        {"colorTheme": "light",
                                            "isTransparent": false,
                                            "width": "100%",
                                            "height": "610",
                                            "locale": "en",
                                            "importanceFilter": "-1,0,1"}
                                    </script>
                                </div>
                            @elseif($code == 'timeTables')
                            <!-- myfxbook.com market widget - Start -->
                                <div><script class="powered" type="text/javascript" src="https://widgets.myfxbook.com/scripts/fxMarkets.js"></script>
                                    <div style="color: #706f6f;font-weight: bold;font-size: 11px;font-family: Tahoma;">Powered by <a href="https://www.myfxbook.com"class="myfxbookLink" ><b style="color: #575454;">Myfxbook.com</b></a></div>
                                    <script type="text/javascript">showMarketsWidget()</script>
                                </div>
                                <!-- myfxbook.com market widget - End -->
                            @elseif($code == 'heatMap')
                            <!-- TradingView Widget BEGIN -->
                                <div class="tradingview-widget-container">
                                    <div class="tradingview-widget-container__widget"></div>
                                    <div class="tradingview-widget-copyright"><a href="https://www.tradingview.com/markets/currencies/forex-heat-map/" rel="noopener" target="_blank"><span class="blue-text">Forex Heat Map</span></a> by TradingView</div>
                                    <script type="text/javascript" src="https://s3.tradingview.com/external-embedding/embed-widget-forex-heat-map.js" async>
                                        {
                                            "width": "100%",
                                            "height": "610",
                                            "currencies": [
                                            "EUR",
                                            "USD",
                                            "JPY",
                                            "GBP",
                                            "CHF",
                                            "AUD",
                                            "CAD",
                                            "NZD",
                                            "CNY"
                                        ],
                                            "locale": "en"
                                        }
                                    </script>
                                </div>
                                <!-- TradingView Widget END -->

                            @elseif($code == 'trade_sentiment')
                            <!-- myfxbook.com outlook widget - Start -->
                                <script type="text/javascript" src=https://widgets.myfxbook.com/scripts/fxOutlook.js?type=1&symbols=,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,31,32,33,34,36,37,38,40,41,42,43,45,46,47,48,49,50,51,103,107,129,131,136,137,1209,1233,1234,1235,1236,1245,1246,1247,1249,1252,1253,1259,1260,1327,1692,1694,1773,1778,1781,1806,1816,1863,1864,1893,1959,1965,2012,2076,2090,2099,2103,2114,2115,2119,2326,2348,2438,2482,2511,2516,2519,2694,2729,2872,3001,3240,3304,3473,3771,3887,4845,4963,5079,5281,5435,5539,5779,5851,5858,5879,6106,8397,8669,8686,8895,8899,9657,9667,10064,12755,13517,14216,14247,17184,19780,20010,34882,54505,69230,79789,123633,177761></script>
                                <!-- myfxbook.com outlook widget - End -->
                            @elseif($code == 'news_feed')
                            <!-- myfxbook.com top news widget - Start -->
                                <div><script class="powered" type="text/javascript" src="https://widgets.myfxbook.com/scripts/fxTopNews.js"></script>
                                    <div style="color: #706f6f;font-weight: bold;font-size: 11px;font-family: Tahoma;">Powered by <a href="https://www.myfxbook.com"class="myfxbookLink" ><b style="color: #575454;">Myfxbook.com</b></a></div>
                                    <script type="text/javascript">showTopNewsWidget()</script>
                                </div>
                                <!-- myfxbook.com top news widget - End -->
                            @endif
                        </div><!--/tab-content-->
                    </div>
                </div>
            </div>
        </div>
        <!--/col-9-->
    </div>
    <div id="loading"></div>
@endsection

@section('script')
{{--    <script>--}}
{{--        function onReady(callback) {--}}
{{--            var intervalId = window.setInterval(function() {--}}
{{--                if (document.getElementsByTagName('body')[0] !== undefined) {--}}
{{--                    window.clearInterval(intervalId);--}}
{{--                    callback.call(this);--}}
{{--                }--}}
{{--            }, 1500);--}}
{{--        }--}}

{{--        function setVisible(selector, visible) {--}}
{{--            document.querySelector(selector).style.display = visible ? 'block' : 'none';--}}
{{--        }--}}

{{--        onReady(function() {--}}
{{--            setVisible('.page', true);--}}
{{--            setVisible('#loading', false);--}}
{{--        });--}}

{{--    </script>--}}

@endsection

