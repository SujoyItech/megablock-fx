
<!-- jquery plugin -->
<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<script src="{{asset('user/')}}/js/jquery.min.js"></script>
<script src="{{asset('user/')}}/js/bootstrap.min.js"></script>
<script src="{{asset('user/')}}/js/dropify.js"></script>
<script src="{{asset('user/')}}/js/dc-custom.js"></script>
<script src="{{asset('user/')}}/js/metisMenu.min.js"></script>
<script src="{{asset('user/')}}/js/perfect-scrollbar.jquery.js"></script>
<script src="{{asset('user/')}}/admin/js/admin.js"></script>
<script src="{{asset('user/')}}/admin/js/megablock_user.js"></script>
<script src="{{asset('user/')}}/vendors/scrollbar/jquery.nicescroll.min.js"></script>
<script src="{{asset('assets/toaster/toastr.js')}}"></script>
<script>
    $(function(){
        // this will get the full URL at the address bar
        var url = window.location.href;

        // passes on every "a" tag
        $(".sidebar-menu a").each(function() {
            // checks if its the same on the address bar
            if(url == (this.href)) {
                $(this).closest("li").addClass("mnu-active");
            }
        });
    });

    $('ul.metismenu').metisMenu({});

    $('.scroll-popup').perfectScrollbar();

    $(".collopse-menu").click(function() {
        $("body").removeClass("sidebar-open");
    });


</script>

<script type="text/javascript">
    function toggleFullScreen(elem) {
        if ((document.fullScreenElement !== undefined && document.fullScreenElement === null) || (document.msFullscreenElement !== undefined && document.msFullscreenElement === null) || (document.mozFullScreen !== undefined && !document.mozFullScreen) || (document.webkitIsFullScreen !== undefined && !document.webkitIsFullScreen)) {
            if (elem.requestFullScreen) {
                elem.requestFullScreen();
            } else if (elem.mozRequestFullScreen) {
                elem.mozRequestFullScreen();
            } else if (elem.webkitRequestFullScreen) {
                elem.webkitRequestFullScreen(Element.ALLOW_KEYBOARD_INPUT);
            } else if (elem.msRequestFullscreen) {
                elem.msRequestFullscreen();
            }
        } else {
            if (document.cancelFullScreen) {
                document.cancelFullScreen();
            } else if (document.mozCancelFullScreen) {
                document.mozCancelFullScreen();
            } else if (document.webkitCancelFullScreen) {
                document.webkitCancelFullScreen();
            } else if (document.msExitFullscreen) {
                document.msExitFullscreen();
            }
        }
    }


    $(document).ready(function(){
        $('.flag-item').click(function(){
            var flagItem = $(this).html();
            $(this).empty().html($('.flag-button').html());
            $('.flag-button').empty().html(flagItem);
        });

        $('.lang-menu').find('ul').addClass('sh');
        $('.lang-menu').find('ul').addClass('sh');

        $('.flag-button, .flag-item').on('click', function(){
            $('.lang-menu').find('ul').toggleClass('sh');
        })
    });

</script>

<script>
    toastr.options = {
        "closeButton": true,
        "debug": false,
        "newestOnTop": true,
        "progressBar": true,
        "positionClass": "toast-bottom-right",
        "preventDuplicates": false,
        "onclick": null,
        "showDuration": "3000",
        "hideDuration": "1000",
        "timeOut": "5000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
    }

    @if(Session::has('dismiss') && !empty(Session::get('dismiss')))
    toastr.warning("{{ucfirst(strtolower(Session::get('dismiss')))}}",'Warning')
    @endif
    @if(Session::has('success') && !empty(Session::get('success')))
    toastr.success("{{ucfirst(strtolower(Session::get('success')))}}",'Congratulations')
    @endif
    @if ($errors && count($errors) > 0)
    toastr.warning('{{ucfirst(strtolower($errors->first()))}}','Warning')
    @endif

    function getEmbeddedUrl(url) {
        var regExp = /^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|\&v=)([^#\&\?]*).*/;
        var match = url.match(regExp);

        if (match && match[2].length == 11) {
            return match[2];
        } else {
            return 'error';
        }
    }
    $(document.body).on('click','#agree',function(){
        $('#loginBtn').prop('disabled', true);
        if($(this).is(':checked')){
            $('#loginBtn').prop('disabled', false);
        }
    });

</script>
