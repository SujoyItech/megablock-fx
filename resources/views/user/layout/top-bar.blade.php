<header class="main-header navbar-static-top">
    <nav class="navbar navbar-static-top">
        <div class="left-heder">
            <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
                <span class="sr-only">{{__('Toggle navigation')}}</span>
            </a>
        </div>
        <!-- Sidebar toggle button-->
        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <li style="color: #666; padding-top: 2px; padding-right: 15px">
                    <h3>Credit <span style="color: {{getBalance() > 0 ? '#0f7079' : 'red'}}">{{getBalance()}}</span>
                    </h3>
                </li>

                <li class="dropdown notifications-menu messages-menu">
                    @includeWhen(Auth::user()->notification, 'user.layout.notification')
                </li>
                <!-- Tasks: style can be found in dropdown.less -->
                <li style="color: #666; padding-top: 10px; padding-right: 15px;padding-left: 15px">
                    <div class="btn-group ">
                        <a class="btn btn-sm btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            @php
                            $user = Auth::user();
                            @endphp
                            @if($user->language == 'en')
                                <img src="{{asset('user/')}}/images/flag/gb.svg" alt="" width="15"> {{__('English')}} <span class="caret text-right"></span>
                            @elseif($user->language == 'pt')
                                <img src="{{asset('user/')}}/images/flag/pt.svg" alt="" width="15"> {{__('Portuguese')}} <span class="caret text-right"></span>
                            @elseif($user->language == 'sp')
                                <img src="{{asset('user/')}}/images/flag/sp.svg" alt="" width="15"> {{__('Spanish')}} <span class="caret text-right"></span>
                            @elseif($user->language == 'fr')
                                <img src="{{asset('user/')}}/images/flag/fr.svg" alt="" width="15">  {{__('French')}} <span class="caret text-right"></span>
                            @else
                                <img src="{{asset('user/')}}/images/flag/gb.svg" alt="" width="15"> {{__('English')}} <span class="caret text-right"></span>
                            @endif
                        </a>
                        <ul class="dropdown-menu" style="left: -70px">
                            <li>
                                <a href="{{url('lang/en')}}"><h5> <img src="{{asset('user/')}}/images/flag/gb.svg" alt="" width="15">  {{__('English')}} </h5></a>
                            </li>
                            <li>
                                <a href="{{url('lang/pt')}}"><h5> <img src="{{asset('user/')}}/images/flag/pt.svg" alt="" width="15">  {{__('Portuguese')}} </h5></a>
                            </li>
                            <li>
                                <a href="{{url('lang/sp')}}"><h5><img src="{{asset('user/')}}/images/flag/sp.svg" alt="" width="15"> {{__('Spanish')}} </h5></a>
                            </li>
                            <li>
                                <a href="{{url('lang/fr')}}"><h5><img src="{{asset('user/')}}/images/flag/fr.svg" alt="" width="15"> {{__('French')}} </h5></a>
                            </li>
                        </ul>
                    </div>
                </li>
                <li style="color: #666; padding-top: 10px; padding-right: 15px">
                    <div class="btn-group">
                        <a class="image-design" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"
                           style="cursor: pointer;background: none">
                            <img src="{{isset(Auth::user()->photo) && file_exists(getImagePath('user').Auth::user()->photo) ? asset(getImagePath('user').Auth::user()->photo) : asset(getImagePath('user').'avater.jpg')}}" class="img-circle">
                        </a>
                        <ul class="dropdown-menu" style="left: -200px">
                            <li class="image-design">
                                <a>
                                    <div class="profile-text">
                                        <h6>{{Auth::user()->first_name.' '.Auth::user()->last_name}}</h6>
                                        <span>{{strlen(Auth::user()->email)>35 ? substr(Auth::user()->email,0,35).'..' : Auth::user()->email}}</span>
                                    </div>
                                </a>
                            </li>
                            <li role="separator" class="divider"></li>
                            <li><a href="{{route('userProfile')}}"><i class="fa fa-user-circle-o"></i> {{__('Profile')}}</a></li>
                            <li>
                                <a href="{{route('userProfile','payment')}}">
                                    <i class="fa fa-history" aria-hidden="true"></i>
                                    {{__('Subscription Histories')}}
                                </a>
                            </li>
                            <li><a href="{{route('logout')}}" style="color: red"><i class="fa fa-sign-out"></i> {{__('Logout')}}</a></li>
                        </ul>
                    </div>
                </li>
            </ul>
        </div>
    </nav>
</header>
