@php($segment1 = Request::segment(1))
<aside class="main-sidebar">
    <div class="collopse-menu">
    <i class="fa fa-times" aria-hidden="true"></i>
    </div>
    <section class="sidebar">
        <!-- Logo -->
        <div class="side-logo">
            <a href="{{route('userDashBoard')}}" class="logo logo-normal"><img class="first" src="{{asset('user/')}}/images/logo.png" alt=""><span>FX</span></a>
            <a href="{{route('userDashBoard')}}" class="logo colupsLogo"><img class="normal" src="{{asset('user/')}}/images/logo.png" alt=""></a>
        </div>
        <!-- Sidebar user panel -->
        <div class="user-panel pt-30 pb-30">
            <div class="image">
                <img src="{{isset(Auth::user()->photo) && file_exists(getImagePath('user').Auth::user()->photo) ? asset(getImagePath('user').Auth::user()->photo) : asset(getImagePath('user').'avater.jpg')}}" class="img-responsive" alt="User Image">
            </div>
            <div class="info">
                <div class="row">
                    <h3 style="color:#fff;"><a href="{{route('userProfile')}}" style="color: #fff">@yield('name')</a></h3>
                </div>

            </div>
        </div>

        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu  metismenu scroll-popup" data-widget="tree">
            <li @if(!empty($menu) && ($menu == 'dashboard')) class="active mnu-active" @endif>
                <a href="{{route('userDashBoard')}}">
                    <i class="icon">
                        <img class="normal" src="{{asset('user/')}}/images/icon-mega/1.svg"alt="">
                        <img class="hover" src="{{asset('user/')}}/images/icon-mega/1.svg" alt="">
                    </i>
                    <span>{{__('Dashboard')}}</span>
                </a>
            </li>

            <li @if(!empty($menu) && ($menu == 'signal')) class="active mnu-active" @endif>
                <a href="{{route('signal')}}">
                    <i class="icon">
                        <img class="normal" src="{{asset('user/')}}/images/icon-mega/2.svg"alt="">
                        <img class="hover" src="{{asset('user/')}}/images/icon-mega/2.svg" alt="">
                    </i>
                    <span>{{__('Signals')}}</span>
                </a>
            </li>
            <li @if(!empty($menu) && ($menu == 'broker')) class="active mnu-active" @endif>
                <a href="{{route('broker')}}">
                    <i class="icon">
                        <img class="normal" src="{{asset('user/')}}/images/icon-mega/3.svg"alt="">
                        <img class="hover" src="{{asset('user/')}}/images/icon-mega/3.svg" alt="">
                    </i>
                    <span>{{__('Brokers')}}</span>
                </a>
            </li>
            <li @if(!empty($menu) && ($menu == 'subscription')) class="active mnu-active" @endif>
                <a href="{{route('subscriptions')}}">
                    <i class="icon">
                        <img class="normal" src="{{asset('user/')}}/images/icon-mega/4.svg"alt="">
                        <img class="hover" src="{{asset('user/')}}/images/icon-mega/4.svg" alt="">
                    </i>
                    <span>{{__('Subscription')}}</span>
                </a>
            </li>
            <li class="sidemenu-items @if(!empty($menu) && ($menu == 'tools')) active @endif " >
                <a class="has-arrow" aria-expanded="false" href="javascript:void(0);">
                    <i class="icon">
                         <img class="normal" src="{{asset('user/')}}/images/icon-mega/5.svg"alt="">
                        <img class="hover" src="{{asset('user/')}}/images/icon-mega/5.svg" alt="">
                     </i>
                    <span>Tools</span>
                </a>
                <ul aria-expanded="false">
                    <li><a href="#" id="charts" class="modal_button">{{__('Charts')}}</a></li>
                    <li><a href="#" id="cross_rate" class="modal_button">{{__('Forex Cross Rates')}}</a></li>
                    <li><a href="#" id="economic_calendar" class="modal_button">{{__('Economic calendar')}}</a></li>
                    <li><a href="#" id="time_table" class="modal_button">{{__('Timetables &')}}<br/>{{__('Trading Sessions')}}</a></li>
                    <li><a href="#" id="forex_heat_map" class="modal_button"> {{__('Forex Heat Map')}}</a></li>
                    <li><a href="#" id="trader_sentiment" class="modal_button"> {{__('Trader Sentiment')}}</a></li>
                    <li><a href="#" id="news_feed" class="modal_button"> {{__('News Feed')}}</a></li>
                </ul>
            </li>
            <li class="sidemenu-items @if(!empty($menu) && ($menu == 'support')) active @endif">
                <a class="has-arrow" aria-expanded="false" href="javascript:void(0);">
                    <i class="icon">
                        <img class="normal" src="{{asset('user/')}}/images/icon-mega/6.svg"alt="">
                        <img class="hover" src="{{asset('user/')}}/images/icon-mega/6.svg" alt="">
                    </i>
                    <span>{{__('Support')}}</span>
                </a>
                <ul aria-expanded="false">
                    <li><a href="{{route('aboutPlatform')}}">{{__('Platforms')}}</a></li>
                    <li><a href="{{route('faqs')}}">{{__('FAQs')}}</a></li>
                    <li><a href="{{route('howItWorks')}}">{{__('How It Works')}}</a></li>
                    <li><a href="{{route('termCondition')}}">{{__('Terms & Conditions')}}</a></li>
                </ul>
            </li>
            <li>
                <a href="{{route('learningCenter')}}">
                    <i class="icon">
                        <img class="normal" src="{{asset('user/')}}/images/icon-mega/8.svg"alt="">
                        <img class="hover" src="{{asset('user/')}}/images/icon-mega/8.svg" alt="">
                    </i>
                    <span>{{__('Learning Center')}}</span>
                </a>
            </li>
        </ul>
    </section>
    <footer class="footer">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <h4> {{allSetting('copyright_text')}} All rights reserved. </h4>
                </div>
            </div>
        </div>
    </footer>

</aside>
<style>
    .modal-backdrop {
        display: none;
    }

    .modal-maps {
        position: fixed;
        top: 0;
        left: 200px;
        bottom: 0;
        z-index: 1050;
        display: none;
        overflow: hidden!important;
        -webkit-overflow-scrolling: touch;
        outline: 0;
        margin: 0;
        width: 500px!important;
        min-width: 500px!important;
        max-width: 500px!important;
        /*display: inline-block!important;*/
    }
    .modal-full{
        width: 100%!important;
    }
</style>
<div class="modal modal-maps inmodal fade" id="cross_rate_modal" role="dialog" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-full">
        <div class="modal-content animated">
            <div class="modal-header" style="cursor: move;">
                <button type="button" class="close" id="tool_close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body cross_rate">
                @include('user.dashboard_content.dashboard_maps.cross_rate')
            </div>
        </div>
    </div>
</div>
<div class="modal modal-maps inmodal fade" id="charts_modal" role="dialog" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-full">
        <div class="modal-content animated">
            <div class="modal-header" style="cursor: move;">
                <button type="button" class="close" id="tool_close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body charts"></div>
        </div>
    </div>
</div>
<div class="modal modal-maps inmodal fade" id="economic_calendar_modal" role="dialog" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-full">
        <div class="modal-content animated">
            <div class="modal-header" style="cursor: move;">
                <button type="button" class="close" id="tool_close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body economic_calendar">
                @include('user.dashboard_content.dashboard_chats.economic_calendar')
            </div>
        </div>
    </div>
</div>
<div class="modal modal-maps inmodal fade" id="time_table_modal" role="dialog" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-full">
        <div class="modal-content animated">
            <div class="modal-header" style="cursor: move;">
                <button type="button" class="close" id="tool_close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body time_table">
                @include('user.dashboard_content.dashboard_maps.time_table')
            </div>
        </div>
    </div>
</div>
<div class="modal modal-maps inmodal fade" id="forex_heat_map_modal" role="dialog" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-full">
        <div class="modal-content animated">
            <div class="modal-header" style="cursor: move;">
                <button type="button" class="close" id="tool_close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                @include('user.dashboard_content.dashboard_maps.forex_heat_map')
            </div>
        </div>
    </div>
</div>
<div class="modal modal-maps inmodal fade" id="trader_sentiment_modal" role="dialog" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-full">
        <div class="modal-content animated">
            <div class="modal-header" style="cursor: move;">
                <button type="button" class="close" id="tool_close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body trader_sentiment">
                @include('user.dashboard_content.dashboard_chats.trader_sentiment')
            </div>
        </div>
    </div>
</div>
<div class="modal modal-maps inmodal fade" id="news_feed_modal" role="dialog" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-full">
        <div class="modal-content animated">
            <div class="modal-header" style="cursor: move;">
                <button type="button" class="close" id="tool_close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body news_feed">
                @include('user.dashboard_content.dashboard_chats.news_feed')
            </div>
        </div>
    </div>
</div>
<script>
    $('.modal_button').on('click', function () {
        var idval = $(this).attr('id');
        if (idval == 'charts'){
            var className = '.'+idval;
            var url = 'tools/'+idval;
            $(className).load(url);
            $( "#charts" ).trigger( "click" );
        }
        $('#'+$(this).attr('id')+'_modal').modal('show');
    });
    $('.close').on('click',function () {
        location.reload();
    });
    $(".modal-header").on("mousedown", function (mousedownEvt) {
        var $draggable = $(this);
        var x = mousedownEvt.pageX - $draggable.offset().left,
            y = mousedownEvt.pageY - $draggable.offset().top;
        $("body").on("mousemove.draggable", function (mousemoveEvt) {
            $draggable.closest(".modal").offset({
                "left": mousemoveEvt.pageX - x,
                "top": mousemoveEvt.pageY - y
            });
        });
        $("body").one("mouseup", function () {
            $("body").off("mousemove.draggable");
        });
        $draggable.closest(".modal").one("bs.modal.hide", function () {
            $("body").off("mousemove.draggable");
        });
    });
    $(document).ready(function() {

        $('.modal').on('hidden.bs.modal', function(event) {
            $(this).removeClass( 'fv-modal-stack' );
            $('body').data( 'fv_open_modals', $('body').data( 'fv_open_modals' ) - 1 );
        });

        $('.modal').on('shown.bs.modal', function (event) {
            // keep track of the number of open modals
            if ( typeof( $('body').data( 'fv_open_modals' ) ) == 'undefined' ) {
                $('body').data( 'fv_open_modals', 0 );
            }

            // if the z-index of this modal has been set, ignore.
            if ($(this).hasClass('fv-modal-stack')) {
                return;
            }

            $(this).addClass('fv-modal-stack');
            $('body').data('fv_open_modals', $('body').data('fv_open_modals' ) + 1 );
            $(this).css('z-index', 1040 + (10 * $('body').data('fv_open_modals' )));
            $('.modal-backdrop').not('.fv-modal-stack').css('z-index', 1039 + (10 * $('body').data('fv_open_modals')));
            $('.modal-backdrop').not('fv-modal-stack').addClass('fv-modal-stack');

        });
    });
</script>
