@php($notification_list = getNotification())
<div style="max-height: 400px; overflow-x: hidden; overflow-y: auto;">
    @if(notificationData($notification_list, 'count') > 0)
        @foreach($notification_list as $notification)
            @php($not_content = getNotificationContent($notification->data))
            <div class="noti-list {{$notification->status == NEW_NOTIFICATION ? 'new-noti': ''}}"
                 data-noti="{{$notification->id}}"
                 {{in_array($notification->type, [SIGNAL_CREATED, SIGNAL_UPDATED]) ? 'data-signal='.$not_content->signal_id : ''}}
                 style="padding: 0 8px; border-bottom: 1px solid #ddd; ">
                <h5 style="{{$notification->status == NEW_NOTIFICATION ? 'font-weight:bold': ''}}" id="{{$notification->status}}">
                    <i class="fa fa-info-circle"></i> {{$not_content->title}}
                </h5>
                <p>{!! $not_content->message !!}</p>
            </div>
        @endforeach
    @else
        <h4 class="text-center"><i class="fa fa-exclamation-circle"></i> {{__('No New Notification') }}</h4>
    @endif
</div>
