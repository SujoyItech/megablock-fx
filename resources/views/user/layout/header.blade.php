<!-- bootstrap css -->
<link rel="stylesheet" href="{{asset('user/')}}/css/bootstrap.min.css">
<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
<link rel="stylesheet" href="{{asset('user/')}}/vendors/scrollbar/scroll.css">
<link rel="stylesheet" href="{{asset('user/')}}/admin/css/admin.css">
<link rel="stylesheet newest" href="{{asset('user/')}}/admin/css/megablockstyle.css">
<link href="{{asset('assets/toaster')}}/toastr.css" rel="stylesheet">
<!-- main stylesheet css -->
<link rel="stylesheet" href="{{asset('user/')}}/css/dropify.css">
<link rel="stylesheet" href="{{asset('user/')}}/css/metisMenu.min.css">
<link rel="stylesheet" href="{{asset('user/')}}/css/perfect-scrollbar.css">
<link rel="stylesheet" href="{{asset('user/')}}/style.css">
<link rel="stylesheet" href="{{asset('user/')}}/css/draggable-resizable-dialog.css">
<link href="{{asset('assets/toaster')}}/toastr.css" rel="stylesheet">
<style>
    .metismenu .has-arrow:after {
        right: 15px;
    }
    .sidemenu-items ul li a{
        background: #f9f9f9;
        border-bottom: 1px solid #fdfdfd;
    }
    .sidemenu-items ul li{
        margin-bottom: 0;
    }

</style>
<script type="text/javascript">
    // Remove the ugly Facebook appended hash
    // <https://github.com/jaredhanson/passport-facebook/issues/12>
    if (window.location.hash && window.location.hash === "#_=_") {
        window.location.hash = "";

    }
</script>
<script src="{{asset('user/')}}/js/draggable-resizable-dialog.js"></script>
<script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>


@yield('style')
