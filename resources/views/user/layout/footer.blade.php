<div class="modal inmodal fade" id="signal_view_modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content animated signal_view_data"></div>
    </div>
</div>
<div class="tradeviewcontent" style="display: none">
    <div class="tradingview-widget-container">
        <div id="tradingview_bf9c0"></div>
        <script type="text/javascript" src="https://s3.tradingview.com/tv.js"></script>
    </div>
</div>
<script>
    /***************************************/
    $(document).on('click', '.custom-grid, .noti-list', function (event) {
        event.preventDefault();
        var notification_id = 0;
        var new_noti_click = 0;
        if($(this).hasClass('noti-list')){
            notification_id = $(this).data('noti');
        }
        if($(this).hasClass('new-noti')){
            new_noti_click = 1;
        }

        $.ajax({
            url: '{{url('signal-details')}}',
            type: 'POST',
            data:{
                signal: $(this).data('signal'),
                notification_id: notification_id
            },
            headers: { 'X-CSRF-Token' : $('meta[name=csrf-token]').attr('content') },
            cache: false,
            beforeSend: function(){

            },
            success: function (msg) {
                if(new_noti_click == 1){
                    var count = $('#count_notification').html() == '' ? 0 : $('#count_notification').html();
                    var new_count = parseInt(count) - 1;
                    $('#count_notification').html(new_count);
                }
                $('.signal_view_data').html(msg);
                var fxsymbol = $('.fx_symbol').html().replace('/','');
                $('#trade-fetch').html($('.tradeviewcontent').html());
                gettradeviewdata(fxsymbol);
                $('#signal_view_modal').modal('show');
            }
        });
    });
    function gettradeviewdata(fxsymbol) {
        new TradingView.widget({
            "width": '100%',
            'height':300,
            "symbol": "FX:"+fxsymbol,
            "interval": "5",
            "timezone": "Etc/UTC",
            "theme": "Light",
            "style": "1",
            "locale": "en",
            "hide_top_toolbar": true,
            "toolbar_bg": "#f1f3f6",
            "enable_publishing": false,
            "allow_symbol_change": true,
            "container_id": "tradingview_bf9c0"
        });
    }

    $('body').on('hidden.bs.modal', '.modal', function () {
        $(this).removeData('bs.modal');
    });
    $(document).on('hidden.bs.modal', '.modal', function () {
        $(this).removeData('bs.modal');
    });
</script>
