<!DOCTYPE html>
<html lang="en">
@php
    $all_settings = allSetting();
@endphp
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="shortcut icon" type="image/png" href="{{asset('user/')}}/images/favicon.png" alt="FX"/>
    <title>@yield('title')</title>
    @include('user.layout.header')
    <style>
        body::-webkit-scrollbar {
            display: none;
        }
    </style>
</head>

<body class="sidebar-mini" style="overflow: scroll">
<!-- start header area
======================================== -->
@include('user.layout.top-bar')
<!-- end header area
======================================== -->

<!-- start left side bar
======================================== -->
@include('user.layout.left-menu')
<!-- start left side bar
======================================== -->

<!-- start main area
======================================== -->
@yield('content')
<!-- end main area
======================================== -->

<!-- start footer area
======================================== -->
@include('user.layout.footer')
<!-- end footer area
======================================== -->
@include('user.layout.script')
@yield('script')

</body>
</html>
