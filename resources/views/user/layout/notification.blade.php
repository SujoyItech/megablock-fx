<a href="#" class="dropdown-toggle notify-expand" data-toggle="dropdown">
    <i><img src="{{asset('user/')}}/images/not.svg" alt=""></i>
    <span class="label label-warning"
          id="count_notification">{{countUnreadNotification()}}</span>
</a>
<ul class="dropdown-menu" style="background-color: #efefef; box-shadow: 0px 9px 9.66px 0.34px rgba(0, 0, 0, 0.23)">
    <li class="header">
        <h4 class="pull-left"><i class="fa fa-envelope"></i> Notification </h4>
        <button class="btn btn-xs btn-default pull-right clear_all" style="margin-right: -40px;">{{__('Clear All')}}</button>
        <button class="btn btn-xs btn-primary pull-right see_all">{{__('All Seen')}}</button>
    </li>
    <li class="notification-content"></li>
</ul>
<script>
    window.HOST_NAME = "{{env('PUSHER_SOCKET_HOST')}}";
    window.WS_PORT = "{{env('LARAVEL_WEBSOCKETS_PORT')}}";
</script>
<script src="https://js.pusher.com/5.0/pusher.min.js"></script>
<script src="{{asset('js/app.js')}}"></script>
<script>
    /************************************/
    Pusher.logToConsole = true;
    var channel_name = "signal_all";
    Echo.channel(channel_name).listen('.signal_notification', (response) => {
        notifyIconUpdate();
        makeToast(response.body, response.title);
    });

    function makeToast(title, body) {
        toastr.success(title, body);
    }

    function notifyIconUpdate() {
        var count = $('#count_notification').html() == '' ? 0 : $('#count_notification').html();
        var new_count = parseInt(count) + 1;
        $('#count_notification').html(new_count);
        $('#count_notification').show();
        $('#count_notification').addClass('notifying');
        setTimeout(function () {
            $('#count_notification').removeClass('notifying')
        }, 1000);
    }
    /************************************/
    $(document).on("click", ".notify-expand", function (event) {
        event.preventDefault();
        $( ".notification-content" ).load("{{route('load-notification-body')}}");
    });

    $(document).on("click", ".see_all", function (event) {
        event.preventDefault();
        $.ajax({
            url: '{{route('see-all-notification')}}',
            type: 'GET',
            cache: false,
            success: function (msg) {
                $('#count_notification').html('0');
                toastr.success("{{__('All Notification Seen')}}");
            }
        });
    });

    $(document).on("click", ".clear_all", function (event) {
        event.preventDefault();
        $.ajax({
            url: '{{route('clear-all-notification')}}',
            type: 'GET',
            cache: false,
            success: function (msg) {
                $('#count_notification').html('0');
                toastr.success("{{__('All Notification Cleared')}}");
            }
        });
    });
</script>
