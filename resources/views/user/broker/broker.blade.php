@extends('user.layout.master',['menu'=>'subscription'])
@section('title',__('Broker'))
@section('style')
@endsection
@section('name',Auth::user()->name)
@section('content')
    <div class="main content-wrapper">
        <div class="container">
            <h4 class="ac-header" style="margin-top: 20px;margin-bottom: 0px"><img src="{{asset('user/')}}/images/icon-mega/h3.svg"alt="">{{__('Brokers')}}</h4>
            <div class="brokers-area" style="margin-top: 20px">
                <div class="row">
                    <form name=frm id="brokerByOrder" action='{{route('brokerByOrder')}}'>
                        <div class="col-lg-4">
                            <div class="country-select">
                                <label for="">{{__('Available for (country)')}}</label>
                                <select id="apports" type="dropdown-toggle" class="form-control" name="country" onchange='document.frm.submit()'>
                                    @foreach(country() as $key=>$country)
                                        <option class="apports" @if(isset($country_key)){{isSelect($country_key,$key)}} @endif value="{{$key}}">{{$country}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="country-select">
                                <label for="">{{__('Sort By Vote')}}</label>
                                <select id="vote"  data-select="vote" name='vote' onchange='formSubmit(this)'>
                                    <option @if(isset($vote)){{isSelect($vote,VOTED)}}@endif value="{{VOTED}}">{{__('Voted')}}</option>
                                    <option @if(isset($vote)){{isSelect($vote,UN_VOTED)}}@endif value="{{UN_VOTED}}">{{__('Un Voted')}}</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="country-select">
                                <label for="">{{__('Sort By Name')}}</label>
                                <select name='name' id="name" data-select="name" onchange='formSubmit(this)'>
                                    <option @if(isset($order)){{isSelect($order,"asc")}}@endif value="asc">{{__('Name (A - Z)')}}</option>
                                    <option @if(isset($order)){{isSelect($order,"desc")}}@endif value="desc">{{__('Name (Z - A)')}}</option>
                                </select>
                            </div>
                        </div>
                    </form>
                </div>
                <?php $i=0?>
                <div class="row">
                    @if(isset($brokers))
                        @foreach($brokers as $broker)
                            @if($i%2 == 0)
                                <div class="clearfix"></div>
                            @endif
                            <div class="col-lg-6">
                                <div class="brokers-text">
                                    <div class="broker-header" id="toggle" data-broker="{{$broker->id}}">
                                        <h2>{{$broker->name}}</h2>
                                        <i class="fa fa-angle-down" ></i>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="broker-in create-account open-tab{{$broker->id}}">
                                                <p>{{$broker->advantage}}</p>
                                            </div>
                                        </div>
                                        <div class="col-lg-12">
                                            <div class="brokers-wrap">
                                                <div class="broker-l">
                                                    <img src="{{asset(getImagePath('brokers').$broker->logo)}}" alt="">
                                                </div>
                                                <div class="bro-r">
                                                    <ul>
                                                        <li>{{__('Licences:')}} <span>{{$broker->brokerLicences()}}</span></li>
                                                        <li>{{__('Spreads:')}} <span> {{$broker->average_spreads}}</span></li>
                                                        <li>{{__('Recommended Deposit:')}} <span> {{$broker->recommended_deposit}}</span></li>
                                                    </ul>
                                                    <ul class="bro-t">
                                                        <li>
                                                            <a href="#" class="like" data-id="{{$broker->id}}">
                                                                <i class="fa fa-thumbs-o-up bg-like{{$broker->id}}" @if(\App\Models\BrokerLike::where(['broker_id'=>$broker->id,'user_id'=>Auth::user()->id,'like'=>1])->first()) style="background:rgb(0, 255, 0);color:#fff;border-color:#fff;" @endif></i>
                                                            </a>
                                                            <span class="like-txt{{$broker->id}}">{{$broker->broker_like_count}}</span>
                                                        </li>
                                                        <li>
                                                            <a href="#" class="dislike" data-id="{{$broker->id}}">
                                                                <i class="fa fa-thumbs-o-down bg-dislike{{$broker->id}}" @if(\App\Models\BrokerLike::where(['broker_id'=>$broker->id,'user_id'=>Auth::user()->id,'unlike'=>1])->first()) style="background: rgb(255, 0, 0);color:#fff;border-color:#fff" @endif></i>
                                                            </a>
                                                            <span class="dislike-txt{{$broker->id}}">{{$broker->broker_dislike_count}}</span>
                                                        </li>
                                                        <li>
                                                            <div class="brokers-wrap brokers-wrap-2">
                                                                <div class="brokers-right">
                                                                    <a class="brokers-btn" href="{!! $broker->URL !!}" target="_blank">{{__('Link')}}</a>
                                                                </div>
                                                            </div>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php $i++?>
                        @endforeach
                    @endif
                </div>

                {{$brokers->appends($_GET)->links()}}
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script type="text/javascript">
       function formSubmit(event){
           $('#brokerByOrder').submit();
       };
    </script>
    <script>
        $(".like").click(function(event){
            event.preventDefault();
            var brokerId = $(this).data("id");
            var token = $("meta[name='csrf-token']").attr("content");
            var like = "."+"bg-like"+brokerId;
            var bgcol = $(like).css('backgroundColor');
            if($(like).css('backgroundColor') === 'rgb(0, 255, 0)')
            {
                $.ajax(
                    {
                        method: "GET",
                        url: 'broker-unlike/' + brokerId,
                        success: function (data) {
                            var liketext = "." + "like-txt" + brokerId;
                            var disliketext = "." + "dislike-txt" + brokerId;
                            $(like).css("background-color","");
                            $(like).css("color", "#000");
                            $(like).css("border-color", "#000");
                            $(liketext).text(data.broker_like);
                            $(disliketext).text(data.broker_unlike);
                        },
                        error: function (error) {
                            console.log(error.msg);
                        }
                    });
            }else {
                $.ajax(
                    {
                        method: "GET",
                        url: 'broker-like/' + brokerId,
                        success: function (data) {
                            var like = "." + "bg-like" + brokerId;
                            var dislike = "." + "bg-dislike" + brokerId;
                            var liketext = "." + "like-txt" + brokerId;
                            var disliketext = "." + "dislike-txt" + brokerId;
                            $(like).css("background-color", "#00FF00");
                            $(like).css("color", "#fff");
                            $(like).css("border-color", "#00FF00");
                            $(dislike).css("background-color", "");
                            $(dislike).css("color", "#000");
                            $(dislike).css("border-color", "#000");
                            $(liketext).text(data.broker_like);
                            $(disliketext).text(data.broker_unlike);
                        },
                        error: function (error) {
                            console.log(error.msg);
                        }
                    });
            }


        });
        $(".dislike").click(function(event){
            event.preventDefault();

            var brokerId = $(this).data("id");

            var token = $("meta[name='csrf-token']").attr("content");

            var dislike = "."+"bg-dislike"+brokerId;
            var bgcol = $(dislike).css('backgroundColor');
            if($(dislike).css('backgroundColor') === 'rgb(255, 0, 0)')
            {
                $.ajax(
                    {
                        method: "GET",
                        url: 'broker-unDisLike/' + brokerId,
                        success: function (data) {
                            var liketext = "." + "like-txt" + brokerId;
                            var disliketext = "." + "dislike-txt" + brokerId;
                            $(dislike).css("background-color","");
                            $(dislike).css("color", "#000");
                            $(dislike).css("border-color", "#000");
                            $(liketext).text(data.broker_like);
                            $(disliketext).text(data.broker_unlike);
                        },
                        error: function (error) {
                            console.log(error.msg);
                        }
                    });
            }else {
                $.ajax(
                    {
                        method: "GET",
                        url: 'broker-dislike/' + brokerId,
                        success: function (data) {
                            var like = "." + "bg-like" + brokerId;
                            var dislike = "." + "bg-dislike" + brokerId;
                            var liketext = "." + "like-txt" + brokerId;
                            var disliketext = "." + "dislike-txt" + brokerId;
                            $(like).css("background-color","");
                            $(like).css("color", "#000");
                            $(like).css("border-color", "#000");
                            $(dislike).css("background-color", 'rgb(255, 0, 0)');
                            $(dislike).css("border-color", "#fff");
                            $(dislike).css("color", "#fff");
                            $(liketext).text(data.broker_like);
                            $(disliketext).text(data.broker_unlike);
                        },
                        error: function (error) {
                            console.log(error.msg);
                        }
                    });
            }
        });

        // toggle

        $('.broker-header').click(function() {
            var id = $(this).data('broker');
            var broker_class = '.'+'open-tab'+id;
            $(broker_class).slideToggle();
        })

    </script>
    <script>

    </script>

{{--    <script>--}}
{{--        $(document).ready(function(){--}}
{{--            $("select.country").change(function(){--}}
{{--                var selectedCountry = $(this).children("option:selected").val();--}}
{{--                $.ajax(--}}
{{--                    {--}}
{{--                        method : "GET",--}}
{{--                        url: 'user-broker/'+selectedCountry--}}
{{--                    });--}}
{{--               alert("You have selected the country - " + selectedCountry);--}}
{{--            });--}}
{{--        });--}}
{{--    </script>--}}


@endsection

