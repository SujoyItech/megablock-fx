@php($signal_preview = signalPreview($signal->available_for_free))
<div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 {{$signal_preview ? 'custom-grid' : ''}}" data-signal="{{$signal->id}}">
    <div class="cart-wrap cart-wrap-b">
        <table class="table-responsive" width="100%">
            <thead>
            <tr>
                <th width="10%">
                    {!! getSignalResultText($signal->status, $signal->action, $signal->signal_result) !!}
                </th>
                <th width="40%" class="pl-10"><h3>{{strtoupper($signal->financial_instrument_code)}}</h3></th>
                <th class="text-right"><h6>{{Carbon\Carbon::parse($signal->created_at)->setTimezone(Auth::user()->time_zone)->format('d-m-Y|G\hi')}}</h6></th>
            </tr>
            </thead>
        </table>
        <table class="table-responsive" width="100%">
            <thead>
            <tr class="">
                <th width="28%" class="pt-5 signal_tbl_td text-center">{{__('Entry Price')}}</th>
                <th width="28%" class="pt-5 signal_tbl_td text-center">{{__('Take Profit')}}</th>
                <th width="28%" class="pt-5 signal_tbl_td text-center">{{__('Stop Loss')}}</th>
                <th width="" class="pt-5 signal_tbl_td  text-center">{{__('Status')}}</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                @if($signal_preview)
{{--                    <td class="text-center "><small><i class="fa {{isset($all_settings['currency']) && ($all_settings['currency'] == CURRENCY_EURO) ?'fa-eur':'fa-usd'}}"></i></small> {{number_format($signal->entry_price, 3)}}</td>--}}
                    <td class="text-center ">{{number_format($signal->entry_price, 3)}}</td>
                    <td class="text-center">
                        <h5>@if(isset($signal->take_profit_1) && !empty($signal->take_profit_1)) {{number_format($signal->take_profit_1, 4)}}@endif</h5>
                        <h5>@if(isset($signal->take_profit_2) && !empty($signal->take_profit_2)) {{number_format($signal->take_profit_2, 4)}}@endif</h5>
                        <h5>@if(isset($signal->take_profit_3) && !empty($signal->take_profit_3)) {{number_format($signal->take_profit_3, 4)}}@endif</h5>
                        <h5>@if(isset($signal->take_profit_4) && !empty($signal->take_profit_4)) {{number_format($signal->take_profit_4, 4)}}@endif</h5>
                    </td>
                    <td class="text-center">
                        <h5>@if(isset($signal->stop_loss_1) && !empty($signal->stop_loss_1)) {{number_format($signal->stop_loss_1, 4)}}@endif</h5>
                        <h5>@if(isset($signal->stop_loss_2) && !empty($signal->stop_loss_2)) {{number_format($signal->stop_loss_2, 4)}}@endif</h5>
                        <h5>@if(isset($signal->stop_loss_3) && !empty($signal->stop_loss_3)) {{number_format($signal->stop_loss_3, 4)}}@endif</h5>
                        <h5>@if(isset($signal->stop_loss_4) && !empty($signal->stop_loss_4)) {{number_format($signal->stop_loss_4, 4)}}@endif</h5>
                    </td>
                @else
                    <td class="text-center" colspan="3" style="padding: 10px">
                        {{--<i class="fa fa-frown-o"></i>--}}
                        <button class="btn btn-lg btn-block theme-btn got_to_buy">Buy Credit to Preview</button>
                    </td>
                @endif
                <td class="text-center">
                    {!! getSignalStatus($signal->status) !!}
                </td>
            </tr>
            </tbody>
        </table>
    </div>
</div>
@if(($index+1)%3 == 0)
    <div class="row"></div>
@endif
