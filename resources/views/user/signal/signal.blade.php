@extends('user.layout.master',['menu'=>'signal'])
@section('title',__('Signal'))
@section('style')
    <style>
        .custom-grid{
            cursor: pointer;
        }
    </style>
@endsection
@section('name', Auth::user()->name)
@section('content')
    @php
        $all_settings = allSetting();
    @endphp
    <div class="main content-wrapper"  style="">
        <div class="container">
            <div class="account-area">
                <h4 class="ac-header"><img src="{{asset('user/')}}/images/icon-mega/h2.svg"alt=""> {{__('Signal')}}
                </h4>
                <div class="container">
                    <div class="row">
                        <div class="col-12">
                            <div class="account-header">
                                <ul class="nav nav-tabs">
                                    <li class="{{$mode == 'all' ? 'active':'' }}"><a href="{{route('signal','all')}}">{{__('All')}}</a></li>
                                    <li class="{{$mode == 'active' ? 'active':'' }}"><a  href="{{route('signal','active')}}">{{__('Active')}}</a></li>
                                    <li class="{{$mode == 'pending' ? 'active':'' }}"><a  href="{{route('signal','pending')}}">{{__('Pending')}}</a></li>
                                    <li class="{{$mode == 'closed' ? 'active':'' }}"><a  href="{{route('signal','closed')}}">{{__('Closed')}}</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <div>
                                {{--=============================All Tab Open=======================--}}
                                @includeWhen(($mode == 'all'),'user.signal.all_signal')
                                {{--=============================All Tab Closed=======================--}}
                                {{--=============================Active Tab Open=======================--}}
                                @includeWhen(($mode == 'active'),'user.signal.active_signal')
                                {{--=============================Active Tab Closed=======================--}}
                                {{--=============================Pending Tab Open=======================--}}
                                @includeWhen(($mode == 'pending'),'user.signal.pending_signal')
                                {{--=============================Pending Tab Close=======================--}}
                                {{--=============================Close Tab Open=======================--}}
                                @includeWhen(($mode == 'closed'),'user.signal.closed_signal')
                                {{--=============================Close Tab Closed=======================--}}
                            </div>
{{--                            <div class="ajax-load text-center" style="display:none">--}}
{{--                                <p>Loading...</p>--}}
{{--                            </div>--}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script>
        $(document).on('click', '.got_to_buy', function (event) {
            window.location.href = '{{route('subscriptions')}}';
        });
    </script>
    <script>
        var page = 1; //track user scroll as page number, right now page number is 1
        load_more(page); //initial content load
        $(window).scroll(function() { //detect page scroll
            if($(window).scrollTop() == $(document).height() - $(window).height()) { //if user scrolled from top to bottom of the page
                page++; //page number increment
                load_more(page); //load content
            }
        });
        function load_more(page){
            $.ajax(
                {
                    url: '?page=' + page,
                    type: "get",
                    datatype: "html",
                    beforeSend: function()
                    {
                        $('.ajax-loading').show();
                    }
                })
                .done(function(data)
                {
                    if(data.length == 0){
                        console.log(data.length);

                        //notify user if nothing to load
                        $('.ajax-loading').html("No more signals!");
                        return;
                    }
                    $('.ajax-loading').hide(); //hide loading animation once data is received
                    $("#results").append(data); //append data into #results element
                })
                .fail(function(jqXHR, ajaxOptions, thrownError)
                {
                    alert('No response from server');
                });
        }
    </script>
@endsection
