<div class="modal-header">
    <div class="modal-title">
        <h4 class="">
            <span class="fx_symbol">{{$signals->financial_instrument_code}}</span>
            <span> {!! getSignalResultText($signals->status, $signals->action, $signals->signal_result) !!}</span>
            <small class="pull-right">{{Carbon\Carbon::parse($signals->created_at)->format('d-m-Y | G\h:i')}}</small>
        </h4>
    </div>
</div>
<div class="modal-body">
    {{--    {{dd($signals)}}--}}
    @php($signal_preview = signalPreview($signals->available_for_free))
    @if($signal_preview)
        <div id="trade-fetch"></div>
    @endif
    <table class="table-responsive mt-15" width="100%">
        <thead>
            <tr>
                <th class="text-info " width="20%">{{__('Entry Price')}}</th>
                <th class="text-info " width="20%">{{__('Take Profit')}}</th>
                <th class="text-info " width="20%">{{__('Stop Loss')}}</th>
                <th class="text-info" width=""></th>
                <th class="text-info text-center" width="10%">{{__('Status')}}</th>
            </tr>
        </thead>
        <tbody>
        <tr>
            @if($signal_preview)
                <td class=""><h4>{{isset($signals) ? number_format($signals->entry_price, 4) : ''}}</h4></td>
                <td class="">
                    <h4>@if(isset($signals) && !empty($signals->take_profit_1)) {{number_format($signals->take_profit_1, 4)}} @endif</h4>
                    <h4>@if(isset($signals) && !empty($signals->take_profit_2)) {{number_format($signals->take_profit_2, 4)}} @endif</h4>
                    <h4>@if(isset($signals) && !empty($signals->take_profit_3)) {{number_format($signals->take_profit_3, 4)}} @endif</h4>
                    <h4>@if(isset($signals) && !empty($signals->take_profit_4)) {{number_format($signals->take_profit_4, 4)}} @endif</h4>
                </td>
                <td class="">
                    <h4>@if(isset($signals) && !empty($signals->stop_loss_1)) {{number_format($signals->stop_loss_1, 4)}} @endif</h4>
                    <h4>@if(isset($signals) && !empty($signals->stop_loss_2)) {{number_format($signals->stop_loss_2, 4)}} @endif</h4>
                    <h4>@if(isset($signals) && !empty($signals->stop_loss_3)) {{number_format($signals->stop_loss_3, 4)}} @endif</h4>
                    <h4>@if(isset($signals) && !empty($signals->stop_loss_4)) {{number_format($signals->stop_loss_4, 4)}} @endif</h4>
                </td>
                <td class=""></td>
            @else
                <td class="text-center" colspan="4" style="padding: 50px ">
                    {{--<i class="fa fa-frown-o"></i>--}}
                    <a href="{{route('subscriptions')}}" class="btn btn-lg btn-block theme-btn got_to_buy">{{__('Buy Credit to Preview')}}</a>
                </td>
            @endif
            <td class="text-center">
                {!! getSignalStatus($signals->status, 'btn-md') !!}
            </td>
        </tr>
        </tbody>
    </table>
    @if($signal_preview)
        <p>{{$signals->comments}}</p>
    @endif
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-danger pull-right" data-dismiss="modal">Close</button>
</div>
