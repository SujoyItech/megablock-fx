@extends('user.layout.master')
@section('title',__('Signal'))
@section('style')
    <style>
        .custom-grid{
            cursor: pointer;
        }
    </style>
@endsection
@section('name', Auth::user()->name)
@section('content')
    @php
        $all_settings = allSetting();
    @endphp
    <div class="main content-wrapper">
        <div class="container">
            <div class="account-area">
                <h2 class="ac-header"><img src="{{asset('user/')}}/images/icon-mega/h2.svg"alt=""> {{__('Signal')}}
                </h2>
                <div class="container">
                    <div class="row">
                        <div class="col-12">
                            <div class="account-header">
                                <ul class="nav nav-tabs">
                                    <li class="active"><a data-toggle="tab" href="#all">{{__('All')}}</a></li>
                                    <li><a data-toggle="tab" href="#active">{{__('Active')}}</a></li>
                                    <li><a data-toggle="tab" href="#pending">{{__('Pending')}}</a></li>
                                    <li><a data-toggle="tab" href="#close">{{__('Closed')}}</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <div class="tab-content">
                                {{--=============================All Tab Open=======================--}}
                                <div id="all" class="tab-pane active">
                                    <div class="account-wrap order-wrap">
                                        <div class="row">
                                            {{--==============================================--}}
                                            @if(isset($signals) && !empty($signals))
                                                @foreach($signals as $index => $signal)
                                                    @include('user.signal.signal_card')
                                                @endforeach
                                            @else
                                                <h4 class="text-center">{{__('No signal available')}}</h4>
                                            @endif
                                            {{--==============================================--}}
                                        </div>
                                    </div>
                                </div>
                                {{--=============================All Tab Closed=======================--}}
                                {{--=============================Active Tab Open=======================--}}
                                <div id="active" class="tab-pane">
                                    <div class="account-wrap order-wrap">
                                        <div class="row">
                                            @include('user.signal.active_signal')
                                        </div>
                                    </div>
                                </div>
                                {{--=============================Active Tab Closed=======================--}}
                                {{--=============================Pending Tab Open=======================--}}
                                <div id="pending" class="tab-pane">
                                    <div class="account-wrap order-wrap">
                                        <div class="row">
                                            @include('user.signal.pending_signal')
                                        </div>
                                    </div>
                                </div>
                                {{--=============================Pending Tab Close=======================--}}
                                {{--=============================Close Tab Open=======================--}}
                                <div id="close" class="tab-pane">
                                    <div class="account-wrap order-wrap">
                                        <div class="row">
                                            @include('user.signal.closed_signal')
                                        </div>
                                    </div>
                                </div>
                                {{--=============================Close Tab Closed=======================--}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script>
        $(document).on('click', '.got_to_buy', function (event) {
            window.location.href = '{{route('subscriptions')}}';
        });
    </script>
@endsection
