<div class="tab-pane @if($tab == 'payment')  active @endif">
    @php
    $all_settings = allSetting();
    @endphp
    <hr>
    <div class="form-group">
        @if(isset($payment_histories) && (count($payment_histories) >= 1))
        <table class="table table-bordered" >
            <thead class="text-center">
            <tr>
                <th scope="col" class="text-center">{{__('No')}}</th>
                <th scope="col" class="text-center">{{__('Payment Date')}}</th>
                <th scope="col" class="text-center">{{__('Packages')}}</th>
                <th scope="col" class="text-center">{{__('Credit')}}</th>
                <th scope="col" class="text-center">{{__('Price')}}</th>
            </tr>
            </thead>
            <tbody>
                <?php
                    $i=1;
                ?>
                @foreach($payment_histories as $payment_history)
                <tr class="success text-center">
                    <th scope="row" class="text-center">{{$i++}}</th>
                    <td>{{Carbon\Carbon::parse($payment_history->created_at)->setTimezone(Auth::user()->time_zone)->format('d-m-Y|G\hi')}}</td>
                    <td>{{$payment_history->title}}</td>
                    <td>{{$payment_history->credit}}</td>
                    <td><i class="fa {{isset($all_settings['currency']) && ($all_settings['currency'] == CURRENCY_EURO) ?'fa-eur':'fa-usd'}}"></i> {{number_format($payment_history->price,2)}}</td>
                </tr>
                @endforeach

            </tbody>

        </table>
        @else
            <h4 class="text-center">{{__('No payment history yet.')}}</h4>
        @endif
    </div>
</div>

