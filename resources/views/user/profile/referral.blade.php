<div class="tab-pane @if($tab == 'referral')  active @endif">
    <hr>
    <label for="copy_url"><i class="fa fa-link"></i> {{__('Url')}}</label>
    <div class="input-group">
        <input type="text" readonly class="form-control" name="copy_url" id="url" @if(isset($users->own_code))value="{{url('user-sign-up/'.$users->own_code)}}" @endif>
        <div class="input-group-btn">
            <button value="copy" onclick="copyToClipboard('url')" class="btn btn-primary"><i class="fa fa-copy"></i> {{__('Copy')}}</button>
        </div>
    </div>
    <br/>
    <label for="copy_ref"><i class="fa fa-random"></i> {{__('Referral Code')}}</label>
    <div class="input-group">
        <input type="text" readonly class="form-control" name="copy_ref" id="code" @if(isset($users->own_code))value="{{$users->own_code}}" @endif>
        <div class="input-group-btn">
            <button value="copy" onclick="copyToClipboard('code')" class="btn btn-primary"><i class="fa fa-copy"></i> {{__('Copy')}}</button>
        </div>
    </div>
</div>
<script>
    function copyToClipboard(id) {
        toastr.success('Copied.');
        document.getElementById(id).select();
        document.execCommand('copy');
        this.value = "Copied";
    }
</script>
