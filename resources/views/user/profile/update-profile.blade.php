<div class="tab-pane @if($tab == 'profile')  active @endif">
    <h2></h2>
    <hr>
    <form class="form" action="{{route('userProfileUpdate')}}" method="post" id="registrationForm">
        @csrf
        <div class="form-group">
            <div class="col-xs-6">
                <label for="first_name"><h4>{{__('First name')}}</h4></label>
                <input type="text" class="form-control" name="first_name" id="first_name" placeholder="first name"
                       @if(isset($users->first_name)) value="{{$users->first_name}}" @endif >
            </div>
        </div>
        <div class="form-group">
            <div class="col-xs-6">
                <label for="last_name"><h4>{{__('Last name')}}</h4></label>
                <input type="text" class="form-control" name="last_name" id="last_name" placeholder="last name"
                       @if(isset($users->last_name)) value="{{$users->last_name}}" @endif >
            </div>
        </div>
        <div class="form-group">
            <div class="col-xs-6">
                <label for="phone"><h4>{{__('Phone')}}</h4></label>
                <input type="text" class="form-control" name="phone" id="phone" placeholder="enter phone"
                       @if(isset($users->phone)) value="{{$users->phone}}" @endif >
            </div>
        </div>
        <div class="form-group">
            <div class="col-xs-6">
                <label for="email"><h4>{{__('Email')}}</h4></label>
                <input type="email" readonly class="form-control" name="email" id="email" placeholder="you@email.com"
                       @if(isset($users->email)) value="{{$users->email}}" @endif >
            </div>
        </div>
        <div class="form-group">
            <div class="col-xs-6">
                <label for="Date of Birth"><h4>{{__('Date of Birth')}}</h4></label>
                <div id='datetimepicker1'>
                    <input type='text' readonly name="dob" class="form-control" id="datepicker"
                           @if(isset($users->dob)) value="{{$users->dob}}" @endif/>
                </div>
            </div>
        </div>
        <div class="form-group">
            <div class="col-xs-6">
                <label for="email"><h4>{{__('Location')}}</h4></label>
                <select class="form-control form-control-lg" name="country">
                    @foreach(country() as $key=> $country)
                        <option value="{{$country}}" {{isSelect($users->country,$country)}}>{{$country}}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="row"></div>
        <div class="form-group">
            <div class="col-xs-6 col-md-4 col-lg-4 col-sm-6">
                <br>
                <input type="hidden" name="edit_id" value="{{encrypt($users->id)}}">
                <button class="btn theme-btn" type="submit"><i
                        class="fa fa-save"></i> {{__('Update')}}</button>
            </div>
        </div>
    </form>

</div><!--/tab-pane-->
