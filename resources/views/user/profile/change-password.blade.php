<div class="tab-pane @if($tab == 'change_password')  active @endif">
    <h2></h2>
    <hr>
    <form class="form" action="{{route('userChangePassword')}}" method="post" id="registrationForm">
        @csrf
        <div class="form-group">
            <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
                <label>{{__('Old Password')}}</label>
                <input type="password" name="old_password" autocomplete="off" class="form-control" value="">
                @if($errors->first('old_password')) <span class="text-danger">{{$errors->first('old_password')}}</span> @endif
            </div>
        </div>
        <div class="form-group">
            <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12 mt-10">
                <label>{{__('New Password')}}</label>
                <input type="password" name="password" autocomplete="off" class="form-control" value="">
                @if($errors->first('password')) <span class="text-danger">{{$errors->first('password')}}</span> @endif
            </div>
        </div>
        <div class="form-group">
            <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12 mt-10">
                <label>{{__('Confirm Password')}}</label>
                <input type="password" name="password_confirmation" autocomplete="off" class="form-control" value="">
                @if($errors->first('password_confirmation')) <span class="text-danger">{{$errors->first('password_confirmation')}}</span> @endif
            </div>
        </div>
        <div class="row"></div>
        <div class="form-group">
            <div class="col-xs-12 col-md-4 col-lg-4 col-sm-6">
                <input type="hidden" name="edit_id" value="{{encrypt($users->id)}}">
                <button class="btn theme-btn" type="submit"><i class="fa fa-save"></i> {{__('Save Password')}}</button>
            </div>
        </div>
    </form>

</div><!--/tab-pane-->
