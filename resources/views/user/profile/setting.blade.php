<div class="tab-pane @if($tab == 'setting')  active @endif">
    <hr>
    <div class="row">
        <div class="col-sm-12 col-md-12">
            <div class="thumbnail">
                <div class="caption">
                    <h3>{{__('Time Zone')}}</h3>
                    <form method="post" action="{{route('userTimeZone')}}" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group">
                            <select class="form-control form-control-lg" name="time_zone">
                                @foreach(timezone() as $key=> $timezone)
                                    <option value="{{$key}}" @if(isset($users->time_zone)){{isSelect($key,Auth::user()->time_zone)}}@endif>{{$timezone}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary">
                                <i class="fa fa-save"></i> {{__('Save')}}
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12 col-md-12">
            <div class="thumbnail">
                <div class="caption">

                    <!-- Button trigger modal -->
                @if(Auth::user()->notification)
                    <h3>{{__('Notification')}} {{__('ON')}}</h3>
                    <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#exampleModal">
                        <i class="fa fa-power-off"></i> {{__('Turn OFF')}}
                    </button>
                @else
                    <h3>{{__('Notification')}} {{__('OFF')}}</h3>
                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
                        <i class="fa fa-power-off"></i> {{__('Turn On')}}
                    </button>
                @endif

                <!-- Modal -->
                    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-sm" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title"><strong>{{__('Notification')}} </strong></h5>
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                </div>
                                <div class="modal-body">
                                    <p>{{__('Are You Sure ??')}}</p>
                                </div>
                                <div class="modal-footer">
                                    <a href="{{route('userNotification')}}" class="btn btn-primary delete-btn">{{__('Confirm')}}</a>
                                    <button type="button" class="btn btn-primary delete-btn" data-dismiss="modal">{{__('No')}}</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
{{--<script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>--}}
