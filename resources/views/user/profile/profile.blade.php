@extends('user.layout.master')
@section('title',__('Profile'))
@section('style')
@endsection
@section('name',Auth::user()->name)
@section('content')
    <div class="main content-wrapper">
        <div class="container">
            <div class="account-area">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-12 col-md-3 col-lg-3 col-xs-12"><!--left col-->
                            <h3 class="text-left">{{$users->first_name.' '.$users->last_name}}</h3>
                            <form method="post" action="{{route('userProfileImageUpdate')}}"
                                  enctype="multipart/form-data">
                                @csrf
                                <div id="file-upload" class="section mt-20">
                                    <input type="hidden" name="edit_id" value="{{encrypt($users->id)}}">
                                    <input type="file" value="" id="file" ref="file" type="file"
                                           name="photo"
                                           class="dropify"
                                           data-allowed-file-extensions="png jpg jpeg jfif"
                                           data-default-file="{{isset(Auth::user()->photo) && file_exists(getImagePath('user').Auth::user()->photo) ? asset(getImagePath('user').Auth::user()->photo) : asset(getImagePath('user').'avater.jpg')}}"
                                           data-max-file-size="2M"/>
                                </div>
                                <button type="submit" class="btn btn-block theme-btn" style="width: 90%"><i class="fa fa-image"></i> {{__('Save Image')}}</button>
                            </form>

                        </div><!--/col-3-->
                        <div class="col-sm-12 col-md-9 col-lg-9 col-xs-12">
                            <ul class="nav nav-tabs">
                                <li @if($tab == 'profile')  class="active" @endif><a
                                        href="{{route('userProfile')}}/profile"><i class="fa fa-user-circle-o"></i> {{__('Profile')}}</a></li>
                                <li @if($tab == 'referral')  class="active" @endif><a
                                        href="{{route('userProfile')}}/referral"><i class="fa fa-random"></i> {{__('Referral Code')}}</a></li>
                                <li @if($tab == 'setting')  class="active" @endif><a
                                        href="{{route('userProfile')}}/setting"><i class="fa fa-gears"></i> {{__('Settings')}}</a></li>
                                <li @if($tab == 'payment')  class="active" @endif><a
                                        href="{{route('userProfile')}}/payment"><i class="fa fa-line-chart"></i> {{__('Subscription History')}}</a></li>
                                <li @if($tab == 'change_password')  class="active" @endif><a
                                        href="{{route('userProfile')}}/change_password"><i class="fa fa-key"></i> {{__('Change Password')}}</a>
                                </li>
                            </ul>
                            @includeWhen($tab == 'profile', 'user.profile.update-profile')
                            @includeWhen($tab == 'referral', 'user.profile.referral')
                            @includeWhen($tab == 'setting', 'user.profile.setting')
                            @includeWhen($tab == 'payment', 'user.profile.payment-histories')
                            @includeWhen($tab == 'change_password', 'user.profile.change-password')
                        </div><!--/tab-content-->
                    </div>

                </div>
            </div>
        </div>
        <!--/col-9-->
    </div>
@endsection

@section('script')
    <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
    <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css"/>
    <script type="text/javascript">
        $('#datepicker').datepicker({
            uiLibrary: 'bootstrap',
            endDate: new Date(),
            format: 'yyyy/mm/dd'
        });
    </script>
    <script>

    </script>

@endsection
