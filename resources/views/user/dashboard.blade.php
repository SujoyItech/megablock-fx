@extends('user.layout.master',['menu'=>'dashboard'])
@section('title',__('DashBoard'))
@section('name',Auth::user()->name)
@section('content')
    <div class="main-content-wrapper">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <h4 class="ac-header" style="margin-bottom: 15px"><img src="{{asset('user/')}}/images/icon-mega/h1.svg" alt="">{{__('Dashboard')}}</h4>
                </div>
                <div class="col-md-6 ac-header text-right">
                    @include('user.dashboard_content.dashboard_maps.dashboard_modals')
                </div>
            </div>
            @include('user.dashboard_content.ticket_tape')
            <hr/>
            <div class="row">
                <div class="col-lg-6">
                    @include('user.dashboard_content.latest_signals')
                </div>
                <div class="col-lg-6">
                    @include('user.dashboard_content.dashboard_chats.dashboard_charts')
                </div>
            </div>
            <div class="row">
                @include('user.dashboard_content.pips_records')
            </div>
        </div>
    </div>
    <style>

        .pips_segments {
            text-align: left;
            border-left: 2px solid #147d87;
        }

        .pips_block {
            padding: 40px 0;
        }

        .pips_block_left {
            border-right: 1px solid #147d87;
        }

        .pips_segments_num {
            font-size: 200% !important;
            font-weight: 900 !important;
        }

        .pips_segments_text {
            color: #117f89 !important;
            font-weight: 600 !important;
        }

        .pips_segments_pips {
            color: #16aebc !important;
            font-weight: 600 !important;
        }
        .modal-backdrop.in {
            opacity: 0;
        }
    </style>
@endsection
