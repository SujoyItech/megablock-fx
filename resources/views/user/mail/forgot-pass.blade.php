<h3>{{__('Hello')}}, {{ $name}}</h3>
<p>
    {{__('Please click on the following link or paste the link on address bar of your browser and hit - ')}}
</p>

<p>
    <a href="{{url('user-forget-password-change/'.$reset_token)}}">{{__('Password Recovery')}}</a>
</p>

<p>
    {{__('Thanks a lot for being with us.')}} <br />
</p>
