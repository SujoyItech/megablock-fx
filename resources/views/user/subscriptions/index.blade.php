@extends('user.layout.master',['menu'=>'subscription'])
@section('title',__('Subscription Packages'))
@section('style')

@endsection
@section('name',Auth::user()->first_name.' '.Auth::user()->last_name)
@section('content')
    @php
        $all_settings = allSetting();
    @endphp
    <div class="main content-wrapper">
        <div class="container">
            <div class="account-area">
                <h4 class="ac-header" style="margin-top: 15px;margin-bottom: 30px"><img src="{{asset('user/')}}/images/icon-mega/h4.svg"alt="">{{__('Packages')}}</h4>
                <div class="container">
                    <div class="row">
                        @foreach($packages as $package)
                            <div class="col-lg-4 col-md-6 col-sm-6 ">
                                <div class="card pakage-wrap" style="">
                                    <div class="pakage-top">
                                        <h5 class="card-title">{{\Illuminate\Support\Str::limit($package->title,30)}}</h5>
                                        <div class="pakage-img">
                                            <img src="{{ asset(getImagePath('subscription_package').$package->image) }}"
                                                 class="card-img-top img-responsive">
                                        </div>
                                        <h4><small><i
                                                    class="fa {{isset($all_settings['currency']) && ($all_settings['currency'] == CURRENCY_EURO) ?'fa-eur':'fa-usd'}}"></i></small> {{number_format($package->price,2)}}
                                        </h4>
                                        <p>{{__('Credit')}} : <strong>{{ number_format($package->credit,2) }}</strong>
                                        </p>
                                    </div>
                                    <div class="card-body">
                                        <p>{{__('Vat (Fixed)')}} : <small><i
                                                    class="fa {{isset($all_settings['currency']) && ($all_settings['currency'] == CURRENCY_EURO) ?'fa-eur':'fa-usd'}}"></i></small>
                                            <strong>{{number_format($package->vat,2)}}</strong></p>
                                        <p>{{__('Fess (Fixed)')}} : <small><i
                                                    class="fa {{isset($all_settings['currency']) && ($all_settings['currency'] == CURRENCY_EURO) ?'fa-eur':'fa-usd'}}"></i></small>
                                            <strong>{{number_format($package->fees,2)}}</strong></p>
                                        <p class="card-text">{{\Illuminate\Support\Str::limit($package->description,30)}}</p>
                                        <a href="javascript:"
                                           data-price="{{$package->price+$package->vat+$package->fees}}"
                                           data-id="{{encrypt($package->id)}}"
                                           class="theme-btn buycredit">{{__('Buy Now')}}</a>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>

    <script>

        $(document.body).on('click', '.buycredit', function () {
            var price = $(this).data('price');
            var id = $(this).data('id');
            console.log(id);
            Swal.fire({
                title: "<h5>{{__('Pay By')}}</h5>",
                html: "" +
                    "<a data-id=" + id + " href='javascript:' onclick='openCoinPayment(this)' class='btn btn-primary'>{{__('BTC')}}</a> "
                    +
                    "<a data-id=" + id + " href='javascript:' onclick='openCardPayment(this)' class='btn btn-primary'>{{__('Card')}}</a> "
                    +
                    "</div>",
                confirmButtonText: "{{__('Back')}}",
            }).then((result) => {
                console.log(result);

            })
        });
    </script>
    <script>
        function openCardPayment(event) {
            var id = $(event).data('id');
            $.ajax({
                url: "{{route('stripeGateway')}}?id=" + id + "",
                success: function (result) {
                    Swal.fire({
                        title: "<h4>{{__('Card details')}}</h4>",
                        html: result.data,
                        confirmButtonText: "{{__('Back')}}",
                    }).then((result) => {
                        console.log(result);

                    })
                }
            });
        }

        function openCoinPayment(event) {
            var id = $(event).data('id');
            Swal.fire('Please wait')
            Swal.showLoading();
            $.ajax({
                url: "{{route('payWithBTC')}}?id=" + id + "", success: function (result) {
                    Swal.fire({
                        title: "<h5>{{__('Card details')}}</h5>",
                        html: result.message,
                        confirmButtonText: "{{__('Back')}}",
                    }).then((result) => {
                        console.log(result);

                    })
                }
            });
        }
    </script>
    <script type="text/javascript" src="https://js.stripe.com/v2/"></script>
    <script type="text/javascript">
        $(function () {
            var $form = $(".stripe_form");

            $(document.body).on('click', '.payButton', function (e) {
                var $form = $(".require-validation"),
                    inputSelector = ['input[type=email]', 'input[type=password]',
                        'input[type=text]', 'input[type=file]',
                        'textarea'].join(', '),
                    $inputs = $form.find('.required').find(inputSelector),
                    $errorMessage = $form.find('div.error'),
                    valid = true;
                $errorMessage.addClass('hide');

                $('.has-error').removeClass('has-error');
                $inputs.each(function (i, el) {
                    var $input = $(el);
                    if ($input.val() === '') {
                        $input.parent().addClass('has-error');
                        $errorMessage.removeClass('hide');
                        e.preventDefault();
                    }
                });


                if (!$(".stripe_form").data('cc-on-file')) {
                    e.preventDefault();
                    var stripe_publishable_key = "{{env('stripe-publishable-key')}}";
                    Stripe.setPublishableKey($(".stripe_form").data('stripe-publishable-key'));
                    Stripe.createToken({
                        number: $('.card-number').val(),
                        cvc: $('.card-cvc').val(),
                        exp_month: $('.card-expiry-month').val(),
                        exp_year: $('.card-expiry-year').val()
                    }, stripeResponseHandler);
                }

            });

            function stripeResponseHandler(status, response) {
                if (response.error) {
                    $('.error')
                        .removeClass('hide')
                        .find('.alert')
                        .text(response.error.message);

                    console.log('error');
                } else {
                    var token = response['id'];
                    // insert the token into the form so it gets submitted to the server

                    $('.stripe_form').find('input[type=text]').empty();
                    $('.stripe_form').append("<input type='hidden' name='stripeToken' value='" + token + "'/>");
                    $('.stripe_form').submit();
                }
            }
        });
    </script>
@endsection




