<style type="text/css">
    .panel-title {
        display: inline;
        font-weight: bold;
    }

    .display-table {
        display: table;
    }

    .display-tr {
        display: table-row;
    }

    .display-td {
        display: table-cell;
        vertical-align: middle;
        width: 61%;
    }
</style>
@php
    $all_settings = allSetting();
@endphp

<div class="panel panel-default credit-card-box">
    <div class="panel-heading display-table">
        <div class="row display-tr">
            <h3 class="panel-title display-td">{{__('Payment Details')}}</h3>
            <div class="display-td">
                <img class="img-responsive pull-right" src="http://i76.imgup.net/accepted_c22e0.png">
            </div>
        </div>
    </div>
    <div class="panel-body">

        @if (Session::has('success'))
            <div class="alert alert-success text-center">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                <p>{{ Session::get('success') }}</p>
            </div>
        @endif

        <form id="" role="form" action="{{ route('stripePost') }}" method="post" class="require-validation stripe_form"
              data-cc-on-file="false"
              data-stripe-publishable-key="{{ env('STRIPE_KEY') }}"
              id="payment-form">
            <input type="hidden" name="package" value="{{encrypt($package_details->id)}}">
            @csrf

            <div class='form-row row'>
                <div class='col-xs-12 form-group required'>
                    <label class='control-label'>{{__('Name on Card')}}</label>
                    <input
                        value="Test" class='form-control' size='4' type='text'>
                </div>
            </div>

            <div class='form-row row'>
                <div class='col-xs-12 form-group card required'>
                    <label class='control-label'>{{__('Card Number')}}</label>
                    <input
                        value="4242 4242 4242 4242" autocomplete='off' class='form-control card-number' size='20'
                        type='text'>
                </div>
            </div>

            <div class='form-row row'>
                <div class='col-xs-12 col-md-4 form-group cvc required'>
                    <label class='control-label'>{{__('CVC')}}</label>
                    <input value="123" autocomplete='off'
                           class='form-control card-cvc'
                           placeholder='ex. 311' size='4'
                           type='text'>
                </div>
                <div class='col-xs-12 col-md-4 form-group expiration required'>
                    <label class='control-label'>{{__('Expiration Month')}}</label>
                    <input
                        value="12" class='form-control card-expiry-month' placeholder='MM' size='2'
                        type='text'>
                </div>
                <div class='col-xs-12 col-md-4 form-group expiration required'>
                    <label class='control-label'>{{__('Expiration Year')}}</label>
                    <input
                        value="2024" class='form-control card-expiry-year' placeholder='YYYY' size='4'
                        type='text'>
                </div>
            </div>

            <div class='form-row row'>
                <div class='col-md-12 error form-group hide'>
                    <div class='alert-danger alert'>{{__('Please correct the errors and try again.')}}</div>
                </div>
            </div>

            <div class="row">
                <div class="col-xs-12">
                    <button class="btn btn-primary btn-lg btn-block payButton" type="button">{{__('Pay Now')}} ( <i
                            class="fa {{isset($all_settings['currency']) && ($all_settings['currency'] == CURRENCY_EURO) ?'fa-eur':'fa-usd'}}"></i>
                        {{number_format($package_details->price+$package_details->vat+$package_details->fees,2)}})
                    </button>
                </div>
            </div>

        </form>
    </div>
</div>
