<div class="buy-coin-form text-center">
    <h4> {{__('Please send')}} <span>{{$coin_amount}} {{env('COINPAYMENT_CURRENCY')}} </span>{{__('to this deposit address')}}: </h4>
    <p class="ps-address" data-ps-address="3P8TxWa8TjqANEoRHWVzHeDUdwifuAvXNe">{{__('Address')}} : {{$coin_address}}</p>
    <div><!--?xml version="1.0" encoding="UTF-8"?-->
        <div>{!! \SimpleSoftwareIO\QrCode\Facades\QrCode::encoding('UTF-8')->size(200)->generate(__('Address') . ':' .$coin_address . PHP_EOL . __('Amount') . ':' . $coin_amount); !!}</div>
    </div>

</div>