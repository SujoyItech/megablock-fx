@extends('user.layout.master')
@section('title',__('Check Out'))
@section('style')
@endsection
@section('name',Auth::user()->name)
@section('content')
<div class="main content-wrapper">
    <div class="container">
        <div class="account-area">
            <div class="container">
                <div class="row">
                    <div class="col-md-3"></div>
                    <div class="col-md-6 alert-float alert alert-danger alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
                        <p class="text-center">{{__('Please fill the missing information to get full access.')}}</p>
                    </div>
                </div>

                <div class="row">
                     <form class="form" action="{{route('updateMissingData')}}" method="post" id="registrationForm">
                        @csrf

                        <div class="form-group">
                            <div class="col-xs-6">
                                <label for="first_name"><h4>{{__('First name')}}</h4></label>
                                <input type="text" class="form-control" name="first_name" id="first_name" placeholder="first name" @if(isset(Auth::user()->first_name)) value="{{Auth::user()->first_name}}" @else value="{{old('first_name')}}" @endif >
                            </div>
                        </div>
                        <div class="form-group">

                            <div class="col-xs-6">
                                <label for="last_name"><h4>{{__('Last name')}}</h4></label>
                                <input type="text" class="form-control" name="last_name" id="last_name" placeholder="last name" @if(isset(Auth::user()->last_name)) value="{{Auth::user()->last_name}}" @else value="{{old('last_name')}}" @endif >
                            </div>
                        </div>

                        <div class="form-group">

                            <div class="col-xs-6">
                                <label for="phone"><h4>{{__('Phone')}}</h4></label>
                                <input type="text" class="form-control" name="phone" id="phone" placeholder="enter phone" @if(isset(Auth::user()->phone)) value="{{Auth::user()->phone}}" @else value="{{old('phone')}}" @endif >
                            </div>

                        </div>
                        <div class="form-group">

                            <div class="col-xs-6">
                                <label for="email"><h4>{{__('Email')}}</h4></label>
                                <input type="email" class="form-control" name="email" id="email" placeholder="you@email.com" @if(isset(Auth::user()->email)) value="{{Auth::user()->email}}" @else value="{{old('email')}}" @endif>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-xs-6">
                                <label for="Date of Birth"><h4>{{__('Date of Birth')}}</h4></label>
                                <div id='datetimepicker1'>
                                    <input type='text' readonly name="dob" class="form-control" id="datepicker" @if(isset(Auth::user()->dob)) value="{{Auth::user()->dob}}" @else value="{{old('dob')}}" @endif/>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">

                            <div class="col-xs-6">
                                <label for="email"><h4>{{__('Location')}}</h4></label>
                                <select class="form-control form-control-lg" name="country">
                                    @foreach(country() as $key=> $country)
                                        <option value="{{$country}}" {{isSelect(Auth::user()->country,$country)}} {{ old('country') == $country ? 'selected' : '' }}>{{$country}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                         <div class="form-group">
                             <div class="col-xs-6">
                                 <label for="first_name"><h4>{{__('Reference Code')}}</h4></label>
                                 <input type="text" class="form-control" name="code" id="first_name" placeholder="Use Referral Code"  value="{{old('code')}}">
                             </div>
                         </div>
                        <div class="form-group">
                            <div class="col-xs-12">
                                <br>
                                <input type="hidden" name="edit_id" value="{{encrypt(Auth::user()->id)}}">
                                <button class="btn theme-btn" type="submit"><i
                                        class="fa fa-save"></i> {{__('Update')}}</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
    <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript">
        $('#datepicker').datepicker({
            uiLibrary: 'bootstrap',
            endDate: new Date(),
            format: 'yyyy/mm/dd'
        });
    </script>

@endsection
