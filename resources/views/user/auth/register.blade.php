@extends('admin.auth.master')
@section('title',__('Sign Up'))
@section('content')
<div class="ht-100v text-center">
    <div class="row pd-0 mg-0">
        <div class="col-md-6 col-lg-6 bg-gradient hidden-sm hidden-xs">
            <div class="d-flex">
                <div class="align-self-center">
                    <img src="{{asset('user/')}}/images/login.svg" alt="">
                    <h3 class="tx-20 tx-semibold tx-gray-100 pd-t-50">{{__('MegaBlock Fx')}}</h3>
                    <p class="pd-y-15 pd-x-10 pd-md-x-100 tx-gray-200">
                    </p>
                    <a href="{{route('login')}}" class="btn btn-outline-info"><span class="tx-gray-200"><i class="fa fa-sign-in"></i> {{__('Log in')}}</span></a>
                </div>
            </div>
        </div>
        <div class="col-md-6 col-lg-6 bg-light bg-right" style="padding: 20px 0">
            <div class="d-flex align-items-center justify-content-center">
                <div class="col-12">
                    <h3 class="tx-dark text-left ml-3 mg-b-5">{{__('Signup')}}</h3>
                    <p class="tx-gray-500 tx-15 mg-b-40 tx-center"></p>
                    @if(Session::has('message'))
                        <div class="alert alert-success alert-dismissible" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            {!! Session::get('message') !!}
                        </div>
                    @endif
                    @if(Session::has('dismiss'))
                        <div class="alert alert-danger alert-dismissible" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            {{Session::get('dismiss')}}
                        </div>
                    @endif
                    @if(Session::has('success'))
                        <div class="alert alert-success alert-dismissible" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            {{Session::get('success')}}
                        </div>
                    @endif
                    <form action="{{route('userSignUpSave')}}" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="row col-12">
                        <div class="col-12">
                            <div class="form-group tx-left">
                                <label class="tx-gray-500 mg-b-5">{{__('Reference Code')}}</label>
                                <input type="text" id="code" class="form-control" name="code" placeholder="{{__('Use Reference Code')}}" @if(isset($code))value="{{$code}}" @else value="{{old('code')}}"@endif>
                            </div>
                            @if($errors->first('code'))<span class="text-danger tx-left">{{$errors->first('code')}}</span>@endif
                        </div>
                        <div class=" col-lg-6 col-sm-6 col-12">
                            <div class="form-group">
                                <div class="d-flex justify-content-between mg-b-5">
                                    <label class="tx-gray-500 mg-b-0">{{__('First Name')}}</label>
                                </div>
                                <input type="text" class="form-control" id="first_name" name="first_name" placeholder="{{__('First Name')}}" value="{{old('first_name')}}">
                                @if($errors->first('first_name'))<span class="text-danger tx-left">{{$errors->first('first_name')}}</span>@endif
                            </div>
                        </div>
                        <div class=" col-lg-6 col-sm-6 col-12">
                            <div class="form-group">
                                <div class="d-flex justify-content-between mg-b-5">
                                    <label class="tx-gray-500 mg-b-0">{{__('Last Name')}}</label>
                                </div>
                                <input type="text" class="form-control" id="last_name" name="last_name" placeholder="{{__('Last Name')}}"  value="{{old('last_name')}}">
                                @if($errors->first('last_name'))<span class="text-danger tx-left">{{$errors->first('last_name')}}</span>@endif
                            </div>
                        </div>
                        <div class=" col-lg-6 col-sm-6 col-12">
                            <div class="form-group tx-left">
                                <label class="tx-gray-500 mg-b-5">{{__('Email')}}</label>
                                <input type="email" class="form-control" id="email" name="email" placeholder="{{__('Email')}}"  value="{{old('email')}}">
                                @if($errors->first('email'))<span class="text-danger tx-left">{{$errors->first('email')}}</span>@endif
                            </div>
                        </div>
                        <div class=" col-lg-6 col-sm-6 col-12">
                            <div class="form-group tx-left">
                                <label class="tx-gray-500 mg-b-5">{{__('Phone')}}</label>
                                <input class="form-control" type="text" id="phone" name="phone" placeholder="{{__('Phone')}}"  value="{{old('phone')}}">
                                @if($errors->first('phone'))<span class="text-danger tx-left">{{$errors->first('phone')}}</span>@endif
                            </div>
                        </div>
                        <div class=" col-lg-6 col-sm-6 col-12">
                            <div class="form-group tx-left">
                                <label class="tx-gray-500 mg-b-5">{{__('Country')}}</label>
                                <select class="form-control" name="country">
                                    @foreach(country() as $key=> $country)
                                        <option value="{{$country}}" {{ old('country') == $country ? 'selected' : '' }}>{{$country}}</option>
                                    @endforeach
                                </select>
                                @if($errors->first('country'))<span class="text-danger tx-left">{{$errors->first('country')}}</span>@endif
                            </div>
                        </div>

                        <div class=" col-lg-6 col-sm-6 col-12">
                            <div class="form-group tx-left">
                                <label class="tx-gray-500 mg-b-5">{{__('Date Of Birth')}}</label>
                                <div class="form-group">
                                    <div id='datetimepicker1'>
                                        <input type='text'  readonly="" name="dob" class="form-control datepicker" id="datepicker" value="{{old('dob')}}"/>
                                    </div>
                                </div>
                                @if($errors->first('dob'))<span class="text-danger tx-left">{{$errors->first('dob')}}</span>@endif
                            </div>
                        </div>
                        <div class=" col-lg-6 col-sm-6 col-12">
                            <div class="form-group">
                                <div class="d-flex justify-content-between mg-b-5">
                                    <label class="tx-gray-500 mg-b-5" for="Phone">{{__('Password')}}</label>
                                </div>
                                <input type="password" class="form-control" placeholder="{{__('Password')}}" id="pass" value="" name="password">
                                @if($errors->first('password'))<span class="text-danger tx-left">{{$errors->first('password')}}</span>@endif
                            </div>
                        </div>
                        <div class=" col-lg-6 col-sm-6 col-12">
                            <div class="form-group">
                                <div class="d-flex justify-content-between mg-b-5">
                                    <label class="tx-gray-500 mg-b-5" for="Phone">{{__('Confirm Password')}}</label>
                                </div>
                                <input type="password" class="form-control" placeholder="{{__('Confirm Password')}}" id="pass1" value="" name="password_confirmation">
                                @if($errors->first('password_confirmation'))<span class="text-danger tx-left">{{$errors->first('password_confirmation')}}</span>@endif
                            </div>
                        </div>
                        <div class="col-12 row">
                            <div class="check-box-wrap col-lg-4 col-md-6 col-sm-6 col-12">
                                <div class="input-box">
                                    <input type="checkbox" id="terms" name="terms_condition" value="1" @if(old('terms_condition')) checked @endif>
                                    <label for="terms">{{__('I agree to the terms and conditions')}}</label>
                                </div>
                                @if($errors->first('terms_condition'))<span class="text-danger tx-left">{{$errors->first('terms_condition')}}</span>@endif
                            </div>
                            <div class="check-box-wrap col-lg-4 col-md-6 col-sm-6 col-12">
                                <div class="input-box">
                                    <input type="checkbox" id="risk" name="risk_warning" value="1" @if(old('risk_warning')) checked @endif>
                                    <label for="risk">{{__('I accept the risk & warning')}}</label>
                                </div>
                                @if($errors->first('risk_warning'))<span class="text-danger tx-left">{{$errors->first('risk_warning')}}</span>@endif
                            </div>
                            <div class="check-box-wrap col-lg-4 col-md-6 col-sm-6 col-12">
                                <div class="input-box">
                                    <input type="checkbox" id="adult" name="adult_warning" value="1" @if(old('adult_warning')) checked @endif>
                                    <label for="adult">{{__('I am over 18 years old')}}</label>
                                </div>
                                @if($errors->first('adult_warning'))<span class="text-danger tx-left">{{$errors->first('adult_warning')}}</span>@endif
                            </div>
                        </div>
                        <div class="col-3"></div>
                        <div class="col-6 pd-t-10">
                            <button type="submit" class="btn btn-brand btn-block"><i class="fa fa-user-plus"></i> {{__('Create Account')}}</button>
                        </div>
                    </div>
                    </form>
                    <div class="pd-y-20 tx-uppercase tx-gray-500">or</div>
                    <a href="{{route('socialLogin','facebook')}}" class="btn bg-facebook"><i class="fa fa-facebook"></i> Facebook</a>
                    <a href="{{route('socialLogin','google')}}" class="btn bg-google"><i class="fa fa-google"></i> Google</a>
                    <div class="tx-13 mg-t-20 tx-center tx-gray-500">{{__('Do you have already an account?')}} <a href="{{route('login')}}" class="tx-semibold">{{__('Login Here')}}</a></div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
{{--<script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>--}}
{{--<link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />--}}

{{--<script type="text/javascript">--}}
{{--    $('#datepicker').datepicker({--}}
{{--        uiLibrary: 'bootstrap',--}}
{{--        endDate: 2019-12-30,--}}
{{--        format: 'yyyy/mm/dd'--}}
{{--    });--}}
{{--</script>--}}
{{--<script type="text/javascript">--}}
{{--    var start = new Date();--}}
{{--    start.setFullYear(start.getFullYear() - 70);--}}
{{--    var end = new Date();--}}
{{--    end.setFullYear(end.getFullYear() - 1);--}}
{{--    $('#datepicker').datepicker({--}}
{{--        changeMonth: true,--}}
{{--        changeYear: true,--}}
{{--        minDate: start,--}}
{{--        maxDate: end,--}}
{{--        yearRange: start.getFullYear() + ':' + end.getFullYear(),--}}
{{--        format: 'yyyy/mm/dd'--}}
{{--    });--}}
{{--</script>--}}
@endsection
