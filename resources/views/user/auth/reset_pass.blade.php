@extends('admin.auth.master')
@section('content')
<div class="ht-100v text-center">
 <div class="row pd-0 mg-0">
    <div class="col-md-6 col-lg-6 bg-gradient hidden-sm hidden-xs">
       <div class="ht-100v d-flex">
          <div class="align-self-center">
          <img src="{{asset('user/')}}/images/login.svg" alt="">
             <h3 class="tx-20 tx-semibold tx-gray-100 pd-t-50">{{__('MEGABLOCK FX')}}</h3>
             <p class="pd-y-15 pd-x-10 pd-md-x-100 tx-gray-200"></p>
              <a href="{{route('userSignUp')}}" class="btn btn-outline-info">
                  <span class="tx-gray-200"><i class="fa fa-user-plus"></i> {{__('Get An Account')}}</span>
              </a>
          </div>
       </div>
    </div>
    <div class="col-md-6 col-lg-6 bg-light">
       <div class="ht-100v d-flex align-items-center justify-content-center">
            <div class="reset-area">
                {{Form::open(['route'=>'userForgetPasswordResetProcess','method'=>'post'])}}
                <h3 class="tx-dark mg-b-5 tx-left">{{__('Reset Password')}}</h3>
                <p class="tx-gray-500 tx-15 mg-b-40 tx-left"></p>
                @if(Session::has('message'))
                    <div class="alert alert-success alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        {!! Session::get('message') !!}
                    </div>
                @endif
                @if(Session::has('dismiss'))
                    <div class="alert alert-danger alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        {{Session::get('dismiss')}}
                    </div>
                @endif
                @if(Session::has('success'))
                    <div class="alert alert-success alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        {{Session::get('success')}}
                    </div>
                @endif

                <div class="form-group tx-left">
                    <label class="tx-gray-500 mg-b-5" for="Email">{{__('Password')}}</label>
                    <input type="password" placeholder="{{__('Password')}}" name="password" class="form-control">
                    @if($errors->first('password'))<span class="text-danger">{{$errors->first('password')}}</span>@endif
                </div>
                <div class="form-group tx-left">
                    <label class="tx-gray-500 mg-b-5" for="Email">{{__('Confirm Password')}}</label>
                    <input type="password" name="password_confirmation" class="form-control" placeholder="{{__('Confirm Password')}}">
                    @if($errors->first('password_confirmation'))<span class="text-danger">{{$errors->first('password_confirmation')}}</span>@endif
                </div>
                <input type="hidden" name="reset_token" value="{{isset($reset_token)? $reset_token: ''}}">
                <button type="submit" class="btn btn-brand btn-block accountBtn">{{__('Update Password')}}</button>
                {{Form::close()}}
            </div>

       </div>
    </div>
 </div>
</div>
@endsection
      <!--/ Page Content End -->

