@extends('admin.auth.master')
@section('content')
<div class="ht-100v text-center">
 <div class="row pd-0 mg-0">
    <div class="col-lg-6 bg-gradient hidden-sm">
       <div class="ht-100v d-flex">
          <div class="align-self-center">
          <img src="{{asset('user/')}}/images/login.svg" alt="">
             <h3 class="tx-20 tx-semibold tx-gray-100 pd-t-50">{{__('MEGABLOCK FX')}}</h3>
             <p class="pd-y-15 pd-x-10 pd-md-x-100 tx-gray-200"></p>
             <a href="{{route('userSignUp')}}" class="btn btn-outline-info"><span class="tx-gray-200"> <i class="fa fa-user-plus"></i> {{__('Get An Account')}}</span></a>
          </div>
       </div>
    </div>
    <div class="col-lg-6 bg-light">
       <div class="ht-100v d-flex align-items-center justify-content-center">
            <div class="">
                @if(Session::has('message'))
                    <div class="alert alert-success alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        {!! Session::get('message') !!}
                    </div>
                @endif
                @if(Session::has('dismiss'))
                    <div class="alert alert-danger alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        {{Session::get('dismiss')}}
                    </div>
                @endif
                @if(Session::has('success'))
                    <div class="alert alert-success alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        {{Session::get('success')}}
                    </div>
                @endif
                @if(count($errors) > 0)
                    @foreach($errors->all() as $error)
                        <div class="alert alert-danger alert-dismissible" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            {!! $error !!}
                        </div>
                    @endforeach
                @endif
                {{Form::open(['route'=>'userForgetPasswordProcess','method'=>'post'])}}
                <h3 class="tx-dark mg-b-5 tx-left">{{__('Reset Password')}}</h3>
                <p class="tx-gray-500 tx-15 mg-b-40 tx-left"></p>
                <div class="form-group tx-left">
                    <label class="tx-gray-500 mg-b-5" for="Email">{{__('Email')}}</label>
                    <input class="form-control"  type="text" id="email" name="email" placeholder="{{__('Email')}}" value="{{old('email')}}">
                </div>
                <button type="submit" class="btn btn-brand btn-block accountBtn">{{__('Send Password Reset Link')}}</button>
                {{Form::close()}}
                <div class="pd-y-20 tx-uppercase tx-gray-500">or</div>
                <a href="{{route('socialLogin','facebook')}}" class="btn bg-facebook"><i class="fa fa-facebook"></i> {{__('Facebook')}}</a>
                <a href="{{route('socialLogin','google')}}" class="btn bg-google"><i class="fa fa-google"></i> {{__('Google')}}</a>
                <div class="tx-13 mg-t-20 tx-center tx-gray-500">{{__('Don\'t have an account?')}} <a href="{{route('userSignUp')}}" class="tx-dark tx-semibold">{{__('Create an Account')}}</a></div>
            </div>

       </div>
    </div>
 </div>
</div>
@endsection
      <!--/ Page Content End -->

