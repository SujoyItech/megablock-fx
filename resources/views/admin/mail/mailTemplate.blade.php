<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="stylesheet" media="all" href="/assets/application-mailer-dbc5154d3c4160e8fa7ef52fa740fa402760c39b5d22c8f6d64ad5999499d263.css" />
    <style><!-- Add custom styles that you want inlined here --></style>
</head>
<!-- Edit the code below this line -->
<body class="bg-light">
<div class="container">

    <div class="card mb-4" style="border-top: 5px solid #ff00bf;">
        <div class="card-body">

            <hr/>
            <p>{{$e_message}}</p>
            <a href="{{url('email-verify'.'/'.encrypt($key).'/'.$parentId)}}" class="btn btn-success">{{__('Click Here To Verify Email')}}</a>

        </div>
    </div>

</div>

</body>
</html>

