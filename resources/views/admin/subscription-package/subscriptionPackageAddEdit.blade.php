@extends('admin.layout.master',['menu'=>'subscription'])
@section('title',$title)
@section('style')
@endsection
@section('content')
    @php
        $all_settings=allSetting();
    @endphp
    <div class="main-wrapper">
        <div class="pageheader pd-t-25 pd-b-35">
            <div class="d-flex justify-content-between">
                <div class="pd-t-5 pd-b-5">
                    <h2 class="s-title"><img src="{{asset('user/')}}/images/icon-mega/h4.svg"alt="" width="40"> {{__('Subscription Packages')}}</h2>
                </div>
            </div>
        </div>
        {{ Form::open(array('route' => 'subscriptionPackageSave', 'files' => true,'method' => 'post'))}}
        @csrf
        <div class="row">
            <div class="col-md-9 row">
                <div class="col-sm-8 mb-3">
                    <label>{{__('Title')}}</label>
                    <input type="text" name="title" autocomplete="off" class="form-control mb-1" @if(isset($item)) value="{{$item->title}}"@else value="{{old('title')}}" @endif >
                    @if($errors->first('title')) <span class="text-danger">{{$errors->first('title')}}</span>@endif
                </div>
                <div class="col-sm-4 mb-3">
                    <label>{{__('Price')}} (<small><i class="fa {{isset($all_settings['currency']) && ($all_settings['currency'] == CURRENCY_EURO) ?'fa-eur':'fa-usd'}}"></i></small>)</label>
                    <input type="number" step='0.01' min="0" name="price" autocomplete="off" class="form-control mb-1" @if(isset($item)) value="{{$item->price}}" @else value="{{old('price')}}" @endif >
                    @if($errors->first('price'))<span class="text-danger">{{$errors->first('price')}}</span>@endif
                </div>
                <div class="col-sm-4 mb-3">
                    <label>{{__('Credit')}}</label>
                    @php($interval = !(empty($item)) ? $item->credit : old('credit'))
                    <input type="number" min="0"  name="credit" autocomplete="off" class="form-control mb-1" value="{{$interval}}" >
                    @if($errors->first('credit'))<span class="text-danger">{{$errors->first('credit')}}</span>@endif
                </div>
                <div class="col-sm-4 mb-3">
                    <label>{{__('Fees ( Fixed )')}} (<small><i class="fa {{isset($all_settings['currency']) && ($all_settings['currency'] == CURRENCY_EURO) ?'fa-eur':'fa-usd'}}"></i></small>)</label>
                    <input type="number" step='0.01'  min="0"  name="fees" autocomplete="off" class="form-control mb-1" @if(isset($item)) value="{{number_format($item->fees,2)}}"@else value="{{old('fees')}}" @endif >
                    @if($errors->first('fees'))<span class="text-danger">{{$errors->first('fees')}}</span>@endif
                </div>
                <div class="col-sm-4 mb-3">
                    <label>{{__('Vat ( Fixed )')}} (<small><i class="fa {{isset($all_settings['currency']) && ($all_settings['currency'] == CURRENCY_EURO) ?'fa-eur':'fa-usd'}}"></i></small>)</label>
                    <input type="number" step='0.01' min="0" name="vat" autocomplete="off" class="form-control mb-1" @if(isset($item)) value="{{number_format($item->vat,2)}}"@else value="{{old('vat')}}" @endif >
                    @if($errors->first('vat'))<span class="text-danger">{{$errors->first('vat')}}</span>@endif
                </div>
                <div class="col-sm-12 mb-3">
                    <label>{{__('Description')}}</label>
                    <textarea type="text" name="description" autocomplete="off" class="form-control mb-1">@if(isset($item)){{$item->description}}@else{{old('description')}}@endif</textarea>
                    @if($errors->first('description'))<span class="text-danger">{{$errors->first('description')}}</span>@endif
                </div>
            </div>
            <div class="col-3">
                <div class="col-12">
                    <label for="name">{{__('Image')}}</label>
                    <small>{{__('jpg, png or jpeg & max size 2mb')}}</small>
                    <div id="file-upload" class="section">
                        <div class="row section">
                            <div class="col s12 m12 l12">
                                <input name="image" type="file" id="input-file-now" class="dropify mb-1"
                                       data-default-file="{{isset($item->image) &&!empty($item->image) ? asset(getImagePath('subscription_package').$item->image) : ''}}" />
                                @if ($errors->has('image'))
                                    <div class="text-danger text-center">{{ $errors->first('image') }}</div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
{{--            <div class="col-6 mb-3">--}}
{{--                <div class="custom-control custom-checkbox">--}}
{{--                    <input type="checkbox" class="custom-control-input" id="customCheck1" name="is_free"  @if(isset($item->is_free) && $item->is_free ==1 ) checked value="1" @else value="0" @endif>--}}
{{--                    <label class="custom-control-label" for="customCheck1">{{__('Available For Free')}}</label>--}}
{{--                </div>--}}
{{--            </div>--}}
            <div class="col-md-12">
                @if(isset($item)) <input type="hidden" name="edit_id" value="{{encrypt($item->id)}}"> @endif
                <button type="submit" class="btn btn-lg btn-primary"><i class="fa fa-save"></i> {{$button_title}}</button>
            </div>
        </div>
        {{Form::close()}}
    </div>
{{--    @include('admin.layout.page-title',['title'=>$title])--}}
@endsection
@section('script')
{{--    <script>--}}
{{--        $(document).ready(function () {--}}
{{--            var ckbox = $('#customCheck1');--}}

{{--            $('#customCheck1').on('click',function () {--}}
{{--                if (ckbox.is(':checked')) {--}}
{{--                    $("#customCheck1").val(1);--}}
{{--                } else {--}}
{{--                    $("#customCheck1").val(0);--}}
{{--                }--}}
{{--            });--}}
{{--        });--}}
{{--    </script>--}}
@endsection


{{--Licences (from list. Multi selection)--}}
{{--Markets (multi selection list of countries)--}}
{{--Average spreads (text)--}}
{{--Recommended deposit (text)--}}
{{--URL--}}
