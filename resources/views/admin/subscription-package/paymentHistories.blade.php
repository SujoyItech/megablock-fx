@extends('admin.layout.master',['menu'=>'subscription'])
@section('title',__('Subscription Package'))
@section('after-style')
@endsection
@section('content')
    <div class="main-wrapper">
        <div class="pageheader pd-t-25 pd-b-35">
            <div class="d-flex justify-content-between">
                <div class="pd-t-5 pd-b-5">
                    <h1 class="pd-0 mg-0 tx-30 tx-dark"><i class="fa fa-paypal"></i> {{__('Payment Histories')}}</h1>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 col-lg-12">
                <div class="card mg-b-30">
                    <div class="card-body pd-0">
                        <table id="basicDataTable" class="table responsive nowrap">
                            <thead>
                            <tr>
                                <th class="all">{{__('Name')}}</th>
        {{--                        <th class="all">{{__('coin address')}}</th>--}}
                                {{--<th class="all">{{__('description')}}</th>--}}
                                <th class="all">{{__('Credit')}}</th>
                                <th class="all">{{__('coin amount')}}</th>
                                <th class="all">{{__('Status')}}</th>
                                <th class="all">{{__('Created At')}}</th>
                                <th class="all" width="30">{{__('Actions')}}</th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
{{--    @include('admin.layout.page-title',['title'=>__('Subscription payment Pending List')])--}}
@endsection
@section('script')
    <script>
        $('#basicDataTable').DataTable({
            processing: true,
            serverSide: true,
            pageLength: 10,
            responsive: true,
            ajax: '{{route('paymentPackageHistories')}}',
            order: [5, 'desc'],
            autoWidth:false,
            columns: [
                {"data": "name"},
                // {"data": "coin_address"},
                // {"data": "description"},
                {"data": "amount"},
                // {"data": "amount"},
                {"data": "status"},
                {"data": "created_at"},

            {"data": "actions",orderable: false, searchable: false}
            ]
        });
    </script>
@endsection
