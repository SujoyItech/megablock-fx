@extends('admin.layout.master',['menu'=>'profile'])
@section('title',__('Profile'))
@section('style')
@endsection
@section('content')
    <div class="main-wrapper">
        <div class="pageheader pd-t-25 pd-b-35">
            <div class="d-flex justify-content-between">
                <div class="pd-t-5 pd-b-5">
                    <h1 class="pd-0 mg-0 tx-30 tx-dark"><i class="fa fa-user-circle-o"></i> {{__('Profile')}}</h1>
                </div>
            </div>
        </div>        <!-- single status area start -->
        {{ Form::open(array('route' => 'updateProfile','files'=>true,'id'=>'myForm')) }}
        <div class="row">
            <div class="col-md-3">
                <div class="card">
                    <div class="card-body text-center">
                        <div class="clearfix"></div>
                        <div id="file-upload" class="section">
                            <div class="">
                                <input name="photo" type="file" id="input-file-now" class="dropify" data-default-file="{{!empty($user->photo) ? asset(getImagePath('user').$user->photo) : asset('assets/images/users-face/1.png')}}" />
                                @if ($errors->has('photo'))
                                    <div class="text-danger">{{ $errors->first('photo') }}</div>
                                @endif
                            </div>

                        </div>
                        <br>
                        <p class="text-center">{{$user->name}}</p>
                        <p class="text-center">{{$user->email}}</p>
                    </div>
                </div>
            </div>
            <div class="col-md-9">
                <div class="card">
                    <div class="card-body">
                        <div class="clearfix"></div>
                        <div class="row">
                            <div class="col-sm-12 mb-3">
                                <label>{{__('Name')}}</label>
                                <input type="text" name="name" autocomplete="off" class="form-control" value="{{$user->name}}" >
                                @if($errors->first('name')) <pre class="text-danger">{{$errors->first('name')}}</pre> @endif
                            </div>
                            <div class="col-sm-6 mb-3">
                                <label>{{__('Country')}}</label>
                                <select name="country" id="" class="form-control">
                                    @foreach(country() as $key => $value)
                                        <option @if(isset($user) && $user->country == $key) selected @elseif(old('country') != null && (old('country') == $key)) selected @endif value="{{$key}}">{{$value}}</option>
                                    @endforeach
                                </select>
                                @if($errors->first('country')) <pre class="text-danger">{{$errors->first('country')}}</pre> @endif
                            </div>
                            <div class="col-sm-6 mb-3">
                                <label>{{__('Mobile')}}</label>
                                <input type="text" name="phone" autocomplete="off" class="form-control"
                                       @if(isset($user->phone)) value="{{$user->phone}}" @else value="{{old('phone')}}" @endif >
                                @if($errors->first('phone')) <pre class="text-danger">{{$errors->first('phone')}}</pre> @endif
                            </div>
                            {{--<div class="col-sm-12 mb-3">--}}
                            {{--<label>{{__('Address')}}</label>--}}
                            {{--<input type="text" name="address" autocomplete="off" class="form-control"--}}
                            {{--value="{{$user->address}}" >--}}
                            {{--@if() <pre class="text-danger">{{$errors->first('address')}}</pre> @endif--}}
                            {{--</div>--}}

                            <div class="col-md-12" style="margin-top: 15px;">
                                <button type="submit" class="btn btn-lg btn-primary"><i class="fa fa-save"></i> {{__('Update')}}</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {{Form::close()}}
        <div class="row">

            <div class="col-md-12 mt-5">
                <div class="card">
                    <div class="card-body">
                        <div class="clearfix"></div>
                        {{ Form::open(array('route' => 'changePassword','files'=>true,'id'=>'myForm')) }}
                        <div class="row">
                            <div class="col-sm-12 mb-3"><h3>{{__('Change Password')}}</h3></div>
                            <div class="col-sm-4 mt-4">
                                <label>{{__('Old Password')}}</label>
                                <input type="password" name="old_password" autocomplete="off" class="form-control" value="">
                                @if($errors->first('old_password')) <pre class="text-danger">{{$errors->first('old_password')}}</pre> @endif
                            </div>
                            <div class="col-sm-4 mt-4">
                                <label>{{__('New Password')}}</label>
                                <input type="password" name="password" autocomplete="off" class="form-control" value="">
                                @if($errors->first('password')) <pre class="text-danger">{{$errors->first('password')}}</pre> @endif
                            </div>
                            <div class="col-sm-4 mt-4">
                                <label>{{__('Confirm Password')}}</label>
                                <input type="password" name="password_confirmation" autocomplete="off" class="form-control" value="">
                                @if($errors->first('password_confirmation')) <pre class="text-danger">{{$errors->first('password_confirmation')}}</pre> @endif
                            </div>

                            <div class="col-md-12 mt-3">
                                <button type="submit" class="btn btn-lg btn-primary"><i class="fa fa-save"></i> {{__('Update')}}</button>
                            </div>
                        </div>
                        {{Form::close()}}
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('script')


@endsection
