{{ Form::open(array('route' => 'adminImageUploadSave','files'=>true,'id'=>'myForm')) }}
<div class="row mt-4">
    <div class="col-sm-4  col-6 service-wrapper">
        <label for="name">{{__('Logo')}}</label>
        <small>{{__('Please upload jpg or png file and size should be under 2mb')}}</small>
        <div id="file-upload" class="section">
            <div class="row section">
                <div class="col s12 m12 l12">
                    <input name="logo" type="file" id="input-file-now" class="dropify"
                           data-default-file="{{isset($all_settings['logo']) &&
                                                           !empty($all_settings['logo']) ? asset(getImagePath().$all_settings['logo']) : ''}}" />
                    @if ($errors->has('logo'))
                        <div class="text-danger">{{ $errors->first('logo') }}</div>
                    @endif
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-4 offset-2 col-6 service-wrapper">
        <label for="name">{{__('App Logo')}}</label>
        <small>{{__('Please upload jpg or png file and size should be under 2mb')}}</small>
        <div id="file-upload" class="section">
            <div class="row section">
                <div class="col s12 m12 l12">
                    <input name="app_logo" type="file" id="input-file-now" class="dropify"
                           data-default-file="{{isset($all_settings['app_logo']) &&
                                                           !empty($all_settings['app_logo']) ? asset(getImagePath().$all_settings['app_logo']) : ''}}" />
                    @if ($errors->has('app_logo'))
                        <div class="text-danger">{{ $errors->first('app_logo') }}</div>
                    @endif
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-4 col-6 service-wrapper mt-4">
        <label for="name">{{__('Favicon')}}</label>
        <small>{{__('Please upload jpg or png file and size should be under 2mb')}}</small>
        <div id="file-upload" class="section">
            <div class="row section">
                <div class="col s12 m12 l12">
                    <input name="favicon" type="file" id="input-file-now" class="dropify"
                           data-default-file="{{isset($all_settings['favicon']) &&
                                                           !empty($all_settings['favicon']) ? asset(getImagePath().$all_settings['favicon']) : ''}}" />
                    @if ($errors->has('favicon'))
                        <div class="text-danger">{{ $errors->first('favicon') }}</div>
                    @endif
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-4 offset-2 col-6 service-wrapper mt-4">
        <label for="name">{{__('Login Sidebar Image')}}</label>
        <small>{{__('Please upload jpg or png file and size should be under 2mb')}}</small>
        <div id="file-upload" class="section">
            <div class="row section">
                <div class="col s12 m12 l12">
                    <input name="login_side_image" type="file" id="input-file-now" class="dropify"
                           data-default-file="{{isset($all_settings['login_side_image']) &&
                                                           !empty($all_settings['login_side_image']) ? asset(getImagePath().$all_settings['login_side_image']) : ''}}" />
                    @if ($errors->has('login_side_image'))
                        <div class="text-danger">{{ $errors->first('login_side_image') }}</div>
                    @endif
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-4 col-6 service-wrapper mt-4">
        <label for="name">{{__('Login Logo / Frontend logo')}}</label>
        <small>{{__('Please upload jpg or png file and size should be under 2mb')}}</small>
        <div id="file-upload" class="section">
            <div class="row section">
                <div class="col s12 m12 l12">
                    <input name="login_logo" type="file" id="input-file-now" class="dropify"
                           data-default-file="{{isset($all_settings['login_logo']) &&
                                                           !empty($all_settings['login_logo']) ? asset(getImagePath().$all_settings['login_logo']) : ''}}" />
                    @if ($errors->has('login_logo'))
                        <div class="text-danger">{{ $errors->first('login_logo') }}</div>
                    @endif
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-12 mt-3">
        <button type="submit" class="btn btn-lg btn-primary"><i class="fa fa-save"></i> {{$button_title}}</button>
    </div>
</div>
{{Form::close()}}
