@extends('admin.layout.master',['menu'=>'settings'])
@section('title',$title)
@section('style')
@endsection
@section('content')
    @php($pclass = 'btn-success')
    @php($fclass = 'btn-success')
    @php($wclass = 'btn-success')
    @php($tclass = 'btn-success')
    @php($cclass = 'btn-success')
    @php($mclass = 'btn-success')
    @if($mode == 'general')
        @php($pclass = 'btn-primary')
    @elseif($mode == 'logo')
        @php($fclass = 'btn-primary')
    @elseif($mode == 'about_us')
        @php($wclass = 'btn-primary')
    @elseif($mode == 'payment_methods')
        @php($tclass = 'btn-primary')
    @elseif($mode == 'currency')
        @php($cclass = 'btn-primary')
    @elseif($mode == 'mobile_app')
        @php($mclass = 'btn-primary')
    @endif
    <div class="main-wrapper">
        <div class="pageheader pd-t-25 pd-b-35">
            <div class="d-flex justify-content-between">
                <div class="pd-t-5 pd-b-5">
                    <h1 class="pd-0 mg-0 tx-30 tx-dark"><i class="fa fa-support"></i> {{__('Settings')}}</h1>
                </div>
            </div>
        </div>
        <div class="btn-group" role="group" aria-label="Large button group">
            <a href="{{route('adminSettings', 'general')}}" class="btn {{$pclass ?? ''}} rounded-pill text-light">{{__('General')}}</a>&nbsp;&nbsp;
            <a href="{{route('adminSettings', 'logo')}}" class="btn {{$fclass ?? ''}} rounded-pill text-light">{{__('Logo')}}</a>&nbsp;&nbsp;
            <a href="{{route('adminSettings', 'about_us')}}" class="btn {{$wclass ?? ''}} rounded-pill text-light">{{__('About Us')}}</a>&nbsp;&nbsp;
            <a href="{{route('adminSettings', 'payment_methods')}}" class="btn {{$tclass ?? ''}} rounded-pill text-light">{{__('Payment Methods')}}</a>
            <a href="{{route('adminSettings', 'currency')}}" class="btn {{$cclass ?? ''}} rounded-pill text-light" style="margin-left: 5px;">{{__('Currency')}}</a>
            <a href="{{route('adminSettings', 'mobile_app')}}" class="btn {{$mclass ?? ''}} rounded-pill text-light" style="margin-left: 5px;">{{__('Mobile App')}}</a>
        </div>
        @includeWhen(($mode == 'general'),'admin.settings.general')
        @includeWhen(($mode == 'logo'),'admin.settings.logo')
        @includeWhen(($mode == 'about_us'),'admin.settings.about_us')
        @includeWhen(($mode == 'payment_methods'),'admin.settings.payment_methods')
        @includeWhen(($mode == 'currency'),'admin.settings.currency')
        @includeWhen(($mode == 'mobile_app'),'admin.settings.mobile_app')
    </div>
@endsection
@section('script')
    <script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.12/summernote.js"></script>
    <script>
        $(document).ready(function () {
            $('.summernote').summernote({
                tabsize: 6,
                height: 300
            });
        });
    </script>
@endsection

