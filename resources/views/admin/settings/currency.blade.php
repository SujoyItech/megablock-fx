{{ Form::open(array('route' => 'adminCurrencySettingsSave','files'=>true,'id'=>'myForm')) }}
<div class="row mt-4">
    <div class="col-sm-6 mb-3">
        <label>{{__('Currency Settings')}}</label>
        <select name="currency" id="" class="form-control">
            <option value="{{CURRENCY_USD}}" @if(isset($all_settings['currency']) ) {{isSelect($all_settings['currency'],CURRENCY_USD)}} @endif >{{__("USD")}}</option>
            <option value="{{CURRENCY_EURO}}" @if(isset($all_settings['currency'])) {{isSelect($all_settings['currency'],CURRENCY_EURO)}} @endif >{{__("EURO")}}</option>
        </select>
    </div>
    <div class="col-md-12 mt-3">
        <button type="submit" class="btn btn-lg btn-primary"><i class="fa fa-save"></i> {{$button_title}}</button>
    </div>
</div>
{{Form::close()}}

