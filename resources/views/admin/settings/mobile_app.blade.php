{{ Form::open(array('route' => 'adminMobileAppSettingsSave','files'=>true,'id'=>'myForm')) }}
<div class="row mt-4">
    <div class="col-sm-6 mb-3">
        <label>{{__('Paginate Per Page')}}</label>
        <input class="form-control" type="number" name="mobile_app_signal_per_page" value="{{isset($all_settings['mobile_app_signal_per_page']) ? $all_settings['mobile_app_signal_per_page']:old('mobile_app_signal_per_page')}}">
    </div>
    <div class="col-md-12 mt-3">
        <button type="submit" class="btn btn-lg btn-primary"><i class="fa fa-save"></i> {{$button_title}}</button>
    </div>
</div>
{{Form::close()}}
