@extends('admin.layout.master',['menu'=>'settings','sub-menu'=>'contact-support'])
@section('title',$title)
@section('style')
    <link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.12/summernote.css" rel="stylesheet">
@endsection
@section('content')
    @php($pclass = 'btn-success')
    @php($fclass = 'btn-success')
    @php($wclass = 'btn-success')
    @php($tclass = 'btn-success')
    @if($mode == 'platform')
        @php($pclass = 'btn-primary')
    @elseif($mode == 'faq')
        @php($fclass = 'btn-primary')
    @elseif($mode == 'work')
        @php($wclass = 'btn-primary')
    @elseif($mode == 'terms')
        @php($tclass = 'btn-primary')
    @endif
    <div class="main-wrapper">
        <div class="pageheader pd-t-25 pd-b-35">
            <div class="d-flex justify-content-between">
                <div class="pd-t-5 pd-b-5">
                    <h1 class="pd-0 mg-0 tx-30 tx-dark"><i class="fa fa-support"></i> {{__('Contact & Support')}}</h1>
                </div>
            </div>
        </div>
        <div class="btn-group" role="group" aria-label="Large button group">

            <a href="{{route('adminContactSupportSetting', 'platform')}}" class="btn {{$pclass ?? ''}} rounded-pill text-light">{{__('About the platform')}}</a>&nbsp;&nbsp;
            <a href="{{route('adminContactSupportSetting', 'work')}}" class="btn {{$wclass ?? ''}} rounded-pill text-light">{{__('How does it work?')}}</a>&nbsp;&nbsp;
            <a href="{{route('adminContactSupportSetting', 'terms')}}" class="btn {{$tclass ?? ''}} rounded-pill text-light">{{__('Terms & Conditions')}}</a>
        </div>
        @includeWhen(($mode == 'platform'),'admin.settings.contract_support.platform')
        @includeWhen(($mode == 'work'),'admin.settings.contract_support.work')
        @includeWhen(($mode == 'terms'),'admin.settings.contract_support.terms')
    </div>
@endsection
@section('script')
    <script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.12/summernote.js"></script>
    <script>
        $(document).ready(function () {
            $('.summernote').summernote({
                tabsize: 6,
                height: 300
            });
        });
    </script>
@endsection

