<div class="row mt-4">
    {{ Form::open(array('route' => 'adminContractSupportSettingsSave','files'=>true,'id'=>'myForm')) }}
    @csrf
    <input name="mode" type="hidden" value="{{$mode}}" />
    <div class="col-sm-12">
        <textarea class="summernote" id="summernote1" name="term_condition_text">{{isset($all_settings['term_condition_text']) ? $all_settings['term_condition_text'] : old('term_condition_text')}}</textarea>
    </div>
    <div class="col-md-12 mt-3">
        <button type="submit" class="btn btn-lg btn-primary"><i class="fa fa-save"></i> {{$button_title}}</button>
    </div>
    {{Form::close()}}
</div>

