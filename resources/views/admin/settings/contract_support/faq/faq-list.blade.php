@extends('admin.layout.master',['menu'=>'settings'])
@section('title',__('Faqs'))
@section('after-style')
@endsection
@section('content')
    <div class="main-wrapper">
        <div class="pageheader pd-t-25 pd-b-35">
            <div class="d-flex justify-content-between">
                <div class="pd-t-5 pd-b-5">
                    <h1 class="pd-0 mg-0 tx-30 tx-dark"><i class="fa fa-question-circle-o"></i> {{__('FAQs')}}</h1>
                </div>
                <div class="d-flex align-items-center hidden-xs">
                    <a href="{{route('faqAdd')}}" class="btn btn-primary"><i class="fa fa-plus"></i> {{__('Add')}}</a>
                </div>

            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <table class="table" id="table" width="100%">
                    <thead>
                    <tr>
                        <th class="all">{{__('Question')}}</th>
                        <th class="all">{{__('Answer')}}</th>
                        <th class="all" width="30">{{__('Actions')}}</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script>
        $('#table').DataTable({
            processing: true,
            serverSide: true,
            pageLength: 10,
            responsive: true,
            ajax: '{{route('faqList')}}',
            order: [2, 'desc'],
            autoWidth:false,
            columns: [
                {"data": "title"},
                {"data": "description"},
                {"data": "actions",orderable: false, searchable: false}
            ]
        });
    </script>
@endsection
