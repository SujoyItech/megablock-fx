{{ Form::open(array('route' => 'adminAboutSettingsSave','files'=>true,'id'=>'')) }}
<div class="row mt-4">
    <div class="col-sm-8 mb-3">
        <label>{{__('Title')}}</label>
        <input type="text" name="about_title" autocomplete="off" class="form-control mb-1" @if(isset($all_settings['about_title'])) value="{{$all_settings['about_title']}}" @endif >
        @if($errors->first('about_title')) <span class="text-danger">{{$errors->first('about_title')}}</span> @endif

        <label>{{__('Description')}}</label>
        <textarea name="about_description" class="form-control mb-1" rows="4" cols="10">@if(isset($all_settings['about_description'])){!! $all_settings['about_description'] !!}@endif</textarea>
        @if($errors->first('about_description')) <span class="text-danger">{{$errors->first('about_description')}}</span> @endif
    </div>
    <div class="col-sm-4">
        <label for="name">{{__('Image')}}</label>
        <div id="file-upload" class="section">
            <div class="row section">
                <div class="col s12 m12 l12">
                    <input name="about_image" type="file" id="input-file-now" class="dropify"
                           data-default-file="{{isset($all_settings['about_image']) &&
                                                           !empty($all_settings['about_image']) ? asset(getImagePath().$all_settings['about_image']) : ''}}" />
                    @if ($errors->has('about_image'))
                        <div class="error">{{ $errors->first('about_image') }}</div>
                    @endif
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-12 mt-2">
        <button type="submit" class="btn btn-lg btn-primary"><i class="fa fa-save"></i> {{$button_title}}</button>
    </div>
</div>
{{Form::close()}}

