{{ Form::open(array('route' => 'adminPaymentSettingsSave','files'=>true,'id'=>'myForm')) }}
<div class="row mt-4">
    <div class="col-sm-6 mb-3">
        <label>{{__('Braintree Merchant Id')}}</label>
        <input type="text" name="braintree_marchant_id" autocomplete="off" class="form-control mb-1"
               @if(isset($all_settings['braintree_marchant_id'])) value="{{$all_settings['braintree_marchant_id']}}" @endif >
        @if($errors->first('braintree_marchant_id')) <span class="text-danger">{{$errors->first('braintree_marchant_id')}}</span> @endif
    </div>
    <div class="col-sm-6 mb-3">
        <label>{{__('Braintree Public Key')}}</label>
        <input type="text" name="braintree_public_key" autocomplete="off" class="form-control mb-1"
               @if(isset($all_settings['braintree_public_key'])) value="{{$all_settings['braintree_public_key']}}" @endif >
        @if($errors->first('braintree_marchant_id')) <span class="text-danger">{{$errors->first('braintree_public_key')}}</span> @endif
    </div>
    <div class="col-sm-6 mb-3">
        <label>{{__('Braintree Private Key')}}</label>
        <input type="text" name="braintree_private_key" autocomplete="off" class="form-control mb-1"
               @if(isset($all_settings['braintree_private_key'])) value="{{$all_settings['braintree_private_key']}}" @endif >
        @if($errors->first('braintree_private_key')) <span class="text-danger">{{$errors->first('braintree_private_key')}}</span> @endif
    </div>
    <div class="col-sm-6 mb-3">
        <label>{{__('Braintree Client Token')}}</label>
        <input type="text" name="braintree_client_token" autocomplete="off" class="form-control mb-1"
               @if(isset($all_settings['braintree_client_token'])) value="{{$all_settings['braintree_client_token']}}" @endif >
        @if($errors->first('braintree_client_token')) <span class="text-danger">{{$errors->first('braintree_client_token')}}</span> @endif
    </div>
    <div class="col-sm-6 mb-3">
        <label>{{__('Google Pay Merchant Id')}}</label>
        <input type="text" name="google_pay_marchent_id" autocomplete="off" class="form-control mb-1"
               @if(isset($all_settings['google_pay_marchent_id'])) value="{{$all_settings['google_pay_marchent_id']}}" @endif >
        @if($errors->first('braintree_client_token')) <span class="text-danger">{{$errors->first('google_pay_marchent_id')}}</span> @endif
    </div>
    <div class="col-sm-6 mb-3">
        <label>{{__('Braintree Mode')}}</label>
        <select name="braintree_env" id="" class="form-control mb-1">
            <option value="sandbox" @if(isset($all_settings['braintree_env']) && ($all_settings['braintree_env'] == 'sandbox'))
            selected  @endif >{{__("Sandbox")}}</option>
            <option value="production" @if(isset($all_settings['braintree_env']) && ($all_settings['braintree_env'] == 'production'))
            selected  @endif >{{__("Production")}}</option>
        </select>
        @if($errors->first('braintree_env')) <span class="text-danger">{{$errors->first('braintree_env')}}</span> @endif
    </div>
    <div class="col-md-12 mt-3">
        <button type="submit" class="btn btn-lg btn-primary"><i class="fa fa-save"></i> {{$button_title}}</button>
    </div>
</div>
{{Form::close()}}

