<form method="post" enctype="multipart/form-data" action="{{route('adminBasicSettingsSave')}}">
<div class="row mt-4">
    @csrf
    <input name="mode" type="hidden" value="{{$mode}}" />
    <div class="col-sm-6 mb-3">
        <label>{{__('App Title')}}</label>
        <input type="text" name="app_title" autocomplete="off" class="form-control mb-1" @if(isset($all_settings['app_title'])) value="{{$all_settings['app_title']}}" @endif >
        @if($errors->first('app_title')) <span class="text-danger">{{$errors->first('app_title')}}</span> @endif
    </div>
    <div class="col-sm-6 mb-3">
        <label>{{__('Copyright Text')}}</label>
        <input type="text" name="copyright_text" autocomplete="off" class="form-control mb-1" @if(isset($all_settings['copyright_text'])) value="{{$all_settings['copyright_text']}}" @endif >
        @if($errors->first('copyright_text')) <span class="text-danger">{{$errors->first('copyright_text')}}</span> @endif
    </div>
    <div class="col-sm-6 mb-3">
        <label>{{__('Address')}}</label>
        <input type="text" name="address" autocomplete="off" class="form-control mb-1" @if(isset($all_settings['address'])) value="{{$all_settings['address']}}" @endif >
        @if($errors->first('address')) <span class="text-danger">{{$errors->first('address')}}</span> @endif
    </div>
    <div class="col-sm-6 mb-3">
        <label>{{__('Primary Email')}}</label>
        <input type="email" name="primary_email" autocomplete="off" class="form-control mb-1" @if(isset($all_settings['primary_email'])) value="{{$all_settings['primary_email']}}" @endif >
        @if($errors->first('primary_email')) <span class="text-danger">{{$errors->first('primary_email')}}</span> @endif
    </div>
    <div class="col-sm-6 mb-3">
        <label>{{__('Phone')}}</label>
        <input type="text" name="phone" autocomplete="off" class="form-control mb-1" @if(isset($all_settings['phone'])) value="{{$all_settings['phone']}}" @endif >
        @if($errors->first('phone')) <span class="text-danger">{{$errors->first('phone')}}</span> @endif
    </div>
    <div class="col-sm-6 mb-3">
        <label>{{__('Login Text')}}</label>
        <input type="text" name="login_text" autocomplete="off" class="form-control mb-1" @if(isset($all_settings['login_text'])) value="{{$all_settings['login_text']}}" @endif >
        @if($errors->first('login_text')) <span class="text-danger">{{$errors->first('login_text')}}</span> @endif
    </div>
    <div class="col-sm-6 mb-3">
        <label>{{__('Language')}}</label>
        <select name="lang" class="form-control mb-1">
            @foreach(language() as $val)
                <option @if(isset($all_settings['lang']) && $all_settings['lang']==$val) selected @endif value="{{$val}}">{{langName($val)}}</option>
            @endforeach
        </select>
        @if($errors->first('lang')) <span class="text-danger">{{$errors->first('lang')}}</span> @endif
    </div>
    <div class="col-sm-6 mb-3">
        <label>{{__('Helpline')}}</label>
        <input type="text" name="helpline" autocomplete="off" class="form-control mb-1" @if(isset($all_settings['helpline'])) value="{{$all_settings['helpline']}}" @endif >
        @if($errors->first('helpline')) <span class="text-danger">{{$errors->first('helpline')}}</span> @endif
    </div>
    <div class="col-md-12">
        <button type="submit" class="btn btn-lg btn-primary"><i class="fa fa-save"></i> {{$button_title}}</button>
    </div>
</div>
</form>
