@extends('admin.layout.master',['menu'=>'settings'])
@section('title',$title)
@section('style')
@endsection
@section('content')
    <div class="main-wrapper">
        <div class="pageheader pd-t-25 pd-b-35">
            <div class="d-flex justify-content-between">
                <div class="pd-t-5 pd-b-5">
                    <h1 class="pd-0 mg-0 tx-30 tx-dark"><i class="fa fa-superpowers"></i> {{__('Referral Bonus')}}</h1>
                </div>
            </div>
        </div>
        {{ Form::open(array('route' => 'referralBonusSave')) }}
        <div class="row">
            <div class="col-sm-6 mb-3">
                <label>{{__('On Register & Purchase')}}</label>
                <input type="number" name="on_register_and_purchase" autocomplete="off" class="form-control mb-1" @if(isset($all_settings['on_register_and_purchase'])) value="{{$all_settings['on_register_and_purchase']}}"@else value="{{old('on_register_and_purchase')}}" @endif >
                @if($errors->first('on_register_and_purchase')) <span class="text-danger">{{$errors->first('on_register_and_purchase')}}</span> @endif
            </div>
            <div class="col-sm-6 mb-3">
                <label>{{__('Register Through Affiliate')}}</label>
                <input type="number" name="registers_through_affiliate" autocomplete="off" class="form-control mb-1" @if(isset($all_settings['registers_through_affiliate'])) value="{{$all_settings['registers_through_affiliate']}}"@else value="{{old('registers_through_affiliate')}}" @endif >
                @if($errors->first('registers_through_affiliate')) <span class="text-danger">{{$errors->first('registers_through_affiliate')}}</span> @endif
            </div>
            {{--<div class="col-sm-6 mb-3">--}}
            {{--<label>{{__('Standard Registration')}}</label>--}}
            {{--<input type="number" name="standard_registration" autocomplete="off" class="form-control mb-1" @if(isset($item)) value="{{$item->standard_registration}}"@else value="{{old('standard_registration')}}" @endif >--}}
            {{--@if() <span class="text-danger">{{$errors->first('standard_registration')}}</span> @endif--}}
            {{--</div>--}}

            <div class="col-sm-6 mb-3">
                <label>{{__('Affiliate Owner')}}</label>
                <input type="number" name="affiliate_owner" autocomplete="off" class="form-control mb-1" @if(isset($all_settings['affiliate_owner'])) value="{{$all_settings['affiliate_owner']}}"@else value="{{old('affiliate_owner')}}" @endif >
                @if($errors->first('affiliate_owner')) <span class="text-danger">{{$errors->first('affiliate_owner')}}</span> @endif
            </div>
            <div class="col-sm-6 mb-3">
                <label>{{__('Standard Registration')}}</label>
                <input type="number" name="standard_registration" autocomplete="off" class="form-control mb-1" @if(isset($all_settings['standard_registration'])) value="{{$all_settings['standard_registration']}}" @else value="{{old('standard_registration')}}" @endif >
                @if($errors->first('standard_registration')) <span class="text-danger">{{$errors->first('standard_registration')}}</span> @endif
            </div>
            <div class="col-2" style="margin-top: 15px;">
                @if(isset($item)) <input type="hidden" name="edit_id" value="{{encrypt($item->id)}}"> @endif
                    <button type="submit" class="btn btn-lg btn-primary"><i class="fa fa-save"></i> {{$button_title}}</button>
            </div>
        </div>

    </div>
    {{Form::close()}}
{{--    @include('admin.layout.page-title',['title'=>$title])--}}
@endsection
@section('script')

@endsection
