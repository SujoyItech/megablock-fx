@extends('admin.layout.master',['menu'=>'signal'])
@section('title',__('Signal'))
@section('after-style')
@endsection
@section('content')
    <div class="main-wrapper">
        <div class="pageheader pd-t-25 pd-b-35">
            <div class="d-flex justify-content-between">
                <div class="pd-t-5 pd-b-5">
                    <h2 class="ac-header"><img src="{{asset('user/')}}/images/icon-mega/h2.svg"alt="" width="40"> {{__('Signal')}}
                    </h2>
                </div>
                <div class="d-flex align-items-center hidden-xs">
                    <a href="{{route('signalAdd')}}" class="btn btn-primary"><i class="fa fa-plus"></i> {{__('Add')}}</a>
                </div>

            </div>
        </div>
        <div class="row">
            <div class="col-md-12 col-lg-12">
                <div class="card mg-b-30">
                    <div class="card-body pd-0">
                        <table id="basicDataTable" class="table responsive nowrap">
                            <thead>
                                <tr>
                                    <th class="all">{{__('FI Code')}}</th>
                                    <th class="all">{{__('Trade Type')}}</th>
                                    <th class="all">{{__('Order Type')}}</th>
                                    <th class="all">{{__('Action')}}</th>
                                    <th class="all">{{__('Price')}}</th>
                                    <th class="all">{{__('T/P')}}</th>
                                    <th class="all">{{__('S/L')}}</th>
                                    <th class="all">{{__('Is Free')}}</th>
                                    <th class="all">{{__('Result')}}</th>
                                    <th class="all">{{__('Status')}}</th>
                                    <th class="all">{{__('Actions')}}</th>
                                </tr>
                             </thead>
                        </table>
                    </div>
               </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script>
        $('#basicDataTable').DataTable({
            processing: true,
            serverSide: true,
            pageLength: 10,
            responsive: false,
            ajax: '{{route('signalList')}}',
            order: [1, 'asc'],
            autoWidth:false,
            columns: [
                {"data": "financial_instrument_code"},
                {"data": "trade_type_title"},
                {"data": "order_type_title"},
                {"data": "action"},
                {"data": "entry_price"},
                {"data": "take_profit_1"},
                {"data": "stop_loss_1"},
                {"data": "available_for_free"},
                {"data": "signal_result"},
                {"data": "status"},
                {"data": "actions",orderable: false, searchable: false}
            ]
        });
    </script>
@endsection
