@extends('admin.layout.master',['menu'=>'signal'])
@section('title',$title)
@section('style')
@endsection
@section('content')
    @php
    $all_settings = allSetting();
    @endphp
    <div class="main-wrapper">
        <div class="pageheader pd-t-25 pd-b-35">
            <div class="d-flex justify-content-between">
                <div class="clearfix">
                    <div class="pd-t-5 pd-b-5">
                        <h2 class="ac-header"><img src="{{asset('user/')}}/images/icon-mega/h2.svg"alt="" width="40"> {{__('Signal')}}
                        </h2>
                    </div>
                </div>

            </div>
        </div>
        {{ Form::open(array('route' => 'signalSave','method'=>'post')) }}
        @csrf
        <div class="row">
            <div class="col-3 mb-3">
                <label>{{__('Financial Instrument Code')}}</label>
                <select class="select2-demo form-control" name="financial_instrument_code" data-live-search="true">
                    @foreach($fics as $d)
                        <option
                            @if(isset($item)) {{isSelect($d->id,$item->financial_instrument_code)}} @endif value="{{$d->id}}">{{strtoupper($d->title)}}</option>
                    @endforeach
                </select>
                @if(isset($error))
                    <span class="text-danger">{{$errors->first('financial_instrument_code')}}</span>
                @endif

            </div>
            <div class="col-3 mb-3">
                <label>{{__('Trade Type')}}</label>
                <select class="select2-demo form-control" name="trade_type" data-live-search="true">
                    @foreach($tt as $d)
                        <option
                            @if(isset($item)) {{isSelect($d->id,$item->trade_type)}} @endif value="{{$d->id}}">{{$d->title}}</option>
                    @endforeach
                </select>
                @if(isset($error))
                    <span class="text-danger">{{$errors->first('trade_type')}}</span>
                @endif
            </div>
            <div class="col-3 mb-3">
                <label>{{__('Order Type')}}</label>
                <select class="select2-demo form-control" name="order_type" data-live-search="true">
                    @foreach($ot as $d)
                        <option
                            @if(isset($item)) {{isSelect($d->id,$item->order_type)}} @endif value="{{$d->id}}">{{$d->title}}</option>
                    @endforeach
                </select>
                @if(isset($error))
                    <span class="text-danger">{{$errors->first('order_type')}}</span>
                @endif
            </div>
            <div class="col-3 mb-3">
                <label>{{__('Action')}}</label>
                <select class="select2-demo form-control" name="action" data-live-search="true">
                    <option @if(isset($item)) {{isSelect(SIGNAL_ACTION_BUY,$item->action)}} @endif value="buy">{{__('Buy')}}</option>
                    <option @if(isset($item)) {{isSelect(SIGNAL_ACTION_SELL,$item->action)}} @endif value="sell">{{__('Sell')}}</option>
                </select>
                @if($errors->first('action'))
                    <span class="text-danger">{{$errors->first('action')}}</span>
                @endif
            </div>
            <div class="col-3 mb-3">
                <label>{{__('Take Profit 1')}}</label>
                <input type="number" name="take_profit_1" step="any" autocomplete="off" class="form-control mb-1"
                       @if(isset($item)) value="{{$item->take_profit_1}}"
                       @else value="{{old('take_profit_1')}}" @endif >
                @if($errors->first('take_profit_1'))
                    <span class="text-danger">{{$errors->first('take_profit_1')}}</span>
                @endif
            </div>
            <div class="col-3 mb-3">
                <label>{{__('Take Profit 2')}}</label>
                <input type="number" name="take_profit_2" step="any" autocomplete="off" class="form-control mb-1"
                       @if(isset($item)) value="{{$item->take_profit_2}}"
                       @else value="{{old('take_profit_2')}}" @endif >
                @if($errors->first('take_profit_2'))
                    <span class="text-danger">{{$errors->first('take_profit_2')}}</span>
                @endif
            </div>
            <div class="col-3 mb-3">
                <label>{{__('Take Profit 3')}}</label>
                <input type="number" name="take_profit_3" step="any" autocomplete="off" class="form-control mb-1"
                       @if(isset($item)) value="{{$item->take_profit_3}}"
                       @else value="{{old('take_profit_3')}}" @endif >
                @if($errors->first('take_profit_3'))
                    <span class="text-danger">{{$errors->first('take_profit_3')}}</span>
                @endif
            </div>
            <div class="col-3 mb-3">
                <label>{{__('Take Profit 4')}}</label>
                <input type="number" name="take_profit_4" step="any" autocomplete="off" class="form-control mb-1"
                       @if(isset($item)) value="{{$item->take_profit_4}}"
                       @else value="{{old('take_profit_4')}}" @endif >
                @if($errors->first('take_profit_4'))
                    <span class="text-danger">{{$errors->first('take_profit_4')}}</span>
                @endif
            </div>
            <div class="col-3 mb-3">
                <label>{{__('Stop Loss 1')}}</label>
                <input type="number" name="stop_loss_1" step="any" autocomplete="off" class="form-control mb-1"
                       @if(isset($item)) value="{{$item->stop_loss_1}}" @else value="{{old('stop_loss_1')}}" @endif >
                @if($errors->first('stop_loss_1'))
                    <span class="text-danger">{{$errors->first('stop_loss_1')}}</span>
                @endif
            </div>
            <div class="col-3 mb-3">
                <label>{{__('Stop Loss 2')}}</label>
                <input type="number" name="stop_loss_2" step="any" autocomplete="off" class="form-control mb-1"
                       @if(isset($item)) value="{{$item->stop_loss_2}}" @else value="{{old('stop_loss_2')}}" @endif >
                @if($errors->first('stop_loss_2'))
                    <span class="text-danger">{{$errors->first('stop_loss_2')}}</span>
                @endif
            </div>
            <div class="col-3 mb-3">
                <label>{{__('Stop Loss 3')}}</label>
                <input type="number" name="stop_loss_3" step="any" autocomplete="off" class="form-control mb-1"
                       @if(isset($item)) value="{{$item->stop_loss_3}}" @else value="{{old('stop_loss_3')}}" @endif >
                @if($errors->first('stop_loss_3'))
                    <span class="text-danger">{{$errors->first('stop_loss_3')}}</span>
                @endif
            </div>
            <div class="col-3 mb-3">
                <label>{{__('Stop Loss 4')}}</label>
                <input type="number" name="stop_loss_4" step="any" autocomplete="off" class="form-control mb-1"
                       @if(isset($item)) value="{{$item->stop_loss_4}}" @else value="{{old('stop_loss_4')}}" @endif >
                @if($errors->first('stop_loss_4'))
                    <span class="text-danger">{{$errors->first('stop_loss_4')}}</span>
                @endif
            </div>
            <div class="col-4 mb-3">
                <label>{{__('Entry Price')}} (<small><i class="fa {{isset($all_settings['currency']) && ($all_settings['currency'] == CURRENCY_EURO) ?'fa-eur':'fa-usd'}}"></i></small>)</label>
                <input type="number" step="any" name="entry_price"  autocomplete="off" class="form-control mb-1"
                       @if(isset($item)) value="{{$item->entry_price}}" @else value="{{old('entry_price')}}" @endif >
                @if($errors->first('entry_price'))
                    <span class="text-danger">{{$errors->first('entry_price')}}</span>
                @endif
            </div>
            <div class="col-4 mb-3">
                <label>{{__('Status')}}</label>
                <select class="select2-demo form-control" name="status" data-live-search="true" id="selectCategory">
                    <option @if(isset($item)) {{isSelect(SIGNAL_ACTIVE, $item->status)}} @endif value="{{SIGNAL_ACTIVE}}">{{__('Active')}}</option>
                    <option @if(isset($item)) {{isSelect(SIGNAL_PENDING, $item->status)}} @endif value="{{SIGNAL_PENDING}}">{{__('Pending')}}</option>
                    <option @if(isset($item)) {{isSelect(SIGNAL_CLOSED, $item->status)}} @endif value="{{SIGNAL_CLOSED}}">{{__('Closed')}}</option>
                </select>
            </div>
            <div class="col-4 mb-3" id="show-result-div" style="display:none">
                <label>{{__('Signal Result')}}</label>
                <input type="number" name="signal_result"
                    autocomplete="off" class="form-control mb-1"
                    @if(isset($item)) value="{{$item->signal_result}}"
                    @else value="{{old('signal_result')}}" @endif >
                @if($errors->first('signal_result'))
                    <span class="text-danger">{{$errors->first('signal_result')}}</span>
                @endif
            </div>
            <div class="row"></div>
            <div class="col-12 mb-1">
                <label>{{__('Comment')}}</label>
                <textarea rows="4" class="form-control mb-1" name="comments" placeholder="Comment">{{ isset($item) ? $item->comments : old('comments') }}</textarea>
            </div>
            @if($errors->first('comments'))
                <span class="text-danger">{{$errors->first('comments')}}</span>
            @endif
            <div class="col-12 mb-3">
                <input type="checkbox" class="custom-control-input" id="customCheck1" name="available_for_free"
                       @if(isset($item->available_for_free) && $item->available_for_free ==1 ) checked value="1"
                       @else value="0" @endif>
                <label class="custom-control-label" for="customCheck1">{{__('Available For Free')}}</label>
            </div>
            <div class="col-2">
                @if(isset($item)) <input type="hidden" name="edit_id" value="{{encrypt($item->id)}}"> @endif
                <button type="submit" class="btn btn-lg btn-primary"><i class="fa fa-save"></i> {{$button_title}}</button>
            </div>
        </div>
        {{Form::close()}}
    </div>
@endsection
@section('script')
    <script>
        $(document).ready(function () {
            $("#selectCategory").val() == 3 ? $('#show-result-div').show() : $('#show-result-div').hide();

            var ckbox = $('#customCheck1');
            $('#customCheck1').on('click', function () {
                if (ckbox.is(':checked')) {
                    $("#customCheck1").val(1);
                } else {
                    $("#customCheck1").val(0);
                }
            });
        });
        $('#selectCategory').on('change', function () {
            $("#selectCategory").val() == 3 ? $('#show-result-div').show() : $('#show-result-div').hide();
        })
    </script>
@endsection


{{--Licences (from list. Multi selection)--}}
{{--Markets (multi selection list of countries)--}}
{{--Average spreads (text)--}}
{{--Recommended deposit (text)--}}
{{--URL--}}
