@extends('admin.layout.master',['menu'=>'category'])
@section('title',$title)
@section('style')
@endsection
@section('content')
    <div class="main-wrapper">
        <div class="pageheader pd-t-25 pd-b-35">
            <div class="d-flex justify-content-between">
                <div class="pd-t-5 pd-b-5">
                    <h1 class="pd-0 mg-0 tx-30 tx-dark"><i><img src="{{asset('assets/images/icon/1 24 px.svg')}}" width="30"></i> {{__('Order Type')}}</h1>
                </div>

            </div>
        </div>
        {{ Form::open(array('route' => 'orderTypeSave')) }}
        <div class="row">
            <div class="col-sm-6">
                <label>{{__('Title')}}</label>
                <input type="text" name="title" autocomplete="off" class="form-control mb-1" @if(isset($item)) value="{{$item->title}}"@else value="{{old('title')}}" @endif >
                @if($errors->first('title')) <span class="text-danger">{{$errors->first('title')}}</span> @endif
            </div>
            <div class="col-sm-6"></div>
            <div class="col-2" style="margin-top: 15px;">
                @if(isset($item)) <input type="hidden" name="edit_id" value="{{encrypt($item->id)}}"> @endif
                <button type="submit" class="btn btn-lg btn-primary"><i class="fa fa-save"></i> {{$button_title}}</button>
            </div>
        </div>
        {{Form::close()}}
    </div>
{{--    @include('admin.layout.page-title',['title'=>$title])--}}
@endsection
@section('script')

@endsection
