
<!-- Datepicket CSS -->
<link type="text/css" rel="stylesheet" href="{{asset('assets/plugins/daterangepicker/daterangepicker.css')}}"/>
<!-- cryptofont CSS -->
<link type="text/css" rel="stylesheet" href="{{asset('assets/plugins/cryptofont/css/cryptofont.min.css')}}">
<!-- Apex Chart CSS -->
<link type="text/css" rel="stylesheet" href="{{asset('assets/plugins/apex-chart/apexcharts.css')}}">
<!-- Main CSS -->
<link rel="stylesheet" href="{{asset('assets/plugins/smartWizard/css/smart_wizard.min.css')}}">
<link type="text/css" rel="stylesheet" href="{{asset('assets/css/style.css')}}"/>
<link type="text/css" rel="stylesheet" href="{{asset('assets/css/typography.css')}}"/>
<!-- Favicon -->
<link rel="icon" href="{{asset('assets/images/favicon.ico" type="image/x-icon')}}">
<link rel="stylesheet" type="text/css" href="{{asset('assets/plugins/dropify/css/dropify.min.css')}}">
<link href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.12/summernote-bs4.css" rel="stylesheet">

<link type="text/css" rel="stylesheet" href="{{asset('assets/plugins/dataTable/datatables.min.css')}}">
<link type="text/css" rel="stylesheet" href="{{asset('assets/plugins/dataTable/extensions/dataTables.jqueryui.min.css')}}">
<link type="text/css" rel="stylesheet" href="{{asset('assets/datepicker/datepicker3.css')}}">
<link href="{{asset('assets/toaster')}}/toastr.css" rel="stylesheet">
@yield('style')
