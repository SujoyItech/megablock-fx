@php($segment1 = Request::segment(1))
<div class="page-sidebar-inner">
    <div class="page-sidebar-menu">
        <ul class="accordion-menu">
            {{--<li class="mg-l-20-force mg-t-25-force menu-navigation">Navigation</li>--}}
            <li @if(!empty($menu) && ($menu == 'dashboard')) class="open" @endif>
                <a href="{{url('/admin')}}"><i class="icon">
                        <img class="normal" src="{{asset('user/')}}/images/icon-mega/1.svg"alt="" width="15">
                    </i> <span>  {{__('Dashboard')}}</span></a>
            </li>
            <li @if(!empty($menu) && ($menu == 'user')) class="open" @endif>
                <a href="{{url('/')}}"><i class="fa fa-users"></i> <span>  {{__('User Management')}}</span><i class="accordion-icon fa fa-angle-left"></i></a>
                <ul class="sub-menu" style="display: block;">
                    <li class="{{ $segment1 == 'user-add' ? 'active': '' }}"><a href="{{url('user-add')}}"><i class="fa fa-users"></i> {{__('Add User')}}</a></li>
                    <li class="{{ $segment1 == 'users-list' ? 'active': '' }}"><a href="{{url('users-list')}}"><i class="fa fa-users"></i> {{__('User List')}}</a></li>
{{--                    <li class="{{ $segment1 == 'roles' ? 'active': '' }}"><a href="{{url('roles')}}"><i class="fa fa-unlock-alt"></i> {{__('Role')}}</a></li>--}}
                </ul>
            </li>
            <li @if(!empty($menu) && ($menu == 'category')) class="open" @endif>
                <a href="{{url('/')}}"> <i class="icon">
                        <img class="normal" src="{{asset('user/')}}/images/icon-mega/2.svg"alt="" width="15">
                    </i>
                    <span>  {{__('Signal Category')}}</span><i class="accordion-icon fa fa-angle-left"></i></a>
                <ul class="sub-menu" style="display: block;">
                    <li class="{{ $segment1 == 'financial-instrument-code-add' ? 'active': '' }}"><a href="{{url('financial-instrument-code-add')}}"><i class="fa fa-dollar"></i> {{__('Add Financial Instrument Code')}}</a></li>
                    <li class="{{ $segment1 == 'financial-instrument-code' ? 'active': '' }}"><a href="{{url('financial-instrument-code')}}"><i class="fa fa-dollar"></i> {{__('Financial Instrument Code List')}}</a></li>
                    <li class="{{ $segment1 == 'trade-type-add' ? 'active': '' }}"><a href="{{url('trade-type-add')}}"><i><img src="{{asset('assets/images/icon/2_white_ 24_px.svg')}}" width="10"></i>  {{__('Add Trade Type')}}</a></li>
                    <li class="{{ $segment1 == 'trade-type' ? 'active': '' }}"><a href="{{url('trade-type')}}"><i><img src="{{asset('assets/images/icon/2_white_ 24_px.svg')}}" width="10"></i>  {{__('Trade Type List')}}</a></li>
                    <li class="{{ $segment1 == 'order-type-add' ? 'active': '' }}"><a href="{{url('order-type-add')}}"><i><img src="{{asset('assets/images/icon/1 white 24 px.svg')}}" width="10"></i> {{__('Add Order Type')}}</a></li>
                    <li class="{{ $segment1 == 'order-type' ? 'active': '' }}"><a href="{{url('order-type')}}"><i><img src="{{asset('assets/images/icon/1 white 24 px.svg')}}" width="10"></i> {{__('Order Type List')}}</a></li>
                </ul>
            </li>
            <li @if(!empty($menu) && ($menu == 'signal')) class="open" @endif>
                <a href="{{url('/')}}"> <i class="icon">
                        <img class="normal" src="{{asset('user/')}}/images/icon-mega/2.svg"alt="" width="15">
                    </i>
                    <span>  {{__('Signal')}}</span><i class="accordion-icon fa fa-angle-left"></i></a>
                <ul class="sub-menu" style="display: block;">
                    <li class="{{ $segment1 == 'signal-add' ? 'active': '' }}"><a href="{{url('signal-add')}}">
                            <i class="icon">
                                <img class="normal" src="{{asset('user/')}}/images/icon-mega/2.svg"alt="" width="15">
                            </i> {{__('Add Signal')}}</a>
                    </li>
                    <li class="{{ $segment1 == 'signal' ? 'active': '' }}"><a href="{{url('signal')}}"><i class="icon">
                                <img class="normal" src="{{asset('user/')}}/images/icon-mega/2.svg"alt="" width="15">
                            </i> {{__('Signal List')}}</a></li>
                </ul>
            </li>
            <li @if(!empty($menu) && ($menu == 'broker')) class="open" @endif>
                <a href="{{url('/')}}"><i class="fa fa-handshake-o"></i>
                    <span>  {{__('Broker')}}</span><i class="accordion-icon fa fa-angle-left"></i></a>
                <ul class="sub-menu" style="display: block;">
                    <li class="{{ $segment1 == 'licence-add' ? 'active': '' }}"><a href="{{url('licence-add')}}"><i class="fa fa-handshake-o"></i> {{__('Add Licence')}}</a></li>
                    <li class="{{ $segment1 == 'licence' ? 'active': '' }}"><a href="{{url('licence')}}"><i class="fa fa-handshake-o"></i> {{__('Licence List')}}</a></li>
                    <li class="{{ $segment1 == 'broker-add' ? 'active': '' }}"><a href="{{url('broker-add')}}"><i class="fa fa-handshake-o"></i> {{__('Add Broker')}}</a></li>
                    <li class="{{ $segment1 == 'broker' ? 'active': '' }}"><a href="{{url('broker')}}"><i class="fa fa-handshake-o"></i> {{__('Broker List')}}</a></li>
                </ul>
            </li>
            <li @if(!empty($menu) && ($menu == 'subscription')) class="open" @endif>
                <a href="{{url('/')}}"><i class="icon">
                        <img class="normal" src="{{asset('user/')}}/images/icon-mega/4.svg"alt="" width="15">
                    </i>
                    <span>  {{__('Subscription Package')}}</span><i class="accordion-icon fa fa-angle-left"></i></a>
                <ul class="sub-menu" style="display: block;">
                    <li class="{{ $segment1 == 'subscription-package-add' ? 'active': '' }}"><a href="{{url('subscription-package-add')}}"><i class="icon">
                                <img class="normal" src="{{asset('user/')}}/images/icon-mega/4.svg"alt="" width="15">
                            </i> {{__('Add Package')}}</a></li>
                    <li class="{{ $segment1 == 'subscription-package' ? 'active': '' }}"><a href="{{url('subscription-package')}}"><i class="icon">
                                <img class="normal" src="{{asset('user/')}}/images/icon-mega/4.svg"alt="" width="15">
                            </i> {{__('Package List')}}</a></li>
                    <li class="{{ $segment1 == 'subscription-package-histories' ? 'active': '' }}"><a href="{{url('subscription-package-histories')}}"><i class="fa fa-list"></i> {{__('Payment Histories')}}</a></li>
                </ul>
            </li>
            <li @if(!empty($menu) && ($menu == 'settings')) class="open" @endif>
                <a href="{{url('/')}}"><i class="fa fa-gears"></i>
                    <span>  {{__('Settings')}}</span><i class="accordion-icon fa fa-angle-left"></i></a>
                <ul class="sub-menu" style="display: block;">
                    <li class="{{ $segment1 == 'settings' ? 'active': '' }}"><a href="{{url('settings')}}"><i class="fa fa-gears"></i> {{__('App Settings')}}</a></li>
                    <li class="{{ $segment1 == 'contact-support-setting' ? 'active': '' }}"><a href="{{url('contact-support-setting')}}"><i class="fa fa-gears"></i> {{__('Contact and Support')}}</a></li>
                    <li class="{{ $segment1 == 'faq-list' ? 'active': '' }}"><a href="{{url('faq-list')}}"><i class="fa fa-question-circle"></i> {{__('FAQs List')}}</a></li>
                    <li class="{{ $segment1 == 'referral-add' ? 'active': '' }}"><a href="{{url('referral-add')}}"><i class="fa fa-gears"></i> {{((__('Referral Bonus')))}}</a></li>
                </ul>
            </li>
            <li @if(!empty($menu) && ($menu == 'profile')) class="open" @endif>
                <a href="{{url('/')}}"><i class="fa fa-user-circle"></i>
                    <span>  {{__('profile')}}</span><i class="accordion-icon fa fa-angle-left"></i></a>
                <ul class="sub-menu" style="display: block;">
                    <li class="{{ $segment1 == 'profile' ? 'active': '' }}"><a href="{{url('profile')}}"><i class="fa fa-user"></i> {{__('Profile')}}</a></li>
                </ul>
            </li>
        </ul>
    </div>
</div>
