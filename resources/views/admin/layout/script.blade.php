
<script src="{{asset('assets/plugins/jquery/jquery.min.js')}}"></script>
<script src="{{asset('assets/plugins/jquery-ui/jquery-ui.js')}}"></script>
<script src="{{asset('assets/plugins/moment/moment.min.js')}}"></script>
<script src="{{asset('assets/plugins/popper/popper.js')}}"></script>
{{--<script src="{{asset('assets/plugins/feather/feather.min.js')}}"></script>--}}
<script src="{{asset('assets/plugins/bootstrap/js/bootstrap.min.js')}}"></script>
{{--<script src="{{asset('assets/plugins/typeahead/typeahead.js')}}"></script>--}}
{{--<script src="{{asset('assets/plugins/typeahead/typeahead-active.js')}}"></script>--}}
<script src="{{asset('assets/plugins/pace/pace.min.js')}}"></script>
<script src="{{asset('assets/plugins/slimscroll/jquery.slimscroll.min.js')}}"></script>
{{--<script src="{{asset('assets/plugins/highlight/highlight.min.js')}}"></script>--}}
<!-- Dashboard Script -->
{{--<script src="{{asset('assets/plugins/daterangepicker/daterangepicker.js')}}"></script>--}}

<script src="{{asset('assets/plugins/dataTable/datatables.min.js')}}"></script>
<script src="{{asset('assets/plugins/dataTable/responsive/dataTables.responsive.js')}}"></script>
<script src="{{asset('assets/plugins/dataTable/extensions/dataTables.jqueryui.min.js')}}"></script>

<!-- Required Script -->
<script src="{{asset('assets/js/app.js')}}"></script>
{{--<script src="{{asset('assets/js/avesta.js')}}"></script>--}}
{{--<script src="{{asset('assets/js/avesta-customizer.js')}}"></script>--}}
<script src="{{asset('assets/plugins/dropify/js/dropify.min.js')}}"></script>
<script src="{{asset('assets/toaster/toastr.js')}}"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.12/summernote-bs4.js"></script>
<script src="{{asset('assets/datepicker/bootstrap-datepicker.js')}}"></script>

{{--<script src="{{asset('assets/datepicker/daterangepicker.css')}}"></script>--}}
{{--<script src="{{asset('assets/datepicker/bootstrap-datetimepicker.min.css')}}"></script>--}}
{{--<script src="{{asset('assets/datepicker/bootstrap-datepicker.js')}}"></script>--}}
{{--<script src="{{asset('assets/datepicker/bootstrap-datetimepicker.min.js')}}"></script>--}}
{{--<script src="{{asset('assets/datepicker/daterangepicker.js')}}"></script>--}}
{{--<script src="{{asset('assets/datepicker/daterangepicker-bs3.css')}}"></script>--}}

<script>
    toastr.options = {
        "closeButton": true,
        "debug": false,
        "newestOnTop": true,
        "progressBar": true,
        "positionClass": "toast-top-right",
        "preventDuplicates": false,
        "onclick": null,
        "showDuration": "3000",
        "hideDuration": "1000",
        "timeOut": "5000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
    }

    @if(Session::has('dismiss') && !empty(Session::get('dismiss')))
    toastr.warning("{{ucfirst(strtolower(Session::get('dismiss')))}}",'Warning')
    @endif
    @if(Session::has('success') && !empty(Session::get('success')))
    toastr.success("{{ucfirst(strtolower(Session::get('success')))}}",'Congratulations')
    @endif
{{--    @if ($errors && count($errors) > 0)--}}
{{--    toastr.warning('{{ucfirst(strtolower($errors->first()))}}','Warning')--}}
{{--    @endif--}}

    $(document.body).on('click','#agree',function(){
        $('#loginBtn').prop('disabled', true);
        if($(this).is(':checked')){
            $('#loginBtn').prop('disabled', false);
        }
    });

</script>
<script type="text/javascript">
    $('.datepicker').datepicker({
        todayBtn: "linked",
        keyboardNavigation: false,
        forceParse: false,
        autoclose: true,
        autoApply:false,
        format: "yyyy-mm-dd"
    });

    // $('.daterange').daterangepicker({
    //     locale: {
    //         format: 'Y-MM-DD'
    //     },
    //     autoUpdateInput: false,
    //     autoApply:true
    // });
    // $('.daterange').on('apply.daterangepicker', function (ev, picker) {
    //     var start_date = picker.startDate.format('Y-MM-DD')
    //     var end_date = picker.endDate.format('Y-MM-DD')
    //     $(this).val(start_date + ' - ' + end_date);
    // });

</script>

<!-- Required Script -->
<script src="{{asset('assets/plugins/dropify/js/form-file-uploads.js')}}"></script>
