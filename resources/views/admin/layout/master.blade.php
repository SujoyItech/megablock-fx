<!DOCTYPE html>
<html lang="zxx">
    <head>
        @php
            $all_settings=allSetting();
        @endphp
        <!-- The above 6 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="keyword" content="">
        <meta name="author"  content=""/>
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>@if(isset($all_settings['app_title'])){{$all_settings['app_title']}}@endif :: @yield('title')</title>
        <link rel="shortcut icon" type="image/png" @if(isset($all_settings['favicon'])) href="{{asset(getImagePath().$all_settings['favicon'])}}" @else href="{{asset('assets/images/favicon.png')}}" @endif>
        @include('admin.layout.header')
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn"t work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
<body>
<div class="page-container">
    <div class="page-sidebar">
        <div class="logo">
            <a  href="{{route('adminDashboard')}}"><img @if(isset($all_settings['logo']) && !empty($all_settings['logo'])) src="{{asset(getImagePath().$all_settings['logo'])}}" @else src="{{asset('assets/images/logo.png')}}" @endif alt="logo">FX</a>
        </div>
        @include('admin.layout.left-menu')
    </div>
    <div class="page-content">
        <div class="page-header">
            <nav class="navbar navbar-default">
                <div class="navbar-header">
                    <div class="navbar-brand">
                        <ul class="list-inline">
                            <li class="list-inline-item"><a class="hidden-md hidden-lg" href="#" id="sidebar-toggle-button"><i data-feather="menu" class="wd-20"></i></a></li>
                            <li class="list-inline-item"><a class=" hidden-xs hidden-sm" href="#" id="collapsed-sidebar-toggle-button"><i data-feather="menu" class="wd-20"></i></a></li>
                        </ul>
                    </div>
                </div>
                <div class="header-right pull-right">
                    <ul class="list-inline justify-content-end">
                        <li class="list-inline-item dropdown">
                            <a  href="" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <img class="img-fluid wd-30 ht-30 rounded-circle" @if(isset(Auth::user()->photo) && file_exists(Auth::user()->photo)) src="{{asset(getImagePath('user').Auth::user()->photo)}}" @else src="{{asset('assets/images/users-face/1.png')}}" @endif alt="image">
                                {{Auth::user()->name}}
                            </a>
                            <div class="dropdown-menu dropdown-menu-right dropdown-profile">
                                <div class="user-profile-area">
                                    <div class="user-profile-heading">
                                        <div class="profile-thumbnail">
                                            <img class="img-fluid wd-30 ht-30 rounded-circle" @if(!empty(Auth::user()->photo)) src="{{asset(getImagePath('user').Auth::user()->photo)}}" @else src="{{asset('assets/images/users-face/1.png')}}" @endif alt="image">
                                        </div>
                                        <div class="profile-text">
                                            <h6>{{Auth::user()->name}}</h6>
                                            <span>{{Auth::user()->email}}</span>
                                        </div>
                                    </div>
                                    <a href="{{route('profile')}}" class="dropdown-item"><i class="fa fa-user"></i> {{__('My profile')}}</a>
                                    <a href="{{route('logout')}}" class="dropdown-item text-danger"><i class="fa fa-sign-out"></i> {{__('Sign-out')}}</a>
                                </div>
                            </div>
                        </li>
                        <!-- Profile Dropdown End -->
                    </ul>
                </div>
                <!--/ Header Right End -->
            </nav>
        </div>
        <!--/ Page Header End -->
        <!--================================-->
        <!--================================-->
        <div class="page-inner">
            <div class="content-wrapper">
                @yield('content')
            </div>
        </div>
        <!--/ Page Inner End -->
        <!--================================-->
        @include('admin.layout.footer')
    </div>
    <!--/ Page Content End -->
</div>

@include('admin.layout.script')
@yield('script')
<!-- Javascript -->

<!-- / Javascript -->
</body>
</html>
