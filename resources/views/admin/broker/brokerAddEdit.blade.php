@extends('admin.layout.master',['menu'=>'broker'])
@section('title',$title)
@section('style')
    <link href="https://cdn.jsdelivr.net/npm/select2@4.0.12/dist/css/select2.min.css" rel="stylesheet" />
@endsection
@section('content')
    <div class="main-wrapper">
        <div class="pageheader pd-t-25 pd-b-35">
            <div class="d-flex justify-content-between">
                <div class="pd-t-5 pd-b-5">
                    <h1 class="pd-0 mg-0 tx-30 tx-dark"><i class="fa fa-handshake-o"></i> {{__('Broker')}}</h1>
                </div>
            </div>
        </div>
        {{  Form::open(array('route' => 'brokerSave', 'files' => true,'method' => 'post'))}}
        @csrf
        <div class="row">
            <div class="col-sm-9 row">
                <div class="col-sm-4 mb-3">
                    <label>{{__('Name')}}</label>
                    <input type="text" name="name" autocomplete="off" class="form-control mb-1" @if(isset($item)) value="{{$item->name}}"@else value="{{old('name')}}" @endif >
                    @if($errors->first('name'))<span class="text-danger">{{$errors->first('name')}}</span> @endif
                </div>
                <div class="col-sm-8 mb-3">
                    <label>{{__('Licence')}}</label>
                    <select class="js-example-basic-multiple form-control dropdown-toggle" multiple name="licences[]" id="sports1">
                        @foreach($licences as $licence)
                            <option @if(isset($item)) {{isMultiSelectLicence($licence->id,$broker_licences)}} @endif value="{{$licence->id}}" {{ (collect(old('licences'))->contains($licence->id)) ? 'selected':'' }}>{{$licence->title}}</option>
                        @endforeach
                    </select>
                    @if($errors->first('licences[]'))<span class="text-danger">{{$errors->first('licences[]')}}</span> @endif
                </div>
                <div class="col-sm-6 mb-3">
                    <label>{{__('Markets ( Select Unavailable Markets)')}}</label>
                    <select class="js-example-basic-multiple2 form-control" multiple name="markets[]" id="sports">
                        @foreach(country() as $key=>$country)
                            <option @if(isset($markets)) {{isMultiSelect($key,$markets)}} @endif value="{{$key}}" {{ (collect(old('markets'))->contains($key)) ? 'selected':'' }}>{{$country}}</option>
                        @endforeach
                    </select>
                    @if($errors->first('markets[]'))<span class="text-danger">{{$errors->first('markets[]')}}</span> @endif
                </div>
                <div class="col-sm-6 mb-3">
                    <label>{{__('Advantage')}}</label>
                    <input type="text" name="advantage" autocomplete="off" class="form-control mb-1" @if(isset($item)) value="{{$item->advantage}}"@else value="{{old('advantage')}}" @endif >
                    @if($errors->first('advantage'))<span class="text-danger">{{$errors->first('advantage')}}</span>@endif
                </div>
                <div class="col-sm-12 mb-3">
                    <label>{{__('Average Spreads')}}</label>
                    <textarea type="text" name="average_spreads" autocomplete="off" class="form-control mb-1">@if(isset($item)){{$item->average_spreads}} @else {{old('average_spreads')}} @endif</textarea>@if($errors->first('average_spreads'))<span class="text-danger">{{$errors->first('average_spreads')}}</span>@endif
                </div>
                <div class="col-sm-12 mb-3">
                    <label>{{__('Recommended Deposit')}}</label>
                    <textarea type="text" name="recommended_deposit" autocomplete="off" class="form-control mb-1">@if(isset($item)){{$item->recommended_deposit}} @else {{old('recommended_deposit')}} @endif</textarea>
                    @if($errors->first('recommended_deposit'))<span class="text-danger">{{$errors->first('recommended_deposit')}}</span>@endif
                </div>
            </div>
            <div class="col-3">
                <div class="col-12">
                    <label for="name">{{__('Logo')}}</label>
                    <small>{{__('jpg, png or jpeg & max size 2mb')}}</small>
                    <div id="file-upload" class="section">
                        <div class="row section">
                            <div class="col s12 m12 l12">
                                <input name="logo" type="file" id="input-file-now" class="dropify mb-1"
                                       data-default-file="{{isset($item->logo) &&!empty($item->logo) ? asset(getImagePath('brokers').$item->logo) : ''}}" />
                                @if ($errors->has('logo'))
                                    <div class="text-danger text-center">{{ $errors->first('logo') }}</div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>
        <div class="row">
            <div class="col-sm-6 mb-3">
                <label>{{__('URL')}}</label>
                <input type="text" name="URL" autocomplete="off" class="form-control mb-1" @if(isset($item))
                value="{{$item->URL}}"@else value="{{old('URL')}}" @endif >
                @if($errors->first('URL')) <span class="text-danger">{{$errors->first('URL')}}</span> @endif
            </div>
            <div class="col-6"></div>

            <div class="col-2 mb-5">
                @if(isset($item)) <input type="hidden" name="edit_id" value="{{encrypt($item->id)}}"> @endif
                <button type="submit" class="btn btn-lg btn-primary"><i class="fa fa-save"></i> {{$button_title}}</button>
            </div>
        </div>
        {{Form::close()}}
    </div>
@endsection
@section('script')
    <script src="https://cdn.jsdelivr.net/npm/select2@4.0.12/dist/js/select2.min.js"></script>

    <script>
        $(document).ready(function() {
            $('.js-example-basic-multiple').select2();

        });
        $(document).ready(function() {
            $('.js-example-basic-multiple2').select2();

        });
    </script>
@endsection


{{--Licences (from list. Multi selection)--}}
{{--Markets (multi selection list of countries)--}}
{{--Average spreads (text)--}}
{{--Recommended deposit (text)--}}
{{--URL--}}
