@extends('admin.layout.master',['menu'=>'broker'])
@section('title',__('Broker'))
@section('after-style')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <style>
        #sortable { list-style-type: none; margin: 0; padding: 0; width: 60%; }
        #sortable li { margin: 0 3px 3px 3px; padding: 0.4em; padding-left: 1.5em; font-size: 1.4em; height: 18px; }
        #sortable li span { position: absolute; margin-left: -1.3em; }
    </style>
@endsection
@section('content')
    <div class="main-wrapper">
        <div class="pageheader pd-t-25 pd-b-35">
            <div class="d-flex justify-content-between">
                <div class="pd-t-5 pd-b-5">
                    <h1 class="pd-0 mg-0 tx-30 tx-dark"><i class="fa fa-handshake-o"></i> {{__('Broker')}}</h1>
                </div>
                <div class="d-flex align-items-center hidden-xs">
                    <a href="{{route('brokerAdd')}}" class="btn btn-primary"><i class="fa fa-plus"></i> {{__('Add')}}</a>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 col-lg-12">
                <div class="card mg-b-30">
                    <div class="card-body pd-0">
                        <table id="basicDataTable" class="table responsive">
                            <thead>
                            <tr>
                                <th class="all text-center" width="1">{{__('Move')}}</th>
                                <th>{{__('Logo')}}</th>
                                <th>{{__('Name')}}</th>
                                <th>{{__('Advantages')}}</th>
                                <th>{{__('Average')}}<br>{{__('spreads')}}</th>
                                <th>{{__('Recommended')}}<br>{{__('deposit')}}</th>
                                <th>{{__('Created')}}</th>
                                <th>{{__('Actions')}}</th>
                            </tr>
                            </thead>
                            <tbody class="sortable"></tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
{{--    @include('admin.layout.page-title',['title'=>__('Broker List')])--}}
{{--    <div class="main-content-inner">--}}
{{--        <div class="row">--}}
{{--            <div class="col-12 mt-3">--}}
{{--                <div class="card">--}}
{{--                    <div class="card-body">--}}
{{--                        <a href="{{route('brokerAdd')}}" class="btn btn-primary pull-right mb-4"><i class="fa fa-plus"></i> {{__('Add')}}</a>--}}
{{--                        <div class="clearfix"></div>--}}
{{--                        --}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}
@endsection
@section('script')
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script>
        $('#basicDataTable').DataTable({
            processing: true,
            serverSide: true,
            pageLength: 10,
            responsive: false,
            ajax: '{{route('brokerList')}}',
            order: [1, 'asc'],
            autoWidth:false,
            createdRow: function ( row, data, index ) {
                // console.log(data.id);
                $(row).addClass('ui-state-default');
                $(row).attr('data-id',data.id);
            },
            drawCallback: function(  ) {

                $( function() {
                    $( ".sortable" ).sortable();
                    $( ".sortable" ).disableSelection();
                } );
            },
            columns: [
                {"data": "move"},
                {"data": "logo"},
                {"data": "name"},
                {"data": "advantage"},
                {"data": "average_spreads"},
                {"data": "recommended_deposit"},
                {"data": "created_at"},
                {"data": "actions",orderable: false, searchable: false}
            ]
        });
    </script>
    <script>
        $(".sortable").sortable({
            /*stop: function(event, ui) {
                alert("New position: " + ui.item.index());
            }*/
            start: function(e, ui) {

                // creates a temporary attribute on the element with the old index
                $(this).attr('data-previndex', ui.item.data('id'));
            },
            update: function(e, ui) {
                // gets the new and old index then removes the temporary attribute
                var xx = [];
                $(".ui-state-default").each(function(index,key){
                    xx.push($(this).data('id'));
                });

                //  var newIndex = ui.item.attr('data-id');
                //  var oldIndex = $(this).attr('data-previndex');
                // var element_id = [newIndex,oldIndex];

                $.ajax({
                    url: "{{route('Order')}}?table=brokers&column=order&ids="+xx,
                    context: document.body
                }).done(function() {
                    toastr.success("Order updated successfully",'Congratulations');

                });
                $(this).removeAttr('data-previndex');
            }
        });
        $(".sortable").disableSelection();
    </script>
@endsection
