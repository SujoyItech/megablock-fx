@extends('admin.layout.master',['menu'=>'user'])
@section('title',__('Roles'))
@section('after-style')
@endsection
@section('content')
    <div class="main-wrapper">
        <div class="pageheader pd-t-25 pd-b-35">
            <div class="d-flex justify-content-between">
                <div class="pd-t-5 pd-b-5">
                    <h1 class="pd-0 mg-0 tx-30 tx-dark"><i class="fa fa-users"></i> {{__('Roll')}}</h1>
                </div>
                <div class="d-flex align-items-center hidden-xs">
                    <a href="{{route('adminRoleAdd')}}" class="btn btn-primary"><i class="fa fa-plus"></i> {{__('Add')}}</a>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="card-body pd-0">
                <table id="cellBorder" class="cell-border responsive nowrap">
                    <thead>
                    <tr>
                        <th class="all">{{__('Title')}}</th>
                        <th>{{__('Created At')}}</th>
                        <th class="all" width="30">{{__('Actions')}}</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>

@endsection
@section('script')
    <script>
        $('#cellBorder').DataTable({
            processing: true,
            serverSide: true,
            pageLength: 10,
            responsive: true,
            ajax: '{{route('adminRoleList')}}',
            order: [0, 'asc'],
            autoWidth:false,
            columns: [
                {"data": "title"},
                {"data": "created_at"},
                {"data": "actions",orderable: false, searchable: false}
            ]
        });
    </script>
@endsection
