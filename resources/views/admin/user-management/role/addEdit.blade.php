@extends('admin.layout.master',['menu'=>'user'])
@section('title',$title)
@section('style')
    <style>
        #sbOne,#sbTwo  {
            height:400px;
        }
        .inpbtn {
            padding: 8px 9px;
            background: #02BC77;
            color: #fff;
            border: none;
            font-weight: 700;
        }
    </style>
@endsection
@section('content')
    <div class="main-wrapper">
        <div class="pageheader pd-t-25 pd-b-35">
            <div class="d-flex justify-content-between">
                <div class="pd-t-5 pd-b-5">
                    <h1 class="pd-0 mg-0 tx-30 tx-dark"><i class="fa fa-users"></i> {{__('Role')}}</h1>
                </div>
            </div>
        </div>
        {{ Form::open(array('route' => 'adminRoleSave','files'=>true,'id'=>'myForm')) }}
        <div class="row">
            <div class="col-sm-12">
                <label>{{__('Title')}}</label>
                <input type="text" name="title" class="form-control" @if(isset($item)) value="{{$item->title}}" @endif >
                <pre class="text-danger">{{$errors->first('title')}}</pre>
            </div>
            @php
                $actions=[];
                if(isset($item)){
                    $actions=array_filter(explode('|',$item->actions));
                }
            @endphp
            <div class="col-md-12">
                <h3>{{__('Please select your task')}}</h3>
                <div class="row align-items-center">
                    <div class="col-md-5">
                        <select id="sbOne" multiple="multiple" id="exampleInputFile" class="pay-methd-frm form-control">
                            @foreach(actions() as $value)
                                @if(!in_array($value['id'], $actions))
                                    <option value="{{$value['id']}}">{{$value['name']}}</option>
                                @endif
                            @endforeach
                        </select>
                    </div>
                    <div style="margin: 30px 0;" class="col-md-2 text-center">
                        <input type="button" id="left" class="inpbtn" value="<" />
                        <input type="button" id="leftall" class="inpbtn" value="<<" />
                        <input type="button" id="rightall" class="inpbtn" value=">>" />
                        <input type="button" id="right" class="inpbtn" value=">" />
                    </div>
                    <div class="col-md-5 {{$errors->has('description') ? 'has-error' : '' }}">

                        <select name="tasks[]" id="sbTwo" multiple="multiple" id="exampleInputFile" class="pay-methd-frm form-control">
                            @foreach(actions() as $value)
                                @if(in_array($value['id'],$actions))
                                    <option value="{{$value['id']}}">{{$value['name']}}</option>
                                @endif
                            @endforeach
                        </select>
                        <span class="text-danger">  {{ $errors->has('tasks') ? $errors->first('tasks') : '' }}</span>
                    </div>
                </div>
            </div>
            <div class="col-md-12" style="margin-top: 15px;">
                @if(isset($item)) <input type="hidden" name="edit_id" value="{{encrypt($item->id)}}"> @endif
                <button type="submit" class="btn btn-lg btn-primary"><i class="fa fa-save"></i> {{$button_title}}</button>
            </div>
        </div>
        {{Form::close()}}
    </div>
@endsection
@section('script')
    <script>
        $(function () { function moveItems(origin, dest) {
            $(origin).find(':selected').appendTo(dest);
        }
            function moveAllItems(origin, dest) {
                $(origin).children().appendTo(dest);
            }
            $('#left').click(function () {
                moveItems('#sbTwo', '#sbOne');
            });
            $('#right').on('click', function () {
                moveItems('#sbOne', '#sbTwo');
            });
            $('#leftall').on('click', function () {
                moveAllItems('#sbTwo', '#sbOne');
            });
            $('#rightall').on('click', function () {
                moveAllItems('#sbOne', '#sbTwo');
            });
        });
        $(document.body).on('submit','#myForm',function (e) {
            $('#sbTwo option').prop('selected', true);
        });
    </script>
@endsection
