@extends('admin.layout.master',['menu'=>'user'])
@section('title',__('Users'))
@section('after-style')
    <style>
        . textAlign{
            text-align: left!important;
        }
    </style>
@endsection
@section('content')
    <div class="main-wrapper">
        <div class="pageheader pd-t-25 pd-b-35">
            <div class="d-flex justify-content-between">
                <div class="pd-t-5 pd-b-5">
                    <h1 class="pd-0 mg-0 tx-30 tx-dark"><i class="fa fa-users"></i> {{__('Users')}}</h1>
                </div>
                <div class="d-flex align-items-center hidden-xs">
                    <a href="{{route('adminUserAdd')}}" class="btn btn-primary"><i class="fa fa-plus"></i>  {{__('Add')}}</a>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 col-lg-12">
                <div class="card mg-b-30">
                    <div class="card-body pd-0">
                        <table id="basicDataTable" class="table responsive nowrap">
                            <thead>
                            <tr>
                                <th class="all">{{__('Name')}}</th>
                                <th>{{__('Email')}}</th>
                                <th>{{__('Phone')}}</th>
                                <th>{{__('Status')}}</th>
                                <th>{{__('Balance')}}</th>
                                <th>{{__('Created At')}}</th>
                                <th class="all" width="30">{{__('Actions')}}</th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script>
        $('#basicDataTable').DataTable({
            processing: true,
            serverSide: true,
            pageLength: 25,
            responsive: true,
            ajax: '{{route('userList')}}',
            order: [5, 'desc'],
            autoWidth:false,
            columns: [
                {"data": "name",className:'textAlign'},
                {"data": "email"},
                {"data": "phone"},
                {"data": "active_status"},
                {"data": "balance"},
                {"data": "created_at"},
                {"data": "actions",orderable: false, searchable: false}
            ]
        });
    </script>
@endsection
