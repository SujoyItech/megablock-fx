@extends('admin.layout.master',['menu'=>'user'])
@section('title',__('Users'))
@section('after-style')
@endsection
@section('content')
    @include('admin.layout.page-title',['title'=>__('Users')])
    {{--<div class="breadcrumbs-area">--}}
    {{--<ul class="crumb-list user-crumb">--}}
    {{--<li><a href="{{route('adminUserList')}}" class="list-active">{{__('User List')}}</a></li>--}}
    {{--<li><a href="{{route('appUserList')}}" class="">{{__('App User List')}}</a></li>--}}
    {{--</ul>--}}
    {{--</div>--}}
    <div class="main-content-inner">

        <div id="smartwizard-1" >
            <ul>
                <li class="done">
                    <a href="#smartwizard-1-step-1">
                        <span class="sw-number">1</span>
                        {{--<div class="tx-gray-500 small"></div>--}}
                        {{__('Admin List')}}
                    </a>
                </li>
                <li class="done">
                    <a href="#smartwizard-1-step-2">
                        <span class="sw-number">2</span>
                        {{--<div class="tx-gray-500 small">{{__('Logo')}}</div>--}}
                        {{__('User List')}}
                    </a>
                </li>
            </ul>
            <div class="mb-3">
                <div id="smartwizard-1-step-1" class="card animated fadeIn">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-12 mt-5">
                                <div class="card">
                                    <div class="card-body">
                                        <a href="{{route('adminUserAdd')}}" class="btn btn-primary pull-right mb-4"><i class="fa fa-plus"></i> {{__('Add')}}</a>
                                        <div class="clearfix"></div>
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <table class="table" id="adminTable" width="100%">
                                                    <thead>
                                                    <tr>
                                                        <th class="all">{{__('Name')}}</th>
                                                        <th>{{__('Email')}}</th>
                                                        <th>{{__('Phone')}}</th>
                                                        <th>{{__('Role')}}</th>
                                                        <th>{{__('Status')}}</th>
                                                        <th>{{__('Created At')}}</th>
                                                        <th class="all" width="30">{{__('Actions')}}</th>
                                                    </tr>
                                                    </thead>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="smartwizard-1-step-2" class="card animated fadeIn">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-12 mt-5">
                                <div class="card">
                                    <div class="card-body">
                                        <a href="{{route('adminUserAdd')}}" class="btn btn-primary pull-right mb-4"><i class="fa fa-plus"></i> {{__('Add')}}</a>
                                        <div class="clearfix"></div>
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <table class="table" id="userTable" width="100%">
                                                    <thead>
                                                    <tr>
                                                        <th class="all">{{__('Name')}}</th>
                                                        <th>{{__('Email')}}</th>
                                                        <th>{{__('Phone')}}</th>
                                                        <th>{{__('Role')}}</th>
                                                        <th>{{__('Status')}}</th>
                                                        <th>{{__('Created At')}}</th>
                                                        <th class="all" width="30">{{__('Actions')}}</th>
                                                    </tr>
                                                    </thead>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection
@section('script')
    <script>
        $(function() {
            $('#smartwizard-1').smartWizard({
                autoAdjustHeight: false,
                backButtonSupport: false,
                useURLhash: false,
                showStepURLhash: false
            });
        });
        $('#userTable').DataTable({
            processing: true,
            serverSide: true,
            pageLength: 25,
            responsive: true,
            ajax: '{{route('userList')}}',
            order: [5, 'desc'],
            autoWidth:false,
            columns: [
                {"data": "name"},
                {"data": "email"},
                {"data": "phone"},
                {"data": "role"},
                {"data": "active_status"},
                {"data": "created_at"},
                {"data": "actions",orderable: false, searchable: false}
            ]
        });
        $('#adminTable').DataTable({
            processing: true,
            serverSide: true,
            pageLength: 25,
            responsive: true,
            ajax: '{{route('adminUserList')}}',
            order: [5, 'desc'],
            autoWidth:false,
            columns: [
                {"data": "name"},
                {"data": "email"},
                {"data": "phone"},
                {"data": "role"},
                {"data": "active_status"},
                {"data": "created_at"},
                {"data": "actions",orderable: false, searchable: false}
            ]
        });
    </script>
@endsection
