@extends('admin.layout.master',['menu'=>'user'])
@section('title',__('Users'))
@section('after-style')
@endsection
@section('content')
    <div class="main-wrapper">
        <div class="pageheader pd-t-25 pd-b-35">
            <div class="d-flex justify-content-between">
                <div class="pd-t-5 pd-b-5">
                    <h1 class="pd-0 mg-0 tx-30 tx-dark"><i class="fa fa-users"></i> {{__('Users')}}</h1>
                </div>
            </div>
        </div>

        {{ Form::open(array('route' => 'adminUserSave','files'=>true,'id'=>'myForm')) }}
        <div class="row">
            <div class="col-sm-6 mb-3">
                <label>{{__('First Name')}}</label>
                <input type="text" name="first_name" autocomplete="off" class="form-control mb-1" @if(isset($item)) value="{{$item->first_name}}" @else value="{{old('first_name')}}" @endif >
                @if($errors->first('first_name'))<span class="text-danger">{{$errors->first('first_name')}}</span> @endif
            </div>
            <div class="col-sm-6 mb-3">
                <label>{{__('Last Name')}}</label>
                <input type="text" name="last_name" autocomplete="off" class="form-control mb-1" @if(isset($item)) value="{{$item->last_name}}" @else value="{{old('last_name')}}" @endif >
                @if($errors->first('last_name'))<span class="text-danger">{{$errors->first('last_name')}}</span> @endif
            </div>
            <div class="col-sm-6 mb-3">
                <label>{{__('Email')}}</label>
                <input type="text" name="email" autocomplete="off" class="form-control mb-1" @if(isset($item)) value="{{$item->email}}" @else value="{{old('email')}}" @endif >
                @if($errors->first('email'))<span class="text-danger">{{$errors->first('email')}}</span> @endif
            </div>
            <div class="col-sm-6 mb-3">
                <label>{{__('Phone')}}</label>
                <input type="text" name="phone" autocomplete="off" class="form-control mb-1" @if(isset($item)) value="{{$item->phone}}" @else value="{{old('phone')}}" @endif >
                @if($errors->first('phone'))<span class="text-danger">{{$errors->first('phone')}}</span> @endif
            </div>
            <div class="col-sm-6 mb-3">
                <label>{{__('Credit')}}</label>
                <input type="number" min="0" name="balance" autocomplete="off" class="form-control mb-1" @if(isset($balance)) value="{{$balance}}" @else value="{{old('balance')}}" @endif >
                @if($errors->first('balance'))<span class="text-danger">{{$errors->first('balance')}}</span> @endif
            </div>
            <div class="col-sm-6 mb-3">
                <label>{{__('Role')}}</label>
                <select name="role" class="form-control mb-1" >
                    <option>{{__('Select')}}</option>
                    {{--                                    <option value="{{USER_ROLE_ADMIN}}">{{__('Admin')}}</option>--}}
                    <option value="{{USER_ROLE_USER}}" @if(isset($item)) {{isSelect($item->role,USER_ROLE_USER)}} @endif {{ old('role') == USER_ROLE_USER ? 'selected' : '' }}>{{__('User')}}</option>
                    {{--                                    @if(isset($roles[0]))--}}
                    {{--                                        @foreach($roles as $role)--}}
                    {{--                                        <option @if(isset($item) && $item->role==$role->id) selected @endif value="{{$role->id}}">{{$role->title}}</option>--}}
                    {{--                                        @endforeach--}}
                    {{--                                    @endif--}}
                </select>
                @if($errors->first('role'))<span class="text-danger">{{$errors->first('role')}}</span> @endif
            </div>
            <div class="col-sm-6 mb-3">
                <label for="{{__('Country')}}">{{__('Country')}}</label>
                <select class="form-control mb-1" name="country">
                    @foreach(country() as $key=> $country)
                        <option value="{{$country}}" @if(isset($item)){{isSelect($country,$item->country)}}@endif {{ old('country') == $country ? 'selected' : '' }}>{{$country}}</option>
                    @endforeach
                </select>
                @if($errors->first('country'))<span class="text-danger">{{$errors->first('country')}}</span> @endif
            </div>
            <div class='col-sm-6'>
                <div class="form-group">
                    <label for="Date of Birth">{{__('Date of Birth')}}</label>
                    <div id='datetimepicker1'>
                        <input type='text' readonly="" name="dob" class="form-control datepicker mb-1" @if(isset($item->dob)) value="{{$item->dob}}" @else value="{{old('dob')}}" @endif/>
                    </div>
                    @if($errors->first('dob'))<span class="text-danger">{{$errors->first('dob')}}</span> @endif
                </div>
            </div>
            @if(!isset($item))
                <div class="col-sm-6 mb-3">
                    <label>{{__('Password')}}</label>
                    <input type="password" autocomplete="off" name="password" class="form-control mb-1" >
                    @if($errors->first('password'))<span class="text-danger">{{$errors->first('password')}}</span> @endif
                </div>
                <div class="col-sm-6 mb-3">
                    <label>{{__('Confirm Password')}}</label>
                    <input type="password" autocomplete="off" name="password_confirmation" class="form-control mb-1" >
                    @if($errors->first('password_confirmation'))<span class="text-danger">{{$errors->first('password_confirmation')}}</span> @endif
                </div>
            @endif
            <div class="col-sm-6 mb-3">
                <label for="{{__('Status')}}">{{__('Status')}}</label>
                <select class="form-control mb-1" name="active_status">
                    <option value="{{STATUS_SUCCESS}}" @if(isset($item)){{isSelect($item->active_status,STATUS_SUCCESS)}}@else {{ old('active_status') == STATUS_SUCCESS ? 'selected' : '' }}@endif>{{__('Active')}}</option>
                    <option value="{{STATUS_PENDING}}" @if(isset($item)){{isSelect($item->active_status,STATUS_PENDING)}}@else {{ old('active_status') == STATUS_PENDING ? 'selected' : '' }}@endif>{{__('Pending')}}</option>
                    <option value="{{STATUS_BLOCKED}}" @if(isset($item)){{isSelect($item->active_status,STATUS_BLOCKED)}}@else {{ old('active_status') == STATUS_BLOCKED ? 'selected' : '' }}@endif>{{__('Blocked')}}</option>
                </select>
            </div>
            <div class="col-12"></div>

            <div class="col-md-2 mb-3">
                @if(isset($item)) <input type="hidden" name="edit_id" value="{{encrypt($item->id)}}"> @endif
                    <button type="submit" class="btn btn-lg btn-primary"><i class="fa fa-save"></i> {{$button_title}}</button>
            </div>
        </div>
        {{Form::close()}}
    </div>
@endsection
@section('script')
{{--    <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>--}}
{{--    <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />--}}

{{--    <script type="text/javascript">--}}
{{--        var start = new Date();--}}
{{--        start.setFullYear(start.getFullYear() - 70);--}}
{{--        var end = new Date();--}}
{{--        end.setFullYear(end.getFullYear() - 1);--}}
{{--        $('.datepicker').datepicker({--}}
{{--            bootstrap:true,--}}
{{--            changeMonth: true,--}}
{{--            changeYear: true,--}}
{{--            minDate: start,--}}
{{--            maxDate: end,--}}
{{--            yearRange: start.getFullYear() + ':' + end.getFullYear(),--}}
{{--            format: 'yyyy/mm/dd'--}}
{{--        });--}}
{{--    </script>--}}

@endsection
