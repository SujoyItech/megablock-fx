@extends('admin.master',['menu'=>'user'])
@section('title',$title)
@section('style')
@endsection
@section('content')
    @include('admin.widget.page-title',['title'=>$title])
    <div class="main-content-inner">
        <div class="row">
            <div class="col-md-3 mt-5">
                <div class="card">
                    <div class="card-body">
                        <div class="clearfix"></div>
                        <img id="photo" width="100"
                             @if(!empty($item->photo)) src="{{asset(getImagePath('user').$item->photo)}}" @else
                             src="{{asset('assets/images/avater.png')}}"
                             @endif
                             class="upload_image">
                    </div>
                </div>
            </div>
            <div class="col-md-9 mt-5">
                <div class="card">
                    <div class="card-body">
                        <div class="clearfix"></div>
                        <div class="row">
                            <div class="col-12">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="clearfix"></div>
                                        <div class="row">
                                            <div class="col-sm-3">
                                                <label>{{__('Name')}}</label>
                                            </div>
                                            <div class="col-sm-1">:</div>
                                            <div class="col-sm-8">
                                                @if(isset($item->name)) {{$item->name}} @endif
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-3">
                                                <label>{{__('Email')}}</label>
                                            </div>
                                            <div class="col-sm-1">:</div>
                                            <div class="col-sm-8">
                                                @if(isset($item->email)) {{$item->email}} @endif
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-3">
                                                <label>{{__('Phone')}}</label>
                                            </div>
                                            <div class="col-sm-1">:</div>
                                            <div class="col-sm-8">
                                                @if(isset($item->phone)) {{$item->phone}} @endif
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-3">
                                                <label>{{__('Role')}}</label>
                                            </div>
                                            <div class="col-sm-1">:</div>
                                            <div class="col-sm-8">
                                                {{__('User')}}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection
@section('script')
    <script>
        $(function () { function moveItems(origin, dest) {
            $(origin).find(':selected').appendTo(dest);
        }
            function moveAllItems(origin, dest) {
                $(origin).children().appendTo(dest);
            }
            $('#left').click(function () {
                moveItems('#sbTwo', '#sbOne');
            });
            $('#right').on('click', function () {
                moveItems('#sbOne', '#sbTwo');
            });
            $('#leftall').on('click', function () {
                moveAllItems('#sbTwo', '#sbOne');
            });
            $('#rightall').on('click', function () {
                moveAllItems('#sbOne', '#sbTwo');
            });
        });
        $(document.body).on('submit','#myForm',function (e) {
            $('#sbTwo option').prop('selected', true);
        });
    </script>
@endsection
