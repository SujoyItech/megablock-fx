@extends('admin.master',['menu'=>'user'])
@section('title',__('App Users'))
@section('after-style')
@endsection
@section('content')
    <div class="breadcrumbs-area">
        <ul class="crumb-list user-crumb">
            <li><a href="{{route('adminUserList')}}" class="">{{__('User List')}}</a></li>
            <li><a href="{{route('appUserList')}}" class="list-active">{{__('App User List')}}</a></li>
        </ul>
    </div>
    <div class="main-content-inner">
        <div class="row">
            <div class="col-12 mt-5">
                <div class="card">
                    <div class="card-body">
                        <a href="{{route('adminUserAdd')}}" class="btn btn-ypto pull-right mb-4"><i class="fa fa-plus"></i> {{__('Add')}}</a>
                        <div class="clearfix"></div>
                        <div class="row">
                            <div class="col-sm-12">
                                <table class="table" id="table" width="100%">
                                    <thead>
                                    <tr>
                                        <th class="all">{{__('Name')}}</th>
                                        <th>{{__('Email')}}</th>
                                        <th>{{__('Phone')}}</th>
                                        <th>{{__('Role')}}</th>
{{--                                        <th>{{__('Status')}}</th>--}}
                                        <th>{{__('Created At')}}</th>
                                        <th width="30">{{__('Actions')}}</th>
                                    </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script>
        $('#table').DataTable({
            processing: true,
            serverSide: true,
            pageLength: 25,
            responsive: true,
            ajax: '{{route('appUserList')}}',
            order: [5, 'desc'],
            autoWidth:false,
            columns: [
                {"data": "name"},
                {"data": "email"},
                {"data": "phone"},
                {"data": "role"},
                // {"data": "active_status"},
                {"data": "created_at"},
                {"data": "actions",orderable: false, searchable: false}
            ]
        });
    </script>
@endsection
