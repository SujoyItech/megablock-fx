@extends('admin.auth.master')
@section('title',__('Login'))
@section('content')
    <div class="ht-100v text-center">
    <div class="row pd-0 mg-0">
        <div class="col-md-6 col-lg-6 bg-gradient hidden-sm hidden-xs">
            <div class="ht-100v d-flex">
                <div class="col-12 align-self-center">
                    <img src="{{asset('user/')}}/images/login.svg" alt="">
                    <h3 class="tx-20 tx-semibold tx-gray-100 pd-t-50">{{__('MEGABLOCK FX')}}</h3>
                    <p class="pd-y-15 pd-x-10 pd-md-x-100 tx-gray-200">
                    </p>
                    <a href="{{route('userSignUp')}}" class="btn btn-outline-info">
                        <span class="tx-gray-200"><i class="fa fa-user-plus"></i> {{__('Get An Account')}}</span>
                    </a>
                </div>
            </div>
        </div>
        <div class="col-md-6 col-lg-6 bg-light">
            <div class="ht-100v d-flex align-items-center justify-content-center">
                    <div class="">
                        <h3 class="tx-dark mg-b-5 tx-left">{{__('Login')}}</h3>
                        <p class="tx-gray-500 tx-15 mg-b-40 tx-left"></p>
                        @if(Session::has('message'))
                            <div class="alert alert-success alert-dismissible" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                {!! Session::get('message') !!}
                            </div>
                        @endif
                        @if(Session::has('dismiss'))
                            <div class="alert alert-danger alert-dismissible" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                {{Session::get('dismiss')}}
                            </div>
                        @endif
                        @if(Session::has('success'))
                            <div class="alert alert-success alert-dismissible" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                {{Session::get('success')}}
                            </div>
                        @endif
                        @if(count($errors) > 0)
                            @foreach($errors->all() as $error)
                                <div class="alert alert-danger alert-dismissible" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    {!! $error !!}
                                </div>
                            @endforeach
                        @endif
                        <form action="{{route('postLogin')}}" method="post">
                            @csrf
                            <div class="form-group tx-left">
                                <label class="tx-gray-500 mg-b-5">{{__('Email')}}</label>
                                <input type="email" name="email" id="email" class="form-control" placeholder="email@domain.com" value="{{old('email')}}" autocomplete="off">
                            </div>
                            <div class="form-group">
                                <div class="d-flex justify-content-between mg-b-5">
                                    <label class="tx-gray-500 mg-b-0">{{__('Password')}}</label>
                                    <a href="{{route('userForgetPassword')}}" class="tx-13 mg-b-0 tx-semibold">{{__('Forgot Password?')}}</a>
                                </div>
                                <input type="password" name="password" id="pass" class="form-control" placeholder="Enter your password" autocomplete="off">
                            </div>
                            <button type="submit" class="btn btn-brand btn-block"><i class="fa fa-sign-in"></i> {{__('Login')}}</button>
                        </form>
                        <div class="pd-y-20 tx-uppercase tx-gray-500">or</div>
{{--                        <a href="{{route('socialLogin','facebook')}}" class="btn bg-facebook"><i class="fa fa-facebook"></i> Facebook</a>--}}
                        <a href="{{url('login/facebook')}}" class="btn bg-facebook"><i class="fa fa-facebook"></i> {{__('Facebook')}}</a>
                        <a href="{{url('login/google')}}" class="btn bg-google"><i class="fa fa-google"></i> {{__('Google')}}</a>
                        <div class="tx-13 mg-t-20 tx-center tx-gray-500">
                            {{__('Don\'t have an account?')}} <a href="{{url('user-sign-up')}}" class="tx-dark tx-semibold">{{__('Create account')}}</a>
                        </div>
                    </div>

            </div>
        </div>
    </div>
</div>
@endsection
