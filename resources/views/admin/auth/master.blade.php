<!DOCTYPE html>
<html lang="en">
<head>
    @php($all_settings=allSetting())
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>@if(isset($all_settings['app_title'])){{$all_settings['app_title']}}@endif :: @yield('title')</title>
    <!-- main stylesheet css -->
    <link rel="shortcut icon" type="image/png" href="{{asset('user/')}}/images/favicon.png" alt="FX"/>
    <link rel="stylesheet" href="{{asset('assets/css')}}/style.css">
    <link href="{{asset('assets/toaster')}}/toastr.css" rel="stylesheet">
    <link type="text/css" rel="stylesheet" href="{{asset('assets/datepicker/datepicker3.css')}}">
    <script src="{{asset('assets/toaster/toastr.js')}}"></script>
{{--    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">--}}
</head>
<body>
    @yield('content')
    <!-- jQuery library -->
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Bootstrap JS -->
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
    <script src="{{asset('assets/datepicker/bootstrap-datepicker.js')}}"></script>

    <!-- Initialize Bootstrap functionality -->
    <script>
        // Initialize tooltip component
        $(function () {
            $('[data-toggle="tooltip"]').tooltip()
        })

        // Initialize popover component
        $(function () {
            $('[data-toggle="popover"]').popover()
        })
    </script>

    <script type="text/javascript">
        $('.datepicker').datepicker({
            todayBtn: "linked",
            keyboardNavigation: false,
            forceParse: false,
            autoclose: true,
            autoApply:false,
            format: "yyyy-mm-dd"
        });
    </script>

</body>
</html>

