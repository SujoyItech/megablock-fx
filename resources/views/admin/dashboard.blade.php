@extends('admin.layout.master',['menu'=>'dashboard'])
@section('title',__('Dashboard'))
@section('style')
@endsection
@section('content')
    <div class="main-wrapper">
        <div class="pageheader pd-t-25 pd-b-35">
            <div class="d-flex justify-content-between">
                <div class="pd-t-5 pd-b-5">
                    <h2 class="s-title"><img src="{{asset('user/')}}/images/icon-mega/h1.svg"alt="" width="35"> {{__('Dashboard')}}</h2>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('script')


@endsection
