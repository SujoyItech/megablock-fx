<?php
//=================================User Dashboard=============================
Route::get('user-dashboard','CustomerController@index')->name('userDashBoard');
Route::get('lang/{code}','CustomerController@setLang')->name('setLang');

//===================================Notification==================================================
Route::get('load-notification-body', function (){
    return view('user.layout.notification_body');
})->name('load-notification-body');

Route::get('see-all-notification', function (){ makeSeenNotification(); })->name('see-all-notification');

Route::get('clear-all-notification', function (){ makeClearNotification(); })->name('clear-all-notification');

//===================================User Profile==================================================
Route::get('user-profile/{mode?}','CustomerProfileController@profile')->name('userProfile');
Route::post('user-profile-update','CustomerProfileController@useProfileUpdate')->name('userProfileUpdate');
Route::post('user-profile-image-update','CustomerProfileController@userProfileImageUpdate')->name('userProfileImageUpdate');
Route::post('user-change-password','CustomerProfileController@userChangePassword')->name('userChangePassword');

Route::get('user-notification','CustomerProfileController@userNotification')->name('userNotification');
Route::post('user-timezone','CustomerProfileController@userTimeZone')->name('userTimeZone');
Route::get('user-payment-history','CustomerProfileController@userPaymentHistory')->name('userPaymentHistory');

//============================Broker=========================================
Route::get('user-broker','CustomerController@broker')->name('broker');
Route::get('user-broker-order','CustomerController@brokerByOrder')->name('brokerByOrder');
Route::get('user-broker/{key}','CustomerController@brokerByCountry')->name('brokerByCountry');

//===================Broker Like Dislike==================================
Route::get('broker-like/{brokerId?}','CustomerController@brokerLike');
Route::get('broker-unlike/{brokerId?}','CustomerController@brokerUnLike');
Route::get('broker-dislike/{brokerId?}','CustomerController@brokerDisLike');
Route::get('broker-unDisLike/{brokerId?}','CustomerController@brokerUnDisLike');

//=============================Signal====================================
Route::get('user-signal/{mode?}','CustomerController@signal')->name('signal');
Route::post('signal-details','CustomerController@signalDetails')->name('signalDetails');

//======================================Subscription=================================
Route::get('subscriptions','SubscriptionController@subscriptions')->name('subscriptions');
Route::get('package-subscription-{id}','SubscriptionController@packageSubscription')->name('packageSubscription');

//=====================================Payments=========================================
Route::get('pay-by-btc','SubscriptionController@payWithCoin')->name('payWithBTC');
Route::get('pay-by-coin', 'SubscriptionController@payWithCoin')->name('payWithCoin');
Route::get('stripe-gateway', 'StripePaymentController@stripeGateway')->name('stripeGateway');
Route::post('stripe-submit', 'StripePaymentController@stripePost')->name('stripePost');

//===============================Tools==============================
Route::get('tools/{code}','CustomerController@tools')->name('tools');
Route::get('news-market-widget-show','CustomerController@newsMarketTool')->name('newsMarketTool');
Route::get('trader-sentiment-show','CustomerController@traderSentimentTool')->name('newsMarketTool');

//=============================Term Condition=======================================
Route::get('about-platform','CustomerController@aboutPlatform')->name('aboutPlatform');
Route::get('faqs','CustomerController@faqs')->name('faqs');
Route::get('how-it-works','CustomerController@howItWorks')->name('howItWorks');
Route::get('term-condition','CustomerController@termCondition')->name('termCondition');

//===================================Learning Center ==================================
Route::get('learning-center','CustomerController@learningCenter')->name('learningCenter');


