<?php
Route::fallback(function(){
    return response()->view('errors.404');
});


Route::get('/getftp', 'MegaController@getFTP')->name('getftp');
Route::get('/gethtmlcontent', 'MegaController@getHtmlContent')->name('gethtmlcontent');



Route::get('/login', 'AuthController@login')->name('login');
Route::get('/', 'AuthController@login')->name('login');
Route::post('/post-login', 'AuthController@postLogin')->name('postLogin');
Route::get('/logout', 'AuthController@logout')->name('logout');

//User Sign Up
Route::get('user-sign-up/{code?}','AuthController@userSignUp')->name('userSignUp');
Route::get('user-info-check','AuthController@userInfoCheck')->name('userInfoCheck');
//Route::get('user-sign-up-{code}','AuthController@userSignUpByCode')->name('userSignUpByCode');
Route::post('sign-up-save','AuthController@userSignUpSave')->name('userSignUpSave');
Route::get('email-verify/{code}/{parentId?}','AuthController@userVerifyEmail')->name('userVerifyEmail');


//forgot password
Route::get('user-forget-password', 'AuthController@userForgetPassword')->name('userForgetPassword');
Route::post('user-forget-password-process', 'AuthController@userForgetPasswordProcess')->name('userForgetPasswordProcess');
Route::get('user-forget-password-change/{reset_token}', 'AuthController@userForgetPasswordChange')->name('userForgetPasswordChange');
Route::post('user-forget-password-reset-process', 'AuthController@userForgetPasswordResetProcess')->name('userForgetPasswordResetProcess');

//Social Login
Route::get('login/{driver}', 'Auth\LoginController@redirectToProvider')->name('socialLogin');
Route::get('login/{driver}/callback', 'Auth\LoginController@handleProviderCallback');
Route::post('update-missing-data', 'Customer\CustomerProfileController@updateMissingData')->name('updateMissingData');

Route::get('qqq','MegaController@creditDisbursment')->name('qqq');
