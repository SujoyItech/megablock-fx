<?php

/*Route::middleware('auth:api')->post('/user', function (Request $request) {
    return $request->user();
});*/

Route::get('testForRifat','MegaBlockAPI@testForRifat'); // Test for rifat

Route::get('get-country-list','MegaBlockAPI@getCountryList');
Route::post('login','AuthController@login');
Route::post('reset-password','AuthController@resetPassword');
Route::post('registration','AuthController@registration');


Route::post('social-login', 'AuthController@socialLogin')->name('social-login');
Route::post('social-registration', 'AuthController@socialRegistration')->name('social-registration');


Route::group(['middleware' => ['auth:api', 'user.api','user']], function (){

    Route::post('logout','AuthController@logout');

    Route::get('get-credits','MegaBlockAPI@getCredits');
    Route::get('get-profile','MegaBlockAPI@getUserProfileInformation');
    Route::post('change-profile','MegaBlockAPI@changeProfileInformation');
    Route::post('change-password','MegaBlockAPI@changePassword');
    Route::get('get-user-payment-history','MegaBlockAPI@getUserPaymentHistory');

    Route::get('get-brokers','BrokersAPI@getBrokerForMobileApp');
    Route::post('action-brokers','BrokersAPI@brokerLikeAction');

    Route::get('get-signal/{id}','SignalsAPI@getSignal');
    Route::get('get-signals/{status?}','SignalsAPI@getSignalList');
    Route::get('get-platform-results','SignalsAPI@getPlatformResult');

    Route::get('get-credit-packages','SubscriptionAPI@getSubscriptionPackages');
    Route::post('buy-package','SubscriptionAPI@buySubscriptionPackage');

    /********** Written by sujoy ***********/

});
/*******************************/
/********** For Jose ***********/

Route::get('get-closed-signal/{limit?}/{order?}','SignalsAPI@getLastClosedSignals');
Route::get('get-broker-list','BrokersAPI@getBrokerForJose');


/********** Written by sujoy ***********/
