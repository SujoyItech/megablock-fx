<?php
Route::get('/admin', 'AdminController@adminDashboard')->name('adminDashboard');
Route::get('profile', 'AdminController@profile')->name('profile');
Route::post('update-profile', 'AdminController@updateProfile')->name('updateProfile');
Route::post('change-password', 'AdminController@changePassword')->name('changePassword');
Route::get('admin/lang/{code}','AdminController@setLang')->name('setAdminLang');

// admin settings routes
Route::get('settings/{mode?}', 'SettingsController@adminSettings')->name('adminSettings');
Route::post('basic-settings-save', 'SettingsController@adminBasicSettingsSave')->name('adminBasicSettingsSave');
Route::post('currency-settings-save', 'SettingsController@adminCurrencySettingsSave')->name('adminCurrencySettingsSave');
Route::post('mobile-app-settings-save', 'SettingsController@adminMobileAppSettingsSave')->name('adminMobileAppSettingsSave');
Route::post('image-upload-save', 'SettingsController@adminImageUploadSave')->name('adminImageUploadSave');
Route::post('basic-settings-save', 'SettingsController@adminBasicSettingsSave')->name('adminBasicSettingsSave');
Route::post('about-settings-save', 'SettingsController@adminAboutSettingsSave')->name('adminAboutSettingsSave');
Route::post('payments-settings-save', 'SettingsController@adminPaymentSettingsSave')->name('adminPaymentSettingsSave');
Route::get('contact-support-setting/{mode?}','SettingsController@adminContactSupportSetting')->name('adminContactSupportSetting');
Route::post('contact-support-setting-save','SettingsController@adminContractSupportSettingsSave')->name('adminContractSupportSettingsSave');

//Referral Setting
Route::get('referral-add', 'SettingsController@adminReferralSettings')->name('referralBonusAdd');
Route::post('referral-bonus', 'SettingsController@adminReferralSettingsSave')->name('referralBonusSave');

//============================FAQS=====================================================
Route::get('faq-list', 'SettingsController@faqList')->name('faqList');
Route::get('faq-add', 'SettingsController@faqAdd')->name('faqAdd');
Route::get('faq-edit-{id}', 'SettingsController@faqEdit')->name('faqEdit');
Route::get('faq-details-{id}', 'SettingsController@faqDetails')->name('faqDetails');
Route::post('faq-save', 'SettingsController@faqSave')->name('faqSave');
Route::get('faq-delete-{id}', 'SettingsController@faqDelete')->name('faqDelete');


Route::get('subscriber-list', 'AdminController@subscriberList')->name('subscriberList');
Route::get('subscriber-change-status-{id}', 'AdminController@subscriberChangeStatus')->name('subscriberChangeStatus');
Route::get('subscriber-delete-{id}', 'AdminController@subscriberDelete')->name('subscriberDelete');

//User Management Routes
Route::get('admin-list', 'UserManagementController@adminUserList')->name('adminUserList');
Route::get('users-list', 'UserManagementController@userList')->name('userList');
Route::get('user-add', 'UserManagementController@adminUserAdd')->name('adminUserAdd');
Route::get('user-edit-{id}', 'UserManagementController@adminUserEdit')->name('adminUserEdit');
Route::get('user-details-{id}', 'UserManagementController@adminUserDetails')->name('adminUserDetails');
Route::post('user-save', 'UserManagementController@adminUserSave')->name('adminUserSave');
Route::get('user-delete-{id}', 'UserManagementController@adminUserDelete')->name('adminUserDelete');
Route::get('user-change-status-{id}', 'UserManagementController@adminUserChangeStatus')->name('adminUserChangeStatus');
Route::get('user-active-{id}', 'UserManagementController@adminUserActive')->name('adminUserActive');
Route::get('user-block-{id}', 'UserManagementController@adminUserBlock')->name('adminUserBlock');
Route::get('user-email-verify-{id}', 'UserManagementController@adminUserEmailVerify')->name('adminUserEmailVerify');

// user role management
Route::get('roles', 'UserManagementController@adminRoleList')->name('adminRoleList');
Route::get('role-add', 'UserManagementController@adminRoleAdd')->name('adminRoleAdd');
Route::get('role-edit-{id}', 'UserManagementController@adminRoleEdit')->name('adminRoleEdit');
Route::post('role-save', 'UserManagementController@adminRoleSave')->name('adminRoleSave');
Route::get('role-delete-{id}', 'UserManagementController@adminRoleDelete')->name('adminRoleDelete');

//Financial instrument codes
Route::get('financial-instrument-code', 'SignalCategoryController@financialInstrumentCodeList')->name('financialInstrumentCodeList');
Route::get('financial-instrument-code-add', 'SignalCategoryController@financialInstrumentCodeAdd')->name('financialInstrumentCodeAdd');
Route::get('financial-instrument-code-edit-{id}', 'SignalCategoryController@financialInstrumentCodeEdit')->name('financialInstrumentCodeEdit');
Route::get('financial-instrument-code-details-{id}', 'SignalCategoryController@financialInstrumentCodeDetails')->name('financialInstrumentCodeDetails');
Route::post('financial-instrument-code-save', 'SignalCategoryController@financialInstrumentCodeSave')->name('financialInstrumentCodeSave');
Route::get('financial-instrument-code-delete-{id}', 'SignalCategoryController@financialInstrumentCodeDelete')->name('financialInstrumentCodeDelete');

//trade type
Route::get('trade-type', 'SignalCategoryController@tradeTypeList')->name('tradeTypeList');
Route::get('trade-type-add', 'SignalCategoryController@tradeTypeAdd')->name('tradeTypeAdd');
Route::get('trade-type-edit-{id}', 'SignalCategoryController@tradeTypeEdit')->name('tradeTypeEdit');
Route::get('trade-type-details-{id}', 'SignalCategoryController@tradeTypeDetails')->name('tradeTypeDetails');
Route::post('trade-type-save', 'SignalCategoryController@tradeTypeSave')->name('tradeTypeSave');
Route::get('trade-type-delete-{id}', 'SignalCategoryController@tradeTypeDelete')->name('tradeTypeDelete');

//order type
Route::get('order-type', 'SignalCategoryController@orderTypeList')->name('orderTypeList');
Route::get('order-type-add', 'SignalCategoryController@orderTypeAdd')->name('orderTypeAdd');
Route::get('order-type-edit-{id}', 'SignalCategoryController@orderTypeEdit')->name('orderTypeEdit');
Route::get('order-type-details-{id}', 'SignalCategoryController@orderTypeDetails')->name('orderTypeDetails');
Route::post('order-type-save', 'SignalCategoryController@orderTypeSave')->name('orderTypeSave');
Route::get('order-type-delete-{id}', 'SignalCategoryController@orderTypeDelete')->name('orderTypeDelete');

//Licence
Route::get('licence', 'LicenceController@licenceList')->name('licenceList');
Route::get('licence-add', 'LicenceController@licenceAdd')->name('licenceAdd');
Route::get('licence-edit-{id}', 'LicenceController@licenceEdit')->name('licenceEdit');
Route::get('licence-details-{id}', 'LicenceController@licenceDetails')->name('licenceDetails');
Route::post('licence-save', 'LicenceController@licenceSave')->name('licenceSave');
Route::get('licence-delete-{id}', 'LicenceController@licenceDelete')->name('licenceDelete');

//Broker
Route::get('broker', 'BrokerController@brokerList')->name('brokerList');
Route::get('broker-add', 'BrokerController@brokerAdd')->name('brokerAdd');
Route::get('broker-edit-{id}', 'BrokerController@brokerEdit')->name('brokerEdit');
Route::get('broker-details-{id}', 'BrokerController@brokerDetails')->name('brokerDetails');
Route::post('broker-save', 'BrokerController@brokerSave')->name('brokerSave');
Route::get('broker-delete-{id}', 'BrokerController@brokerDelete')->name('brokerDelete');
Route::get('order', 'BrokerController@Order')->name('Order');

//Signal
Route::get('signal', 'SignalController@signalList')->name('signalList');
Route::get('signal-add', 'SignalController@signalAdd')->name('signalAdd');
Route::get('signal-edit-{id}', 'SignalController@signalEdit')->name('signalEdit');
Route::get('signal-details-{id}', 'SignalController@signalDetails')->name('signalDetails');
Route::post('signal-save', 'SignalController@signalSave')->name('signalSave');
Route::get('signal-delete-{id}', 'SignalController@signalDelete')->name('signalDelete');

//Subscription Package
Route::get('subscription-package', 'SubscriptionPackageController@subscriptionPackageList')->name('subscriptionPackageList');
Route::get('subscription-package-add', 'SubscriptionPackageController@subscriptionPackageAdd')->name('subscriptionPackageAdd');
Route::get('subscription-package-edit-{id}', 'SubscriptionPackageController@subscriptionPackageEdit')->name('subscriptionPackageEdit');
Route::get('subscription-package-details-{id}', 'SubscriptionPackageController@subscriptionPackageDetails')->name('subscriptionPackageDetails');
Route::post('subscription-package-save', 'SubscriptionPackageController@subscriptionPackageSave')->name('subscriptionPackageSave');
Route::get('subscription-package-delete-{id}', 'SubscriptionPackageController@subscriptionPackageDelete')->name('subscriptionPackageDelete');
Route::get('subscription-package-histories', 'SubscriptionPackageController@subscriptionPackageHistories')->name('subscriptionPackageHistories');
//Route::get('payment-package-histories', 'SubscriptionPackageController@paymentPackageHistories')->name('paymentPackageHistories');
Route::get('payment-package-active-{id}', 'SubscriptionPackageController@paymentActive')->name('paymentActive');

//Mail
Route::get('/send-mail','MailController@sendMail')->name('sendMail');



