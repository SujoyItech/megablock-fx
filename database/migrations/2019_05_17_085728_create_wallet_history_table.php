<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWalletHistoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wallet_histories', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('user_id');
            $table->integer('package_id')->nullable();
            $table->bigInteger('referral_id')->nullable();
            $table->bigInteger('child_id')->nullable();
            $table->tinyInteger('type');
            $table->string('coin_address')->nullable();
            $table->string('coin_type')->nullable();
            $table->tinyInteger('status')->default(0);
            $table->text('description');
            $table->float('amount')->default(0);
            $table->float('coin_amount',15,5)->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('wallet_histories');
    }
}
