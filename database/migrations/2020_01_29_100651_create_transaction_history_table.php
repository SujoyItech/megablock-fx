<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransactionHistoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transaction_history', function (Blueprint $table) {
            $table->integer('user_id')->comment = 'user who made the purchase' ;
            $table->string('event_slug')->nullable()->comment= 'e.g. \'subscription_packages\'. (name of the purchasing item table)';
            $table->integer('event_id')->nullable()->comment= 'e.g. value of `subscription_packages_id`';
            $table->enum('payment_source',['API','WEB'])->default('WEB');
            $table->string('payment_gateway',50)->nullable()->comment= 'e.g. stripe';
            $table->string('payment_method',50)->nullable()->comment= 'e.g. card';
            $table->longText('response')->nullable()->comment= 'full transaction respone';
            $table->string('status',20)->nullable()->comment= 'succeed/failed/pending/...';
            $table->dateTime('created_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transaction_history');
    }
}
