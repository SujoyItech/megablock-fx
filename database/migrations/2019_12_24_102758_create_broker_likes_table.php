<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBrokerLikesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('broker_likes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('broker_id');
            $table->integer('user_id');
            $table->boolean('like');
            $table->boolean('unlike');
            $table->timestamps();
            $table->unique(['broker_id', 'user_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('broker_likes');
    }
}
