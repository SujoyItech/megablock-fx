<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name',191)->nullable();
            $table->string('first_name',191)->nullable();
            $table->string('last_name',191)->nullable();
            $table->date('dob')->nullable();
            $table->string('phone',191)->nullable();
            $table->string('country',191)->nullable();
            $table->string('language',191)->nullable();
            $table->string('photo',191)->nullable();
            $table->string('email',191)->unique();
            $table->string('password',191)->nullable();
            $table->tinyInteger('active_status')->default(1);
            $table->integer('level')->unsigned()->nullable();
            $table->boolean('notification')->default(1);
            $table->string('time_zone',50)->default('UTC');
            $table->integer('role')->default(2);
            $table->tinyInteger('email_verified')->default(0);
            $table->string('reset_token')->nullable();
            $table->string('google_code')->nullable();
            $table->tinyInteger('terms_condition')->default(1);
            $table->tinyInteger('risk_warning')->default(1);
            $table->tinyInteger('adult_warning')->default(1);
            $table->string('own_code',15)->nullable();
            $table->string('used_code',15)->nullable();
            $table->boolean('subscribed_once')->default(0);
            $table->rememberToken();
            $table->timestamps();
        });
//        Schema::table('users', function($table) {
//            $table->foreign('role')->references('id')->on('roles');
//        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
