<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSignalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('signals', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->boolean('from_statement')->default(true)->comment= 'true means from signal statement';
            //$table->integer('financial_instrument_code');
            $table->string('financial_instrument_code',8)->nullable()->comment= 'gbpeur';
            $table->enum('action',['buy','sell'])->default('buy');
            $table->decimal('entry_price',19,8);
            $table->decimal('closed_price',19,8);

            $table->decimal('take_profit_1',19,8);
            $table->decimal('stop_loss_1',19,8);

            $table->decimal('take_profit_2',19,8)->nullable()->default(null);
            $table->decimal('stop_loss_2',19,8)->nullable()->default(null);

            $table->decimal('take_profit_3',19,8)->nullable()->default(null);
            $table->decimal('stop_loss_3',19,8)->nullable()->default(null);

            $table->decimal('take_profit_4',19,8)->nullable()->default(null);
            $table->decimal('stop_loss_4',19,8)->nullable()->default(null);

            $table->boolean('available_for_free')->default(0);
            $table->text('comments')->nullable();
            $table->integer('signal_result')->nullable();
            $table->integer('trade_type')->nullable();
            $table->integer('order_type')->nullable();
            $table->tinyInteger('status')->default(0);
            $table->string('statement_batch',10)->nullable()->default(0);
            $table->timestamps();
            $table->timestamp('closed_on');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('signals');
    }
}
