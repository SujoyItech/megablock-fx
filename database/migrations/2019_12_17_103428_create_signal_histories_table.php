<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSignalHistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('signal_histories', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('signal_id');
            $table->integer('financial_instrument_code');
            $table->bigInteger('ticket')->nullable();
            $table->integer('trade_type');
            $table->integer('order_type');
            $table->tinyInteger('action');
            $table->decimal('entry_price',19,8);

            $table->decimal('take_profit_1',19,8);
            $table->decimal('stop_loss_1',19,8);

            $table->decimal('take_profit_2',19,8)->nullable()->default(null);
            $table->decimal('stop_loss_2',19,8)->nullable()->default(null);

            $table->decimal('take_profit_3',19,8)->nullable()->default(null);
            $table->decimal('stop_loss_3',19,8)->nullable()->default(null);

            $table->decimal('take_profit_4',19,8)->nullable()->default(null);
            $table->decimal('stop_loss_4',19,8)->nullable()->default(null);

            $table->boolean('available_for_free')->default(0);
            $table->text('comments')->nullable();
            $table->integer('signal_result')->nullable();

            $table->tinyInteger('status')->default(0);
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('signal_histories');
    }
}
