<?php

use App\Models\AdminSetting;
use Illuminate\Database\Seeder;

class AdminSettingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        AdminSetting::create(['slug' => 'app_title', 'value' => 'MegaBlock']);
        AdminSetting::create(['slug' => 'logo', 'value' => '']);
        AdminSetting::create(['slug' => 'app_logo', 'value' => '']);
        AdminSetting::create(['slug' => 'login_logo', 'value' => '']);
        AdminSetting::create(['slug' => 'favicon', 'value' => '']);
        AdminSetting::create(['slug' => 'copyright_text', 'value' => 'Copyright@2019']);
        //General Settings
        AdminSetting::create(['slug' => 'lang', 'value' => 'en']);
        AdminSetting::create(['slug' => 'currency', 'value' => 'USD']);
        AdminSetting::create(['slug' => 'primary_email', 'value' => 'info@email.com']);
        AdminSetting::create(['slug' => 'address', 'value' => 'address here']);
        AdminSetting::create(['slug' => 'phone', 'value' => 'phone no here']);
        AdminSetting::create(['slug' => 'about_us', 'value' => 'about us']);
        AdminSetting::create(['slug' => 'privacy_policy', 'value' => 'privacy policy']);
        AdminSetting::create(['slug' => 'terms_conditions', 'value' => 'terms and conditions']);
        AdminSetting::create(['slug' => 'on_register', 'value' => 20]);
        AdminSetting::create(['slug' => 'on_register_and_purchase', 'value' => 30]);
        AdminSetting::create(['slug' => 'registers_through_affiliate', 'value' => 20]);
        AdminSetting::create(['slug' => 'affiliate_owner', 'value' => 10]);
        AdminSetting::create(['slug' => 'standard_registration', 'value' => 10]);


    }
}
