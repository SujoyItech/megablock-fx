<?php

use App\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create(
            [
                'name' => 'Mr. Admin',
                'first_name' => 'Mr',
                'last_name' => 'Admin',
                'active_status' => 1,
                'email' => 'admin@email.com',
                'password' => bcrypt('123456'),
                'role' => USER_ROLE_ADMIN
            ]
        );
        User::create(
            [
                'name' => 'Mr. Customer',
                'first_name' => 'Mr',
                'last_name' => 'Customer',
                'active_status' => 1,
                'email' => 'customer@email.com',
                'password' => bcrypt('123456'),
                'role' => USER_ROLE_USER
            ]
        );
    }
}
