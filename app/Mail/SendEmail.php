<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SendEmail extends Mailable
{
    use Queueable, SerializesModels;
    public $subject,$message,$key,$email,$parentId;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($subject,$data)
    {
        //
        $this->parentId = $data['parentId'];
        $this->subject = $subject;
        $this->message = $data['message'];
        $this->email = $data['email'];
        $this->key = $data['key'];
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $e_subject = $this->subject;
        $e_message = $this->message;
        $email = $this->email;
        $key = $this->key;
        $parentId = $this->parentId;
        return $this->view('admin.mail.mailTemplate',compact('e_message','key','email','parentId'))->subject($e_subject);
    }
}
