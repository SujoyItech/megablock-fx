<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SendPassResetEmail extends Mailable
{
    use Queueable, SerializesModels;
    public $subject,$name,$reset_token;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($subject,$data)
    {
        //
        $this->subject = $subject;
        $this->name = $data['name'];
        $this->reset_token = $data['reset_token'];
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $e_subject = $this->subject;
        $name = $this->name;
        $reset_token = $this->reset_token;
        return $this->view('user.mail.forgot-pass',compact('name','reset_token'))->subject($e_subject);
    }
}
