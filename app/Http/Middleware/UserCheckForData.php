<?php

namespace App\Http\Middleware;

use Closure;

class UserCheckForData
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if (check_field())
        return $next($request);
        else
            return redirect()->route('userInfoCheck');
    }
}
