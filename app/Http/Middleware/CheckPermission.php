<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class CheckPermission
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = Auth::user();
        if (empty(request()->route()->getName())) {
            $url = strtok($request->getRequestUri(), '?');
        } else {
            $url = request()->route()->getName();
        }

        if (checkRolePermission($url, $user->role)) {
            return $next($request);
        }
        Auth::logout();
        return redirect()->back()->with('dismiss','Permission denied');
    }
}
