<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class User {
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next) {
        $user = Auth::user();
        if (isset($user->id) && $user->role == USER_ROLE_USER && $user->active_status == STATUS_SUCCESS) {
            if ($user->email_verified == 1) {
                return $next($request);
            } else {
                Auth::logout();
                return redirect()->route('login')->with('dismiss', __('Your mail is not verified. please verify your email.'));
            }
        } else {
            Auth::logout();
            return redirect()->route('login')->with('dismiss', __('You are blocked. Contact with the support team.'));
        }
    }
}
