<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class UserApi {
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     *
     * @return mixed
     */
    public function handle($request, Closure $next) {
        $user = Auth::user();
        if ($request->acceptsJson()) {
            if ($user->active_status != STATUS_SUCCESS) {
                $access_token_id = $user->token()->id;
                DB::table('oauth_access_tokens')->where('id', $access_token_id)->delete();

                $data['message'] = __("You Are Not Authorised.");
                $data['success'] = FALSE;
                return response()->json($data, 401);
            }
        }
        return $next($request);
    }
}
