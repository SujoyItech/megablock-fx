<?php


namespace App\Http\Repositories;

use App\Http\Requests\SubscriptionPackageRequest;
use App\Models\SubscriptionPackage;
use Illuminate\Http\Request;

class SubscriptionPackageRepository
{
    public function create(SubscriptionPackageRequest $request)
    {
        $subscriptionPackage = [
            'title' => $request->get('title')
            ,'price' => $request->get('price')
            ,'credit' => $request->get('credit')
            ,'description' => $request->get('description')
            ,'fees' => $request->get('fees')
            ,'vat' => $request->get('vat')
            ,'status' => STATUS_SUCCESS
        ];

        if (!empty($request->image)) {
            $subscriptionPackage['image'] = fileUpload($request->image, getImagePath('subscription_package'), '','','');
        }
        $subscriptionPackage = SubscriptionPackage::create($subscriptionPackage);

        return $subscriptionPackage;
    }

    public function update(SubscriptionPackageRequest $request, $id)
    {
        $cat = $this->find($id);
        $subscriptionPackage = [
            'title' => $request->get('title')
            ,'price' => $request->get('price')
            ,'credit' => $request->get('credit')
            ,'is_free' => !empty($request->get('is_free')) ? $request->get('is_free') : 0
            ,'description' => $request->get('description')
            ,'fees' => $request->get('fees')
            ,'vat' => $request->get('vat')
        ];

        if (!empty($request->image)) {
            $subscriptionPackage['image'] = fileUpload($request->image, getImagePath('subscription_package'), '','','');
        }
        $subscriptionPackage = SubscriptionPackage::where(['id' => $id])->update($subscriptionPackage);

        return $subscriptionPackage;


    }

    public function find($id)
    {
        return SubscriptionPackage::find($id);
    }

}
