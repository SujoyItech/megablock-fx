<?php

namespace App\Http\Repositories;

use App\Http\Services\CommonService;
use App\Models\UserVerificationCode;
use App\Models\Wallet;
use App\Models\WalletHistory;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class UserAuthRepository
{
    public function userDataSave($request)
    {
        $random = Str::random(10);
        $date = str_replace('/', '-', $request->input('dob'));
        $date = Carbon::parse($date)->format('Y-m-d');
        $data = [
            'name' => $request->first_name . ' ' . $request->last_name,
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'email' => $request->email,
            'dob' => $date,
            'phone' => $request->phone,
            'country' => $request->country,
            'reset_token' => md5($request->get('email') . uniqid() . randomString(5)),
            'remember_token' => md5($request->get('email') . uniqid() . randomString(5)),
            'password' => bcrypt($request->password),
            'active_status' => STATUS_PENDING,
            'own_code' => $random,
            'used_code' => $request->code != null ? $request->code : null
        ];
        return User::create($data);
    }

    public function userSocialDataSave($userSocial)
    {
        $random = Str::random(10);
        $name = $userSocial->user['name'];
        $parts = explode(" ", $name);
        if (count($parts) > 1) {
            $lastname = array_pop($parts);
            $firstname = implode(" ", $parts);
        } else {
            $firstname = $name;
            $lastname = " ";
        }
        $profile_contents = file_get_contents($userSocial->avatar_original);
        $image_name = md5($userSocial->user['email']) . '.jpg';

        $user = [
            'name' => $name,
            'first_name' => $firstname,
            'last_name' => $lastname,
            'email' => $userSocial->user['email'],
            'password' => bcrypt('1234'),
            'reset_token' => md5($userSocial->user['email'] . uniqid() . randomString(5)),
            'remember_token' => md5($userSocial->user['email'] . uniqid() . randomString(5)),
            'email_verified' => STATUS_SUCCESS,
            'active_status' => STATUS_SUCCESS,
            'role' => USER_ROLE_USER,
            'photo' => $image_name,
            'own_code' => $random,
        ];
        file_put_contents(getImagePath('user') . '/' . $image_name, $profile_contents);
        return User::create($user);
    }

    public function userMailVerification($user, $parentId = null)
    {
        $mail_key = randomNumber(6);
        UserVerificationCode::create(['user_id' => $user->id, 'code' => $mail_key, 'type' => 1, 'status' => STATUS_PENDING, 'expired_at' => date('Y-m-d', strtotime('+15 days'))]);
        $userName = $user->name;
        $userEmail = $user->email;
        $subject = __('Mega Block Email Verification.');

        $userData['message'] = __('Hello! ') . $userName . __(' Please Verify Your Email.');
        $userData['key'] = $mail_key;
        $userData['email'] = $userEmail;
        $userData['parentId'] = $parentId;
        sendMail($userEmail, $userData, $subject);
    }

    public function userAllInformationSave($request, $user)
    {
        $request->code != null ? $parent_user_id = $this->userParentId($request->code) : $parent_user_id = null;
        DB::table('user_informations')->insert([
            'user_id' => $user->id,
            'parent_user_id' => $parent_user_id
        ]);
    }

    public function userParentId($code)
    {
        return User::where('own_code', $code)->first()->id;
    }

    public function userProfileUpdate(Request $request)
    {
        $date = str_replace('/', '-', $request->input('dob'));
        $date = Carbon::parse($date)->format('Y-m-d');
        $user = [
            'name' => $request->first_name . ' ' . $request->last_name,
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'dob' => $date,
            'phone' => $request->phone,
            'country' => $request->country,
            'used_code' => $request->code
        ];
        $service = new CommonService();
        $user_id = $service->checkValidId($request->edit_id);
        return User::where('id', $user_id)->update($user);

    }

    public function affiliateOwnerWallet($parentId, $userId)
    {
        $affiliate_owner_parent_wallet = Wallet::where('user_id', $parentId)->first()->balance;
        $total_affiliate_owner_wallet = $affiliate_owner_parent_wallet + allSetting('affiliate_owner');
        Wallet::where('user_id', $parentId)->update(['balance' => $total_affiliate_owner_wallet]);
        $affiliate_owner_parent_wallet_histories = [
            'user_id' => $parentId,
            'type' => REFERRAL,
            'description' => __('Bonus For Affiliate Owner'),
            'amount' => allSetting('affiliate_owner'),
            'child_id' => $userId
        ];
        WalletHistory::create($affiliate_owner_parent_wallet_histories);
    }

    public function affiliateUserWallet($parentId, $userId)
    {
        $user_wallet = Wallet::where('user_id', $userId)->first();
        if (!empty($user_wallet)) {
            $user_wallet->update(['balance' => allSetting('registers_through_affiliate')]);
        } else {
            $user_wallet = [
                'user_id' => $userId,
                'balance' => allSetting('registers_through_affiliate')
            ];
            Wallet::create($user_wallet);
        }

        $user_wallet_histories = [
            'user_id' => $userId,
            'referral_id' => $parentId,
            'type' => REFERRAL,
            'description' => __('Bonus For Register Through Affiliate.'),
            'amount' => allSetting('registers_through_affiliate'),
        ];
        WalletHistory::create($user_wallet_histories);
    }

    public function userStandardRegistrationWallet($data)
    {
        $user_wallet = Wallet::where('user_id', $data['userId'])->first();
        if (!empty($user_wallet)) {
            $user_wallet->update(['balance' => $data['balance']]);
        } else {
            $user_wallet = [
                'user_id' => $data['userId'],
                'balance' => $data['balance']
            ];
            Wallet::create($user_wallet);
        }
        $user_wallet_histories = [
            'user_id' => $data['userId'],
            'type' => $data['type'],
            'description' => $data['description'],
            'amount' => $data['balance'],
        ];
        WalletHistory::create($user_wallet_histories);
    }

    public function passwordChange(Request $request, $user_id)
    {
        $response['status'] = false;
        $response['message'] = __('Invalid Request.');
        $user = User::find($user_id);
        if ($user) {
            $old_password = $request['old_password'];
            if (Hash::check($old_password, $user->password)) {
                $user->password = bcrypt($request['password']);
                $user->save();

                $affected_row = $user->save();

                if (!empty($affected_row)) {
                    $response['status'] = true;
                    $response['message'] = __('Password Changed Successfully.');
                }
            } else {
                $response['status'] = false;
                $response['message'] = __('Incorrect old Password!');
            }
        } else {
            $response['status'] = false;
            $response['message'] = __('Invalid User.');
        }

        return $response;
    }

}
