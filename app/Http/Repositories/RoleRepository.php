<?php

namespace App\Http\Repositories;


use App\Models\Role;
use Illuminate\Http\Request;

class RoleRepository
{

    public function lists()
    {
        $role = Role::select('roles.*');
        return $role;
    }

    public function create(Request $request)
    {
        $role = [
            'title' => $request->get('title'),
            'actions' => '|' . implode('|', $request->get('tasks')) . '|',
            'status' => $request->get('status', 1)
        ];
        return Role::create($role);
    }

    public function update(Request $request, $id)
    {

        $role = [
            'title' => $request->get('title'),
            'actions' => '|' . implode('|', $request->get('tasks')) . '|',
            'status' => $request->get('status', 1)
        ];

        return Role::where(['id' => $id])->update($role);
    }
}