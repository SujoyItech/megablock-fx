<?php


namespace App\Http\Repositories;


use App\Http\Requests\BrokerRequest;
use App\Models\BrokerMarketsExclude;
use App\Models\BrokerLicence;
use App\Models\Broker;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class BrokerRepository
{
    public function create(BrokerRequest $request)
    {
        $broker = [
            'name' => $request->get('name')
            ,'advantage' => $request->get('advantage')
            ,'average_spreads' => $request->get('average_spreads')
            ,'recommended_deposit' => $request->get('recommended_deposit')
            ,'URL' => $request->get('URL')
        ];

        if (!empty($request->logo)) {
            $broker['logo'] = fileUpload($request->logo, getImagePath('brokers'), '','','');
        }
        $broker = Broker::create($broker);
        return $broker;

    }

    public function update(BrokerRequest $request, $id)
    {
        $cat = $this->find($id);
        $broker = [
            'name' => $request->get('name')
            ,'advantage' => $request->get('advantage')
            ,'average_spreads' => $request->get('average_spreads')
            ,'recommended_deposit' => $request->get('recommended_deposit')
            , 'URL'=>$request->get('URL')
        ];
        if (!empty($request->logo)) {
            $broker['logo'] = fileUpload($request->logo,getImagePath('brokers'), $cat->logo, '', '');
        }
        $broker = Broker::where(['id' => $id])->update($broker);

        $licences =  $request->licences;
        $markets = $request->markets;


        DB::beginTransaction();
        try {
            $broker_delete = BrokerLicence::where(['broker_id'=>$id])->delete();
            foreach ($licences as $licence)
            {
                $broker_licence = ['broker_id'=>$id,'licence_id'=>$licence];
                BrokerLicence::create($broker_licence);
            }
            BrokerMarketsExclude::where(['broker_id'=>$id])->delete();
            foreach ($markets as $market)
            {
                $broker_market = ['broker_id'=>$id,'country_key'=>$market];
                BrokerMarketsExclude::create($broker_market);
            }
            DB::commit();
            // all good
        } catch (\Exception $e) {
            DB::rollback();
            // something went wrong
        }
        return $broker;
    }

    public function find($id)
    {
        return Broker::find($id);
    }



}
