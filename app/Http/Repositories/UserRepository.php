<?php

namespace App\Http\Repositories;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class UserRepository
{
    public function adminUsers()
    {
        $user = User::leftJoin('roles', 'roles.id', '=', 'users.role')
            ->where('users.id', '<>', Auth::user()->id)
            ->where('users.role', '=', USER_ROLE_ADMIN)
            ->select('users.*', 'roles.title as role');
        return $user;
    }

    public function create(Request $request)
    {
        $random = Str::random(10);
        $user = [
            'name'=>$request->get('first_name').' '.$request->get('last_name')
            ,'first_name' => $request->get('first_name')
            ,'last_name' => $request->get('last_name')
            , 'email' => $request->get('email')
            ,'country' => $request->get('country')
            ,'phone' => $request->get('phone')
            ,'dob' => $request->get('dob')
            , 'password' => bcrypt($request->get('password'))
            , 'reset_token' => md5($request->get('email') . uniqid() . randomString(5))
            , 'remember_token' => md5($request->get('email') . uniqid() . randomString(5))
            , 'active_status' => $request->get('active_status')
            , 'role' => $request->get('role')
            ,'email_verified' => STATUS_SUCCESS,
            'own_code' => $random
        ];

        return User::create($user);
    }

    public function update(Request $request, $id)
    {
        $user = [
            'name'=>$request->get('first_name').' '.$request->get('last_name')
            ,'first_name' => $request->get('first_name')
            ,'last_name' => $request->get('last_name')
            , 'email' => $request->get('email')
            ,'country' => $request->get('country')
            ,'phone' => $request->get('phone')
            ,'dob' => $request->get('dob')
            , 'role' => $request->get('role')
            ,'active_status'=> $request->get('active_status')
        ];
        return User::where(['id' => $id])->update($user);
    }

    public function find($id)
    {
        return User::find($id);
    }

    public function profileUpdate($request, $user_id)
    {
        try {
            $response['data'] = [];
            $response['status'] = false;
            $response['message'] = __('Invalid request');
            $user = User::find($user_id);
            $userData = [];
            if ($user) {
                $userData = [
                    'name' => $request['name'],
                ];
                if (!empty($request['role'])) {
                    $userData['role'] = $request['role'];
                }
                //                if (!empty($request['country'])) {
                $userData['country'] = $request['country'];
                //                }
                //                if (!empty($request['address'])) {
//                $userData['address'] = $request['address'];
                //                }
                if (!empty($request['state'])) {
                    $userData['state'] = $request['state'];
                }
                if (!empty($request['zip'])) {
                    $userData['zip'] = $request['zip'];
                }
                if (!empty($request['language'])) {
                    $userData['language'] = $request['language'];
                }
                //                if (!empty($request['phone'])) {
                $userData['phone'] = $request['phone'];
                //                }
                if (!empty($request['city'])) {
                    $userData['city'] = $request['city'];
                }
                if (!empty($request['photo'])) {
                    $old_img = '';
                    if (!empty($user->photo)) {
                        $old_img = $user->photo;
                    }
                    $userData['photo'] = fileUpload($request['photo'], getImagePath('user'), $old_img);
                }

//                dd($userData);
                $affected_row = User::where('id', $user_id)->update($userData);
                if ($affected_row) {
//                    $response['user'] = $this->userProfile($user_id)['user'];
                    $response['status'] = true;
                    $response['message'] = __('Profile updated successfully.');
                }

            } else {
                $response['status'] = false;
                $response['message'] = __('Invalid user.');
            }
        } catch(\Exception $e) {
            $response['status'] = false;
            $response['message'] = __('Something went wrong.');

        }

        return $response;
    }

    public function passwordChange($request, $user_id)
    {
        $response['status'] = false;
        $response['message'] = __('Invalid Request.');
        $user = User::find($user_id);
        if ($user) {
            $old_password = $request['old_password'];
            if (Hash::check($old_password, $user->password)) {
                $user->password = bcrypt($request['password']);
                $user->save();

                $affected_row = $user->save();

                if (!empty($affected_row)) {
                    $response['status'] = true;
                    $response['message'] = __('Password Changed Successfully.');
                }
            } else {
                $response['status'] = false;
                $response['message'] = __('Incorrect old Password!');
            }
        } else {
            $response['status'] = false;
            $response['message'] = __('Invalid User.');
        }

        return $response;
    }
}
