<?php

namespace App\Http\Repositories;


use App\Models\Category;
use Illuminate\Http\Request;

class CategoryRepository
{
    public function select($id=null)
    {
        if(empty($id)){
            $category = Category::whereNull('parent_id')->select('categories.*');
        }else{
            $category = Category::where(['parent_id'=>$id])->select('categories.*');
        }

        return $category;
    }

    public function create(Request $request)
    {
        $category = [
            'title' => $request->get('title')
            ,'parent_id' => $request->get('parent_id',null)
            , 'status' => $request->get('status', 1)
        ];
        if (!empty($request->image)) {
//            $category['image'] = uploadimage($request->image, getImagePath(), '', 40, 30);
            $category['image'] = fileUpload($request->image, getImagePath(), '','','');
        }
        return Category::create($category);
    }

    public function update(Request $request, $id)
    {
        $cat = $this->find($id);
        $category = [
            'title' => $request->get('title')
            ,'parent_id' => $request->get('parent_id',null)
            , 'status' => $request->get('status', $cat->status)
        ];
        if (!empty($request->image)) {
//            $category['image'] = uploadimage($request->image, getImagePath(), $cat->image, 40, 30);
            $category['image'] = fileUpload($request->image, getImagePath(), $cat->image, '', '');
        }
        return Category::where(['id' => $id])->update($category);
    }

    public function find($id)
    {
        return Category::find($id);
    }
}
