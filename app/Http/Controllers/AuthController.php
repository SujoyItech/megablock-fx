<?php

namespace App\Http\Controllers;
use App\Http\Requests\Customer\LoginRequest;
use App\Http\Requests\Customer\RegistrationRequest;
use App\Http\Requests\ForgetPasswordRequest;
use App\Http\Requests\ForgotPasswordResetRequest;
use App\Http\Requests\ProfileUpdateRequest;
use App\Http\Requests\UserLoginRequest;
use App\Http\Services\AuthService;
use App\Http\Services\CommonService;
use App\Http\Services\UserAuthService;
use App\Models\UserAffiliateCode;
use App\Models\UserVerificationCode;
use App\Models\Wallet;
use App\Models\WalletHistory;
use App\User;
use Carbon\Carbon;
use Illuminate\Contracts\Session\Session;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class AuthController extends Controller {
    // Login view
    public function login() {
        if (Auth::user()) {
            if (auth::user()->role == USER_ROLE_ADMIN) {
                return redirect()->route('adminDashboard');
            } elseif (auth::user()->role == USER_ROLE_USER) {
                return redirect()->route('userDashBoard');
            }
        } else {
            return view('admin.auth.login');
        }
    }

    // Login post
    public function postLogin(UserLoginRequest $request) {
        if (Auth::attempt(['email' => $request->email, 'password' => $request->password])) {
            if (auth::user()->role == USER_ROLE_ADMIN) {
                return redirect()->route('adminDashboard');
            } elseif (auth::user()->role == USER_ROLE_USER && auth::user()->active_status == STATUS_SUCCESS) {
                if (auth::user()->email_verified == STATUS_SUCCESS) {
                    return redirect()->route('userDashBoard');
                } else {

                    $mail_key = randomNumber(6);
                    UserVerificationCode::where(['user_id' => auth::user()->id])->update(['status' => STATUS_SUCCESS]);
                    UserVerificationCode::create(['user_id' => auth::user()->id, 'code' => $mail_key, 'type' => 1, 'status' => STATUS_PENDING, 'expired_at' => date('Y-m-d', strtotime('+15 days'))]);
                    $used_code = Auth::user()->used_code;
                    if(!empty($used_code))
                    {
                        $parentId = User::where('own_code',$used_code)->first()->id;
                    }else
                    {
                        $parentId = null;
                    }
                    $userName = auth::user()->name;
                    $userEmail = auth::user()->email;
                    $subject = __('Mega Block Email Verification.');
                    $userData['message'] = 'Hello! ' . $userName . ' Please Verify Your Email.';
                    $userData['key'] = $mail_key;
                    $userData['email'] = $userEmail;
                    $userData['parentId'] = $parentId;
                    sendMail($userEmail, $userData, $subject);
                    $data['message'] = __('Your email is not verified. Please verify your email to get full access.');
                    Auth::logout();
                    return redirect()->route('login')->with(['success' => $data['message']]);
                }

            } elseif (auth::user()->active_status == STATUS_SUSPENDED) {
                Auth::logout();
                return redirect()->route('login')->with(['dismiss' => __('Your Account has been suspended. please contact support team to active again!')]);
            } elseif (auth::user()->active_status == STATUS_DELETED) {
                Auth::logout();
                return redirect()->route('login')->with(['dismiss' => __('Your Account has been deleted. please contact support team to active again!')]);
            } elseif (auth::user()->active_status == STATUS_PENDING) {
                Auth::logout();
                return redirect()->route('login')->with(['dismiss' => __('Your request is pending. Contact with the support team.')]);
            }
            elseif (auth::user()->active_status == STATUS_BLOCKED) {
                Auth::logout();
                return redirect()->route('login')->with(['dismiss' => __('You are blocked. Contact with the support team.')]);
            }
        } else {

            return redirect()->route('login')->with(['dismiss' => __('Email or Password Not matched!')]);
        }

    }

    //---- done
    public function logout(Request $request) {
        $request->session()->flush();
        Auth::logout();
        return redirect()->route('login');
    }

    /*
     * forgetPassword
     *
     * Forget Password page
     *
     *
     *
     */
    public function forgetPassword() {
        $data['pageTitle'] = __('Forget Password');
        return view('admin.auth.forget_pass', $data);
    }

    /*
     * forgetPasswordProcess
     *
     * Forget Password Process
     *
     *
     *
     */

    public function forgetPasswordProcess(ForgetPasswordRequest $request) {
        $response = app(UserAuthService::class)->sendForgotPasswordMail($request);
        if (isset($response)) {
            if (isset($response['success']) && ($response['success'] == true)) {
                return redirect()->route('login')->with('success', $response['message']);
            } else {
                return redirect()->back()->with('dismiss', $response['message']);
            }
        } else {
            return redirect()->back()->with('dismiss', __('Something went wrong.'));
        }

    }

    /*
     * forgetPasswordReset
     *
     * Password reset page
     *
     *
     *
     */

    public function forgetPasswordReset() {
        $data['pageTitle'] = __('Reset Password');
        return view('auth.forgetpassreset', $data);
    }

    /*
     * forgetPasswordChange
     *
     * Change the forgotten password
     *
     *
     *
     */

    public function forgetPasswordChange($reset_code) {
        $data['pageTitle'] = __('Reset Password');
        $data['reset_code'] = $reset_code;
        $user = User::where(['reset_code' => $reset_code])->first();
        if ($user) {
            return view('auth.reset_pass', $data);
        } else {
            return redirect()->route('login')->with(['dismiss' => __('Invalid request!')]);
        }
    }

    /*
     * forgetPasswordResetProcess
     *
     * Reset process of forgotten password
     *
     *
     *
     */

    public function forgetPasswordResetProcess(ForgotPasswordResetRequest $request, $reset_code) {
        if ($reset_code) {
            $response = app(UserAuthService::class)->forgetPasswordChangeProcess($request, $reset_code);
            if (isset($response['success']) && ($response['success'] == true)) {
                return redirect()->route('login')->with('success', $response['message']);
            } else {
                return redirect()->route('login')->with('dismiss', $response['message']);
            }
        } else {
            return redirect()->back()->with(['dismiss' => __('Code not found.')]);
        }
    }

    /*
     * verifyEmail
     *
     * Verify email code
     *
     *
     *
     */

    public function verifyEmail($code) {

        if (isset($code)) {
            $response = app(AuthService::class)->mailVarification($code);
            if (isset($response)) {
                if (isset($response['success']) && ($response['success'] == true)) {
                    return redirect()->route('login')->with('success', $response['message']);
                } else {
                    return redirect()->route('login')->with('dismiss', $response['message']);
                }
            } else {
                return redirect()->route('login')->with('dismiss', __('Something went wrong.'));
            }
        } else {
            return redirect()->route('login')->with(['dismiss' => __('Verification Code Not found!')]);
        }
    }

    //User SignUp
    public function userSignUp($code = null) {
        $data['code'] = $code;
        return view('user.auth.register', $data);
    }

    public function userInfoCheck() {
        return view('user.auth.user-info-check');
    }

    public function userSignUpByCode($code) {
        $data['code'] = $code;
        return view('user.auth.register', $data);
    }

    // User Sign Up Save
    public function userSignUpSave(RegistrationRequest $request) {
        $service = new UserAuthService();
        if (!empty($request->code)) {
            $parentId = $service->userParentId($request->code);
            if (isset($parentId) && !empty($parentId)) {
                $response = $service->userSignUpProcess($request, $parentId);
            } else {
                return redirect()->back()->with('dismiss', 'Reference Code Not valid.Please input correct code!');
            }
        } else {
            $response = $service->userSignUpProcess($request);
        }

        if (isset($response)) {
            if (isset($response['success']) && ($response['success'] == true)) {
                return redirect()->route('login')->with('success', $response['message']);
            } else {
                return redirect()->route('login')->with('dismiss', $response['message']);
            }
        } else {
            return redirect()->route('login')->with('dismiss', __('Something went wrong!'));
        }
    }


    public function userVerifyEmail($code, $parentId = null) {
        $service = new UserAuthService();
        $response = $service->userVerifyEmail($code, $parentId);
        if (isset($response)) {
            if (isset($response['status']) && ($response['status'] == true)) {
                return redirect()->route('login')->with('success', $response['message']);
            } else {
                return redirect()->route('login')->with('dismiss', $response['message']);
            }
        } else {
            return redirect()->route('login')->with('dismiss', $response['message']);
        }
    }

    public function userLogout(Request $request) {
        $request->session()->flush();
        Auth::logout();
        return redirect()->route('login');
    }


    // Forgot Password
    public function userForgetPassword() {
        $data['pageTitle'] = __('Forget Password');
        return view('user.auth.forget_pass', $data);
    }


    public function userForgetPasswordProcess(ForgetPasswordRequest $request) {
        $user_auth_service = new UserAuthService();
        $response = $user_auth_service->sendForgotPasswordMail($request);
        if (isset($response)) {
            if (isset($response['success']) && ($response['success'] == true)) {
                return redirect()->route('login')->with('success', $response['message']);
            } else {
                return redirect()->back()->with('dismiss', $response['message']);
            }
        } else {
            return redirect()->back()->with('dismiss', __('Something went wrong!'));
        }
    }

    public function userForgetPasswordChange($reset_token) {
        $data['pageTitle'] = __('Reset Password');
        $data['reset_token'] = $reset_token;
        $user = User::where(['reset_token' => $reset_token])->first();
        if ($user) {
            return view('user.auth.reset_pass', $data);
        } else {
            return redirect()->route('login')->with(['dismiss' => __('Invalid request!')]);
        }
    }
    public function userForgetPasswordResetProcess(ForgotPasswordResetRequest $request)
    {
        if ($request->reset_token) {
            $response = app(UserAuthService::class)->forgetPasswordChangeProcess($request);
            if (isset($response['success']) && ($response['success'] == true)) {
                return redirect()->route('login')->with('success', $response['message']);
            } else {
                return redirect()->route('login')->with('dismiss', $response['message']);
            }
        } else {
            return redirect()->back()->with(['dismiss' => __('Code not found!')]);
        }
    }

}
