<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Admin\NotificationController;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Sunra\PhpSimple\HtmlDomParser;

class MegaController extends Controller {
    public function __construct() {
        //-----
    }

    /**
     * Wallet-credit decrement process
     * Per credit per day
     * running schedule - DAILY
     */
    public static function creditDisbursment() {
        DB::table('wallets')
          ->whereNotIn('balance', ['0'])
          ->orWhereNull('updated_at')
          ->decrement('balance', 1, ['updated_at' => Carbon::now()]);
    }

    /**************************************************************************/
    public function getFTP(){
        $data_arr = [
            'message'           => 'hi nirjhar',
            'title'             => 'greetings',
            'id'                => '{any signal id}',
            'is_background'     => TRUE,
            'content_available' => TRUE,
        ];
        $notification_arr = [
            'body' => 'Content Hidden',
            'title' => 'greetings',
        ];
        $dd = pushNotificationToAndroidApp('00000000-54b3-e7c7-0000-000046bffd97', $data_arr, $notification_arr);

        dd($dd);
        /*$conn_id = ftp_connect('ftp.autotexfl.com');
        $login_result = ftp_login($conn_id, 'automibu', 'v2YFrnTPlI0gu24CIBo9');
        $contents = ftp_nlist($conn_id, ".");
        dump($contents);*/

        /*$start = microtime(TRUE);
        $file = Storage::disk('nirjhar_ftp')->has('/public_html/statements/Statement_11069683_full.html');
        if($file){
            $content = Storage::disk('nirjhar_ftp')->get('/public_html/statements/Statement_11069683_full.html');
        }

        $time = microtime(TRUE) - $start;
        dd($time);*/



    }
    /**************************************************************************/
    /**
     * Read Statement and process the operation
     */
    public function getHtmlContent() {
        $start = microtime(TRUE);
        ini_set('max_execution_time', 300);
        define('MAX_FILE_SIZE', 6000000);
        $nowstring = Carbon::now()->timestamp;
        /**********************************************************************************************/
        $file = public_path('statements/Statement_11069683_full.html');
        $file_contents = file_get_contents($file);
        $dom = HtmlDomParser::str_get_html($file_contents);
        /**********************************************************************************************/
        $table_elems = $dom->find('table');
        $items = [];
        $status = SIGNAL_ACTIVE;
        foreach ($table_elems as $table_item) {
            $tr_elems = $dom->find('tr');
            foreach ($tr_elems as $r => $tr_items) {
                $td_elems = $tr_items->find('td');
                $first_td_elem = $td_elems[0]->plaintext;
                if ((count($td_elems) == 14 && is_numeric($first_td_elem))
                    || $first_td_elem == 'Closed Transactions:' //close signal
                    || $first_td_elem == 'Open Trades:' //active signal
                    || $first_td_elem == 'Working Orders:' //pending signal
                ) {
                    if ($first_td_elem == 'Closed Transactions:') {
                        $status = SIGNAL_CLOSED;
                    } else if ($first_td_elem == 'Open Trades:') {
                        $status = SIGNAL_ACTIVE;
                    } else if ($first_td_elem == 'Working Orders:') {
                        $status = SIGNAL_PENDING;
                    } else {
                        $items[$r]['id'] = $td_elems[0]->plaintext;
                        $items[$r]['created_at'] = $td_elems[1]->plaintext;
                        $items[$r]['closed_on'] = $td_elems[8]->plaintext;
                        $items[$r]['action'] = $td_elems[2]->plaintext;
                        $items[$r]['financial_instrument_code'] = $td_elems[4]->plaintext;
                        $items[$r]['entry_price'] = $td_elems[5]->plaintext;
                        $items[$r]['closed_price'] = $td_elems[9]->plaintext;
                        //$items[$r]['closing_price'] = $td_elems[9]->plaintext;
                        $items[$r]['take_profit_1'] = $td_elems[7]->plaintext;
                        $items[$r]['stop_loss_1'] = $td_elems[6]->plaintext;
                        $items[$r]['signal_result'] = $this->calculateSignalResult($td_elems[5]->plaintext, $td_elems[9]->plaintext);
                        $items[$r]['status'] = $status;
                        $items[$r]['statement_batch'] = $nowstring;
                    }
                }
                if ($r == 30) break;
            }
        }
        /**********************************************************************************************/

        $affected = $this->insertOrUpdate($items, $nowstring);
        if($affected > 0){
            $this->sendNotification($nowstring);
        }
        $time = microtime(TRUE) - $start;
        dd($affected, $time);
        /**********************************************************************************************/
    }

    /**
     * @param $entry_price
     * @param $closing_price
     *
     * @return int
     */
    public function calculateSignalResult($entry_price, $closing_price) {
        $entry_number = str_replace('.', '', $entry_price);
        $closing_number = str_replace('.', '', $closing_price);
        return $pips = (int)$closing_number - (int)$entry_number;
    }

    /**
     * @param $table
     * @param $rows
     * @param array $exclude
     *
     * @return string
     */
    public function insertOrUpdate($items, $nowstring) {
        $columns = array_keys($items[array_key_first($items)]);
        $columnsString = "(`" . implode('`,`', $columns) . "`)";
        $values = '';
        foreach ($items as $row) {
            $values .= "('" . implode("','", $row) . "'),";
        }
        $values = rtrim($values, ',');
        $updates = "`entry_price` = VALUES(`entry_price`),
                    `closed_price` = VALUES(`closed_price`),
                    `signal_result` = VALUES(`signal_result`),
                    `status` = VALUES(`status`)";
        $query = "INSERT INTO `signals` {$columnsString} VALUES {$values} ON DUPLICATE KEY UPDATE {$updates}";
        return DB::affectingStatement($query);
    }

    /**
     * @param $now
     *
     * @throws \Pusher\PusherException
     */
    public function sendNotification($nowstring) {
        //DB::enableQueryLog();
        $inserted_data = DB::table('signals')
                    ->select('signals.*', DB::raw(SIGNAL_CREATED.' AS new'))
                    ->where('statement_batch', "{$nowstring}");
        $updated_data = DB::table('signals')
                    ->select('signals.*', DB::raw(SIGNAL_UPDATED.' AS new'))
                    ->where('from_statement', '=', 1)
                    ->where('updated_at', '>', DB::raw('date_sub(now(), INTERVAL 4 MINUTE)'))->union($inserted_data);
        $affected_data = $updated_data->get();
        //dd(DB::getQueryLog(), $updated_data);
        foreach ($affected_data as $data){
            $notification_data[] = array(
                'type' => $data->new,
                'signal_id' => $data->id,
                'financial_code' => substr_replace(strtoupper($data->financial_instrument_code), '/', 3, 0),
                'signal_action' => $data->action,
                'signal_status' => $data->status,
                'signal_result' => $data->signal_result,
                'available_for_free' => $data->available_for_free
            );
        }
        NotificationController::createMultipleNotification($notification_data);
    }

}

