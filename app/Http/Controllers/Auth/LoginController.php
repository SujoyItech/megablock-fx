<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Services\AuthService;
use App\User;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Laravel\Socialite\Facades\Socialite;
use Session;

class LoginController extends Controller {
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = 'user-dashboard';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('guest')->except('logout');
    }

    /**
     * Redirect the user to the GitHub authentication page.
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function redirectToProvider($driver) {
        return Socialite::driver($driver)->redirect();
    }

    /**
     * Obtain the user information from GitHub.
     *
     * @return \Illuminate\Http\Response
     */
    public function handleProviderCallback($driver) {
        $userSocial = Socialite::driver($driver)->stateless()->user();
        //dd($driver,$userSocial);
        $user = User::where('email', $userSocial->user['email'])->first();

        if ($user) {
            //LOGIN
            if (Auth::loginUsingId($user->id)) {
                return redirect()->route('userDashBoard');
            }
        } else {
            // REGISTRATION
            $service = new AuthService();
            $user = $service->userSocialCreateAccount($userSocial);
            if (Auth::loginUsingId($user->id)) {
                return redirect()->route('userDashBoard');
            }
        }
    }
}
