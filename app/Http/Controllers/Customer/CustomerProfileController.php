<?php

namespace App\Http\Controllers\Customer;

use App\Http\Controllers\Controller;
use App\Http\Repositories\UserAuthRepository;
use App\Http\Repositories\UserRepository;
use App\Http\Requests\ChangePasswordRequest;
use App\Http\Requests\ImageUploadRequest;
use App\Http\Requests\ProfileUpdateRequest;
use App\Http\Services\UserAuthService;
use App\Http\Services\UserService;
use App\Models\SubscriptionHistories;
use App\Models\UserAffiliateCode;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Pusher\Pusher;

class CustomerProfileController extends Controller {
    //
    public function updateMissingData(ProfileUpdateRequest $request) {
        $service = new UserAuthService();
        $parentId = null;
        if (!empty($request->code)) {
            $parentId = $service->userParentId($request->code);
            if ($parentId == null) {
                return redirect()->back()->with('dismiss', 'Code not valid.Please enter correct code or keep it empty.');
            }
        }
        $response = $service->userProfileMissingDataUpdate($request, $parentId);
        if (isset($response)) {
            if (isset($response['success']) && ($response['success'] == true)) {
                return redirect()->route('userDashBoard')->with('success', $response['message']);
            } else {
                return redirect()->route('userDashBoard')->with('dismiss', $response['message']);
            }
        } else {
            return redirect()->back()->with('dismiss', __('Something went wrong!'));
        }
    }

    public function profile($mode = 'profile') {
        $data = [];
        $data['users'] = User::where('id',Auth::user()->id)->first();
        $data['payment_histories'] = DB::table('subscription_histories')
            ->select('subscription_histories.id', 'subscription_histories.credit',
                'subscription_histories.status', 'subscription_packages.title', 'subscription_packages.price',
                'subscription_packages.credit', 'subscription_histories.created_at')
            ->leftJoin('subscription_packages', 'subscription_histories.package_id', '=', 'subscription_packages.id')
            ->where('subscription_histories.user_id', Auth::user()->id)
            ->orderBy('subscription_histories.id', 'desc')
            ->get();
        $data['tab'] = $mode;
        return view('user.profile.profile', $data);
    }

    public function useProfileUpdate(ProfileUpdateRequest $request) {
        $service = new UserAuthService();
        $response = $service->userProfileUpdate($request);
        if (isset($response)) {
            if (isset($response['success']) && ($response['success'] == true)) {
                return redirect()->back()->with('success', $response['message']);
            } else {
                return redirect()->back()->with('dismiss', $response['message']);
            }
        } else {
            return redirect()->back()->with('dismiss', __('Something went wrong!'));
        }

    }

    public function userProfileImageUpdate(ImageUploadRequest $request) {
        $service = new UserAuthService();
        $response = $service->userProfileImageUpdate($request);
        if (isset($response)) {
            if (isset($response['success']) && ($response['success'] == true)) {
                return redirect()->back()->with('success', $response['message']);
            } else {
                return redirect()->back()->with('dismiss', $response['message']);
            }
        } else {
            return redirect()->back()->with('dismiss', __('Something went wrong!'));
        }
    }

    public function userChangePassword(ChangePasswordRequest $request) {
        $userRepository = new UserAuthRepository();
        $response = $userRepository->passwordChange($request, Auth::user()->id);

        if ($response['status'] == false) {
            return redirect()->back()->withInput()->with('dismiss', $response['message']);
        } else {
            return redirect()->back()->withInput()->with('success', $response['message']);
        }
    }

    public function userNotification() {
        $service = new UserAuthService();
        $response = $service->updateUserNotification();

        if (isset($response['success']) && ($response['success'] == true)) {
            return redirect()->back()->with('success', $response['message']);
        } else {
            return redirect()->back()->with('dismiss', $response['message']);
        }
    }

    public function userTimeZone(Request $request) {
        $service = new UserAuthService();
        $response = $service->updateUserTimeZone($request);
        if (isset($response['success']) && ($response['success'] == true)) {
            return redirect()->back()->with('success', $response['message']);
        } else {
            return redirect()->back()->with('dismiss', $response['message']);
        }
    }

    public function userPaymentHistory() {
        $service = new UserService();
        $response = $service->userPaymentHistories();
        if (isset($response['status']) && $response['status'] == true) {

        }
    }
}
