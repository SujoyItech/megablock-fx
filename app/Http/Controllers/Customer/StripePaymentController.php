<?php

namespace App\Http\Controllers\Customer;

use App\Http\Controllers\Controller;
use App\Models\SubscriptionPackage;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use function GuzzleHttp\Promise\all;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use Session;
use Stripe;


class StripePaymentController extends Controller {

    /**
     * success response method.
     *
     * @return \Illuminate\Http\Response
     */
    public function stripeGateway(Request $request) {
        $id = decrypt($request->id);
        $data['package_details'] = SubscriptionPackage::find($id);
        $html = '';
        $html .= View::make('user.widgets.stripe', $data);
        return response()->json(['data' => $html]);
        return view('stripe');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws Stripe\Exception\ApiErrorException
     */
    public function stripePost(Request $request) {
        $all_settings = allSetting();
        $packege_id = decrypt($request->package);
        $packege = SubscriptionPackage::find($packege_id);
        //STRIPE_KEY=pk_test_kP4Cw2qzx4ZlcenhH0cZh326
        //STRIPE_SECRET=sk_test_4LSjGzR2wNzPFLlXV44mo6Jx
        DB::beginTransaction();
        try {
            Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));
            $stripe_data = [
                "amount" => intval($packege->price),
                "currency" => isset($all_settings['currency']) && $all_settings['currency'] == CURRENCY_EURO ? "eur":"usd",
                "source" => $request->stripeToken,
                "description" => __('payment from megablock subscription')
            ];
            //dd($stripe_data);
            $res = Stripe\Charge::create($stripe_data);
            if ($res->status == 'succeeded') {
                $data = [
                    'user_id' => Auth::user()->id,
                    'event_slug' => 'Subscription_Packages',
                    'event_id' => $packege_id,
                    'payment_source' => 'WEB',
                    'payment_gateway' => 'stripe',
                    'payment_method' => 'card',
                    'response' => $res,
                    'status' => $res->status
                ];
                DB::table('transaction_history')->insert($data);
                Session::flash('success', __('Payment successful!'));
                app(SubscriptionController::class)->packageSubscription($request->package);
                DB::commit();
            } else {
                Session::flash('dismiss', __('Something went wrong.'));
            }
        }catch (\Exception $exception){
            Session::flash('dismiss', __('Something went wrong.'));
        }
        return back();
    }
}
