<?php

namespace App\Http\Controllers\Customer;

use App\Http\Controllers\Controller;
use App\User;
use App\Models\Broker;
use App\Models\BrokerLike;
use App\Models\Faq;
use App\Models\Notification;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\View;

class CustomerController extends Controller {
    //
    public function index() {
        $user_reg_date = Auth::user()->created_at;
        $data['pips'] = DB::table('signals')
                          ->select(
                              DB::raw("SUM(signals.signal_result) AS total_pips"),
                              DB::raw("SUM(CASE WHEN signals.created_at > '" . $user_reg_date . "' THEN signals.signal_result ELSE 0 END) AS after_registration"),
                              DB::raw("SUM(CASE WHEN signals.created_at BETWEEN DATE_SUB(NOW(), INTERVAL 30 DAY) AND NOW() THEN signals.signal_result ELSE 0 END) AS last_30")
                          )
                          ->where('status', '=', SIGNAL_CLOSED)->get();
//       dd($data['pips']);
        $data['signals'] = $this->getSignalData('', 10);
        return view('user.dashboard', $data);
    }

    public function setLang($code) {
        $user_lang_update = User::where('id', Auth::user()->id)->update(['language' => $code]);
        if ($user_lang_update) {
            return redirect()->back()->with(['success' => __('Language Changed Successfully.')]);
        } else {
            return redirect()->back()->with(['dismiss' => __('Language Change Failed.')]);
        }
    }

//============================Tools================================
    public function tools($code) {
       if ($code == 'charts'){
            return view('user.dashboard_content.dashboard_maps.charts');
        }
    }
    public function newsMarketTool(){
        return view('user.dashboard_content.dashboard_chats.news_feed');
    }
    public function traderSentimentTool(){
        return view('user.dashboard_content.dashboard_chats.trader_sentiment');
    }
//=============================Support===============================
    public function aboutPlatform() {
        $data['about_platform'] = allSetting('about_platform_text');;
        return view('user.term_condition.about_platform', $data);
    }

    public function faqs() {
        $faqs = Faq::all();
        return view('user.term_condition.faqs', compact('faqs'));
    }

    public function howItWorks() {
        $data['how_works_text'] = allSetting('how_works_text');
        return view('user.term_condition.how_it_works', $data);
    }

    public function termCondition() {
        $data['term_condition_text'] = allSetting('term_condition_text');
        return view('user.term_condition.term_condition', $data);
    }

//================================Brokers=================================
    public function broker() {
        $brokers = Broker::withCount('brokerLike')
                         ->where('status', '!=', 6)
                         ->withCount('brokerDislike')
                         ->orderBy('order', 'asc')
                         ->paginate(10);

        return view('user.broker.broker', compact('brokers'));
    }

    public function brokerByCountry($key) {
        $brokers = DB::table('brokers')
                     ->where('status', '!=', 6)
                     ->whereNotIn('id', DB::table('broker_markets_excludes')
                                          ->where('country_key', $key)
                                          ->pluck('broker_id'))
                     ->orderBy('name', 'asc')
                     ->paginate(10);
        $country_key = $key;
        return view('user.broker.broker', compact('brokers', 'country_key'));
    }

    public function brokerByOrder(Request $request) {
        $order = request('name');
        $key = request('country');
        $vote = request('vote');
        $brokers = Broker::where('status', '!=', 6)
                         ->whereNotIn('id', DB::table('broker_markets_excludes')
                                              ->where('country_key', $key)
                                              ->pluck('broker_id'))
                         ->withCount('brokerLike')
                         ->withCount('brokerDislike');

        if (!empty($vote) && ($vote == VOTED)) {
            $brokers = $brokers->orderBy('broker_like_count', 'desc');
            $brokers = $brokers->orderBy('broker_dislike_count', 'asc');
        } else if (!empty($vote) && ($vote == UN_VOTED)) {
            $brokers = $brokers->orderBy('broker_dislike_count', 'desc');
            $brokers = $brokers->orderBy('broker_like_count', 'asc');
        }
        if (!empty($order))
            $brokers = $brokers->orderBy('name', 'asc');

        $brokers = $brokers->paginate(10);
        $country_key = $key;
        return view('user.broker.broker', compact('brokers', 'country_key', 'order', 'vote'));
    }

    public function brokerLike($brokerId) {

        $user_id = Auth::user()->id;
        $is_exist = BrokerLike::where(['user_id' => $user_id, 'broker_id' => $brokerId])->first();
        if (isset($is_exist)) {
            $broker = BrokerLike::where(['broker_id' => $brokerId, 'user_id' => $user_id])
                                ->update(['like' => 1, 'unlike' => 0]);
        } else {
            $like = [
                'broker_id' => $brokerId,
                'user_id'   => $user_id,
                'like'      => 1,
                'unlike'    => 0
            ];
            $broker = BrokerLike::create($like);
        }
        $broker_like = BrokerLike::where(['broker_id' => $brokerId, 'like' => 1])->count();
        $broker_unlike = BrokerLike::where(['broker_id' => $brokerId, 'unlike' => 1])->count();
        if ($broker)
            return response()->json(['broker_like' => $broker_like, 'broker_unlike' => $broker_unlike], 200);

        return response()->json(['msg' => __('No result found!')], 404);

    }

    public function brokerUnLike($brokerId) {

        $user_id = Auth::user()->id;
        $is_exist = BrokerLike::where(['user_id' => $user_id, 'broker_id' => $brokerId])->first();
        if (isset($is_exist)) {
            $broker = BrokerLike::where(['broker_id' => $brokerId, 'user_id' => $user_id])->update(['like' => 0]);
        }
        $broker_like = BrokerLike::where(['broker_id' => $brokerId, 'like' => 1])->count();
        $broker_unlike = BrokerLike::where(['broker_id' => $brokerId, 'unlike' => 1])->count();
        if ($broker)
            return response()->json(['broker_like' => $broker_like, 'broker_unlike' => $broker_unlike], 200);

        return response()->json(['msg' => __('No result found!')], 404);

    }

    public function brokerDisLike($brokerId) {

        $user_id = Auth::user()->id;
        $is_exist = BrokerLike::where(['user_id' => $user_id, 'broker_id' => $brokerId])->first();
        if (isset($is_exist)) {
            $broker = BrokerLike::where(['broker_id' => $brokerId, 'user_id' => $user_id])
                                ->update(['unlike' => 1, 'like' => 0]);
        } else {
            $dislike = [
                'broker_id' => $brokerId,
                'user_id'   => $user_id,
                'like'      => 0,
                'unlike'    => 1
            ];
            $broker = BrokerLike::create($dislike);
        }
        $broker_like = BrokerLike::where(['broker_id' => $brokerId, 'like' => 1])->count();
        $broker_unlike = BrokerLike::where(['broker_id' => $brokerId, 'unlike' => 1])->count();
        if ($broker)
            return response()->json(['broker_like' => $broker_like, 'broker_unlike' => $broker_unlike], 200);

        return response()->json(['msg' => __('No result found!')], 404);
    }

    public function brokerUnDisLike($brokerId) {

        $user_id = Auth::user()->id;
        $is_exist = BrokerLike::where(['user_id' => $user_id, 'broker_id' => $brokerId])->first();
        if (isset($is_exist)) {
            $broker = BrokerLike::where(['broker_id' => $brokerId, 'user_id' => $user_id])->update(['unlike' => 0]);
        }
        $broker_like = BrokerLike::where(['broker_id' => $brokerId, 'like' => 1])->count();
        $broker_unlike = BrokerLike::where(['broker_id' => $brokerId, 'unlike' => 1])->count();
        if ($broker)
            return response()->json(['broker_like' => $broker_like, 'broker_unlike' => $broker_unlike], 200);

        return response()->json(['msg' => __('No result found!')], 404);
    }
//================================Brokers=================================

//================================Signals=========================================
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function signal(Request $request, $mode = 'all') {
        $data = [];
        $signals = $this->getSignalData('', '', $mode);
        $data['signals'] = $signals;
        $data['mode'] = $mode;
        $html = '';
        if ($request->ajax()) {
            if (isset($signals) && !empty($signals)) {
                foreach ($signals as $index => $signal) {
                    $sig_data['signal'] = $signal;
                    $sig_data['index'] = $index;
                    $html .= View::make('user.signal.signal_card', $sig_data);
                }
            } else {
                $html .= `<h4 class="text-center">{{__('No signal available')}}</h4>`;
            }
            return $html;
        } else {
            return view('user.signal.signal', $data);
        }

    }

    /**
     * @param Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function signalDetails(Request $request) {
        if ($request->notification_id != 0) {
            DB::table('notifications')->where('id', $request->notification_id)->update(['status' => READ_NOTIFICATION]);
        }
        $data['signals'] = $this->getSignalData($request->signal);
        return view('user.signal.signal_modal', $data);
    }

    /**
     * @param string $signal_id
     *
     * @return \Illuminate\Database\Eloquent\Model|\Illuminate\Database\Query\Builder|\Illuminate\Support\Collection|object|null
     */
    public function getSignalData($signal_id = '', $limit = '', $mode = '') {
//        if (!empty(allSetting('mobile_app_signal_per_page'))){
//            $signal_per_page = allSetting('mobile_app_signal_per_page');
//        }else{
//            $signal_per_page = 12;
//        }
        $signal_per_page = 12;
        $data = DB::table('signals');
        $data->select('signals.id',
            'signals.from_statement', 'signals.financial_instrument_code AS financial_instrument_key_code',
            DB::raw("UPPER(concat(substring(signals.financial_instrument_code,1,3), '/', substring(signals.financial_instrument_code,4,6))) AS financial_instrument_code"),
            'trade_types.title AS trade_type', 'order_types.title AS order_type',
            'signals.action', 'signals.entry_price',
            'signals.take_profit_1', 'signals.stop_loss_1', 'signals.take_profit_2', 'signals.stop_loss_2',
            'signals.take_profit_3', 'signals.stop_loss_3', 'signals.take_profit_4', 'signals.stop_loss_4',
            'signals.available_for_free',
            'signals.comments', 'signals.signal_result', 'signals.status', 'signals.created_at', 'signals.updated_at');
        $data->leftJoin('trade_types', 'signals.trade_type', '=', 'trade_types.id');
        $data->leftJoin('order_types', 'signals.order_type', '=', 'order_types.id');
        $data->where('signals.status', '<>', STATUS_DELETED);
        if ($signal_id != '') {
            $data->where('signals.id', $signal_id);
        }
        if ($limit != '') {
            $data->limit($limit);
        }
        $data->orderBy('signals.created_at', 'desc');
        if ($signal_id != '') {
            return $data->first();
        } else {
            if ($mode == 'all') {
                return $data->paginate($signal_per_page);
            } else if ($mode == 'active') {
                return $data->where('signals.status', 1)->paginate($signal_per_page);
            } else if ($mode == 'pending') {
                return $data->where('signals.status', 0)->paginate($signal_per_page);
            } else if ($mode == 'closed') {
                return $data->where('signals.status', 3)->paginate($signal_per_page);
            } else {
                return $data->get();
            }

        }
    }

//=====================================Signals=========================================

    /**
     * Learning Center
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */

    public function learningCenter() {
        return view('user.learning-center.learning_center');
    }


}
