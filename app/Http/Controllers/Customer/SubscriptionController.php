<?php

namespace App\Http\Controllers\Customer;

use App\Http\Controllers\Controller;
use App\Http\Services\CoinService;
use App\Http\Services\SubscriptionService;
use Hexters\CoinPayment\Helpers\CoinPaymentFacade;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\View;

class SubscriptionController extends Controller {
    private $service;
    private $coin;

    /**
     * SubscriptionController constructor.
     */
    public function __construct() {
        $this->service = new SubscriptionService();
        $this->coin = new CoinService();
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function subscriptions(){
        $data['packages'] = $this->service->getAllPackages()['packages'];
        return view('user.subscriptions.index',$data);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function packageSubscription($id, $package_id_from_api = ''){
        if($package_id_from_api != ''){
            $id = $package_id_from_api;
        }else{
            $id = decrypt($id);
        }
        try{
            $package_subscription = $this->service->subscriptionPackage($id);
            $status = !empty($package_subscription['status']) ?'success':'dismiss';
            return redirect()->back()->with($status,$package_subscription['message']);
        }catch (\Exception $exception){
            return redirect()->back()->with('dismiss', __('Something went wrong.'));
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function payWithCoin(Request $request){
        $id = decrypt($request->id);
        $data['status'] = false;
        $data['message'] = __('Something went wrong');
        try{
            $package_subscription = $this->coin->payWithCoin($id);
            $data['status'] = $package_subscription['success'];
            if ($data['status']){
                $data_['coin_address'] = $package_subscription['data']->coin_address;
                $data_['coin_type'] = $package_subscription['data']->coin_type;
                $data_['coin_amount'] = $package_subscription['data']->coin_amount;
                $data['message'] = "";
                $data['message'] .= View::make('user.widgets.coinpayment',$data_);
            }
        }catch (\Exception $exception){
            return response()->json($data);
        }
        return response()->json($data);
    }
}
