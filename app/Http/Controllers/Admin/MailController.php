<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

class MailController extends Controller
{
    //
    public function sendMail()
    {
        $data['subject'] = 'Demo Subject';
        $data['message'] = 'Demo Message';
        $email = "laravelItech@gmail.com";
        sendMail($email,$data);
    }
}
