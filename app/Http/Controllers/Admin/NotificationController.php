<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Pusher\Pusher;

class NotificationController extends Controller {
    public function __construct() {
        //----
    }

    /**
     * Read from Signal service (single signal created/updated from admin panel)
     * @param array $notification_data
     * @throws \Pusher\PusherException
     */
    public static function createNewNotification($notification_data = []){
        $mb_not = [];
        if(!empty($notification_data)){
            $body_data = static::defineNotificationBody($notification_data);
            $mobile_body_data = static::triggerMobileNotification($notification_data);
            //dd($mobile_body_data);
            $users = static::getNotifyUsers();
            //$users = static::getNotifyUsers($notification_data['available_for_free']);
            $notification_data['title'] = $body_data['title'];
            $notification_data['message'] = $body_data['body'];
            if(!empty($users)){
                foreach ($users as $user) {
                    $notify_data[] = [
                        'user_id' => $user->id,
                        'data' => json_encode($notification_data),
                        'status' => 0,
                        'type' => $notification_data['type']
                    ];
                    if($user->device_tokens != null){
                        $device_tokens = explode('###', $user->device_tokens);
                        foreach ($device_tokens as $device_token){
                            $mb_not[] = static::pushNotificationToAndroidApp($device_token, $mobile_body_data['data_arr'], $mobile_body_data['notification_arr']);
                        }
                    }

                }
                DB::table('notifications')->insert($notify_data);
                $channel = 'signal_all';
                static::triggerNotification($channel, $body_data['title'], $body_data['body']);
                //static::getSignalResultTextForMobile($notification_data);
            }
        }
        //dump($users, $mb_not);
    }



    /**
     * Read from MegaController (from HTML statement parsing)
     * @param array $notification_data
     *
     * @throws \Pusher\PusherException
     */
    public static function createMultipleNotification($notification_datas = []){
        if(!empty($notification_datas)){
            $users = static::getNotifyUsers();
            foreach ($notification_datas as $notification_data){
                $notify_data = [];
                $body_data = static::defineNotificationBody($notification_data);
                $notification_data['title'] = $body_data['title'];
                $notification_data['message'] = $body_data['body'];
                if(!empty($users)){
                    foreach ($users as $user) {
                        $notify_data[] = [
                            'user_id' => $user,
                            'data' => json_encode($notification_data),
                            'status' => 0,
                            'type' => $notification_data['type']
                        ];
                    }
                    DB::table('notifications')->insert($notify_data);
                    $channel = 'signal_all';
                    static::triggerNotification($channel, $body_data['title'], $body_data['body']);
                }
            }

            // SEND NOTIFICATION TO MOBILE DEVICES
        }
    }

    /**
     * @param string $channel
     * @param string $title
     * @param string $body
     * @throws \Pusher\PusherException
     */
    public static function triggerNotification($channel = '', $title = '', $body = '') {
        if($channel != ''){
            $config = config('broadcasting.connections.pusher');
            try {
                $broadcust = new Pusher($config['key'], $config['secret'], $config['app_id'], $config['options']);
                $data['title'] = $title;
                $data['body'] = $body;
                $broadcust->trigger($channel, 'signal_notification', $data);
            } catch (PusherException $e) {

            }
        }
    }

    /**
     * @param int $free
     * @return array
     */
    public static function getNotifyUsers($free = 1){
        $users = [];
        $user_query = DB::table('users')
                        ->select('users.id', DB::raw('GROUP_CONCAT(oauth_access_tokens.device_token SEPARATOR "###") AS device_tokens'))
                        ->leftJoin('oauth_access_tokens', 'users.id', '=', 'oauth_access_tokens.user_id');
        $user_query->where('role', USER_ROLE_USER);
        if($free == 0){
            $user_query->join('wallets', function ($wallet_join){
                $wallet_join->on('users.id', '=', 'wallets.user_id')
                    ->where('wallets.balance', '>', '0');
            });
        }
        $user_query->groupBy('users.id');
        $users = $user_query->get();
        return $users;
    }

    /**
     * @param $notification_data
     * @return array
     */
    public static function defineNotificationBody($notification_data){
        $data = [];
        if($notification_data['type'] == SIGNAL_CREATED){
            $data['title'] = 'New Signal ('.$notification_data['financial_code'].')';
            $data['body'] = $notification_data['financial_code'].' - '. getSignalResultText($notification_data['signal_status'], $notification_data['signal_action'], $notification_data['signal_result']);
        }elseif($notification_data['type'] == SIGNAL_UPDATED){
            $data['title'] = 'Signal Update ('.$notification_data['financial_code'].')';
            $data['body'] = $notification_data['financial_code'].' - '. getSignalResultText($notification_data['signal_status'], $notification_data['signal_action'], $notification_data['signal_result']);
        }else{
            //--
        }
        return $data;
    }


    /************************************For Mobile App************************************************/
    /************************************************************************************/

    /**
     * @param $notification_data
     *
     * @return mixed
     */
    public static function triggerMobileNotification($notification_data){
        $mobile_body_data = static::getNotificationBodyForMobile($notification_data);
        $data['data_arr'] = [
            'title'             => $mobile_body_data['title'],
            'body'           => $mobile_body_data['body'],
            'is_background'     => TRUE,
            'content_available' => TRUE,
        ];
        $data['notification_arr'] = [
            'title' => $mobile_body_data['title'],
            'body' => $mobile_body_data['body']
        ];
        return $data;
    }

    /**
     * @param $notification_data
     *
     * @return array
     */
    public static function getNotificationBodyForMobile($notification_data){
        $data = [];
        if($notification_data['type'] == SIGNAL_CREATED){
            $data['title'] = 'New Signal ('.$notification_data['financial_code'].')';
            $data['body'] = $notification_data['financial_code'].': '. getSignalResultTextForMobile($notification_data['signal_status'], $notification_data['signal_action'], $notification_data['signal_result']);
        }elseif($notification_data['type'] == SIGNAL_UPDATED){
            $data['title'] = 'Signal Update ('.$notification_data['financial_code'].')';
            $data['body'] = $notification_data['financial_code'].': '. getSignalResultTextForMobile($notification_data['signal_status'], $notification_data['signal_action'], $notification_data['signal_result']);
        }else{
            //--
        }
        return $data;
    }

    /**
     * @param $device_code
     * @param array $data_arr
     * @param array $notification_arr
     *
     * @return bool|string
     */
    public static function pushNotificationToAndroidApp($device_code, array $data_arr = [], array $notification_arr = []) {
        $fields['to'] = $device_code;
        $fields['delay_while_idle'] = TRUE;
        $fields['time_to_live'] = 3;
        if(!empty($notification_arr)){
            $fields['notification'] = $notification_arr;
        }
        $fields['data'] = $data_arr;
        //dd(env('FIREBASE_ACCESS_KEY'));
        $headers = array(
            'Authorization: key=' . env('FIREBASE_ACCESS_KEY'),
            'Content-Type: application/json'
        );
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        $result = curl_exec($ch);
        curl_close($ch);
        return $result;
    }
}
