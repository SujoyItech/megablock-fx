<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\SubscriptionPackageRequest;
use App\Http\Services\SubscriptionPackageService;
use App\Models\SubscriptionHistories;
use App\Models\WalletHistory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SubscriptionPackageController extends Controller
{
    //Subscription Package List
    public function subscriptionPackageList(Request $request)
    {
        if ($request->ajax()) {
            $service = new SubscriptionPackageService();
            $data = $service->subscription();

            return datatables($data)
                ->addColumn('image', function ($item) {
                    $generatedData = '<img style="width: 30px;" src="'.asset(getImagePath('subscription_package').$item['image']).'">';
                    return $generatedData;
                })
                ->addColumn('actions', function ($item) {
                    $html = '<ul class="d-flex activity-menu">';
                    $html .= edit_html('subscriptionPackageEdit', $item->id);
                    $html .= delete_html('subscriptionPackageDelete', $item->id);
                    $html .= '</ul>';
                    return $html;
                })
                ->editColumn('status',function ($item){
                    return $item->status == 1 ? __('Success') : __('Pending');
                })
                ->rawColumns(['image', 'actions'])
                ->make(true);
        }
        return view('admin.subscription-package.subscriptionPackageList');
    }


    // Add new Subscription Package
    public function subscriptionPackageAdd()
    {
        $data['title'] = __('Add Subscription Package');
        $data['button_title'] = __('Save');
        return view('admin.subscription-package.subscriptionPackageAddEdit',$data);
    }

    // Subscription Package save
    public function subscriptionPackageSave (SubscriptionPackageRequest $request){

        $service = new SubscriptionPackageService();
        if (!empty($request->edit_id)) {
            $user =  $service->subscriptionPackageUpdate($request);
        } else {
            $user = $service->subscriptionPackageCreate($request);
        }
        $message = $user['message'];
        return redirect()->route('subscriptionPackageList')->with('success', $message);
    }

    //Subscription Package Edit
    public function subscriptionPackageEdit($id)
    {
        $service = new SubscriptionPackageService();
        $data['title'] = __('Update Subscription Title');
        $data['button_title'] = __('Update');
        $item = $service->subscriptionPackageFind($id);
        $data['item']=$item['data']['user'];
        return view('admin.subscription-package.subscriptionPackageAddEdit', $data);
    }

    //Subscription Package Delete
    public function subscriptionPackageDelete($id)
    {
        $service = new SubscriptionPackageService();
        $update = $service->subscriptionPackageDelete($id);
        if ($update) {
            return redirect()->back()->with(['success' => __("Subscription deleted successfully.")]);
        } else {
            return redirect()->back()->with(['success' => __('Operation Failed.')]);
        }
    }

    public function subscriptionPackageHistories(Request $request){
        if ($request->ajax()) {
            $data = SubscriptionHistories::join('users','users.id','subscription_histories.user_id')
                ->join('subscription_packages','subscription_packages.id','subscription_histories.package_id')
                ->select(
                    'users.name',
                    'subscription_histories.status',
                    'subscription_histories.created_at',
                    'subscription_packages.title',
                    'subscription_packages.price',
                    'subscription_packages.credit'
                )->get();
            return datatables($data)
                    ->editColumn("status",function ($item){
                        return  ($item->status == STATUS_SUCCESS) ? __('Success') : __('Pending');
                })
//                ->addColumn('actions', function ($item) {
//                    $html = '<ul class="d-flex activity-menu">';
//                    $html .= edit_html('subscriptionPackageEdit', $item->id);
//                    $html .= delete_html('subscriptionPackageDelete', $item->id);
//                    $html .= '</ul>';
//                    return $html;
//                })
               // ->rawColumns(['actions'])
                ->make(true);
        }
        return view('admin.subscription-package.subscriptionPackageHistories');
    }

    public function paymentPackageHistories(Request $request){
        if ($request->ajax()) {
//            $service = new SubscriptionPackageService();
            $data = DB::table('wallet_histories')
            ->leftJoin('users','users.id','wallet_histories.user_id')
                ->select(
                    'users.name',
                    'wallet_histories.amount',
                    'wallet_histories.status',
                    'wallet_histories.id',
                    'wallet_histories.created_at'
                )->get();
            return datatables($data)
                ->editColumn("status",function ($item){
                    return  ($item->status == STATUS_SUCCESS) ? __('Success') : __('Pending');
                })
                ->addColumn('actions', function ($item) {
                    $html = '<ul class="d-flex activity-menu">';

                    $html .= edit_html('subscriptionPackageEdit', $item->id);
                    $html .= delete_html('subscriptionPackageDelete', $item->id);
                    if (!empty($item) && ($item->status == STATUS_PENDING   ))
                        $html .= statusChange_html('paymentActive', $item->status,$item->id);
                    $html .= '</ul>';
                    return $html;
                })
                 ->rawColumns(['actions'])
                ->make(true);
        }
        return view('admin.subscription-package.paymentHistories');
    }

    public function paymentActive($id){

        $id = decrypt($id);

       try{
           $subscription = new SubscriptionPackageService();
           $active_payment = $subscription->activeCoinPayment($id);

           return redirect()->back()->with(($active_payment['status'] == STATUS_SUCCESS) ? 'success' : 'dismiss', $active_payment['message']);

       }catch (\Exception $exception){
           return redirect()->back()->with('dismiss',__('Failed to active payment.'));
       }
    }
}
