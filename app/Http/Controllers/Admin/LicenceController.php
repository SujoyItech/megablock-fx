<?php

    namespace App\Http\Controllers\Admin;



    use App\Http\Controllers\Controller;

    use App\Http\Requests\LicenceRequest;
    use App\Http\Services\LicenceService;
    use Illuminate\Http\Request;

    class LicenceController extends Controller
    {

        //licence List
        public function licenceList(Request $request)
        {
            if ($request->ajax()) {
                $service = new LicenceService();
                $data = $service->licence();
                return datatables($data)
                   ->addColumn('actions', function ($item) {
                        $html = '<ul class="d-flex activity-menu">';
                        $html .= edit_html('licenceEdit', $item->id);
                        $html .= delete_html('licenceDelete', $item->id);
                        $html .= '</ul>';
                        return $html;
                    })
                    ->rawColumns(['actions'])
                    ->make(true);
            }
            return view('admin.broker.licenceList');
        }


        // Add new licence
        public function licenceAdd()
        {
            $data['title'] = __('Add Licence');
            $data['button_title'] = __('Save');
            return view('admin.broker.licenceAddEdit', $data);
        }

        // licence save
        public function licenceSave (LicenceRequest $request){
            $service = new LicenceService();
            if (!empty($request->edit_id)) {
                $user =  $service->licenceUpdate($request);
            } else {
                $user = $service->licenceCreate($request);
            }
            $message = $user['message'];
            if($user['status'] == true)
            {
                return redirect()->route('licenceList')->with('success', $message);
            }else
            {
                return redirect()->route('licenceList')->with('dismiss', $message);
            }

        }

        //licence User
        public function licenceEdit($id)
        {
            $service = new LicenceService();

            $data['title'] = __('Update Licence');
            $data['button_title'] = __('Update');
            $item = $service->licenceFind($id);
            $data['item']=$item['data']['user'];
            return view('admin.broker.licenceAddEdit', $data);
        }

        //licence User
        public function licenceDelete($id)
        {
            $service = new LicenceService();
            $data = $service->licenceFind($id);
            if ($data['data']['user']) {
                $update = $data['data']['user']->update(['status' => STATUS_DELETED]);
                if ($update) {
                    return redirect()->back()->with(['success' => __("Licence deleted successfully.")]);
                } else {
                    return redirect()->back()->with(['dismiss' => __('Operation Failed.')]);
                }
            }
            //        $user['data']['user']->delete();
            $message = $data['message'];
            return redirect()->route('licenceList')->with(['success' => $message]);
        }


    }
