<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\FaqRequest;
use App\Http\Services\AdminSettingService;
use App\Models\AdminSetting;
use App\Models\Faq;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SettingsController extends Controller
{
    private function update_or_create($slug,$value){
        return AdminSetting::updateOrCreate(['slug'=>$slug],['slug'=>$slug,'value'=>$value]);
    }
    // Admin setting view
    public function adminSettings($mode = 'general'){
        $data['title']=__('Settings');
        $data['button_title']=__('Update');
        $data['all_settings'] = allSetting();
        $data['mode'] = $mode;
        return view('admin.settings.addEdit',$data);
    }

    //Admin support and contract setting
    public function adminContactSupportSetting($mode = 'platform')
    {
        $data['title']=__('Settings');
        $data['button_title']=__('Update');
        $data['all_settings'] = allSetting();
        $data['mode'] = $mode;
        return view('admin.settings.contract_support.addEdit', $data);
    }

    // save the basic setting
    public function adminBasicSettingsSave(Request $request){
        $rules=[
            'app_title'=>'required|max:256'
            ,'copyright_text'=>'required|max:256'
            ,'primary_email'=>'required|max:256|email'
            //            ,'currency_symbol'=>'required|max:5'
        ];
        if ($request->helpline) {
            $rules['helpline'] = 'numeric|phone_number';
        }
        $this->validate($request,$rules);
        //        dd($request->all());
        if(!empty($request->app_title)){
            $update = $this->update_or_create('app_title',$request->app_title);
        }
        if(!empty($request->copyright_text)){
            $update= $this->update_or_create('copyright_text',$request->copyright_text);
        }
        if(!empty($request->primary_email)){
            $update= $this->update_or_create('primary_email',$request->primary_email);
        }
        if(!empty($request->phone)){
            $update= $this->update_or_create('phone',$request->phone);
        }
        if(!empty($request->currency_symbol)){
            $update= $this->update_or_create('currency_symbol',$request->currency_symbol);
        }
        if(!empty($request->helpline)){
            $update= $this->update_or_create('helpline',$request->helpline);
        }
        if(!empty($request->lang)){
            $update= $this->update_or_create('lang',$request->lang);
        }
        if(!empty($request->address)){
            $update= $this->update_or_create('address',$request->address);
        }
        if(!empty($request->login_text)){
            $update= $this->update_or_create('login_text',$request->login_text);
        }
        if(!empty($request->home_choose_text)){
            $update= $this->update_or_create('home_choose_text',$request->home_choose_text);
        }
        if(!empty($request->home_order_text)){
            $update= $this->update_or_create('home_order_text',$request->home_order_text);
        }
        if(!empty($request->home_payment_text)){
            $update= $this->update_or_create('home_payment_text',$request->home_payment_text);
        }
        if(!empty($request->home_delivery_text)){
            $update= $this->update_or_create('home_delivery_text',$request->home_delivery_text);
        }
        if(!empty($request->available_menu_text)){
            $update= $this->update_or_create('available_menu_text',$request->available_menu_text);
        }
        if(!empty($request->bast_selling_text)){
            $update= $this->update_or_create('bast_selling_text',$request->bast_selling_text);
        }
        if(!empty($request->bast_offer_text)){
            $update= $this->update_or_create('bast_offer_text',$request->bast_offer_text);
        }
        if(!empty($request->special_item_text)){
            $update= $this->update_or_create('special_item_text',$request->special_item_text);
        }
        if(!empty($request->about_platform_text)){
            $update= $this->update_or_create('about_platform_text',$request->about_platform_text);
        }
        if(!empty($request->faq_text)){
            $update= $this->update_or_create('faq_text',$request->faq_text);
        }
        if(!empty($request->how_works_text)){
            $update= $this->update_or_create('how_works_text',$request->how_works_text);
        }
        if(!empty($request->term_condition_text)){
            $update= $this->update_or_create('term_condition_text',$request->term_condition_text);
        }

        if(isset($update)){
            return redirect()->back()->with(['success'=>__('Updated Successfully.')]);
        }else{
            return redirect()->back()->with(['success'=>__('Nothing to update.')]);
        }
    }
    //save contract and support setting
    public function adminContractSupportSettingsSave(Request $request){
        if(!empty($request->about_platform_text)){
            $update= $this->update_or_create('about_platform_text',$request->about_platform_text);
        }
        if(!empty($request->faq_text)){
            $update= $this->update_or_create('faq_text',$request->faq_text);
        }
        if(!empty($request->how_works_text)){
            $update= $this->update_or_create('how_works_text',$request->how_works_text);
        }
        if(!empty($request->term_condition_text)){
            $update= $this->update_or_create('term_condition_text',$request->term_condition_text);
        }

        if(isset($update)){
            return redirect()->back()->with(['success'=>__('Updated Successfully.')]);
        }else{
            return redirect()->back()->with(['success'=>__('Nothing to update.')]);
        }
    }

    // Currency Setting Save
    public function adminCurrencySettingsSave(Request $request)
    {
        if (!empty($request->currency))
        {
            $update= $this->update_or_create('currency',$request->currency);
            if(isset($update)){
                return redirect()->back()->with(['success'=>__('Currency Updated Successfully.')]);
            }else{
                return redirect()->back()->with(['success'=>__('Nothing to update.')]);
            }
        }
    }
    // Mobile App Setting Save
    public function adminMobileAppSettingsSave(Request $request)
    {
        if (!empty($request->mobile_app_signal_per_page))
        {
            $update= $this->update_or_create('mobile_app_signal_per_page',$request->mobile_app_signal_per_page);
            if(isset($update)){
                return redirect()->back()->with(['success'=>__('Mobile App Setting Updated Successfully.')]);
            }else{
                return redirect()->back()->with(['success'=>__('Nothing to update.')]);
            }
        }
    }

    // save the payment setting
    public function adminPaymentSettingsSave(Request $request)
    {
        $rules=[
            'braintree_env' => 'required'
        ];

        $this->validate($request,$rules);
        //        dd($request->all());
        if(!empty($request->braintree_env)){
            $update = $this->update_or_create('braintree_env',$request->braintree_env);
        }
        if(!empty($request->braintree_marchant_id)){
            $update= $this->update_or_create('braintree_marchant_id',$request->braintree_marchant_id);
        }
        if(!empty($request->braintree_public_key)){
            $update= $this->update_or_create('braintree_public_key',$request->braintree_public_key);
        }
        if(!empty($request->braintree_private_key)){
            $update= $this->update_or_create('braintree_private_key',$request->braintree_private_key);
        }
        if(!empty($request->braintree_client_token)){
            $update= $this->update_or_create('braintree_client_token',$request->braintree_client_token);
        }
        if(!empty($request->google_pay_marchent_id)){
            $update= $this->update_or_create('google_pay_marchent_id',$request->google_pay_marchent_id);
        }

        if(isset($update)){
            return redirect()->back()->with(['success'=>__('Updated Successfully.')]);
        }else{
            return redirect()->back()->with(['success'=>__('Nothing to update.')]);
        }
    }
    // save the logo setting
    public function adminImageUploadSave(Request $request){
        $rules=[];
        if (isset($request->logo)) {
            $rules['logo']='required|image|mimes:jpg,jpeg,png|max:3000';
        }
        if (isset($request->favicon)) {
            $rules['favicon']='required|image|mimes:jpg,jpeg,png|max:3000';
        }
        if (isset($request->app_logo)) {
            $rules['app_logo']='required|image|mimes:jpg,jpeg,png|max:3000';
        }
        if (isset($request->login_side_image)) {
            $rules['login_side_image']='required|image|mimes:jpg,jpeg,png|max:3000';
        }
        if (isset($request->login_logo)) {
            $rules['login_logo']='required|image|mimes:jpg,jpeg,png|max:3000';
        }
        $this->validate($request,$rules);

        try {
            if (isset($request->logo)) {
                AdminSetting::updateOrCreate(['slug' => 'logo'], ['value' => fileUpload($request['logo'], getImagePath(), allSetting()['logo'])]);
            }
            if (isset($request->favicon)) {
                AdminSetting::updateOrCreate(['slug' => 'favicon'], ['value' => fileUpload($request['favicon'], getImagePath(), allSetting()['favicon'])]);
            }
            if (isset($request->app_logo)) {
                AdminSetting::updateOrCreate(['slug' => 'app_logo'], ['value' => fileUpload($request['app_logo'], getImagePath(), allSetting()['app_logo'])]);
            }
            if (isset($request->login_logo)) {
                AdminSetting::updateOrCreate(['slug' => 'login_logo'], ['value' => fileUpload($request['login_logo'], getImagePath(), allSetting()['login_logo'])]);
            }
            if (isset($request->login_side_image)) {
                AdminSetting::updateOrCreate(['slug' => 'login_side_image'],['value' => fileUpload($request['login_side_image'], getImagePath(),
                    isset(allSetting()['login_side_image']) ? allSetting()['login_side_image'] : '')]);            }

            return redirect()->back()->with(['success'=>__('Updated Successfully.')]);
        } catch (\Exception $e) {
            return redirect()->back()->with(['dismiss'=>__('Nothing to update.')]);
        }

    }
    // save the about setting
    public function adminAboutSettingsSave(Request $request){
        $rules=[];
        if (isset($request->about_image)) {
            $rules['about_image']='required|image|mimes:jpg,jpeg,png|max:3000';
        }
        if (isset($request->choose_us_image)) {
            $rules['choose_us_image']='required|image|mimes:jpg,jpeg,png|max:3000';
        }

        $this->validate($request,$rules);

        try {
            if(!empty($request->about_title)){
                $this->update_or_create('about_title',$request->about_title);
            }
            if(!empty($request->about_description)){
                $this->update_or_create('about_description',$request->about_description);
            }
            if(!empty($request->choose_text)){
                $this->update_or_create('choose_text',$request->choose_text);
            }
            if(!empty($request->choose_section_one_title)){
                $this->update_or_create('choose_section_one_title',$request->choose_section_one_title);
            }
            if(!empty($request->choose_section_two_title)){
                $this->update_or_create('choose_section_two_title',$request->choose_section_two_title);
            }
            if(!empty($request->choose_section_three_title)){
                $this->update_or_create('choose_section_three_title',$request->choose_section_three_title);
            }
            if(!empty($request->choose_section_four_title)){
                $this->update_or_create('choose_section_four_title',$request->choose_section_four_title);
            }
            if(!empty($request->choose_section_five_title)){
                $this->update_or_create('choose_section_five_title',$request->choose_section_five_title);
            }
            if(!empty($request->choose_section_six_title)){
                $this->update_or_create('choose_section_six_title',$request->choose_section_six_title);
            }
            if(!empty($request->choose_section_one_des)){
                $this->update_or_create('choose_section_one_des',$request->choose_section_one_des);
            }
            if(!empty($request->choose_section_two_des)){
                $this->update_or_create('choose_section_two_des',$request->choose_section_two_des);
            }
            if(!empty($request->choose_section_three_des)){
                $this->update_or_create('choose_section_three_des',$request->choose_section_three_des);
            }
            if(!empty($request->choose_section_four_des)){
                $this->update_or_create('choose_section_four_des',$request->choose_section_four_des);
            }
            if(!empty($request->choose_section_five_des)){
                $this->update_or_create('choose_section_five_des',$request->choose_section_five_des);
            }
            if(!empty($request->choose_section_six_des)){
                $this->update_or_create('choose_section_six_des',$request->choose_section_six_des);
            }
            if (isset($request->about_image)) {
                AdminSetting::updateOrCreate(['slug' => 'about_image'],['value' => fileUpload($request['about_image'], getImagePath(),
                    isset(allSetting()['about_image']) ? allSetting()['about_image'] : '')]);
            }
            if (isset($request->choose_us_image)) {
                AdminSetting::updateOrCreate(['slug' => 'choose_us_image'],['value' => fileUpload($request['choose_us_image'], getImagePath(),
                    isset(allSetting()['choose_us_image']) ? allSetting()['choose_us_image'] : '')]);
            }

            return redirect()->back()->with(['success'=>__('Updated Successfully.')]);
        } catch (\Exception $e) {
            return redirect()->back()->with(['dismiss'=> $e->getMessage()]);
            //            return redirect()->back()->with(['dismiss'=>__('Nothing to update.')]);
        }

    }

    //===========================Admin Referral Setting=====================//

    public function adminReferralSettings(){
        $data['title']=__('Referral Settings');
        $data['button_title']=__('Update');
        $data['all_settings'] = allSetting();
        return view('admin.settings.referral-bonus.addEdit',$data);
    }
    public function adminReferralSettingsSave(Request $request)
    {
        if(!empty($request->on_register_and_purchase || $request->on_register_and_purchase == 0)){
            $this->update_or_create('on_register_and_purchase',$request->on_register_and_purchase);
        }
        if(!empty($request->registers_through_affiliate || $request->registers_through_affiliate == 0)){
            $this->update_or_create('registers_through_affiliate',$request->registers_through_affiliate);
        }
        if(!empty($request->affiliate_owner || $request->affiliate_owner == 0)){
            $this->update_or_create('affiliate_owner',$request->affiliate_owner);
        }
        if(!empty($request->standard_registration || $request->standard_registration == 0)){
            $this->update_or_create('standard_registration',$request->standard_registration);
        }
        return redirect()->back()->with('success','Referral Settings Updated Successfully.');
    }
    //=============================Faqs===============================
    public function faqList(Request $request)
    {
        if ($request->ajax()) {
            $service = new AdminSettingService();
            $data = $service->faqs();
            return datatables($data)
                ->editColumn('description',function ($item){
                    return substr($item->description,0,100).'...';
                })
                ->addColumn('actions', function ($item) {
                    $html = '<ul class="d-flex activity-menu">';
                    $html .= edit_html('faqEdit', $item->id);
                    $html .= delete_html('faqDelete', $item->id);
                    $html .= '</ul>';
                    return $html;
                })
                ->rawColumns(['actions'])
                ->make(true);
        }
        return view('admin.settings.contract_support.faq.faq-list');

    }
    // Add new Faqs
    public function faqAdd()
    {
        $data['title'] = __('Add Question');
        $data['button_title'] = __('Save');
        return view('admin.settings.contract_support.faq.addEdit', $data);
    }

    // Faq save
    public function faqSave (FaqRequest $request){
        $service = new AdminSettingService();
        if (!empty($request->edit_id)) {
            $user =  $service->faqUpdate($request);
        } else {
            $user = $service->faqCreate($request);
        }
        $message = $user['message'];
        return redirect()->route('faqList')->with('success', $message);
    }

    //Faq Edit
    public function faqEdit($id)
    {
        $service = new AdminSettingService();

        $data['title'] = __('Update Faqs');
        $data['button_title'] = __('Update');
        $item = $service->faqFind($id);
        $data['item']=$item['data']['user'];
        return view('admin.settings.contract_support.faq.addEdit', $data);
    }

    //Faq Delete
    public function faqDelete($id)
    {
        $service = new AdminSettingService();
        $data = $service->faqFind($id);
        if ($data['data']['user']) {
            $deleted = $data['data']['user']->delete();
            if ($deleted) {
                return redirect()->back()->with(['success' => __("FAQ deleted successfully.")]);
            } else {
                return redirect()->back()->with(['dismiss' => __('Operation Failed.')]);
            }
        }
        //        $user['data']['user']->delete();
        $message = $data['message'];
        return redirect()->route('faqList')->with(['dismiss' => $message]);
    }

//    // chnage shipping method status
//    public function changeShippingMethodStatus(Request $request)
//    {
//        if (!empty($request->active_id) && is_numeric($request->active_id)) {
//            $item = ShippingMethods::findOrFail($request->active_id);
//            if ($item->status == 1) {
//                $item->status = 2;
//            } elseif ($item->status == 2) {
//                $item->status = 1;
//            }
//            $item->save();
//        }
//        return response()->json(['message'=>__('Status changed successfully')]);
//    }
//
//    // shipping methods
//    public function shippingMethod()
//    {
//        $data['title'] = __('Delivery Methods');
//        $data['button_title'] = __('Add');
//        $data['menu'] = 'shipping-method';
//        $data['shipping_methods'] = ShippingMethods::orderBy('id', 'asc')->get();
//
//        return view('admin.settings.shipping-method', $data);
//    }
//
//    // update shippimg method price
//    public function updateShippingMethodPrice(Request $request)
//    {
//        $rules=[
//            'price'=>'required|numeric|min:0'
//        ];
//        $this->validate($request,$rules);
//        if (isset ($request->price) && isset($request->shipping_id)) {
//            $update = ShippingMethods::where('id', $request->shipping_id)
//                ->update(['amount'=> $request->price]);
//            if ($update) {
//                return redirect()->back()->with('success', __('Price updated successfully'));
//            } else {
//                return redirect()->back()->with('success', __('update failed'));
//            }
//        };
//    }
//
//    // chnage payment method status
//    public function changePaymentMethodStatus(Request $request)
//    {
//        if (!empty($request->active_id) && is_numeric($request->active_id)) {
//            $item = PaymentMethod::findOrFail($request->active_id);
//            if ($item->status == 1) {
//                $item->status = 2;
//            } elseif ($item->status == 2) {
//                $item->status = 1;
//            }
//            $item->save();
//        }
//        return response()->json(['message'=>__('Status changed successfully')]);
//    }
//
//    // payment methods
//    public function paymentMethod()
//    {
//        $data['title'] = __('Payment Methods');
//        $data['button_title'] = __('Add');
//        $data['menu'] = 'payment-method';
//        $data['items'] = PaymentMethod::orderBy('id', 'asc')->get();
//
//        return view('admin.settings.payment-method', $data);
//    }

}
