<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;

use App\Http\Requests\BrokerRequest;
use App\Http\Services\BrokerService;
use App\Http\Services\CommonService;
use App\Models\BrokerLicence;
use App\Models\BrokerMarketsExclude;
use App\Models\Licence;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class BrokerController extends Controller
{
    //
    //Broker List
    public function brokerList(Request $request)
    {
        if ($request->ajax()) {
            $service = new BrokerService();
            $data = $service->broker();
            return datatables($data)
                ->addColumn('logo', function ($item) {
                    $generatedData = '<img style="width: 30px;" src="'.asset(getImagePath('brokers').$item['logo']).'">';
                    return $generatedData;
                })
                ->editColumn('recommended_deposit',function ($item){
                    return substr($item['recommended_deposit'],0,10);
                })
                ->editColumn('advantage',function ($item){
                    return substr($item['advantage'],0,10).'..';
                })
                ->editColumn('created_at',function ($item){
                    return Carbon::parse($item['created_at'])->format('d M Y');
                })
                ->addColumn('actions', function ($item) {
                    $html = '<ul class="d-flex activity-menu">';
                    $html .= edit_html('brokerEdit', $item->id);
                    $html .= delete_html('brokerDelete', $item->id);
                    $html .= get_link( $item->URL);
                    $html .= '</ul>';
                    return $html;
                })
                ->addColumn('move',function ($item){
                    return '<img class="text-center" style="cursor:move" width="20px" src="'.asset('assets/images/move.svg').'" alt="">';
                })
                ->rawColumns(['logo', 'actions','move'])
                ->make(true);
        }
        return view('admin.broker.brokerList');
    }

    public function Order(Request $request){

        try{
            $column = 'order';
            if (!empty($request->column))
                $column = $request->column;

            $order = array_filter(explode(',',$request->ids));

            if (is_array($order)){
                foreach ($order as $key => $item){
                    $bra =  DB::table($request->table)->where(['id'=>$item])->update([$column=>$key]);
                }
            }
            return response()->json(['success'=>__('Order updated successfully.')]);
        }catch (\Exception $exception){
            return response()->json(['dismiss'=>__('Something went wrong.')]);

        }
    }


    // Add new Broker
    public function brokerAdd()
    {
        $data['title'] = __('Add Broker');
        $data['button_title'] = __('Save');
        $data['licences'] = Licence::where('status','!=',6)->get();
        return view('admin.broker.brokerAddEdit',$data);
    }

    // Broker save
    public function brokerSave (BrokerRequest $request){
        $service = new BrokerService();
        if (!empty($request->edit_id)) {
            $user =  $service->brokerUpdate($request);
        } else {
            $user = $service->brokerCreate($request);
        }
        if (isset($user))
        {
            if (isset($user['status']) && ($user['status'] == true))
            {
                return redirect()->route('brokerList')->with('success', $user['message']);
            }else {
                return redirect()->route('brokerList')->with('dismiss', $user['message']);
            }
        }else {
            return redirect()->route('brokerList')->with('dismiss', __('Something went wrong.'));
        }
    }

    //Broker Edit
    public function brokerEdit($id)
    {
        $service = new BrokerService();

        $data['title'] = __('Update Licence');
        $data['button_title'] = __('Update');
        $item = $service->brokerFind($id);
        $data['item']=$item['data']['user'];
        $data['licences'] = Licence::where('status','!=',6)->get();
        $common_service = new CommonService();
        $id = $common_service->checkValidId($id);
        $data['markets'] = BrokerMarketsExclude::where('broker_id',$id)->get();
        $data['broker_licences'] = BrokerLicence::where('broker_id',$id)->get();
        return view('admin.broker.brokerAddEdit', $data);
    }

    //Broker Delete
    public function brokerDelete($id)
    {
        $service = new BrokerService();
        $data = $service->brokerFind($id);
        if ($data['data']['user']) {
            $update = $data['data']['user']->update(['status' => STATUS_DELETED]);
            if ($update) {
                return redirect()->back()->with(['success' => __("Broker deleted successfully.")]);
            } else {
                return redirect()->back()->with(['dismiss' => __('Operation Failed.')]);
            }
        }
        $message = $data['message'];
        return redirect()->route('brokerList')->with(['success' => $message]);
    }
}
