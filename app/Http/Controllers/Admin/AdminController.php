<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Repositories\UserRepository;
use App\Http\Requests\ChangePasswordRequest;
use App\Http\Requests\ProfileUpdateRequest;

use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class AdminController extends Controller {
    //admin dashboard
    public function adminDashboard() {
        $data = [];
        return view('admin.dashboard', $data);
    }

    /*
     * userProfile
     *
     * User profile
     *
     *
     *
     *
     */

    public function profile() {
        $data['user'] = User::where('id', Auth::user()->id)->first();

        return view('admin.profile', $data);
    }
    public function setLang($code)
    {
        $user_lang_update = User::where('id',Auth::user()->id)->update(['language'=>$code]);
        if($user_lang_update){
            return redirect()->back()->with(['success'=>__('Language Changed Successfully.')]);
        }else
        {
            return redirect()->back()->with(['dismiss'=>__('Language Change Failed.')]);
        }
    }

    /*
     * updateProfile
     *
     * Profile Update process
     *
     *
     *
     *
     */

    public function updateProfile(ProfileUpdateRequest $request) {
        $userRepository = app(UserRepository::class);
        $response = $userRepository->profileUpdate($request->all(), Auth::user()->id);
        if ($response['status'] == false) {
            return redirect()->back()->withInput()->with('dismiss', $response['message']);
        } else {
            return redirect()->back()->withInput()->with('success', $response['message']);
        }
    }

    /*
     * changePassword
     *
     * Password change process
     *
     *
     *
     *
     */

    public function changePassword(ChangePasswordRequest $request) {
        $userRepository = app(UserRepository::class);
        $response = $userRepository->passwordChange($request->all(), Auth::user()->id);

        if ($response['status'] == false) {
            return redirect()->back()->withInput()->with('dismiss', $response['message']);
        } else {
            return redirect()->back()->withInput()->with('success', $response['message']);
        }
    }
}
