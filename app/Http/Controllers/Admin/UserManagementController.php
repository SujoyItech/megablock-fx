<?php

namespace App\Http\Controllers\Admin;

use App\Http\Repositories\UserAuthRepository;
use App\Http\Requests\UserRequest;
use App\Http\Services\CommonService;
use App\Http\Services\RoleService;
use App\Http\Services\UserService;
use App\Models\Role;
use App\Models\UserVerificationCode;
use App\Models\Wallet;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\AdminSetting;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class UserManagementController extends Controller
{
    //Role list
    public function adminRoleList(Request $request)
    {
        if ($request->ajax()) {
            $role = Role::query();
            return datatables($role)
                ->addColumn('actions', function ($item) {
                    $html = '<ul class="d-flex activity-menu">';
                    $html .= edit_html('adminRoleEdit', $item->id);
                    $html .= delete_html('adminRoleDelete', $item->id);
                    $html .= '</ul>';
                    return $html;
                })
                ->rawColumns(['actions'])
                ->make(true);
        }
        return view('admin.user-management.role.list');
    }

    // Add new Role
    public function adminRoleAdd()
    {
        $data['title'] = __('Add Role');
        $data['button_title'] = __('Add');
        return view('admin.user-management.role.addEdit', $data);
    }

    //Edit Role
    public function adminRoleEdit($id)
    {
        try {
            $role_id = decrypt($id);
        } catch (\Exception $e) {
            return redirect()->back()->with(['error' => __('Item Not Found!')]);
        }
        $data['title'] = __('Update Role');
        $data['button_title'] = __('Update');
        $data['item'] = Role::find($role_id);
        return view('admin.user-management.role.addEdit', $data);
    }

    // Role Save
    public function adminRoleSave(Request $request)
    {
        $rule = [
            'title' => 'required|max:256',
            'tasks' => 'required',
        ];
        $messages = [
            'title.required' => 'Title can\'t be empty.',
            'title.max' => 'Title field must be maximum 255 Character.',
            'tasks.required' => 'Task can\'t be empty',
        ];
        $this->validate($request, $rule, $messages);

        $role_service = new RoleService();
        if (!empty($request->edit_id)) {
            $role_service->update($request);
            $message = 'Role Updated Successfully.';
        } else {
            $role_service->create($request);
            $message = 'Role Created Successfully.';
        }
        return redirect()->route('adminRoleList')->with('success', $message);
    }

    //Delete Role
    public function adminRoleDelete($id)
    {
        try {
            try {
                $role_id = decrypt($id);
            } catch (\Exception $e) {
                return redirect()->back()->with(['dismiss' => __('Item Not Found!')]);
            }
            $userRole = User::where('role', $role_id)->get();
//                if(isset($userRole[0])) {
//                    return redirect()->route('adminRoleList')->with(['dismiss' => __("This role has already assigned.You can't delete this role")]);
//                }
//                if (in_array($role_id,[1,2,3,4])) {
//                    return redirect()->route('adminRoleList')->with(['dismiss' => __("You can't delete this role")]);
//                }
            Role::where(['id' => $role_id])->delete();
            $message = __('Deleted successfully !');
        } catch (\Exception $exception) {
            $message = __('Something went wrong');
        }
        return redirect()->route('adminRoleList')->with(['success' => $message]);
    }

    //User List
    public function adminUserList(Request $request)
    {
        if ($request->ajax()) {
            $userService = new UserService();
            $user = $userService->adminUsers();
            return datatables($user['data'])
                ->addColumn('active_status', function ($item) {
                    return statusAction($item->active_status);
                })
                ->addColumn('actions', function ($item) {
                    $html = '<ul class="d-flex activity-menu">';
                    //                    $html .= statusChange_html('adminUserChangeStatus', $item->status, $item->id);
                    $html .= edit_html('adminUserEdit', $item->id);
//                        if ($item->active_status != STATUS_DELETED){
//                            $html .= delete_html('adminUserDelete', $item->id);
//                        }
//                        if ($item->active_status != STATUS_ACTIVE){
//                            $html .= activate_html('adminUserActive', $item->id);
//                        }
//                        if ($item->email_verified != STATUS_ACTIVE){
//                            $html .= email_verify_html('adminUserEmailVerify', $item->id);
//                        }
                    $html .= '</ul>';
                    return $html;
                })
                ->rawColumns(['actions'])
                ->make(true);
        }
        return view('admin.user-management.user.admin-list');
    }

    public function userList(Request $request)
    {
        if ($request->ajax()) {
            $user = DB::table('users')
                ->select('users.*', 'roles.title as role','wallets.balance as balance')
                ->leftJoin('wallets','users.id','=','wallets.user_id')
                ->leftJoin('roles', 'roles.id', '=', 'users.role')
                ->where('users.id', '<>', Auth::user()->id)
                ->where('users.role', '!=', USER_ROLE_ADMIN)
                ->get();
            return datatables($user)
                ->addColumn('active_status', function ($item) {
                    return statusAction($item->active_status);
                })
                ->editColumn('name',function ($item){
                    return strlen($item->name) > 30 ? substr($item->name,0,30).'..':$item->name;
                })
                ->editColumn('email',function ($item){
                    return strlen($item->email) > 50 ? substr($item->email,0,50).'..':$item->email;
                })
                ->editColumn('balance', function ($item) {
                    if ($item->balance == null) {
                        return 0;
                    } else {
                        return $item->balance;
                    }
                })
                ->addColumn('actions', function ($item) {
                    $html = '<ul class="d-flex activity-menu">';
                    $html .= edit_html('adminUserEdit', $item->id);
                    if ($item->active_status != STATUS_DELETED) {
                        $html .= delete_html('adminUserDelete', $item->id, 'Delete User', 'Would you want to delete '.$item->name.'?');
                    }
                    if ($item->email_verified == STATUS_ACTIVE && $item->active_status != STATUS_ACTIVE) {
                        $html .= activate_html('adminUserActive', $item->id);
                    }
                    if ($item->active_status == STATUS_ACTIVE) {
                        $html .= block_html('adminUserBlock', $item->id);
                    }
                    if ($item->email_verified != STATUS_ACTIVE) {
                        $html .= email_verify_html('adminUserEmailVerify', $item->id);
                    }
                    $html .= '</ul>';
                    return $html;
                })
                ->rawColumns(['actions'])
                ->make(true);
        }
        return view('admin.user-management.user.user-list');
    }
    // app user list

    // user details
    public function adminUserDetails($id)
    {
        $user_service = new UserService();

        $data['title'] = __('User Details');
        $item = $user_service->find($id);
        $data['item'] = $item['data']['user'];

        //        dd($data['item']);
        return view('admin.user-management.user.userDetails', $data);
    }

    // Add new user
    public function adminUserAdd()
    {
        $role_service = new RoleService();
        $role_data = $role_service->lists();

        if ($role_data['status']) {
            $data['roles'] = $role_data['data']['role']->get();
        }

        $data['title'] = __('Add User');
        $data['button_title'] = __('Add');

        return view('admin.user-management.user.addEdit', $data);
    }

    //Edit User
    public function adminUserEdit($id)
    {
        $role_service = new RoleService();
        $user_service = new UserService();

        $role_data = $role_service->lists();

        if ($role_data['status']) {
            $data['roles'] = $role_data['data']['role']->get();
        }

        $data['title'] = __('Update User');
        $data['button_title'] = __('Update');
        $common_service = new CommonService();
        $userId = $common_service->checkValidId($id);
        $wallet = Wallet::where('user_id', $userId)->first();
        isset($wallet) ? $data['balance'] = $wallet->balance : $data['balance'] = null;

        $item = $user_service->find($id);
        $data['item'] = $item['data']['user'];
        return view('admin.user-management.user.addEdit', $data);
    }

    // User save
    public function adminUserSave(UserRequest $request)
    {
        $userService = new UserService();
        if (!empty($request->edit_id)) {
            $user = $userService->update($request);
        } else {
            $user = $userService->create($request);
        }

        if (isset($user['status']) && ($user['status'] == true)) {
            return redirect()->route('userList')->with(['success' => $user['message']]);
        } else {
            return redirect()->route('userList')->with(['dismiss' => $user['message']]);
        }
    }

    //Delete User
    public function adminUserDelete($id)
    {
        $user_service = new UserService();
        $user = $user_service->find($id);
        if ($user['data']['user']) {
            $update = $user['data']['user']->update(['active_status' => STATUS_DELETED]);
            if ($update) {
                DB::table('oauth_access_tokens')->where('user_id',$user['data']['user']->id)->delete();
                return redirect()->back()->with(['success' => __("User deleted successfully.")]);
            } else {
                return redirect()->back()->with(['dismiss' => __('Operation Failed.')]);
            }
        }
        //        $user['data']['user']->delete();
        $message = $user['message'];
        return redirect()->route('adminUserList')->with(['success' => $message]);
    }

    //Activate User
    public function adminUserActive($id)
    {
        $user_service = new UserService();
        $user = $user_service->find($id);
        if ($user['data']['user']) {
            $update = $user['data']['user']->update(['active_status' => STATUS_ACTIVE]);
            if ($update) {
                return redirect()->back()->with(['success' => __("User activated successfully.")]);
            } else {
                return redirect()->back()->with(['success' => __('Operation Failed.')]);
            }
        }
        $message = $user['message'];
        return redirect()->route('adminUserList')->with(['success' => $message]);
    }

    public function adminUserBlock($id)
    {
        $user_service = new UserService();
        $user = $user_service->find($id);
        if ($user['data']['user']) {
            $update = $user['data']['user']->update(['active_status' => STATUS_BLOCKED]);
            if ($update) {
                DB::table('oauth_access_tokens')->where('user_id',$user['data']['user']->id)->delete();
                return redirect()->back()->with(['success' => __("User Blocked successfully.")]);
            } else {
                return redirect()->back()->with(['success' => __('Operation Failed.')]);
            }
        }
        $message = $user['message'];
        return redirect()->route('adminUserList')->with(['success' => $message]);
    }

    //Activate User
    public function adminUserEmailVerify($id)
    {
        $user_service = new UserService();
        $user = $user_service->find($id);
        $repo =  new UserAuthRepository();
        if ($user['data']['user']) {
            DB::beginTransaction();
            $update = $user['data']['user']->update(['email_verified' => STATUS_ACTIVE,'active_status'=>STATUS_ACTIVE]);
            if ($update) {
                try {
                    $parentId = null;
                    if($user['data']['user']->used_code != null){
                        $parentId = User::where(['own_code'=>$user['data']['user']->used_code])->first()->id;
                    }
                    if (isset($parentId) && $parentId != null) {
                        // Affiliate owner wallet and wallet histories
                        $repo->affiliateOwnerWallet($parentId, $user['data']['user']->id);
                        //User Wallet and wallet histories when use affiliate code
                        $repo->affiliateUserWallet($parentId, $user['data']['user']->id);
                    } else {
                        // User Wallet when user create account without affiliate code
                        $data['balance'] = allSetting('standard_registration');
                        $data['userId'] = $user['data']['user']->id;
                        $data['description'] = 'Bonus For Standard Registration.';
                        $data['type'] = STANDARD_REGISTRATION;
                        $repo->userStandardRegistrationWallet($data);
                    }
                    $verifyCode = UserVerificationCode::where(['user_id' => $user['data']['user']->id, 'type' => 1, 'status' => STATUS_PENDING])->first();
                    if (isset($verifyCode)) {
                        $verifyCode->update(['status' => STATUS_ACTIVE]);
                    }
                    DB::commit();
                }catch (\Exception $e)
                {
                    DB::rollBack();
                    return redirect()->back()->with(['dismiss'=>$e->getMessage()]);
                }
                return redirect()->back()->with(['success' => __("Email verified successfully.")]);
            } else {
                return redirect()->back()->with(['dismiss' => __('Operation Failed.')]);
            }
        }
        $message = $user['message'];
        return redirect()->route('adminUserList')->with(['success' => $message]);
    }
}
