<?php

    namespace App\Http\Controllers\Admin;



    use App\Http\Controllers\Controller;
    use App\Http\Requests\FinancialInstrumentCodeRequest;
    use App\Http\Requests\TradeTypeRequest;
    use App\Http\Services\CommonService;
    use App\Http\Services\SignalCategoryService;
    use App\Models\FinancialInstrumentCode;
    use App\Models\TradeType;
    use Illuminate\Http\Request;
    use Illuminate\Support\Facades\DB;

    class SignalCategoryController extends Controller
    {

        //financialInstrumentCode List
        public function financialInstrumentCodeList(Request $request)
        {
            if ($request->ajax()) {
                $service = new SignalCategoryService();
                $data = $service->financialInstrumentCode();
//                dd($data);
                return datatables($data)
                   ->addColumn('actions', function ($item) {
                        $html = '<ul class="d-flex activity-menu">';
                        $html .= edit_fcs_html('financialInstrumentCodeEdit', $item->id);
                        $html .= delete_fcs_html('financialInstrumentCodeDelete', $item->id);
                        $html .= '</ul>';
                        return $html;
                    })
                    ->rawColumns(['actions'])
                    ->make(true);
            }
            return view('admin.category.financialInstrumentCodeList');
        }


        // Add new financialInstrumentCode
        public function financialInstrumentCodeAdd()
        {

            $data['title'] = __('Add Financial Instrument Code');
            $data['button_title'] = __('Save');

            return view('admin.category.financialInstrumentCodeAddEdit', $data);
        }

        // financialInstrumentCode save
        public function financialInstrumentCodeSave (FinancialInstrumentCodeRequest $request){
            $service = new SignalCategoryService();
            if (!empty($request->edit_id)) {
                $user =  $service->financialInstrumentCodeUpdate($request);
            } else {
                $user = $service->financialInstrumentCodeCreate($request);
            }
            $message = $user['message'];
            if($user['status'] == true){
                return redirect()->route('financialInstrumentCodeList')->with('success', $message);
            }else{
                return redirect()->route('financialInstrumentCodeList')->with('dismiss', $message);
            }

        }

        //financialInstrumentCode User
        public function financialInstrumentCodeEdit($id)
        {
            $service = new SignalCategoryService();

            $data['title'] = __('Update Financial Instrument Code');
            $data['button_title'] = __('Update');
            $item = DB::table('financial_instrument_codes')->where('id',$id)->first();
            $data['item']=$item;
            return view('admin.category.financialInstrumentCodeAddEdit', $data);
        }

        //financialInstrumentCode User
        public function financialInstrumentCodeDelete($id)
        {
            $fcs = DB::table('financial_instrument_codes')->where('id',$id)->first();
            if ($fcs) {
                $update = DB::table('financial_instrument_codes')->where('id',$id)->delete();
                if ($update) {
                    return redirect()->back()->with(['success' => __("Financial Instrument code deleted successfully.")]);
                } else {
                    return redirect()->back()->with(['dismiss' => __('Operation Failed.')]);
                }
            }else{
                return redirect()->back()->with(['dismiss' => 'Item not found.']);
            }
            //        $user['data']['user']->delete();

        }


        //tradeType List
        public function tradeTypeList(Request $request)
        {
            if ($request->ajax()) {
                $service = new SignalCategoryService();
                $data = $service->tradeType();
                return datatables($data)
                   ->addColumn('actions', function ($item) {
                        $html = '<ul class="d-flex activity-menu">';
                        $html .= edit_html('tradeTypeEdit', $item->id);
                        $html .= delete_html('tradeTypeDelete', $item->id);
                        $html .= '</ul>';
                        return $html;
                    })
                    ->rawColumns(['actions'])
                    ->make(true);
            }
            return view('admin.category.tradeTypeList');
        }


        // Add new tradeType
        public function tradeTypeAdd()
        {

            $data['title'] = __('Add Trade Type');
            $data['button_title'] = __('Save');

            return view('admin.category.tradeTypeAddEdit', $data);
        }

        // tradeType save
        public function tradeTypeSave (TradeTypeRequest $request){
            $service = new SignalCategoryService();
            if (!empty($request->edit_id)) {
                $user =  $service->tradeTypeUpdate($request);
            } else {
                $user = $service->tradeTypeCreate($request);
            }
            $message = $user['message'];
            return redirect()->route('tradeTypeList')->with('success', $message);
        }

        //Edit tradeType

        public function tradeTypeEdit($id)
        {
            $service = new SignalCategoryService();

            $data['title'] = __('Update Trade Type');
            $data['button_title'] = __('Update');
            $item = $service->tradeTypeFind($id);
            $data['item']=$item['data']['user'];
            return view('admin.category.tradeTypeAddEdit', $data);
        }

        //Delete tradeType
        public function tradeTypeDelete($id)
        {
            $service = new SignalCategoryService();
            $data = $service->tradeTypeFind($id);
            if ($data['data']['user']) {
                $update = $data['data']['user']->update(['status' => STATUS_DELETED]);
                if ($update) {
                    return redirect()->back()->with(['success' => __("Trade Type deleted successfully.")]);
                } else {
                    return redirect()->back()->with(['success' => __('Operation Failed.')]);
                }
            }
            //        $user['data']['user']->delete();
            $message = $data['message'];
            return redirect()->route('tradeTypeList')->with(['success' => $message]);
        }

        //orderType List
        public function orderTypeList(Request $request)
        {
            if ($request->ajax()) {
                $service = new SignalCategoryService();
                $data = $service->orderType();
                return datatables($data)
                   ->addColumn('actions', function ($item) {
                        $html = '<ul class="d-flex activity-menu">';
                        $html .= edit_html('orderTypeEdit', $item->id);
                        $html .= delete_html('orderTypeDelete', $item->id);
                        $html .= '</ul>';
                        return $html;
                    })
                    ->rawColumns(['actions'])
                    ->make(true);
            }
            return view('admin.category.orderTypeList');
        }


        // Add new orderType
        public function orderTypeAdd()
        {

            $data['title'] = __('Add Order Type');
            $data['button_title'] = __('Save');

            return view('admin.category.orderTypeAddEdit', $data);
        }

        // tradeType save
        public function orderTypeSave (TradeTypeRequest $request){
            $service = new SignalCategoryService();
            if (!empty($request->edit_id)) {
                $user =  $service->orderTypeUpdate($request);
            } else {
                $user = $service->orderTypeCreate($request);
            }
            $message = $user['message'];
            return redirect()->route('orderTypeList')->with('success', $message);
        }

        //Edit orderType

        public function orderTypeEdit($id)
        {
            $service = new SignalCategoryService();

            $data['title'] = __('Update Trade Type');
            $data['button_title'] = __('Update');
            $item = $service->orderTypeFind($id);
            $data['item']=$item['data']['user'];
            return view('admin.category.orderTypeAddEdit', $data);
        }

        //Delete tradeType
        public function orderTypeDelete($id)
        {
            $service = new SignalCategoryService();
            $data = $service->orderTypeFind($id);
            if ($data['data']['user']) {
                $update = $data['data']['user']->update(['status' => STATUS_DELETED]);
                if ($update) {
                    return redirect()->back()->with(['success' => __("Order Type deleted successfully.")]);
                } else {
                    return redirect()->back()->with(['success' => __('Operation Failed.')]);
                }
            }
            //        $user['data']['user']->delete();
            $message = $data['message'];
            return redirect()->route('orderTypeList')->with(['success' => $message]);
        }
    }
