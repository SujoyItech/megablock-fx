<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use App\Http\Requests\SignalRequest;
use App\Http\Services\SignalService;
use App\Models\FinancialInstrumentCode;
use App\Models\OrderType;
use App\Models\Signal;
use App\Models\TradeType;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SignalController extends Controller {

    /**
     * Signal Lists
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\JsonResponse|\Illuminate\View\View|mixed
     * @throws \Exception
     */

    public function signalList(Request $request) {
        if ($request->ajax()) {
            $data = SignalService::signal();
            return datatables($data)
                ->addColumn('actions', function ($item) {
                    $html = '<ul class="d-flex activity-menu">';
                    $html .= edit_html('signalEdit', $item->id);
                    $html .= delete_html('signalDelete', $item->id);
                    $html .= '</ul>';
                    return $html;
                })
                ->editColumn('status', function ($item) {
                    if ($item->status == SIGNAL_ACTIVE) return "Active";
                    elseif ($item->status == SIGNAL_PENDING) return "Pending";
                    elseif ($item->status == SIGNAL_CLOSED) return "Closed";
                })
                ->editColumn('action', function ($item) {
                    return $item->action == SIGNAL_ACTION_BUY ? "Buy" : "Sell";
                })
                ->editColumn('financial_instrument_code', function ($item) {
                    return substr_replace(strtoupper($item->financial_instrument_code), '/', 3, 0);
                })
                ->editColumn('available_for_free', function ($item) {
                    return $item->available_for_free == 1 ? "Free" : "Premium";
                })
                ->rawColumns(['actions'])
                ->make(true);
        }
        return view('admin.signal.list');
    }

    /**
     * Signal Create
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function signalAdd() {
        $data['title'] = __('Add Signal');
        $data['button_title'] = __('Save');
        $data['fics'] = DB::table('financial_instrument_codes')->where('status', '!=', STATUS_DELETED)->get();
        $data['ot'] = OrderType::where('status', '!=', STATUS_DELETED)->get();
        $data['tt'] = TradeType::where('status', '!=', STATUS_DELETED)->get();

        return view('admin.signal.addEdit', $data);
    }
    /**
     * Signal Save
     * @param SignalRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function signalSave(SignalRequest $request) {
        $service = new SignalService();
        if (!empty($request->edit_id)) {
            $user = $service->signalUpdate($request);
        } else {
            $user = $service->signalCreate($request);
        }
        $message = $user['message'];
        if ($user['status'] == true) {
            return redirect()->route('signalList')->with('success', $message);
        } else {
            return redirect()->route('signalList')->with('dismiss', $message);
        }

    }

    /**
     * Signal Edit
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */

    public function signalEdit($id) {
        $service = new SignalService();

        $data['title'] = __('Update Signal');
        $data['button_title'] = __('Update');
        $item = $service->signalFind($id);
        $data['item'] = $item['data']['user'];
        $data['fics'] = DB::table('financial_instrument_codes')->where('status', '!=', STATUS_DELETED)->get();
        $data['ot'] = OrderType::where('status', '!=', STATUS_DELETED)->get();
        $data['tt'] = TradeType::where('status', '!=', STATUS_DELETED)->get();
        return view('admin.signal.addEdit', $data);
    }

    /**
     * Signal Delete
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */

    public function signalDelete($id) {
        $service = new SignalService();
        $data = $service->signalFind($id);
        if ($data['data']['user']) {
            $update = $data['data']['user']->update(['status' => STATUS_DELETED]);
            if ($update) {
                return redirect()->back()->with(['success' => __("Signal deleted successfully.")]);
            } else {
                return redirect()->back()->with(['success' => __('Operation Failed.')]);
            }
        }
        $message = $data['message'];
        return redirect()->route('signalList')->with(['success' => $message]);
    }


}
