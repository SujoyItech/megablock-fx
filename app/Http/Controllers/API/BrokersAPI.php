<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class BrokersAPI extends Controller {
    public $data;
    public function __construct() {
        $this->data = [
            'success' => true,
            'data' => [],
            'message' => __('Successful')
        ];
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getBrokerForJose(){
        $data = $this->getBrokerList();
        return response()->json($data);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getBrokerForMobileApp(){
        $user_id = Auth::user()->id;
        $data = $this->getBrokerList($user_id);

        return response()->json($data);
    }
    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getBrokerList($user_id = ''){
        //DB::enableQueryLog();
        $query = DB::table('brokers');
        $query->select('brokers.id','brokers.name','brokers.logo','brokers.advantage','brokers.average_spreads',
            'brokers.recommended_deposit','brokers.URL','brokers.order','lcns.lcns_title',
            DB::raw('Sum( broker_likes.like ) AS total_like'),
            DB::raw('Sum( broker_likes.unlike ) AS total_dislike')
        );
        if($user_id != ''){
            $query->addSelect( DB::raw('COALESCE(mylike.like, 0) AS my_like'),
                DB::raw('COALESCE(mylike.unlike, 0) AS my_dislike'));

            $query->leftJoin('broker_likes as mylike', function ($mylikejoin) use ($user_id){
                $mylikejoin->on('mylike.broker_id', '=', 'brokers.id')->where('mylike.user_id', '=', $user_id);
            });
        }
        $query->leftJoin('broker_likes', 'brokers.id','=', 'broker_likes.broker_id');
        $query->leftJoin(DB::raw("(SELECT broker_licences.broker_id, GROUP_CONCAT(licences.title SEPARATOR '###') AS lcns_title
                        FROM broker_licences INNER JOIN licences ON broker_licences.licence_id = licences.id
                        GROUP BY broker_licences.broker_id) AS lcns"), function ($lcjoin){
            $lcjoin->on('lcns.broker_id', '=', 'brokers.id');
        });
        $query->where('brokers.status', '<>', STATUS_DELETED);
        $query->groupBy('brokers.id');
        $query->orderBy('brokers.order', 'ASC');
        $this->data['data'] = $query->get();
        //dd(DB::getQueryLog());
        $this->data['image_path'] = asset(getImagePath('brokers'));
        return $this->data;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function brokerLikeAction(Request $request){
        $broker_id = $request->broker_id;
        // 1:like / -1:dislike / 0:nutral
        $action = $request->action;
        $user_id = Auth::user()->id;

        $update_data = array(
            'user_id' => $user_id,
            'broker_id' => $broker_id,
            'like' => $action == 1 ? 1 : 0,
            'unlike' => $action == -1 ? 1 : 0
        );
        DB::table('broker_likes')->updateOrInsert(['user_id' => $user_id, 'broker_id' => $broker_id,], $update_data);

        return response()->json($this->data);
    }
}
