<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Auth;

class SignalsAPI extends Controller {
    public $data;
    public function __construct() {
        $this->data = [
            'success' => true,
            'data' => [],
            'message' => __('Successful')
        ];
    }

    /**
     * To get required signal list
     *
     * @param string $status
     * @return \Illuminate\Http\JsonResponse
     */
    public function getSignalList($status = 'all'){
        $this->data['data'] = $this->getSignalData($status);
        return response()->json($this->data);
    }

    /**
     * To get Specific signal
     *
     * @param string $signal_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getSignal($signal_id = ''){
        $this->data['data'] = $this->getSignalData('', $signal_id);
        return response()->json($this->data);
    }

    /**
     * @param int $limit
     * @return \Illuminate\Http\JsonResponse
     */
    public function getLastClosedSignals($limit = 10, $order = 'DESC'){
        $this->data['data'] = $this->getSignalData(SIGNAL_CLOSED, '',  $order);
        return response()->json($this->data);
    }

    /**
     * @param string $signal_status
     * @param string $signal_id
     * @return \Illuminate\Database\Eloquent\Model|\Illuminate\Database\Query\Builder|\Illuminate\Support\Collection|object|null
     */
    public function getSignalData($signal_status = '', $signal_id = '', $order = 'DESC') {
        if (!empty(allSetting('mobile_app_signal_per_page'))){
            $signal_per_page = allSetting('mobile_app_signal_per_page');
        }else{
            $signal_per_page = 12;
        }
        $data = DB::table('signals');
        $data->select('signals.id',
            'signals.from_statement','signals.financial_instrument_code AS financial_instrument_key_code',
            DB::raw("UPPER(concat(substring(signals.financial_instrument_code,1,3), '/', substring(signals.financial_instrument_code,4,6))) AS financial_instrument_code"),
            'trade_types.title AS trade_type', 'order_types.title AS order_type', 'signals.action', 'signals.entry_price',
            'signals.take_profit_1', 'signals.stop_loss_1', 'signals.take_profit_2', 'signals.stop_loss_2',
            'signals.take_profit_3', 'signals.stop_loss_3', 'signals.take_profit_4', 'signals.stop_loss_4', 'signals.available_for_free',
            'signals.comments', 'signals.signal_result', 'signals.status', 'signals.created_at', 'signals.updated_at');
        $data->leftJoin('trade_types', 'signals.trade_type', '=', 'trade_types.id');
        $data->leftJoin('order_types', 'signals.order_type', '=', 'order_types.id');
        $data->where('signals.status', '<>', STATUS_DELETED);
        if ($signal_id != '') {
            $data->where('signals.id', $signal_id);
        }
        if($signal_status != ''){
            if($signal_status == 'active'){
                $data->where('signals.status', SIGNAL_ACTIVE);
            }
            elseif($signal_status == 'pending'){
                $data->where('signals.status', SIGNAL_PENDING);
            }
            elseif($signal_status == 'closed'){
                $data->where('signals.status', SIGNAL_CLOSED);
            }
            else{
                //---
            }
        }
        $data->orderBy('id', $order);

        if ($signal_id != '') {
            return $data->first();
        } else {
            return $data->paginate($signal_per_page);
        }
    }

    public function getPlatformResult(){
        $user_reg_date = Auth::user()->created_at;
        $query = DB::table('signals')
            ->select(
                DB::raw("SUM(signals.signal_result) AS total_pips"),
                DB::raw("SUM(CASE WHEN signals.created_at > '".$user_reg_date."' THEN signals.signal_result ELSE 0 END) AS after_registration"),
                DB::raw("SUM(CASE WHEN signals.created_at BETWEEN DATE_SUB(NOW(), INTERVAL 30 DAY) AND NOW() THEN signals.signal_result ELSE 0 END) AS last_30")
            )
            ->where('status', '=', SIGNAL_CLOSED);
        $this->data['data'] = $query->get();
        return response()->json($this->data);
    }
}
