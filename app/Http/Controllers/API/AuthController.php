<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\Api\LoginRequest;
use App\Http\Controllers\Controller;
use App\Http\Requests\Customer\RegistrationRequest;
use App\Http\Requests\ForgetPasswordRequest;
use App\Http\Services\UserAuthService;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class AuthController extends Controller {
    public $data;

    public function __construct() {
        $this->data = [
            'success' => FALSE,
            'data'    => [],
            'message' => __('Something Went wrong !')
        ];
    }

    /**
     * @param LoginRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(LoginRequest $request) {
        $user = User::where('email', $request->email)->first();
        $status = 401;
        if (isset($user) && Hash::check($request->password, $user->password)) {
            if ($user->role == USER_ROLE_USER) {
                if ($user->active_status == STATUS_SUCCESS) {
                    $status = 200;
                    if ($user->email_verified == STATUS_SUCCESS) {
                        // Emailed Verified -----
                        $this->data['success'] = TRUE;
                        $this->data['message'] = __('Successfully logged in');
                        $token_info = $this->accessTokenProcess($user, $request);
                        $user_data = [
                            'access_token'   => $token_info['access_token'],
                            'email_verified' => $user->email_verified,
                            'user_info'      => $user,
                            'user_photo'     => !empty($user->photo) ? asset(getImagePath('user') . $user->photo) : ''
                        ];
                        $this->data['data'] = $user_data;
                    } else {
                        // Emailed Not Verified -----
                        // SEND EMAIL VERIFICATION TO GIVEN EMAIL
                        $user_data = [
                            'email_verified' => $user->email_verified
                        ];
                        $this->data['data'] = $user_data;
                        $this->data['message'] = __('Your email is not verified. Please verify your email to get full access.');
                    }
                } else if ($user->active_status == STATUS_SUSPENDED) {
                    $this->data['message'] = __("Your account has been suspended. please contact support team to active again");
                } else if ($user->active_status == STATUS_DELETED) {
                    $this->data['message'] = __("Your account has been deleted. please contact support team to active again");
                } else if ($user->active_status == STATUS_PENDING) {
                    $this->data['message'] = __("Your account has been Pending for admin approval. please contact support team to active again");
                }elseif ($user->active_status == STATUS_BLOCKED) {
                    $this->data['message'] = __('You are blocked. Contact with the support team.');
                }
                else {
                    $this->data['message'] = __("Your Account has some problem. please contact support team.");
                }
            } else {
                $this->data['message'] = __("You Are Not Authorised.");
            }
        } else {
            $user_data['password'] = Hash::check($request->password, $user->password) ? TRUE : FALSE;
            $this->data['data'] = $user_data;
            $this->data['message'] = __("Email or password doesn't match.");
        }
        return response()->json($this->data, $status);
    }

    /**
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function socialLogin(Request $request) {
        $user = User::where('email', $request->email)->first();
        $this->data['success'] = TRUE;
        $this->data['message'] = __('Success');
        if (!$user) {
            $this->data['data']['user_exist'] = FALSE;
        } else {
            $this->data['data']['user_exist'] = TRUE;
            $token_info = $this->accessTokenProcess($user, $request);
            $user_data = [
                'access_token'   => $token_info['access_token'],
                'email_verified' => $user->email_verified,
                'user_info'      => $user,
                'user_photo'     => !empty($user->photo) ? asset(getImagePath('user') . $user->photo) : ''
            ];
            $this->data['data']['user_info'] = $user_data;
        }

        return response()->json($this->data);
    }

    /**
     * SOCIAL REGISTRATION
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function socialRegistration(Request $request) {
        $response = $this->socialRegistrationDataSave($request);
        if ($response) {
            $user = User::where('email', $request->email)->first();
            $token_info = $this->accessTokenProcess($user, $request);
            $user_data = [
                'access_token' => $token_info['access_token'],
                'access_type'  => "Bearer",
                'user_info'    => $user
            ];
            $this->data['success'] = TRUE;
            $this->data['data'] = $user_data;
            $this->data['message'] = __('Registration Successfull');
        }
        return response()->json($this->data);
    }

    /**
     * DATA SAVE OPERATION FROM SOCIAL REGISTRATION
     *
     * @param $request
     *
     * @return mixed
     */
    public function socialRegistrationDataSave($request) {
        $image_name = '';
        if (isset($request->photo_url)) {
            $profile_contents = file_get_contents($request->photo_url);
            $image_name = md5($request->email) . '.jpg';
            file_put_contents(getImagePath('user') . '/' . $image_name, $profile_contents);
        }
        $date = str_replace('/', '-', $request->dob);
        $birth_date = Carbon::parse($date)->format('Y-m-d');
        $user = [
            'name'           => $request->first_name . ' ' . $request->last_name,
            'first_name'     => $request->first_name,
            'last_name'      => $request->last_name,
            'phone'          => $request->phone,
            'country'          => $request->country,
            'email'          => $request->email,
            'dob'            => $birth_date,
            'password'       => bcrypt('1234'),
            'reset_token'    => md5($request->email . uniqid() . randomString(5)),
            'remember_token' => md5($request->email . uniqid() . randomString(5)),
            'email_verified' => STATUS_SUCCESS,
            'active_status'  => STATUS_SUCCESS,
            'role'           => USER_ROLE_USER,
            'photo'          => $image_name,
            'own_code'       => Str::random(10)
        ];

        return User::create($user);
    }

    /**
     * NORMAL REGISTRATION
     *
     * @param RegistrationRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function registration(RegistrationRequest $request) {
        $service = new UserAuthService();
        if (!empty($request->code)) {
            $parentId = $service->userParentId($request->code);
            if (isset($parentId) && !empty($parentId)) {
                $response = $service->userSignUpProcess($request, $parentId);
            } else {
                $this->data['message'] = __('Reference Code Not valid.Please input correct code!');
            }
        } else {
            $response = $service->userSignUpProcess($request);
        }
        if ($response['success'] == TRUE) {
            $user = User::where('email', $request->email)->first();
            $token_info = $this->accessTokenProcess($user, $request);
            $user_data = [
                'access_token' => $token_info['access_token'],
                'access_type'  => "Bearer",
                'user_info'    => $user
            ];
            $response['data'] = $user_data;
        }
        return response()->json($response);
    }

    /**
     * RESET PASSWORD FROM PROFILE
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function resetPassword(ForgetPasswordRequest $request) {
        $user_auth_service = new UserAuthService();
        $this->data = $user_auth_service->sendForgotPasswordMail($request);
        return response()->json($this->data);
    }

    /**
     * FOR LOG OUT
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout(Request $request) {
        $access_token_id = Auth::user()->token()->id;
        DB::table('oauth_access_tokens')->where('id', $access_token_id)->delete();
        $this->data = [
            'success' => TRUE,
            'data'    => ['driver' => $request->driver],
            'message' => __('Logged Out')
        ];
        return response()->json($this->data);
    }

    /**
     * CREATE ACCESS TOKEN AND SAVE THE DEVICE TOKEN ALSO
     *
     * @param $user
     * @param $email
     *
     * @return array
     */
    public function accessTokenProcess($user, $request) {
        $data = [];
        $token_data = $user->createToken($request->email)->toArray();
        $data['access_token'] = $token_data['accessToken'];
        $token_attribute = $token_data['token']->toArray();
        $data['access_token_id'] = $token_attribute['id'];
        if ($request->device_token != '') {
            DB::table('oauth_access_tokens')
              ->where('id', $data['access_token_id'])
              ->update(['device_token' => $request->device_token, 'driver' => $request->driver]);
        }
        return $data;
    }

}
