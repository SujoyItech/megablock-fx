<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Admin\NotificationController;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class MegaBlockAPI extends Controller {
    public $data;
    public function __construct() {
        $this->data = [
            'success' => true,
            'data' => [],
            'message' => __('Successful')
        ];
    }

    public function testForRifat(){
        $notification_data = array(
            'type' => SIGNAL_UPDATED,
            'signal_id' => 301560842,
            'financial_code' => substr_replace(strtoupper('eurusd'), '/', 3, 0),
            'signal_action' => 'buy',
            'signal_status' => 3,
            'signal_result' => -10,
            'available_for_free' => 1
        );
        NotificationController::createNewNotification($notification_data);
        return response()->json($this->data);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getCountryList(){
        $this->data['data'] = country();
        return response()->json($this->data);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getCredits(){
        $this->data['data'] = getBalance();
        return response()->json($this->data);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getUserProfileInformation(){
        $this->data['data'] = Auth::user();
        return response()->json($this->data);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function changeProfileInformation(Request $request){
        $post_data = $request->all();
        $update_data = array_filter($post_data);
        $update_data['name'] = $request->first_name. ' '. $request->last_name;
        Auth::user()->update($update_data);
        $this->data['data'] = Auth::user();
        return response()->json($this->data);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function changePassword(Request $request){
        if($request->password != $request->password_confirmation){
            $this->data['success'] = false;
            $this->data['message'] = __('Password doesn\'t Match');
        }else{
            if(Hash::check($request->old_password, Auth::user()->password)) {
                try {
                    Auth::user()->update(['password' => bcrypt($request->password)]);
                    $this->data['data'] = __('Password Changed Successfully');
                }catch (\Exception $exception){
                    $this->data['success'] = false;
                    $this->data['message'] = __('Password Changing Failed');
                }
            }else{
                $this->data['success'] = false;
                $this->data['message'] = __('Wrong Old Password');
            }
        }
        return response()->json($this->data);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getUserPaymentHistory(){
        $query = DB::table('subscription_histories')
            ->select('subscription_histories.id','subscription_histories.credit',
                'subscription_histories.status','subscription_packages.title','subscription_packages.price',
                'subscription_packages.credit','subscription_packages.is_free','subscription_histories.created_at')
            ->leftJoin('subscription_packages','subscription_histories.package_id','=','subscription_packages.id')
            ->where('subscription_histories.user_id', Auth::user()->id)
            ->orderBy('subscription_histories.id','desc');
        $this->data['data'] = $query->get();
        return response()->json($this->data);
    }


}
