<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Customer\SubscriptionController;
use App\Http\Services\SubscriptionService;
use App\Models\SubscriptionPackage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Stripe;

class SubscriptionAPI extends Controller {
    public $data;
    public function __construct() {
        $this->data = [
            'success' => true,
            'data' => [],
            'message' => __('Successful')
        ];
    }
    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getSubscriptionPackages(){
        $query = DB::table('subscription_packages');
        $query->select('subscription_packages.id','subscription_packages.title','subscription_packages.price',
            'subscription_packages.credit','subscription_packages.is_free','subscription_packages.image',
            'subscription_packages.description','subscription_packages.fees','subscription_packages.vat');
        $query->where('subscription_packages.status', STATUS_ACTIVE);
        $this->data['data'] = $query->get();
        $this->data['image_path'] = asset(getImagePath('subscription_package'));
        $this->data['currency'] = !empty(allSetting('currency')) && allSetting('currency') == 2 ? 'euro':'usd';
        return response()->json($this->data);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws Stripe\Exception\ApiErrorException
     */
    public function buySubscriptionPackage(Request $request){
        $packege = SubscriptionPackage::find($request->package_id);
        if($request->gateway == 'stripe'){
            $response = $this->makeStripePayment($packege, $request->stripeToken);
        }
        return response()->json($response);
    }

    /**
     * @param $packege
     * @param $stripe_token
     * @return array
     * @throws Stripe\Exception\ApiErrorException
     */
    public function makeStripePayment($packege, $stripe_token){
        $all_settings = allSetting();
        try {
            Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));
            $stripe_data = [
                "amount" => intval($packege->price),
                "currency" => isset($all_settings['currency']) && $all_settings['currency'] == CURRENCY_EURO ? "eur":"usd",
                "source" => $stripe_token,
                "description" => "payment from megablock subscription"
            ];
            //dd($stripe_data);
            $response = Stripe\Charge::create($stripe_data);
            if ($response->status == 'succeeded') {
                $data = [
                    'user_id' => Auth::user()->id,
                    'event_slug' => 'Subscription_Packages',
                    'event_id' => $packege->id,
                    'payment_source' => 'API',
                    'payment_gateway' => 'stripe',
                    'payment_method' => 'card',
                    'response' => $response,
                    'status' => $response->status
                ];
                DB::table('transaction_history')->insert($data);
                app(SubscriptionController::class)->packageSubscription('', $packege->id);
                $this->data['data']['credit'] = getBalance();
            } else {
                $this->data['success'] = false;
                $this->data['message'] = $response->status . '. Subscription not purchased.';
            }
        }catch (\Exception $exception){
            $this->data['success'] = false;
            $this->data['message'] = 'Payment Failed';
        }
        return $this->data;
    }


}
