<?php
    namespace App\Http\Services;

    use App\Models\FinancialInstrumentCode;
    use App\Models\OrderType;
    use App\Models\TradeType;
    use Illuminate\Http\Request;
    use Illuminate\Support\Facades\DB;

    class SignalCategoryService{
        public function financialInstrumentCode(){
            return DB::table('financial_instrument_codes')->where('status','!=',6)->orderBy('title','asc');
        }
        public function financialInstrumentCodeCreate(Request $request)
        {
            $title = ($request->get('title'));
            $title = str_replace('/', '', $title);
            $title_id = strtolower($title);
            try {
                $user = [
                    'id' => $title_id,
                    'title' => $request->get('title')
                ];
                $data = FinancialInstrumentCode::create($user);
                return ['status' => true,'data'=>['user'=>$data], 'message' => __('Financial Instrument code created successfully.')];
            } catch (\Exception $e) {
                return ['status' => false,'data'=>[], 'message' => __('Something went wrong.')];
            }
        }

        public function financialInstrumentCodeUpdate(Request $request)
        {
            $id = $request->edit_id;
            $title = ($request->get('title'));
            $title = str_replace('/', '', $title);
            $title_id = strtolower($title);
            try {
                $user = [
                    'id' => $title_id,
                    'title' => $request->get('title')
                ];
                $data = FinancialInstrumentCode::where(['id' => $id])->update($user);
                return ['status' => true,'data'=>['user'=>$data], 'message' => __('Financial Instrument code updated successfully.')];
            } catch (\Exception $e) {
                return ['status' => false,'data'=>[], 'message' => __('Something went wrong.')];
            }
        }

        public function tradeType(){
            return TradeType::where('status','!=',6)->orderBy('title','asc');
        }
        public function tradeTypeCreate(Request $request)
        {
            try {
                $user = [
                    'title' => $request->get('title')
                ];
                $data = TradeType::create($user);
                return ['status' => true,'data'=>['user'=>$data], 'message' => __('Trade type created successfully.')];
            } catch (\Exception $e) {
                return ['status' => false,'data'=>[], 'message' => __('Something went wrong.')];
            }
        }

        public function tradeTypeUpdate(Request $request)
        {
            $common_service = new CommonService();
            $id = $common_service->checkValidId($request->edit_id);
            if(!is_numeric($id)&&$id['success']==false){
                return redirect()->back()->with(['dismiss'=>__('Trade type not found.')]);
            }
            try {
                $user = [
                    'title' => $request->get('title')
                ];
                $data = TradeType::where(['id' => $id])->update($user);
                return ['status' => true,'data'=>['user'=>$data], 'message' => __('Trade type updated successfully.')];
            } catch (\Exception $e) {
                return ['status' => false,'data'=>[], 'message' => __('Something went wrong.')];
            }
        }

        public function tradeTypeFind($id){
            $common_service = new CommonService();
            $id = $common_service->checkValidId($id);
            if(!is_numeric($id)&&$id['success']==false){
                return redirect()->back()->with(['dismiss'=>__('Trade type not found.')]);
            }
            try {
                $user = TradeType::find($id);
            } catch (\Exception $e) {
                return ['status' => false,'data'=>[], 'message' => __('Something went wrong.')];
            }
            return ['status' => true,'data'=>['user'=>$user], 'message' => __('Trade type get successfully.')];
        }

        public function orderType(){
            return OrderType::where('status','!=',6)->orderBy('title','asc');
        }
        public function orderTypeCreate(Request $request)
        {
            try {
                $user = [
                    'title' => $request->get('title')
                ];
                $data = OrderType::create($user);
                return ['status' => true,'data'=>['user'=>$data], 'message' => __('Order Type created successfully.')];
            } catch (\Exception $e) {
                return ['status' => false,'data'=>[], 'message' => __('Something went wrong.')];
            }
        }

        public function orderTypeUpdate(Request $request)
        {
            $common_service = new CommonService();
            $id = $common_service->checkValidId($request->edit_id);
            if(!is_numeric($id)&&$id['success']==false){
                return redirect()->back()->with(['dismiss'=>__('Order Type not found.')]);
            }
            try {
                $user = [
                    'title' => $request->get('title')
                ];
                $data = OrderType::where(['id' => $id])->update($user);
                return ['status' => true,'data'=>['user'=>$data], 'message' => __('Order Type updated successfully.')];
            } catch (\Exception $e) {
                return ['status' => false,'data'=>[], 'message' => __('Something went wrong.')];
            }
        }

        public function orderTypeFind($id){
            $common_service = new CommonService();
            $id = $common_service->checkValidId($id);
            if(!is_numeric($id)&&$id['success']==false){
                return redirect()->back()->with(['dismiss'=>__('Order Type not found.')]);
            }
            try {
                $user = OrderType::find($id);
            } catch (\Exception $e) {
                return ['status' => false,'data'=>[], 'message' => __('Something went wrong.')];
            }
            return ['status' => true,'data'=>['user'=>$user], 'message' => __('Order Type get successfully.')];
        }



    }
