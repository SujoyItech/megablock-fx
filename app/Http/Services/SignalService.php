<?php

namespace App\Http\Services;

use App\Http\Controllers\Admin\NotificationController;
use App\Http\Requests\SignalRequest;
use App\Models\Notification;
use App\Models\Signal;
use App\Models\SignalHistory;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SignalService {
    public function __construct() {
        //----
    }

    public static function signal() {
        $signals = DB::table('signals')
            ->select('signals.id','signals.financial_instrument_code','signals.trade_type','signals.order_type',
                'signals.action',
                DB::raw('TRIM(signals.entry_price) + 0 AS entry_price'),
                DB::raw('TRIM(signals.take_profit_1) + 0 AS take_profit_1'),
                DB::raw('TRIM(signals.take_profit_2) + 0 AS take_profit_2'),
                DB::raw('TRIM(signals.take_profit_3) + 0 AS take_profit_3'),
                DB::raw('TRIM(signals.take_profit_4) + 0 AS take_profit_4'),
                DB::raw('TRIM(signals.stop_loss_1) + 0 AS stop_loss_1'),
                DB::raw('TRIM(signals.stop_loss_2) + 0 AS stop_loss_2'),
                DB::raw('TRIM(signals.stop_loss_3) + 0 AS stop_loss_3'),
                DB::raw('TRIM(signals.stop_loss_4) + 0 AS stop_loss_4'),
                'signals.available_for_free','signals.comments','signals.signal_result','signals.status','signals.created_at',
                'signals.updated_at',
                'trade_types.title as trade_type_title', 'order_types.title as order_type_title')
            ->leftJoin('trade_types', 'signals.trade_type', '=', 'trade_types.id')
            ->leftJoin('order_types', 'signals.order_type', '=', 'order_types.id')
            ->where('signals.status', '<>', STATUS_DELETED)
            ->orderBy('signals.created_at', 'desc')
            ->get();
        return $signals;
    }

    public function createInsertData(Request $request){
        return $insert_data = [
            'financial_instrument_code' => $request->get('financial_instrument_code'),
            'trade_type' => $request->get('trade_type'),
            'order_type' => $request->get('order_type'),
            'from_statement'=> false,
            'action' => strtolower($request->get('action')),
            'entry_price' => $request->get('entry_price'),
            'stop_loss_1' => $request->get('stop_loss_1'),
            'take_profit_1' => $request->get('take_profit_1'),
            'stop_loss_2' => $request->get('stop_loss_2'),
            'take_profit_2' => $request->get('take_profit_2'),
            'take_profit_3' => $request->get('take_profit_3'),
            'stop_loss_3' => $request->get('stop_loss_3'),
            'take_profit_4' => $request->get('take_profit_4'),
            'stop_loss_4' => $request->get('stop_loss_4'),
            'available_for_free' => (!empty($request->get('available_for_free'))) ? $request->get('available_for_free') : 0,
            'comments' => $request->get('comments'),
            'signal_result' => $request->get('signal_result'),
            'status' => $request->get('status'),
            'created_at' => Carbon::now()->format("Y-m-d H:i:s"),
            'updated_at' => Carbon::now()->format("Y-m-d H:i:s")
        ];
    }
    public function createUpdateData(Request $request){
        return $insert_data = [
            'financial_instrument_code' => $request->get('financial_instrument_code'),
            'trade_type' => $request->get('trade_type'),
            'order_type' => $request->get('order_type'),
            'from_statement'=> false,
            'action' => strtolower($request->get('action')),
            'entry_price' => $request->get('entry_price'),
            'stop_loss_1' => $request->get('stop_loss_1'),
            'take_profit_1' => $request->get('take_profit_1'),
            'stop_loss_2' => $request->get('stop_loss_2'),
            'take_profit_2' => $request->get('take_profit_2'),
            'take_profit_3' => $request->get('take_profit_3'),
            'stop_loss_3' => $request->get('stop_loss_3'),
            'take_profit_4' => $request->get('take_profit_4'),
            'stop_loss_4' => $request->get('stop_loss_4'),
            'available_for_free' => (!empty($request->get('available_for_free'))) ? $request->get('available_for_free') : 0,
            'comments' => $request->get('comments'),
            'signal_result' => $request->get('signal_result'),
            'status' => $request->get('status'),
            'updated_at' => Carbon::now()->format("Y-m-d H:i:s")
        ];
    }

    public function signalCreate(SignalRequest $request) {
        try {
            DB::beginTransaction();
            $insert_data = $this->createInsertData($request);
            $id = DB::selectOne(DB::raw('SELECT MIN(t1.ID + 1) AS nextID
                        FROM signals t1 LEFT JOIN signals t2 ON t1.ID + 1 = t2.ID
                        WHERE t2.ID IS NULL'))->nextID;
            $insert_data['id'] = $id;
            $signal_data = DB::table('signals')->insert($insert_data);
            $notification_data = array(
                'type' => SIGNAL_CREATED,
                'signal_id' => $id,
                'financial_code' => substr_replace(strtoupper($insert_data['financial_instrument_code']), '/', 3, 0),
                'signal_action' => $insert_data['action'],
                'signal_status' => $insert_data['status'],
                'signal_result' => $insert_data['signal_result'],
                'available_for_free' => $insert_data['available_for_free']
            );
            NotificationController::createNewNotification($notification_data);
            DB::commit();
            return ['status' => true, 'data' => ['user' => $signal_data], 'message' => __('Signal created successfully.')];
        } catch (\Exception $e) {
            DB::rollBack();
            return ['status' => false, 'data' => [], 'message' => __('Something went wrong.')];
        }
    }

    public function signalUpdate(SignalRequest $request) {
        $common_service = new CommonService();
        $id = $common_service->checkValidId($request->edit_id);
        if (!is_numeric($id) && $id['success'] == false) {
            return redirect()->back()->with(['dismiss' => __('Signal not found.')]);
        }
        try {
            DB::beginTransaction();
            $insert_data = $this->createUpdateData($request);
            $signal_data = Signal::where(['id' => $id])->update($insert_data);
            $notification_data = array(
                'type' => SIGNAL_UPDATED,
                'signal_id' => $id,
                'financial_code' => substr_replace(strtoupper($insert_data['financial_instrument_code']), '/', 3, 0),
                'signal_action' => $insert_data['action'],
                'signal_status' => $insert_data['status'],
                'signal_result' => $insert_data['signal_result'],
                'available_for_free' => $insert_data['available_for_free']
            );
            NotificationController::createNewNotification($notification_data);
            DB::commit();
            return ['status' => true, 'data' => ['user' => $signal_data], 'message' => __('Signal updated successfully.')];
        } catch (\Exception $e) {
            DB::rollBack();
            return ['status' => false, 'data' => [], 'message' => __('Something went wrong.')];
        }
    }

    public function signalFind($id) {
        $common_service = new CommonService();
        $id = $common_service->checkValidId($id);
        if (!is_numeric($id) && $id['success'] == false) {
            return redirect()->back()->with(['dismiss' => __('Item not found.')]);
        }
        try {
            $user = Signal::find($id);
        } catch (\Exception $e) {
            return ['status' => false, 'data' => [], 'message' => __('Something went wrong.')];
        }
        return ['status' => true, 'data' => ['user' => $user], 'message' => __('Signal get successfully.')];
    }

}
