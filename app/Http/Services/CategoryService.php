<?php

namespace App\Http\Services;

use App\Http\Repositories\CategoryRepository;
use Illuminate\Http\Request;

class CategoryService
{
    public function select($id=null){
        if(!empty($id)){
            $common_service = new CommonService();
            $id = $common_service->checkValidId($id);
            if(!is_numeric($id)&&$id['success']==false){
                return redirect()->back()->with(['dismiss'=>__('Item not found.')]);
            }
        }
        $userService = new CategoryRepository();
        try{
            $users = $userService->select($id);
        }catch(\Exception $e){
            return ['status' => false, 'data'=>[], 'message' => __('Something went wrong.')];
        }
        return ['status' => true, 'data'=>$users, 'message' => __('Category data fetched successful.')];
    }

    public function create(Request $request)
    {
        try {
            $user_repo = new CategoryRepository();
            $user = $user_repo->create($request);
        } catch (\Exception $e) {
            return ['status' => false,'data'=>[], 'message' => $e->getMessage()];
//            return ['status' => false,'data'=>[], 'message' => __('Something went wrong.')];
        }
        if ($request->parent_id) {
            $success_message = __('Sub category created successfully.');
         } else {
            $success_message = __('Category created successfully.');
        }
        return ['status' => true,'data'=>['user'=>$user], 'message' => $success_message];
    }

    public function update(Request $request)
    {
        $common_service = new CommonService();
        $id = $common_service->checkValidId($request->edit_id);
        if(!is_numeric($id)&&$id['success']==false){
            return redirect()->back()->with(['dismiss'=>__('Item not found.')]);
        }
        try {
            $user_repo = new CategoryRepository();
            $user = $user_repo->update($request,$id);
        } catch (\Exception $e) {
            return ['status' => false,'data'=>[], 'message' => $e->getMessage()];
//            return ['status' => false,'data'=>[], 'message' => __('Something went wrong.')];
        }
        if (isset($request->parent_id)) {
            return ['status' => true,'data'=>['user'=>$user], 'message' => __('Sub category updated successfully.')];
        } else {
            return ['status' => true,'data'=>['user'=>$user], 'message' => __('Category updated successfully.')];
        }
    }

    public function find($id){
        $common_service = new CommonService();
        $id = $common_service->checkValidId($id);
        if(!is_numeric($id)&&$id['success']==false){
            return redirect()->back()->with(['dismiss'=>__('Item not found.')]);
        }

        try {
            $user_repo = new CategoryRepository();
            $user = $user_repo->find($id);
        } catch (\Exception $e) {
            return ['status' => false,'data'=>[], 'message' => __('Something went wrong.')];
        }
        return ['status' => true,'data'=>['item'=>$user], 'message' => __('Category get successfully.')];
    }
}
