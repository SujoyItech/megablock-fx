<?php


namespace App\Http\Services;

use App\Http\Repositories\SubscriptionPackageRepository;
use App\Http\Requests\SubscriptionPackageRequest;
use App\Models\SubscriptionHistories;
use App\Models\SubscriptionPackage;
use App\Models\Wallet;
use App\Models\WalletHistory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SubscriptionPackageService
{
    public function subscription(){
        return SubscriptionPackage::where('status','!=',6);
    }
    public function subscriptionPackageCreate(SubscriptionPackageRequest $request)
    {
        try {
            $user_repo = new SubscriptionPackageRepository();
            $user = $user_repo->create($request);
            return ['status' => true,'data'=>['user'=>$user], 'message' => __('Subscription Package created successfully.')];
        } catch (\Exception $e) {

            return ['status' => false,'data'=>[], 'message' => __('Something went wrong.')];
        }
    }

    public function subscriptionPackageUpdate(SubscriptionPackageRequest $request)
    {
        $common_service = new CommonService();
        $id = $common_service->checkValidId($request->edit_id);
        if(!is_numeric($id)&&$id['success']==false){
            return redirect()->back()->with(['dismiss'=>__('Item not found.')]);
        }
        try {
            $user_repo = new SubscriptionPackageRepository();
            $user = $user_repo->update($request,$id);
            return ['status' => true,'data'=>['user'=>$user], 'message' => __('Subscription updated successfully.')];
        } catch (\Exception $e) {
            return ['status' => false,'data'=>[], 'message' => $e->getMessage()];
        }
    }

    public function subscriptionPackageFind($id){
        $common_service = new CommonService();
        $id = $common_service->checkValidId($id);
        if(!is_numeric($id)&&$id['success']==false){
            return redirect()->back()->with(['dismiss'=>__('Item not found.')]);
        }
        try {
            $user = SubscriptionPackage::find($id);
        } catch (\Exception $e) {
            return ['status' => false,'data'=>[], 'message' => __('Something went wrong.')];
        }
        return ['status' => true,'data'=>['user'=>$user], 'message' => __('Subscription get successfully.')];
    }
    public function subscriptionPackageDelete($id){
        $common_service = new CommonService();
        $id = $common_service->checkValidId($id);
        if(!is_numeric($id)&&$id['success']==false){
            return redirect()->back()->with(['dismiss'=>__('Item not found.')]);
        }
        try {
            $user = SubscriptionPackage::where('id',$id)->update(['status'=>STATUS_DELETED]);
        } catch (\Exception $e) {
            return ['status' => false,'data'=>[], 'message' => __('Something went wrong.')];
        }
        return ['status' => true,'data'=>['user'=>$user], 'message' => __('Subscription Package get successfully.')];
    }

    public function subscriptionHistories(){
        return SubscriptionHistories::join('users','users.id','subscription_histories.user_id')
            ->join('subscription_packages','subscription_packages.id','subscription_histories.package_id')

            ->select(
                'users.name',
                'subscription_histories.status',
                'subscription_histories.created_at',
                'subscription_packages.title',
                'subscription_packages.price',
                'subscription_packages.credit'
            );
    }
    public function subscriptionPaymentHistories(){
        return WalletHistory::join('users','users.id','wallet_histories.user_id')

            ->select(
                'users.name',
//                'wallet_histories.coin_address',
             //   'wallet_histories.description',
                'wallet_histories.amount',
//                'wallet_histories.coin_amount',
                'wallet_histories.status',
                'wallet_histories.id',
                'wallet_histories.created_at'

            );
    }

    public function activeCoinPayment($id){


        DB::beginTransaction();

        try {
            $pending_payment = WalletHistory::find($id);

            if (!empty($pending_payment) && ($pending_payment->status == STATUS_PENDING)){

                $pending_payment->status = STATUS_SUCCESS;
                $pending_payment->save();

                $wallet = Wallet::find($pending_payment->user_id);
                $wallet->increment('balance',intval($pending_payment->amount));



                $subscription_histories = new SubscriptionHistories();
                $subscription_histories->user_id = $pending_payment->user_id;
                $subscription_histories->package_id = $pending_payment->package_id;
                $subscription_histories->credit = $pending_payment->amount;
                $subscription_histories->status = STATUS_SUCCESS;
                $subscription_histories->save();



            }

            DB::commit();
            return ['status' => true,'data'=>[], 'message' => __('Payment Active successfully.')];
            // all good
        } catch (\Exception $e) {
            DB::rollback();
            dd($e);
            return ['status' => false,'data'=>[], 'message' => __('Something went wrong.')];
            // something went wrong
        }



    }

}
