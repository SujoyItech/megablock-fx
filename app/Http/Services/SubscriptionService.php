<?php
/**
 * Created by PhpStorm.
 * User: jony
 * Date: 12/30/19
 * Time: 1:39 PM
 */

namespace App\Http\Services;


use App\Models\SubscriptionHistories;
use App\Models\SubscriptionPackage;
use App\Models\Wallet;
use App\Models\WalletHistory;
use App\Models\WallletHistory;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use function React\Promise\Stream\first;

class SubscriptionService
{
    public function getAllPackages(){
        $data = ['status'=>true,'message'=>__('Subscription List')];
        $data['packages'] = SubscriptionPackage::where('status',STATUS_SUCCESS)->get();
        return $data;
    }

    public function subscriptionPackage($package_id){
        $data = ['status'=>false,'message'=>__('Subscription Failed.')];

        $package_details = SubscriptionPackage::find($package_id);

        if (!empty($package_details)){
            DB::beginTransaction();
            try {
                if(Auth::user()->subscribed_once == 0 && !empty(Auth::user()->used_code))
                {
                    $credit =  $package_details->credit;
                    // Owner get Subscription Bonus
                    $owner = User::where('own_code',Auth::user()->used_code)->first();
                    if (!empty($owner)) {
                        $owner_wallet_balance = Wallet::where('user_id', $owner->id)->first()->balance;
                        $on_register_purchase = allSetting('on_register_and_purchase');
                        $new_balance = $owner_wallet_balance + $on_register_purchase;
                        Wallet::where('user_id', $owner->id)->update(['balance' => $new_balance]);
                        $owner_wallet_histories = [
                            'user_id' => $owner->id,
                            'type' => REFERRAL,
                            'description' => Auth::user()->name . __('Subscribe By Me. Bonus Credit ') . $on_register_purchase,
                            'amount' => $on_register_purchase
                        ];
                        WalletHistory::create($owner_wallet_histories);
                    }

                    User::where('id',Auth::id())->update(['subscribed_once'=>1]);
                }else{
                    $credit = $package_details->credit;
                }
                $wallet = Wallet::where('user_id', Auth::id())->first();
                if (empty($wallet))
                    $wallet = new Wallet();

                $wallet->user_id = Auth::id();
                $wallet->balance =  $wallet->balance + $credit;
                $wallet->save();

                //////////////////subscription histories///////////////////////
                $subctription_histories = new SubscriptionHistories();
                $subctription_histories->user_id = Auth::id();
                $subctription_histories->package_id = $package_details->id;
                $subctription_histories->credit = $credit;
                $subctription_histories->save();

                //////////////////wallet histories////////////////////////////
                $wallet_histories = new WalletHistory();
                $wallet_histories->user_id = Auth::id();
                $wallet_histories->type = 1;
                $wallet_histories->description = __('Subscribed for ').$credit.__(' credit');
                $wallet_histories->amount = $credit;
                $wallet_histories->save();
                DB::commit();

                $data['status'] = true;
                $data['message'] = __('Subscription Successfully.');

                // all good
            } catch (\Exception $e) {
                DB::rollback();

                if (env('APP_ENV') != 'production' )
                    $data['message'] = $e->getLine().'=='.$e->getFile().'==='.$e->getMessage();
                // something went wrong
            }
            return $data;



           // $wallet
        }

    }

}
