<?php

namespace App\Http\Services;

use App\Http\Repositories\UserAuthRepository;
use App\Http\Requests\Customer\RegistrationRequest;
use App\Http\Requests\ImageUploadRequest;
use App\Http\Requests\ProfileUpdateRequest;
use App\Mail\SendPassResetEmail;
use App\Models\UserAffiliateCode;
use App\Models\UserVerificationCode;
use App\Models\Wallet;
use App\Models\WalletHistory;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;

class UserAuthService
{
    public function _credentials(Request $request)
    {
        $field = filter_var($request->input($this->_username()), FILTER_VALIDATE_EMAIL) ? 'email' : 'username';
        $request->merge([$field => $request->input($this->_username())]);
        return $request->only($field, 'password');
    }

    public function _username()
    {
        return 'email';
    }

    public function _checkActiveStatus()
    {
        $user = Auth::user();

        if ($user->activestatus == 3) {
            return [
                'status' => true,
                'message' => '',
            ];
        } elseif ($user->activestatus == 2) {
            /* $loginDifference = $this->_checkLastLogin();
            $checkloginDifference = isset($default['chk_login_diff']) && !empty($default['chk_login_diff']) ? $default['chk_login_diff'] : 0;

            if ($checkloginDifference != 0 && $loginDifference >= $checkloginDifference)
            {

                $user->activestatus = 2;
                if ($user->update())
                {
                    Auth::logout();
                    return [
                        'status' => false,
                        'message' => __('Your account has been suspended by System. Please contact to support center.')
                    ];
                }
            }*/

            Auth::logout();
            return [
                'status' => false,
                'message' => __('Your account is suspended Please contact our support center!')
            ];
        } elseif ($user->activestatus == 1) {
            Auth::logout();
            return [
                'status' => false,
                'message' => __('Your account has been deleted!')
            ];
        }
    }

    public function _checkLastLogin()
    {
        $lastLogin = Auth::user()->accacts()->where('action', strtolower('sign_in'))->first();
        $now = Carbon::now();
        $lastLogin = Carbon::parse($lastLogin);
        $checkingTime = Carbon::parse($now);

        return $checkingTime->diffInDays($lastLogin);
    }

    public function _checkEmailVerification()
    {
        $user = Auth::user();
        if ($user && $user->evs == 2) {
            return true;
        }

        $mailService = app(MailService::class);
        $userName = $user->fname . ' ' . $user->lname;
        $userEmail = $user->email;
        $companyName = isset($default['company']) && !empty($default['company']) ? $default['company'] : __('Trade By Trade');
        $subject = __('Email Verification | :companyName', ['companyName' => $companyName]);
        $data['data'] = $user;
        $mailService->send('email.verifyemail', $data, $userEmail, $userName, $subject);

        return false;
    }

    public function _checkDeviceConfirmation()
    {
        $user = Auth::user();
        $confirmDeviceService = app(ConfirmDeviceService::class);
        $confirmedDevice = $confirmDeviceService->getConfirmedDevice();

        if (!$confirmedDevice) {
            // create device confirmation
            $confirmedDevice = $confirmDeviceService->createDevice(1, $user->id);
        }

        if ($confirmedDevice && $confirmedDevice->is_confirmed != 2) {
            //send device confirmation email
            $companyName = isset($default['company']) && !empty($default['company']) ? $default['company'] : __('Trade By Trade');
            $userName = $user->fname . ' ' . $user->lname;
            $userEmail = $user->email;
            $subject = __('Device Confirmation | :companyName', ['companyName' => $companyName]);
            $data = [
                'data' => $user,
                'confdev' => $confirmedDevice,
            ];
            $mailService = app(MailService::class);
            $mailService->send('email.confdevmail', $data, $userEmail, $userName, $subject);

            return [
                'status' => false,
                'message' => ''
            ];
        }

        // get confirm device
        app(WebSessionService::class)->createWebSession($confirmedDevice->id, $user->id);
        app(ActivitiesRepository::class)->create($user->id, 'Sign In');
    }

    public function _twoStepVerification()
    {
        $user = Auth::user();
        if (
            $user->role == 2 &&
            $user->mvs == 2 &&
            !empty($user->phone) &&
            isset($user->usersetting->phn_check) &&
            $user->usersetting->phn_check == 2
        ) {
            // random number reserve session
            $randomNumber = randomNumber(6);
            // reserving new code
            session()->put('randno', $randomNumber);
            // sent message to the number with session
            app(SmsService::class)->send($user->phone, $randomNumber);
            return true;
        }
        return false;
    }

    public function sendForgotPasswordMail($request)
    {
        try {
            $user = User::where(['email' => $request->email])->first();

            if ($user) {
                // send verifyemail
                $userName = $user->name;
                $userEmail = $request->email;
                $subject = __('Forget Password');
                $data['name'] = $userName;
                $data['reset_token'] = $user->reset_token;
                $sentmail = sendForgotPassWordMail($userEmail, $data, $subject);
                return [
                    'success' => true,
                    'role' => $user->role,
                    'message' => __('Mail sent successfully.')
                ];
            } else {
                return [
                    'success' => false,
                    'message' => __('Your email is not correct !')
                ];
            }
        } catch (\Exception $e) {
            return [
                'success' => false,
                'message' => __('Something went wrong . Please try again!')
            ];
        }
    }

    public function forgetPasswordChangeProcess($request)
    {
        try {
            $user = User::where(['reset_token' => $request->reset_token])->first();
            if ($user) {
                $update_password['reset_token'] = md5($user->email . uniqid() . randomString(5));
                $update_password['password'] = Hash::make($request->password);
                $update_password['email_verified'] = STATUS_SUCCESS;

                $updated = User::where(['id' => $user->id, 'reset_token' => $user->reset_token])->update($update_password);

                if ($updated) {
                    return [
                        'success' => true,
                        'role' => $user->role,
                        'message' => __('Password changed successfully.')
                    ];
                } else {
                    return [
                        'success' => false,
                        'message' => __('Password not changed try again.')
                    ];
                }
            } else {
                return [
                    'success' => false,
                    'message' => __('Password not changed try again.')
                ];
            }
        } catch (\Exception $e) {
            return [
                'success' => false,
                'message' => __('Something went wrong . Please try again!')
            ];
        }
    }


    public function userSignUpProcess(RegistrationRequest $request, $parentId = null)
    {
        $data = ['success' => false, 'data' => [], 'message' => __('Invalid request!')];
        $date = str_replace('/', '-', $request->input('dob'));
        DB::beginTransaction();
        try {
            $repo = new UserAuthRepository();
            $user = $repo->userDataSave($request);
            if ($user) {
                $repo->userAllInformationSave($request, $user);
                $repo->userMailVerification($user, $parentId);
                $data = ['success' => true, 'message' => __('Verification code has sent to your email. please check your email.')];
            } else {
                $data = ['success' => false, 'data' => [], 'message' => __('Something went wrong.')];
            }
        } catch (\Exception $e) {
            DB::rollBack();
            $data = ['success' => false, 'data' => [], 'message' => $e->getMessage()];
        }
        DB::commit();
        return $data;
    }

    public function userProfileMissingDataUpdate(ProfileUpdateRequest $request)
    {
        $repo = new UserAuthRepository();
        $userUpdate = $repo->userProfileUpdate($request);
        $service = new CommonService();
        $userId = $service->checkValidId($request->edit_id);
        $user = User::where('id',$userId)->first();
        DB::beginTransaction();
        try {
            if ($userUpdate) {
                $parentId = null;
                if($user->used_code != null)
                {
                    $parentId = User::where('own_code',$user->used_code)->first()->id;
                }

                if (isset($parentId) && $parentId != null) {
                    // Affiliate owner wallet and wallet histories
                    $repo->affiliateOwnerWallet($parentId, $userId);

                    //User Wallet and wallet histories when use affiliate code
                    $repo->affiliateUserWallet($parentId, $userId);

                    $data = ['success' => true, 'message' => __('Profile Updated Successfully.')];
                } else {
                    // User Wallet when user create account without affiliate code
//                    dd($userId);
                    $data['balance'] = allSetting('standard_registration');
                    $data['userId'] = $userId;
                    $data['description'] = __('Bonus For Standard Registration.');
                    $data['type'] = STANDARD_REGISTRATION;
                    $repo->userStandardRegistrationWallet($data);
                    $data = ['success' => true, 'message' => __('Profile Updated Successfully.')];
                }
            } else {
                $data = ['status' => false, 'message' => __('Profile Update Failed!')];
            }
            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            $data = ['status' => false, 'data' => [], 'message' => $e->getMessage()];
        }
        return $data;

    }
    public function userProfileUpdate(ProfileUpdateRequest $request)
    {
        $repo = new UserAuthRepository();
        $userUpdate = $repo->userProfileUpdate($request);
        try {
            if ($userUpdate) {
                    $data = ['success' => true, 'message' => __('Profile Updated Successfully.')];
            } else {
                $data = ['status' => false, 'message' => __('Profile Update Failed!')];
            }
        } catch (\Exception $e) {
            $data = ['status' => false, 'data' => [], 'message' => $e->getMessage()];
        }
        return $data;
    }

    public function userProfileImageUpdate(ImageUploadRequest $request)
    {
        $user = $this->find($request->edit_id);

        if (!empty($request->photo)) {
            if (isset($user->photo)) {
                $data['photo'] = fileUpload($request->photo, getImagePath('user'), $user->photo, '', '');
            } else {
                $data['photo'] = fileUpload($request->photo, getImagePath('user'), '', '', '');
            }
            $service = new CommonService();
            $user_id = $service->checkValidId($request->edit_id);
            $user_update = User::where('id', $user_id)->update($data);
            if ($user_update)
                return ['success' => true, 'message' => __('Profile Image Updated Successfully.')];
            else
                return ['success' => false, 'message' => __('Profile Image Update Failed.')];
        } else {
            return ['success' => false, 'message' => __('Profile Image Not Selected.')];
        }
    }

    public function find($id)
    {
        return User::find($id);
    }

    public function userParentId($code)
    {
        return User::where('own_code', $code)->first()->id;
    }

    public function updateUserNotification(){
        try{
            if(Auth::user()->notification){
                Auth::user()->update(['notification' => 0]);
                $data = ['success' => true, 'message' => __('Notification Disabled.')];
            }else{
                Auth::user()->update(['notification' => 1]);
                $data = ['success' => true, 'message' => __('Notification Enabled.')];
            }
        }catch (\Exception $exception){
            $data = ['success' => true, 'message' => __('Something Went Wrong!!')];
        }
        return $data;
    }

    public function updateUserTimeZone(Request $request)
    {
        $user_id = User::where('id',Auth::user()->id)->update(['time_zone' => $request->time_zone]);
        if ($user_id)
            return ['success' => true, 'message' => __('Time Zone Updated Successfully.')];
        else
            return ['success' => false, 'message' => __('Time Zone Update Failed.')];
    }

    public function userVerifyEmail($code, $parentId = null)
    {
        $service = new CommonService();
        $code = $service->checkValidId($code);
        $user_verification = UserVerificationCode::where(['code' => $code])->first();
        $repo = new UserAuthRepository();
        $data = [];
        $response = [];
        if ($user_verification) {
            DB::beginTransaction();
            try {
                UserVerificationCode::where(['id' => $user_verification->id])->update(['status' => STATUS_SUCCESS]);
                $user = $this->find($user_verification->user_id);
                if (!empty($user)) {
                    if ($user->email_verified == STATUS_PENDING) {
                        $user_update = $user->update(['email_verified' => STATUS_SUCCESS, 'active_status' => STATUS_SUCCESS]);
                        if ($parentId != null) {
                            // Affiliate owner wallet and wallet histories
                            $repo->affiliateOwnerWallet($parentId, $user->id);

                            //User Wallet and wallet histories when use affiliate code
                            $repo->affiliateUserWallet($parentId, $user->id);

                            $response = ['status' => true, 'message' => __('Successfully Verified Email.')];
                        } else {
                            // User Wallet when user create account without affiliate code
                            $data['balance'] = allSetting('standard_registration');
                            $data['userId'] = $user->id;
                            $data['description'] = __('Bonus For Standard Registration.');
                            $data['type'] = STANDARD_REGISTRATION;
                            $repo->userStandardRegistrationWallet($data);

                            $response = ['status' => true, 'message' => __('Email verified successfully.')];
                        }
                    } else {
                        $response = ['status' => true, 'message' => __('You already verified email!')];
                    }
                } else {
                    $response = ['status' => false, 'message' => __('Email Verification Failed')];
                }
                DB::commit();
            } catch (\Exception $e) {
                DB::rollBack();
                $response = ['status' => false, 'data' => [], 'message' => $e->getMessage()];
            }

        } else {
            $response = ['status' => false, 'message' => __('Verification Code Not Found!')];
        }
        return $response;
    }

}
