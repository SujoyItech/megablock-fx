<?php
    namespace App\Http\Services;

    use App\Models\Licence;
    use Illuminate\Http\Request;

    class LicenceService{
        public function licence(){
            return Licence::where('status','!=',6);
        }
        public function licenceCreate(Request $request)
        {
            try {
                $user = [
                    'title' => $request->get('title')
                ];
                $data = Licence::create($user);
                return ['status' => true,'data'=>['user'=>$data], 'message' => __('Licence created successfully.')];
            } catch (\Exception $e) {
                return ['status' => false,'data'=>[], 'message' => __('Something went wrong.')];
            }
        }

        public function licenceUpdate(Request $request)
        {
            $common_service = new CommonService();
            $id = $common_service->checkValidId($request->edit_id);
            if(!is_numeric($id)&&$id['success']==false){
                return redirect()->back()->with(['dismiss'=>__('Licence not found.')]);
            }
            try {
                $user = [
                    'title' => $request->get('title')
                ];
                $data = Licence::where(['id' => $id])->update($user);
                return ['status' => true,'data'=>['user'=>$data], 'message' => __('Licence updated successfully.')];
            } catch (\Exception $e) {
                return ['status' => false,'data'=>[], 'message' => __('Something went wrong.')];
            }
        }

        public function licenceFind($id){
            $common_service = new CommonService();
            $id = $common_service->checkValidId($id);
            if(!is_numeric($id)&&$id['success']==false){
                return redirect()->back()->with(['dismiss'=>__('Item not found.')]);
            }
            try {
                $user = Licence::find($id);
            } catch (\Exception $e) {
                return ['status' => false,'data'=>[], 'message' => __('Something went wrong.')];
            }
            return ['status' => true,'data'=>['user'=>$user], 'message' => __('Licence get successfully.')];
        }


    }
