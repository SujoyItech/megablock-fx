<?php

namespace App\Http\Services;

use App\Http\Repositories\CategoryRepository;
use App\Models\Faq;
use Illuminate\Http\Request;

class AdminSettingService
{
    public function faqs()
    {
        return Faq::orderBy('id','desc')->get();
    }
    public function faqCreate(Request $request)
    {
        try {
            $user = [
                'title' => $request->get('title')
                ,'description' => $request->get('description')
            ];
            $data = Faq::create($user);
            return ['status' => true,'data'=>['user'=>$data], 'message' => __('FAQs created successfully.')];
        } catch (\Exception $e) {
            return ['status' => false,'data'=>[], 'message' => __('Something went wrong.')];
        }
    }

    public function faqUpdate(Request $request)
    {
        $common_service = new CommonService();
        $id = $common_service->checkValidId($request->edit_id);
        if(!is_numeric($id)&&$id['success']==false){
            return redirect()->back()->with(['dismiss'=>__('FAQs not found.')]);
        }
        try {
            $user = [
                'title' => $request->get('title')
                ,'description' => $request->get('description')
            ];
            $data = Faq::where(['id' => $id])->update($user);
            return ['status' => true,'data'=>['user'=>$data], 'message' => __('FAQs updated successfully.')];
        } catch (\Exception $e) {
            return ['status' => false,'data'=>[], 'message' => __('Something went wrong.')];
        }
    }

    public function faqFind($id){
        $common_service = new CommonService();
        $id = $common_service->checkValidId($id);
        if(!is_numeric($id)&&$id['success']==false){
            return redirect()->back()->with(['dismiss'=>__('FAQs not found.')]);
        }
        try {
            $user = Faq::find($id);
        } catch (\Exception $e) {
            return ['status' => false,'data'=>[], 'message' => __('Something went wrong.')];
        }
        return ['status' => true,'data'=>['user'=>$user], 'message' => __('FAQs get successfully.')];
    }

}
