<?php

namespace App\Http\Services;


use App\Http\Repositories\RoleRepository;
use App\Http\Repositories\UserRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class RoleService
{
    public function lists(){
        $roleService = new RoleRepository();
        try{
            $role = $roleService->lists();
        }catch(\Exception $e){
            return ['status' => false, 'data'=>[], 'message' => __('Something went wrong.')];
        }
        return ['status' => true, 'data'=>['role'=>$role], 'message' => __('Successful.')];
    }
    public function create(Request $request)
    {
        try {
            $createRole = new RoleRepository();
            $createRole->create($request);
        } catch (\Exception $e) {
            return ['status' => false, 'message' => $e->getMessage()];
        }
        return ['status' => true, 'message' => __('Role created successfully.')];
    }

    public function update(Request $request)
    {
        $common_service = new CommonService();
        $id = $common_service->checkValidId($request->edit_id);
        if(!is_numeric($id)&&$id['success']==false){
            return redirect()->back()->with(['dismiss'=>__('Item not found.')]);
        }

        try {
            $roleRepo = new RoleRepository();
            $roleRepo->update($request,$id);
        } catch (\Exception $e) {
            return ['status' => false, 'message' => $e->getMessage()];
        }
        return ['status' => true, 'message' => __('Role updated successfully.')];
    }
}
