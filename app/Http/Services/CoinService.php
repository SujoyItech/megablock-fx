<?php
/**
 * Created by PhpStorm.
 * User: jony
 * Date: 12/31/19
 * Time: 4:11 PM
 */

namespace App\Http\Services;


use App\Models\SubscriptionPackage;
use App\Models\WalletHistory;
use App\Models\WallletHistory;
use Illuminate\Support\Facades\Auth;

class CoinService
{
    public function payWithCoin($pakege_id)
    {
        $data = ['success' => false,'data'=>'', 'message' => __('Something went wrong.')];
       // $primary_wallet = Wallet::where(['user_id' => $request->user_id, 'type' => WALLET_AMZ])->first();

        try{
            $pakege = SubscriptionPackage::find($pakege_id);

            $coin_payment = new CoinPaymentsAPI();
            $address = $coin_payment->GetCallbackAddress(env('COINPAYMENT_CURRENCY'));
            if (isset($address['error']) && ($address['error'] == 'ok')){

                $coinpayment = new CoinPaymentsAPI();
                $api_rate = $coinpayment->GetRates('');

                ////////////////////////////////////


                $coin_price = convert_currencyCoinPayment($pakege->price, env('COINPAYMENT_CURRENCY'),$api_rate);



                $validAmount = $pakege->price;
                if ( $address ) {
                    $details = [
                        'user_id' => Auth::id()
                        , 'coin_address' => $address['result']['address']
                        , 'coin_type' => env('COINPAYMENT_CURRENCY')

                        , 'coin_amount' => $coin_price
                        , 'type' => 1
                        , 'amount' => $pakege->price
                        , 'package_id' => $pakege->id
                        , 'status' => STATUS_PENDING
                        , 'description' => "Add credit by coin"
                    ];
                    $data['data'] = WalletHistory::create($details); // placed order
                    $data['success'] = true;
                    $data['message'] = __('Order placed successfully.');
                }
            }else{
                if ((env('APP_ENV') == 'LOCAL'))
                    $data['message'] = __('Coin payment address not generated.');
                return $data;
            }
        }catch (\Exception $exception){

            $data['success'] = false;
            if ((env('APP_ENV') == 'DEV'))
                $data['message'] =  $exception->getMessage();
            return $data;
        }

        return $data;
    }

}
