<?php
namespace App\Http\Services;
use App\Http\Repositories\UserAuthRepository;
use App\Models\UserAffiliateCode;
use App\Models\UserVerificationCode;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;

class AuthService{
    public function _credentials(Request $request)
    {
        $field = filter_var($request->input($this->_username()), FILTER_VALIDATE_EMAIL) ? 'email' : 'username';
        $request->merge([$field => $request->input($this->_username())]);
        return $request->only($field, 'password');
    }

    public function _username()
    {
        return 'email';
    }

    public function _checkActiveStatus()
    {
        $user = Auth::user();

        if ($user->activestatus == 3)
        {
            return [
                'status' => true,
                'message' => '',
            ];
        }
        elseif ($user->activestatus == 2)
        {
            /* $loginDifference = $this->_checkLastLogin();
            $checkloginDifference = isset($default['chk_login_diff']) && !empty($default['chk_login_diff']) ? $default['chk_login_diff'] : 0;

            if ($checkloginDifference != 0 && $loginDifference >= $checkloginDifference)
            {

                $user->activestatus = 2;
                if ($user->update())
                {
                    Auth::logout();
                    return [
                        'status' => false,
                        'message' => __('Your account has been suspended by System. Please contact to support center.')
                    ];
                }
            }*/

            Auth::logout();
            return [
                'status' => false,
                'message' => __('Your account is suspended Please contact our support center!')
            ];
        }
        elseif($user->activestatus == 1)
        {
            Auth::logout();
            return [
                'status' => false,
                'message' => __('Your account has been deleted')
            ];
        }
    }

    public function _checkLastLogin()
    {
        $lastLogin = Auth::user()->accacts()->where('action', strtolower('sign_in'))->first();
        $now = Carbon::now();
        $lastLogin = Carbon::parse($lastLogin);
        $checkingTime = Carbon::parse($now);

        return $checkingTime->diffInDays($lastLogin);
    }

    public function _twoStepVerification()
    {
        $user = Auth::user();
        if (
            $user->role == 2 &&
            $user->mvs == 2 &&
            !empty($user->phone) &&
            isset($user->usersetting->phn_check) &&
            $user->usersetting->phn_check == 2
        )
        {
            // random number reserve session
            $randomNumber = randomNumber(6);
            // reserving new code
            session()->put('randno',$randomNumber);
            // sent message to the number with session
            app(SmsService::class)->send($user->phone,$randomNumber);
            return true;
        }
        return false;
    }

    public function userSocialCreateAccount($userSocial)
    {
       $repo = new UserAuthRepository();
       $user = $repo->userSocialDataSave($userSocial);
        return $user;
    }


}
