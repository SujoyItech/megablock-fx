<?php


namespace App\Http\Services;

use App\Http\Repositories\BrokerRepository;
use App\Http\Requests\BrokerRequest;
use App\Models\Broker;
use App\Models\BrokerLicence;
use App\Models\BrokerMarketsExclude;
use Illuminate\Http\Request;

class BrokerService
{
    public function broker(){
        return Broker::where('status','!=',6)->orderBy('order','asc');
    }
    public function brokerCreate(BrokerRequest $request)
    {
        try {
            $broker_repo = new BrokerRepository();
            $broker = $broker_repo->create($request);
            $licences =  $request->licences;
            $markets = $request->markets;
            if (!empty($licences))
            {
                foreach ($licences as $licence)
                {
                    $broker_licence = ['broker_id'=>$broker->id,'licence_id'=>$licence];
                    BrokerLicence::create($broker_licence);
                }
            }

            if(!empty($markets))
            {
                foreach ($markets as $market)
                {
                    $broker_market = ['broker_id'=>$broker->id,'country_key'=>$market];
                    BrokerMarketsExclude::create($broker_market);
                }
            }
            return ['status' => true,'data'=>['user'=>$broker], 'message' => __('Broker created successfully.')];
        } catch (\Exception $e) {
            return ['status' => false,'data'=>[], 'message' => __('Something went wrong.')];
        }
    }

    public function brokerUpdate(Request $request)
    {
        $common_service = new CommonService();
        $id = $common_service->checkValidId($request->edit_id);
        if(!is_numeric($id)&&$id['success']==false){
            return redirect()->back()->with(['dismiss'=>__('Item not found.')]);
        }
        try {
            $user_repo = new BrokerRepository();
            $user = $user_repo->update($request,$id);
            return ['status' => true,'data'=>['user'=>$user], 'message' => __('Broker updated successfully.')];
        } catch (\Exception $e) {
            return ['status' => false,'data'=>[], 'message' => $e->getMessage()];
        }
    }

    public function brokerFind($id){
        $common_service = new CommonService();
        $id = $common_service->checkValidId($id);
        if(!is_numeric($id)&&$id['success']==false){
            return redirect()->back()->with(['dismiss'=>__('Item not found.')]);
        }
        try {
            $user = Broker::find($id);
        } catch (\Exception $e) {
            return ['status' => false,'data'=>[], 'message' => __('Something went wrong.')];
        }
        return ['status' => true,'data'=>['user'=>$user], 'message' => __('Broker get successfully.')];
    }

}
