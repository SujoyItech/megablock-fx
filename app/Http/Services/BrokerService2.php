<?php
    namespace App\Http\Services;

    use App\Models\Broker;
    use App\Models\Licence;
    use Illuminate\Http\Request;

    class BrokerService2{
        public function broker(){
            return Broker::where('status','!=',6);
        }
        public function brokerCreate(Request $request)
        {
            try {
                $user = [
                    'title' => $request->get('title')
                    ,'description' => $request->get('description')
                ];
                $data = Licence::create($user);
                return ['status' => true,'data'=>['user'=>$data], 'message' => __('Broker created successfully.')];
            } catch (\Exception $e) {
                return ['status' => false,'data'=>[], 'message' => __('Something went wrong.')];
            }
        }

        public function brokerUpdate(Request $request)
        {
            $common_service = new CommonService();
            $id = $common_service->checkValidId($request->edit_id);
            if(!is_numeric($id)&&$id['success']==false){
                return redirect()->back()->with(['dismiss'=>__('Broker not found')]);
            }
            try {
                $user = [
                    'title' => $request->get('title')
                    ,'description' => $request->get('description')
                ];
                $data = Broker::where(['id' => $id])->update($user);
                return ['status' => true,'data'=>['user'=>$data], 'message' => __('Broker updated successfully.')];
            } catch (\Exception $e) {
                return ['status' => false,'data'=>[], 'message' => __('Something went wrong.')];
            }
        }

        public function brokerFind($id){
            $common_service = new CommonService();
            $id = $common_service->checkValidId($id);
            if(!is_numeric($id)&&$id['success']==false){
                return redirect()->back()->with(['dismiss'=>__('Item not found')]);
            }
            try {
                $user = Broker::find($id);
            } catch (\Exception $e) {
                return ['status' => false,'data'=>[], 'message' => __('Something went wrong.')];
            }
            return ['status' => true,'data'=>['user'=>$user], 'message' => __('Broker get successfully.')];
        }


    }