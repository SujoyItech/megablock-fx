<?php

namespace App\Http\Services;


use App\Http\Repositories\UserAuthRepository;
use App\Http\Repositories\UserRepository;
use App\Http\Requests\UserRequest;
use App\Models\SubscriptionHistories;
use App\Models\UserAffiliateCode;
use App\Models\Wallet;
use App\Models\WalletHistory;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class UserService
{
    public function adminUsers(){
        $userService = new UserRepository();
        try{
            $users = $userService->adminUsers();
        }catch(\Exception $e){
            return ['status' => false, 'data'=>[], 'message' => __('Something went wrong.')];
        }
        return ['status' => true, 'data'=>$users, 'message' => __('Sign up is successful.')];
    }
    public function create(UserRequest $request)
    {

        try {
            $user_repo = new UserRepository();
            $user_auth_repo = new UserAuthRepository();
            $user = $user_repo->create($request);
            if ($user)
            {
                DB::beginTransaction();
                try {
                    DB::table('user_informations')->insert([
                        'user_id' => $user->id,
                    ]);

                    if($user->role == USER_ROLE_USER)
                    {
                        $request->balance == null ? $data['balance'] = 0: $data['balance'] = $request->balance;
                        $data['userId'] = $user->id;
                        $data['description'] = __('Bonus For Registered By Admin.');
                        $data['type'] = REGISTERED_BY_ADMIN;
                        $user_auth_repo->userStandardRegistrationWallet($data);
                    }
                    DB::commit();
                    $response = ['status' => true,'data'=>['user'=>$user], 'message' => __('User created successfully.')];
                }catch (\Exception $e) {
                    DB::rollBack();
                    $response = ['status' => false, 'data' => [], 'message' => $e->getMessage()];
                }

            }
        } catch (\Exception $e) {
            $response = ['status' => false,'data'=>[], 'message' => __('Something went wrong.')];
        }
        return $response;
    }

    public function update(UserRequest $request)
    {
        $common_service = new CommonService();
        $id = $common_service->checkValidId($request->edit_id);
        if(!is_numeric($id)&&$id['success']==false){
            return redirect()->back()->with(['dismiss'=>__('Item not found')]);
        }
        DB::beginTransaction();
        try {
            $user_repo = new UserRepository();
            $user_auth_repo = new UserAuthRepository();
            $user = $user_repo->update($request,$id);
            if($user && $request->role == USER_ROLE_USER)
            {
                $amount = 0;
                $request->balance == null ? $amount = 0: $amount = $request->balance;
                $wallet = Wallet::where(['user_id'=>$id])->first();
                if(isset($wallet))
                {
                    if ($wallet->balance != $amount)
                    {
                        Wallet::where(['user_id' => $id])->update(['balance' => $amount]);
                        $user_wallet_histories = [
                            'user_id' => $id,
                            'type' => REGISTERED_BY_ADMIN,
                            'description' => __('Bonus By Admin'),
                            'amount' => $amount,
                        ];
                        WalletHistory::create($user_wallet_histories);
                    }

                }else
                {
                    $data['balance'] = $amount;
                    $data['userId'] = $id;
                    $data['description'] = __('Bonus By Admin.');
                    $data['type'] = REGISTERED_BY_ADMIN;
                    $user_auth_repo->userStandardRegistrationWallet($data);
                }

            }
            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            return ['status' => false,'data'=>[], 'message' => __('Something went wrong.')];
        }
        return ['status' => true,'data'=>['user'=>$user], 'message' => __('User updated successfully.')];
    }

    public function find($id){
        $common_service = new CommonService();
        $id = $common_service->checkValidId($id);
        if(!is_numeric($id)&&$id['success']==false){
            return redirect()->back()->with(['dismiss'=>__('Item not found.')]);
        }
        try {
            $user_repo = new UserRepository();
            $user = $user_repo->find($id);

        } catch (\Exception $e) {
            return ['status' => false,'data'=>[], 'message' => __('Something went wrong.')];
        }
        return ['status' => true,'data'=>['user'=>$user], 'message' => __('User get successfully.')];
    }
    public function userPaymentHistories()
    {
        try{
            $payment_histories = SubscriptionHistories::where('user_id',Auth::id());
            return ['status'=>true,'data'=>['payment_histories'=>$payment_histories]];
        } catch (\Exception $e) {
            return ['status' => false,'data'=>[], 'message' => __('Something went wrong.')];
        }
    }
}
