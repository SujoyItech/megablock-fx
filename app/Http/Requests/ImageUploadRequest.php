<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ImageUploadRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rule = [
            'logo'=>'image|mimes:jpg,png,jpeg|max:2000'
        ];
        if (empty($this->edit_id))
            $rules['logo'] = 'required|image|mimes:jpg,png,jpeg|max:2000';

        return $rule;
    }

    public function messages()
    {
        $messages=[
            'logo.required'=>__('Image field can\'t be empty!')
            ,'logo.image'=>__('Image must be in jpg,jpeg or png format!')
            ,'logo.mimes'=>__('Image must be in jpg,jpeg or png format!')
            ,'logo.max' => __('Image size can\'t be more than 2MB!')
        ];
        return $messages;
    }
}
