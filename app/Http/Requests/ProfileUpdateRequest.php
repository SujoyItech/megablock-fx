<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProfileUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rule = [
            'first_name' => 'required',
            'last_name' => 'required',
            'phone' => 'numeric|phone_number',
            'email' => 'required|email',
            'dob'=> 'required|before:yesterday',
            'country'=>'required',
            'photo' => 'mimes:jpeg,jpg,JPG,png,PNG,gif|max:2048'
        ];
        if ($this->phone) {
            $rule['phone'] = 'numeric|phone_number';
        }
        if ($this->photo) {
            $rule['photo'] = 'mimes:jpeg,jpg,JPG,png,PNG,gif|max:2000';
        }
        return $rule;
    }

    public function messages()
    {
        return [
            'first_name.required' => __('The First Name field can not empty!'),
            'last_name.required' => __('The First Name field can not empty!'),
            'phone.phone_number' => __('Invalid phone number!'),
            'phone.numeric' => __('Invalid phone number!')
            ,'email.required'=>__('Email field can\'t be empty!')
            ,'email.email'=>__('Invalid Email!'),
            'dob.required' => __('Date of Birth field can not be empty!'),
            'dob.before'=>__('Date Format Must be before today!')
            ,'country.required' => __('Country field can not be empty!')
            ,'photo.image'=>__('Image must be in jpg,jpeg or png format!')
            ,'photo.mimes'=>__('Image must be in jpg,jpeg or png format!')
            ,'photo.max' => __('Image size can\'t be more than 2MB!')
        ];
    }
}
