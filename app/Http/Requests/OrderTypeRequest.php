<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class OrderTypeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules=[
            'title'=>'required|max:256'
            ,'description'=>'required|max:256'
        ];
        return $rules;
    }
    public function messages()
    {
        $message=[
            'title.required'=>__('Title field can\'t be empty!')
            ,'title.max'=>__('Title can\'t be more than 255 character!')
            ,'description.required'=>__('Description field can\'t be empty!')
            ,'description.max'=>__('Description can\'t be more than 255 character!')

        ];
        return $message;
    }
}
