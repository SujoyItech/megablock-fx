<?php

namespace App\Http\Requests\Customer;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\JsonResponse;
use Illuminate\Validation\ValidationException;
use Illuminate\Contracts\Validation\Validator;

class RegistrationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules=[
            'first_name'=>'required|max:60',
            'last_name'=>'required|max:60',
            'email' => 'required|email|unique:users,email',
            'phone'=>'required|max:13',
            'country'=>'required',
            'dob'=> 'required|before:yesterday',
            'password' => 'required|min:8|strong_pass|confirmed',
            'password_confirmation' => 'required',
            'terms_condition' => 'required|in:1',
            'risk_warning' =>'required|in:1',
            'adult_warning' =>'required|in:1',
        ];
        return $rules;
    }

    public function messages()
    {
        $messages = [
            'first_name.required' => __('First Name field can not be empty!'),
            'first_name.max' => __('First Name field can not be more than 60 character!'),
            'last_name.required' => __('First Name field can not be empty!'),
            'last_name.max' => __('First Name field can not be more than 60 character!'),
            'password.required' => __('Password field can not be empty!'),
            'password.min' => __('Password length must be minimum 8 characters!'),
            'password.strong_pass' => __('Password must be consist of one uppercase, one lowercase and one number!'),
            'password.confirmed' => __('Password not matched!'),
            'password_confirmation.required' => __('Confirm password field can not be empty!'),
            'email.required' => __('Email field can not be empty!'),
            'email.unique' => __('Email address already exists!'),
            'email.email' => __('Invalid email!'),
            'phone.required' => __('Phone field can not be empty!'),
            'phone.max' => __('Invalid Phone Number!'),
            'country.required' => __('Country field can not be empty!'),
            'dob.required' => __('Date of Birth field can not be empty!'),
            'dob.before'=>__('Date Format Must be before today!'),
            'terms_condition.required' => __('You must check Terms & Condition!'),
            'risk_warning.required' => __('You must check Risk & Warning!'),
            'adult_warning.required' => __('You must check Adult Warning!'),
        ];
        return $messages;
    }

    protected function failedValidation(Validator $validator)
    {
        if ($this->header('accept') == "application/json") {
            $errors = [];
            if ($validator->fails()) {
                $e = $validator->errors()->all();
                foreach ($e as $error) {
                    $errors[] = $error;
                }
            }
            $json = ['success'=>false,
                'data'=>[],
                'message' => $errors[0],
            ];
            $response = new JsonResponse($json, 200);

            throw (new ValidationException($validator, $response))->errorBag($this->errorBag)->redirectTo($this->getRedirectUrl());
        } else {
            throw (new ValidationException($validator))
                ->errorBag($this->errorBag)
                ->redirectTo($this->getRedirectUrl());
        }

    }
}
