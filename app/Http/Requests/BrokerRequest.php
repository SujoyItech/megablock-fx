<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BrokerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules=[
            'name'=>'required|max:255',
            'advantage'=>'required',
            'average_spreads'=>'required',
            'recommended_deposit'=>'required',
            'licences'=>'array',
            'markets'=>'array',
            'logo'=>'required|image|mimes:jpg,png,jpeg|max:2000',
            'URL' => 'required'
        ];
        if(!empty($this->edit_id)){
            $rules['logo']='image|mimes:jpg,png,jpeg|max:2000';
        }

        return $rules;
    }
    public function messages()
    {
        $messages=[
            'name.required'=>__('Name field can\'t be empty!')
            ,'name.max'=>__('Name field can\'t be more than 255 character!')
            ,'URL.required'=>__('URL field can\'t be empty!')
            ,'advantage.required'=>__('Advantage field can\'t be empty!')
            ,'average_spreads.required'=>__('Average Spreads field can\'t be empty!')
            ,'recommended_deposit.required'=>__('Recommended Deposit field can\'t be empty!')
            ,'logo.required'=>__('Image field can\'t be empty!')
            ,'logo.image'=>__('Image must be in jpg,jpeg or png format!')
            ,'logo.mimes'=>__('Image must be in jpg,jpeg or png format!')
            ,'logo.max' => __('Image size can\'t be more than 2MB!')
        ];
        return $messages;
    }
}
