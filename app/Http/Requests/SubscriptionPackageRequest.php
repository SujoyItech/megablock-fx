<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SubscriptionPackageRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        $rules=[
            'title'=>'required|max:255'
            ,'description' => 'required'
            ,'credit'=>'required|integer|min:0|max:9999999999'
            ,'price'=>'required|numeric|min:0|max:9999999999'
            ,'fees'=>'required|numeric|min:0|max:9999999999'
            ,'vat'=>'required|numeric|min:0|max:9999999999'
            ,'image'=>'required|max:2048|image|mimes:jpg,png,jpeg'
        ];
        return $rules;
    }
    public function messages()
    {
        $messages=[
            'title.required'=>__('Title field can\'t be empty!')
            ,'title.max'=>__('Title field can\'t be more than 255 character!')
            ,'description.required'=>__('Description field can\'t be empty!')
            ,'credit.required'=>__('Credit field can\'t be empty!')
            ,'price.required'=>__('Price field can\'t be empty!')
            ,'fees.required'=>__('Fees field can\'t be empty!')
            ,'vat.required'=>__('Fees field can\'t be empty!')
            ,'image.required'=>__('Image field can\'t be empty!')
            ,'image.image'=>__('Image must be in jpg,jpeg or png format!')
            ,'image.mimes'=>__('Image must be in jpg,jpeg or png format!')
            ,'image.max' => __('Image size can\'t be more than 2MB!')
        ];
        return $messages;
    }
}
