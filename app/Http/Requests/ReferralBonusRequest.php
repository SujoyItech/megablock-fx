<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ReferralBonusRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules=[
            'on_register_and_purchase'=>'required',
            'registers_through_affiliate'=>'required',
            'standard_registration'=>'required',
            'affiliate_owner'=>'required',
        ];
        return $rules;
    }
    public function messages()
    {
        $message=[
            'on_register_and_purchase.required'=>__('On Register and Purchase field can\'t be empty!')
            ,'registers_through_affiliate.required'=>__('Register Through Affiliate field can\'t be empty!')
            ,'standard_registration.required'=>__('Standard Registration field can\'t be empty!')
            ,'affiliate_owner.required'=>__('Affiliate owner field can\'t be empty!')
        ];
        return $message;
    }
}
