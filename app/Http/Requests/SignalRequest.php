<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SignalRequest extends FormRequest {
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        $rules = [
            'financial_instrument_code' => 'required|string'
            , 'trade_type' => 'required|integer|min:1'
            , 'order_type' => 'required|integer|min:1'
            , 'action' => 'required'
            , 'entry_price' => 'required|numeric'
            , 'take_profit_1' => 'required|numeric'
            , 'stop_loss_1' => 'required|numeric'
            , 'status' => 'required|integer'
            , 'comments' => 'required'
        ];
        $this->status == SIGNAL_CLOSED ? $rules['signal_result'] = 'required|numeric' : '';
        return $rules;
    }

    public function messages() {
        $message = [
            'financial_instrument_code.required' => __('Financial Instrument field can\'t be empty!')
            , 'financial_instrument_code.string' => __('Financial Instrument field must be string!')
            , 'trade_type.required' => __('Trade Type field can\'t be empty!')
            , 'trade_type.integer' => __('Financial Instrument field must be integer!')
            , 'order_type.required' => __('Order Type field can\'t be empty!')
            , 'order_type.integer' => __('Order Type field must be integer!')
            , 'action.required' => __('Action field can\'t be empty!')
            , 'action.integer' => __('Action field field must be integer!')
            , 'entry_price.required' => __('Entry Price field can\'t be empty!')
            , 'entry_price.numeric' => __('Action field field must be numeric!')
            , 'take_profit_1.required' => __('Take Profit 1 field can\'t be empty!')
            , 'take_profit_1.numeric' => __('Take Profit 1  field field must be numeric!')
            , 'stop_loss_1.required' => __('Stop Loss 1 field can\'t be empty!')
            , 'stop_loss_1.numeric' => __('Stop Loss 1 field field must be numeric!'),
            'signal_result.required' => __('Signal Result field can\'t be empty!As you want to close the signal!')
            , 'signal_result.numeric' => __('Signal Result field field must be numeric!')
            , 'status.required' => __('Status field can\'t be empty!')
            , 'comments.required' => __('Comments field can\'t be empty!')

        ];
        return $message;
    }
}
