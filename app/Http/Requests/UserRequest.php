<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest {
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        $rules = [
            'first_name' => 'required|max:60',
            'last_name' => 'required|max:60',
            'email' => 'required|email|unique:users,email',
            'phone'=>'required|max:13',
            'country'=>'required',
            'dob'=> 'required|before:yesterday',
            'password'=>'required|confirmed',
            'role'=>'required|numeric',
        ];
        if ($this->balance != null)
        {
            $rules['balance'] ='integer|max:9999999999';
        }
        if (!empty($this->edit_id)) {
            $rules['email'] = 'required|email';
        }
        if (!empty($this->edit_id)) {
            $rules['phone'] = 'max:13';
        }
        if (!empty($this->edit_id)) {
            $rules['email'] = 'required|email';
            $rules['password'] = '';
        }
        return $rules;
    }

    public function messages() {
        $message = [
            'first_name.required' => __('First Name field can\'t be empty!')
            , 'first_name.max' => __('First Name can\'t be more than 60 character!')
            , 'last_name.required' => __('Last Name field can\'t be empty!')
            , 'last_name.max' => __('Last Name can\'t be more than 60 character!')
            , 'email.required' => __('Email field can\'t be empty!')
            , 'email.email' => __('Invalid Email!')
            , 'email.max' => __('Email can\'t be more than 255 character!')
            , 'email.unique' => __('Email already exists!')
            , 'role.required' => __('Role field can\'t be empty!')
            , 'role.numeric' => __('Role field can\'t be empty!')
            , 'country.required' => __('Country field can not be empty!'),
            'dob.required' => __('Date of Birth field can not be empty!'),
            'dob.before' => __('Date Format Must be before today!'),
            'phone.required' => __('Phone can\'t be empty!'),
            'phone.max' => __('Phone can\'t be more than 13 character!')
        ];
        if (!empty($this->phone)) {
            $message['phone.max'] = __('Phone can\'t be more than 13 character!');
        }
        if (empty($this->edit_id)) {
            $message['password.required'] = __('Password field can\'t be empty!');
            $message['password.confirmed'] = __('Password doesn\'t matched!');
        }
        return $message;
    }
}
