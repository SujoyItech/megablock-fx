<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BrokerLike extends Model
{
    //
    protected $fillable = ['broker_id','user_id','like','unlike'];
}
