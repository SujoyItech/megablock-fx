<?php
    namespace App\Models;

    use Illuminate\Database\Eloquent\Model;

    class FinancialInstrumentCode extends Model
    {
        protected $fillable = ['id','title','status'];

    }
