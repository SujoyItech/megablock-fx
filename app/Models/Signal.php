<?php
    namespace App\Models;

    use Illuminate\Database\Eloquent\Model;

    class Signal extends Model
    {
        protected $fillable = [

              'financial_instrument_code'
            , 'trade_type'
            , 'order_type'
            , 'action'
            , 'entry_price'
            , 'take_profit_1'
            , 'stop_loss_1'
            , 'take_profit_2'
            , 'stop_loss_2'
            , 'take_profit_3'
            , 'stop_loss_3'
            , 'take_profit_4'
            , 'stop_loss_4'
            , 'available_for_free'
            , 'comments'
            , 'signal_result'
            , 'status'
        ];


    }