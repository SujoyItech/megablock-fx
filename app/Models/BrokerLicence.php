<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BrokerLicence extends Model
{
    //
    protected $fillable = ['broker_id','licence_id'];
}
