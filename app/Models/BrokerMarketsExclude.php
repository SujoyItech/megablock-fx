<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BrokerMarketsExclude extends Model
{
    //
    protected $fillable = ['broker_id','country_key'];
}
