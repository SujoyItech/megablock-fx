<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserAffiliateCode extends Model
{
    //
    protected $fillable = ['user_id','code'];
}
