<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class WalletHistory extends Model
{
    protected $fillable = ['user_id','referral_id','child_id','coin_address','coin_type','amount',
        'package_id','coin_amount','status','description','type'];
}
