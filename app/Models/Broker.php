<?php
    namespace App\Models;

    use Illuminate\Database\Eloquent\Model;

    class Broker extends Model
    {
        protected $fillable = ['name', 'logo','advantage','average_spreads','recommended_deposit', 'URL','status'];
     function brokerLike()
     {
         return $this->hasMany(BrokerLike::class,'broker_id','id')->where('broker_likes.like', '=', 1);
     }
     function brokerDislike()
     {
         return $this->hasMany(BrokerLike::class,'broker_id','id')->where('broker_likes.unlike', '=', 1);
     }

     function brokerLicences(){
         $licences_ids = $this->hasMany(BrokerLicence::class,'broker_id','id')->pluck('licence_id');

         $licence_titles  = Licence::whereIn('id',$licences_ids)->pluck('title');

         return implode(',',$licence_titles->toArray());
     }

    }
