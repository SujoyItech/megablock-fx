<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ReferralBonusSetting extends Model
{
    //
    protected $fillable = ['on_register','on_register_and_purchase','affiliate_owner','registers_through_affiliate','standard_registration'];
}
