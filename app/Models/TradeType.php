<?php
    namespace App\Models;

    use Illuminate\Database\Eloquent\Model;

    class TradeType extends Model
    {
        protected $fillable = ['title', 'description','status'];


    }