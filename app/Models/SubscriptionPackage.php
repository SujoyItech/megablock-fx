<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SubscriptionPackage extends Model
{
    //
    protected $fillable = ['title','price','credit','status','description','image','is_free','fees','vat'];
}
