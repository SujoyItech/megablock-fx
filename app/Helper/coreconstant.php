<?php
//Product Type
const PRODUCT_TYPE_REGULAR = 1;
const PRODUCT_TYPE_BUNDLE = 2;
// User Role Type
const USER_ROLE_ADMIN = 0;
const USER_ROLE_USER = 2;
const USER_ROLE_CHEF_MANAGER = 3;
const USER_ROLE_DELIVERY_MANAGER = 4;

// User  Type
const USER_TYPE_ADMIN = 1;
const USER_TYPE_USER = 2;

// Slider status
const STATUS_ACTIVE = 1;
const STATUS_DEACTIVE = 0;

//Voted
const VOTED = 1;
const UN_VOTED = 2;

// User Active Status
const STATUS_PENDING = 0;
const STATUS_SUCCESS = 1;
const STATUS_SUSPENDED = 2;
const STATUS_REJECT = 3;
const STATUS_FINISHED = 5;
const STATUS_DELETED = 6;
const STATUS_BLOCKED = 7;

// list of days
const DAY_OF_WEEK_MONDAY = 1;
const DAY_OF_WEEK_TUESDAY = 2;
const DAY_OF_WEEK_WEDNESDAY = 3;
const DAY_OF_WEEK_THURSDAY = 4;
const DAY_OF_WEEK_FRIDAY = 5;
const DAY_OF_WEEK_SATURDAY = 6;
const DAY_OF_WEEK_SUNDAY = 7;

// Gender
const GENDER_MALE = 1;
const GENDER_FEMALE = 2;
const GENDER_OTHER = 3;

//Varification send Type
const Mail = 1;
const PHONE = 2;

// User Activity
const USER_ACTIVITY_LOGIN = 1;

const CASH_ON_DALIVERY = 1;
const CREDIT_CARD = 2;

//Discount Type
const DISCOUNT_TYPE_FIXED = 1;
const DISCOUNT_TYPE_PERCENTAGE = 2;

// Landing Pages
const YPTO_CARD = 'ypto_card';
const YPTO_TRADE = 'ypto_trade';
const YPTO_BET = 'ypto_bet';
const YPTO_BOT = 'ypto_bot';
const YPTO_BANK = 'ypto_bank';
const YPTO_BUY = 'ypto_buy';
const YPTO_PAY = 'ypto_pay';
const YPTO_COIN = 'ypto_coin';

// File Type
const FILE_TYPE_VIDEO = 1;
const FILE_TYPE_IMAGE = 2;

// Actions
const ACTION_DASHBOARD_VIEW = 'action_dashboard_view';
const ACTION_USER_VIEW = 'action_user_view';
const ACTION_USER_ADD = 'action_user_add';
const ACTION_USER_EDIT = 'action_user_edit';
const ACTION_USER_DELETE = 'action_user_delete';

const ACTION_USER_MANAGEMENT = 'action_user_management';
const ACTION_ADMIN_SETTING = 'action_admin_setting';

const ACTION_ROLE_VIEW = 'action_role_view';
const ACTION_ROLE_ADD = 'action_role_add';
const ACTION_ROLE_EDIT = 'action_role_edit';
const ACTION_ROLE_DELETE = 'action_role_delete';

const ACTION_OFFER_VIEW = 'action_offer_view';
const ACTION_OFFER_ADD = 'action_offer_add';
const ACTION_OFFER_EDIT = 'action_offer_edit';
const ACTION_OFFER_DELETE = 'action_offer_delete';

const ACTION_PRODUCT_VIEW = 'action_product_view';
const ACTION_PRODUCT_ADD = 'action_product_add';
const ACTION_PRODUCT_EDIT = 'action_product_edit';
const ACTION_PRODUCT_DELETE = 'action_product_delete';

const ACTION_ORDER_VIEW = 'action_order_view';
const ACTION_PENDING_ORDER_VIEW = 'action_pending_order_view';
const ACTION_DELIVERED_ORDER_VIEW = 'action_delivered_order_view';
const ACTION_FAILED_ORDER_VIEW = 'action_failed_order_view';

const ACTION_CATEGORY_VIEW = 'action_category_view';
const ACTION_SUB_CATEGORY_VIEW = 'action_sub_category_view';
const ACTION_CATEGORY_ADD = 'action_category_add';
const ACTION_CATEGORY_EDIT = 'action_category_edit';
const ACTION_CATEGORY_DELETE = 'action_category_delete';

const ACTION_CUPON_VIEW = 'action_cupon_view';
const ACTION_CUPON_ADD = 'action_cupon_add';
const ACTION_CUPON_EDIT = 'action_cupon_edit';
const ACTION_CUPON_DELETE = 'action_cupon_delete';

const ACTION_COST_VIEW = 'action_cost_view';
const ACTION_COST_ADD = 'action_cost_add';
const ACTION_COST_EDIT = 'action_cost_edit';
const ACTION_COST_DELETE = 'action_cost_delete';
const ACTION_COST_EXCEL_UPLOAD = 'action_cost_excel_upload';

// First Time Subscription
const SUBSCRIBED_FIRST = 0;
const SUBSCRIBED_AGAIN = 1;

//  Notification Type
const SIGNAL_CREATED = 1;
const SIGNAL_UPDATED = 2;

// Signal Status
const SIGNAL_PENDING = 0;
const SIGNAL_ACTIVE = 1;
const SIGNAL_CLOSED = 3;

// Signal Action
const SIGNAL_ACTION_BUY = 'buy';
const SIGNAL_ACTION_SELL = 'sell';

// Notification Status
const NEW_NOTIFICATION = 0;
const READ_NOTIFICATION = 1;
const NOTIFICATION_CLEARED = 2;

// Wallet History
const CREDIT_CARD_PAYMENT = 1;
const COIN_PAYMENT = 2;
const REFERRAL = 3;
const STANDARD_REGISTRATION = 4;
const REGISTERED_BY_ADMIN = 5;


// Currency
const CURRENCY_USD = 1;
const CURRENCY_EURO = 2;



