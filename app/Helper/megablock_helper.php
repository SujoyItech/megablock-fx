<?php

use Illuminate\Support\Facades\DB;

/***********CREDIT RELATED**************/

function getBalance($user_id = '') {
    if ($user_id != '') {
        $wallet = DB::table('wallets')->where('user_id', '=', $user_id)->pluck('balance');
        $balance = !empty($wallet[0]) ? $wallet[0] : 0;
    } else {
        $wallet = \App\Models\Wallet::where('user_id', Auth::id())->first();
        $balance = !empty($wallet) ? $wallet->balance : 0;
    }
    return $balance;
}

/***********NOTIFICATION RELATED**************/

function getNotification() {
    $data = DB::table('notifications')
              ->select('*')
              ->where(['user_id' => Auth::user()->id])
              ->where('status', '<>', NOTIFICATION_CLEARED)
              ->orderBy('id', 'desc')
              ->get()
              ->toArray();
    return $data;
}

function countUnreadNotification() {
    $data = DB::table('notifications')
              ->where(['user_id' => Auth::user()->id])
              ->where('status', '=', NEW_NOTIFICATION)
              ->count();
    return $data;
}

function makeSeenNotification() {
    DB::table('notifications')
      ->where(['user_id' => Auth::user()->id])
      ->where('status', '=', NEW_NOTIFICATION)
      ->update(['status' => READ_NOTIFICATION]);
}

function makeClearNotification() {
    DB::table('notifications')
      ->where(['user_id' => Auth::user()->id])
      ->where('status', '<>', NOTIFICATION_CLEARED)
      ->update(['status' => NOTIFICATION_CLEARED]);
}

function notificationData($data = [], $mode = '') {
    if ($mode == 'count') {
        $return = count($data);
    } else if ($mode == 'count_unread') {
        $count_data = array_count_values(array_column($data, 'status'));
        $return = $count_data[NEW_NOTIFICATION];
    }
    return $return;
}

function getNotificationContent($json = '') {
    return json_decode($json);
}

/***********SIGNAL RELATED**************/

function getSignalResultText($signals_status, $signals_action, $signals_signal_result) {
    $signal_button = '';
    if ($signals_status == SIGNAL_CLOSED) {
        if ($signals_signal_result < 0) {
            // e.g. -25 PIPS with red label
            $signal_button = '<a class="signal_result_danger btn btn-sm btn-danger">' . $signals_signal_result . ' ' . __('PIPS') . '</a>';
        } else {
            // e.g. 25 PIPS with green label
            $signal_button = '<a class="signal_result_success btn btn-sm btn-success">' . $signals_signal_result . ' ' . __('PIPS') . '</a>';
        }
    } else {
        if ($signals_action == SIGNAL_ACTION_BUY) {
            // e.g. buy label with green
//            $signal_button = '<a class="btn btn-sm btn-primary text-light" style="border-radius: 50%">' . __('BUY') . '</a>';
            $signal_button = '<label class="label label-primary pl-10 pr-10" style="border-radius: 10px">' . __('BUY') . '</label>';
        } else {
            // e.g. buy label with red
//            $signal_button = '<a class="btn btn-sm btn-danger text-light" style="border-radius: 50%">' . __('SELL') . '</a>';
            $signal_button = '<label class="label label-danger pl-10 pr-10" style="border-radius: 10px">' . __('Sell') . '</label>';
        }
    }
    return $signal_button;
}

function getSignalResultTextForMobile($signals_status, $signals_action, $signals_signal_result) {
    $signal_button = '';
    if ($signals_status == SIGNAL_CLOSED) {
        $signal_button = $signals_signal_result . ' ' . __('PIPS');
    } else {
        if ($signals_action == SIGNAL_ACTION_BUY) {
            $signal_button = __('BUY');
        } else {
            $signal_button = __('Sell');
        }
    }
    return $signal_button;
}

function getSignalStatus($status_id = '', $btn_size = 'btn-sm') {
    $signal_button = '';
    if ($status_id == SIGNAL_CLOSED) {
        $signal_button = '<a class="signal_status_danger btn ' . $btn_size . ' btn-danger">' . __('Closed') . '</a>';
    } else if ($status_id == SIGNAL_ACTIVE) {
        $signal_button = '<a class="signal_status_success btn ' . $btn_size . ' btn-success">' . __('Active') . '</a>';
    } else if ($status_id == SIGNAL_PENDING) {
        $signal_button = '<a class="signal_status_warning btn ' . $btn_size . ' btn-warning">' . __('Pending') . '</a>';
    }
    return $signal_button;
}

function signalPreview($available_for_free = 0) {
    // 0 - premium, 1 - free
    if ($available_for_free == 1) {
        return TRUE; // signal data preview
    } else if (getBalance() > 0 && $available_for_free == 0) {
        return TRUE;
    } else {
        return FALSE; // signal data no-preview
    }
}
