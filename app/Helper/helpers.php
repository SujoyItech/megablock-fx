<?php
use App\Mail\SendEmail;
use App\Mail\SendPassResetEmail;
use App\Models\Action;
use App\Models\AdminSetting;

use App\Models\Notification;
use App\Models\Role;
use Illuminate\Support\Facades\Auth;

use Illuminate\Support\Facades\Mail;
use Intervention\Image\Facades\Image;


function checkRolePermission($userAction, $userRole) {
    if ($userRole == USER_ROLE_ADMIN) {
        return true;
    }

    $action = Action::where('name', $userAction)->orWhere('url', $userAction)->first();
    $role = Role::where('id', $userRole)->first();

    if (!empty($role->actions) && !empty($action)) {
        if (!empty($role->actions)) {
            $tasks = array_filter(explode('|', $role->actions));
        }
        if (isset($tasks)) {
            if (in_array($action->id, $tasks)) {
                return true;
            } else {
                return false;
            }
        }
    }

    return false;
}

function allSetting($array = null) {
    if (!isset($array[0])) {
        $allSettings = AdminSetting::get();
        if ($allSettings) {
            $output = [];
            foreach ($allSettings as $setting) {
                $output[$setting->slug] = $setting->value;
            }
            return $output;
        }
        return false;
    } elseif (is_array($array)) {
        $allSettings = AdminSetting::whereIn('slug', $array)->get();
        if ($allSettings) {
            $output = [];
            foreach ($allSettings as $setting) {
                $output[$setting->slug] = $setting->value;
            }
            return $output;
        }
        return false;
    } else {
        $allSettings = AdminSetting::where(['slug' => $array])->first();
        if ($allSettings) {
            $output = $allSettings->value;
            return $output;
        }
        return false;
    }
}

//Random string
function randomString($a) {
    $x = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    $c = strlen($x) - 1;
    $z = '';
    for ($i = 0; $i < $a; $i++) {
        $y = rand(0, $c);
        $z .= substr($x, $y, 1);
    }
    return $z;
}

// random number
function randomNumber($a = 10) {
    $x = '123456789';
    $c = strlen($x) - 1;
    $z = '';
    for ($i = 0; $i < $a; $i++) {
        $y = rand(0, $c);
        $z .= substr($x, $y, 1);
    }
    return $z;
}

//use array key for validator
function arrKeyOnly($array, $seperator = ',', $exception = []) {
    $string = '';
    $sep = '';
    foreach ($array as $key => $val) {
        if (in_array($key, $exception) == false) {
            $string .= $sep . $key;
            $sep = $seperator;
        }
    }
    return $string;
}

function fileUpload($new_file, $path, $old_file_name = null, $width = null, $height = null) {
    if (!file_exists(public_path($path))) {
        mkdir(public_path($path), 0777, true);
    }
    if (isset($old_file_name) && $old_file_name != "" && file_exists($path . substr($old_file_name, strrpos($old_file_name, '/') + 1))) {

        unlink($path . '/' . substr($old_file_name, strrpos($old_file_name, '/') + 1));
    }

    $input['imagename'] = uniqid() . time() . '.' . $new_file->getClientOriginalExtension();
    $imgPath = public_path($path . $input['imagename']);

    $makeImg = Image::make($new_file);
    if ($width != null && $height != null && is_int($width) && is_int($height)) {
        $makeImg->resize($width, $height);
        $makeImg->fit($width, $height);
    }

    if ($makeImg->save($imgPath)) {
        return $input['imagename'];
    }
    return false;
}

function uploadimage($img, $path, $user_file_name = null, $width = null, $height = null) {
    if (!file_exists($path)) {
        mkdir($path, 0777, true);
    }
    if (isset($user_file_name) && $user_file_name != "" && file_exists($path . $user_file_name)) {
        unlink($path . '/' . $user_file_name);
    }
    // saving image in target path
    $imgName = uniqid() . '.' . $img->getClientOriginalExtension();
    $imgPath = public_path($path . $imgName);


    //    if($img->getClientOriginalExtension() != 'svg'){
    // making image
    $makeImg = Image::make($img);
    if ($width != null && $height != null && is_int($width) && is_int($height)) {
        $makeImg->resize($width, $height);
        $makeImg->fit($width, $height);
    }
    //    }else{
    //        File::copy($imgPath);
    //    }


    if ($makeImg->save($imgPath)) {
        return $imgName;
    }
    return false;
}

function removeImage($path, $file_name) {
    if (isset($file_name) && $file_name != "" && file_exists($path . $file_name)) {
        unlink($path . $file_name);
    }
    //    if (isset($file_name) && $file_name != "" && file_exists($path . substr($file_name, strrpos($file_name, '/') + 1))) {
    //
    //        unlink($path . '/' . substr($file_name, strrpos($file_name, '/') + 1));
    //    }
}

function multipleuploadimage($images, $path, $width = null, $height = null) {
    if (isset($images[0])) {
        $imgNames = [];
        if (!file_exists($path)) {
            mkdir($path, 0777, true);
        }

        foreach ($images as $img) {

            // saving image in target path
            $imgName = uniqid() . '.' . $img->getClientOriginalExtension();
            $imgPath = public_path($path . '/' . $imgName);

            // making image
            $makeImg = Image::make($img);
            if ($width != null && $height != null && is_int($width) && is_int($height)) {
                $makeImg->resize($width, $height);
                $makeImg->fit($width, $height);
            }

            if ($makeImg->save($imgPath)) {
                $imgNames[] = $imgName;
            }
        }

        return $imgNames;
    }

}

if (!function_exists('status_type')) {
    function status_type($val = null) {
        $data = array(
            1 => __('Active'),
            2 => __('Inactive')
        );
        if ($val == null) {
            return $data;
        } else {
            return $data[$val];
        }
    }
}

if (!function_exists('stock_type')) {
    function stock_type($val = null) {
        $data = array(
            IN_STOCK => __('In Stock'),
            OUT_OF_STOCK => __('Out of Stock')
        );
        if ($val == null) {
            return $data;
        } else {
            return $data[$val];
        }
    }
}

if (!function_exists('addon_type')) {
    function addon_type($val = null) {
        $data = array(
            1 => __('Addons'),
            2 => __('Supplement'),
        );
        if ($val == null) {
            return $data;
        } else {
            return $data[$val];
        }
    }
}
// count category product
if (!function_exists('count_category_product')) {
    function count_category_product($cat_id) {
        $product = 0;
        $item = Product::where(['category_id' => $cat_id, 'status' => STATUS_ACTIVE])->get();
        if (isset($item[0])) {
            $product = $item->count();
        }

        return $product;
    }
}

// my cart list
function getImagePath($mode = ''){
    if($mode == 'user'){
        $path = 'uploads/img/user/';
    }elseif ($mode == 'brokers'){
        $path = 'uploads/brokers/';
    }elseif ($mode == 'subscription_package'){
        $path = 'uploads/subscription/';
    }elseif ($mode == 'slider'){
        $path = 'uploads/slider/';
    }elseif ($mode == 'service'){
        $path = 'uploads/service/';
    }elseif ($mode == 'barber'){
        $path = 'uploads/barber/';
    }else{
        $path = 'uploads/img/';
    }
    return $path;
}

// Convert currency
function convertCurrency($amount, $to = 'USD', $from = 'USD') {
    try {
        $url = "https://min-api.cryptocompare.com/data/price?fsym=$from&tsyms=$to";
        $json = file_get_contents($url); //,FALSE,$ctx);
        $jsondata = json_decode($json, TRUE);
        return $amount * $jsondata[$to];
    } catch (\Exception $e) {
        return $amount * allSetting()['coin_price'];
    }
}

if (!function_exists('all_month')) {
    function all_month($val = null) {
        $data = array(
            12 => 12,
            11 => 11,
            10 => 10,
            9 => 9,
            8 => 8,
            7 => 7,
            6 => 6,
            5 => 5,
            4 => 4,
            3 => 3,
            2 => 2,
            1 => 1,
        );
        if ($val == null) {
            return $data;
        } else {
            return $data[$val];
        }
    }
}

if (!function_exists('all_months')) {
    function all_months($val = null) {
        $data = array(
            1 => 1,
            2 => 2,
            3 => 3,
            4 => 4,
            5 => 5,
            6 => 6,
            7 => 7,
            8 => 8,
            9 => 9,
            10 => 10,
            11 => 11,
            12 => 12,
        );
        if ($val == null) {
            return $data;
        } else {
            return $data[$val];
        }
    }
}

//function for getting client ip address
function get_clientIp() {
    return isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : '0.0.0.0';
}

function language() {
    $lang = [];
    $path = base_path('resources/lang');
    foreach (glob($path . '/*.json') as $file) {
        $langName = basename($file, '.json');
        $lang[$langName] = $langName;
    }
    return empty($lang) ? false : $lang;
}

function langName($input = null) {
    $output = [
        'en' => __('English'),
        'pt' => __('Portuguese'),
        'sp' => __('Spanish'),
        'fr' => __('French'),
        'ja' => 'Japanese',
        'zh' => 'Chinese',
        'ko' => 'Korean',
    ];
    if (is_null($input)) {
        return $output;
    } else {
        return $output[$input];
    }
}

if (!function_exists('settings')) {

    function settings($keys = null) {
        if ($keys && is_array($keys)) {
            return Adminsetting::whereIn('slug', $keys)->pluck('value', 'slug')->toArray();
        } elseif ($keys && is_string($keys)) {
            $setting = Adminsetting::where('slug', $keys)->first();
            return empty($setting) ? false : $setting->value;
        }
        return Adminsetting::pluck('value', 'slug')->toArray();
    }
}
//Call this in every function
function set_lang($lang) {
    $default = settings('lang');
    $lang = strtolower($lang);
    $languages = language();
    if (in_array($lang, $languages)) {
        app()->setLocale($lang);
    } else {
        if (isset($default)) {
            $lang = $default;
            app()->setLocale($lang);
        }
    }
}

//find odd even
function oddEven($number) {
    //    dd($number);
    if ($number % 2 == 0) {
        return 'even';
    } else {
        return 'odd';
    }
}

function convert_currencyCoinPayment($amount, $to = 'USD', $price) {
    try {
        $main_amount = $amount;

        $array['amount'] = $amount;
        if ($to == 'EUR')
            return $amount;


        if (($price['error'] == "ok")) {

            $one_coin = $price['result'][$to]['rate_btc']; // dynamic coin rate in btc
            $one_euro = $price['result']['EUR']['rate_btc']; // 1 euro ==  btc rate
            $total_amount_in_btc = bcmul($one_euro, $amount);

            return number_format(bcdiv($total_amount_in_btc, $one_coin), 8);
        }

//        $url = "https://min-api.cryptocompare.com/data/price?fsym=$from&tsyms=$to";
//        $json = file_get_contents($url);
//
//        $jsondata = json_decode($json, TRUE);
//        dd($jsondata);
//        return bcmul($amount, $jsondata[$to]);
    } catch (\Exception $e) {

        return number_format(0, 8);
    }
}


function convert_currency($amount, $to = 'USD', $from = 'USD') {
    $url = "https://min-api.cryptocompare.com/data/price?fsym=$from&tsyms=$to";
    $json = file_get_contents($url); //,FALSE,$ctx);
    $jsondata = json_decode($json, TRUE);
    return bcmul($amount, $jsondata[$to]);
}

// fees calculation
function calculate_fees($amount, $method) {
    $settings = allSetting();
    try {
        if ($method == SEND_FEES_FIXED) {
            return $settings['send_fees_fixed'];
        } elseif ($method == SEND_FEES_PERCENTAGE) {
            return ($settings['send_fees_percentage'] * $amount) / 100;
        } elseif ($method == SEND_FEES_BOTH) {
            return $settings['send_fees_fixed'] + (($settings['send_fees_percentage'] * $amount) / 100);
        } else {
            return 0;
        }
    } catch (\Exception $e) {
        return 0;
    }
}


function getToastrMessage($message = null) {
    if (!empty($message)) {

        // example
        // return redirect()->back()->with('message','warning:Invalid username or password');

        $message = explode(':', $message);
        if (isset($message[1])) {
            $data = 'toastr.' . $message[0] . '("' . $message[1] . '")';
        } else {
            $data = "toastr.error(' write ( errorType:message ) ')";
        }

        return '<script>' . $data . '</script>';

    }

}

// Html render
function get_link($link)
{
    $html = '<li class="viewuser"> <a href="' . $link . '" target="_blank"><i class="fa fa-link"></i></a><span>' . __('Link') . '</span></li>';
    return $html;
}
function edit_html($route, $id) {
    $html = '<li class="viewuser"><a href="' . route($route, encrypt($id)) . '"><i class="fa fa-pencil"></i></a> <span>' . __('Edit') . '</span></li>';
    return $html;
}
function edit_fcs_html($route, $id) {
    $html = '<li class="viewuser"><a href="' . route($route, $id) . '"><i class="fa fa-pencil"></i></a> <span>' . __('Edit') . '</span></li>';
    return $html;
}
function delete_fcs_html($route, $id, $heading = '', $body = '') {
    if($heading == '') $heading = 'Delete';
    if($body == '') $body = 'Would you want to delete ?';

    $html = '<li class="deleteuser"><a href="#delete_' . $id . '" data-toggle="modal"><i class="fa fa-trash"></i></a> <span>' . __($heading) . '</span></li>';
    $html .= '<div id="delete_' . $id . '" class="modal fade delete" role="dialog">';
    $html .= '<div class="modal-dialog modal-sm">';
    $html .= '<div class="modal-content">';
    $html .= '<div class="modal-header"><h6 class="modal-title"><strong>' . __($heading) . '</strong></h6><button type="button" class="close" data-dismiss="modal">&times;</button></div>';
    $html .= '<div class="modal-body"><p>' . __($body) . '</p></div>';
    $html .= '<div class="modal-footer"><button type="button" class="btn btn-primary delete-btn" data-dismiss="modal">' . __("Close") . '</button>';
    $html .= '<a class="btn btn-primary delete-btn"href="' . route($route, $id) . '">' . __('Confirm') . '</a>';
    $html .= '</div>';
    $html .= '</div>';
    $html .= '</div>';
    $html .= '</div>';
    return $html;
}

function view_html($route, $id) {
    $html = '<li class="viewuser"><a href="' . route($route, encrypt($id)) . '"><i class="fa fa-eye"></i></a> <span>' . __('View') . '</span></li>';
    return $html;
}

function view_html2($id, $review) {

    $html = '<li class="viewuser"><a data-message="' . $review . '" href="javascript:" onclick="open_modal(this)"><i class="fa fa-eye"></i></a> <span>' . __('View') . '</span></li>';
    return $html;
}

function delete_html($route, $id, $heading = '', $body = '') {
    if($heading == '') $heading = 'Delete';
    if($body == '') $body = 'Would you want to delete ?';

    $html = '<li class="deleteuser"><a href="#delete_' . $id . '" data-toggle="modal"><i class="fa fa-trash"></i></a> <span>' . __($heading) . '</span></li>';
    $html .= '<div id="delete_' . $id . '" class="modal fade delete" role="dialog">';
    $html .= '<div class="modal-dialog modal-sm">';
    $html .= '<div class="modal-content">';
    $html .= '<div class="modal-header"><h6 class="modal-title"><strong>' . __($heading) . '</strong></h6><button type="button" class="close" data-dismiss="modal">&times;</button></div>';
    $html .= '<div class="modal-body"><p>' . __($body) . '</p></div>';
    $html .= '<div class="modal-footer"><button type="button" class="btn btn-primary delete-btn" data-dismiss="modal">' . __("Close") . '</button>';
    $html .= '<a class="btn btn-primary delete-btn"href="' . route($route, encrypt($id)) . '">' . __('Confirm') . '</a>';
    $html .= '</div>';
    $html .= '</div>';
    $html .= '</div>';
    $html .= '</div>';
    return $html;
}

function deactive_html($route, $id) {
    $html = '<li class="deleteuser"><a href="#deactivate_' . $id . '" data-toggle="modal"><i class="fa fa-ban"></i></a> <span>' . __('Deactivate') . '</span></li>';
    $html .= '<div id="deactivate_' . $id . '" class="modal fade delete" role="dialog">';
    $html .= '<div class="modal-dialog modal-sm">';
    $html .= '<div class="modal-content">';
    $html .= '<div class="modal-header"><h6 class="modal-title"><strong>' . __('Deactive') . '</strong></h6><button type="button" class="close" data-dismiss="modal">&times;</button></div>';
    $html .= '<div class="modal-body"><p>' . __('Would you want to deactivate ?') . '</p></div>';
    $html .= '<div class="modal-footer"><button type="button" class="btn btn-primary delete-btn" data-dismiss="modal">' . __("Close") . '</button>';
    $html .= '<a class="btn btn-primary delete-btn"href="' . route($route, encrypt($id)) . '">' . __('Confirm') . '</a>';
    $html .= '</div>';
    $html .= '</div>';
    $html .= '</div>';
    $html .= '</div>';
    return $html;
}

function activate_html($route, $id) {
    $html = '<li class="deleteuser"><a href="#active_' . $id . '" data-toggle="modal"><i class="fa fa-check"></i></a> <span>' . __('Activate') . '</span></li>';
    $html .= '<div id="active_' . $id . '" class="modal fade delete" role="dialog">';
    $html .= '<div class="modal-dialog modal-sm">';
    $html .= '<div class="modal-content">';
    $html .= '<div class="modal-header"><h6 class="modal-title"><strong>' . __('Activate user') . '</strong></h6><button type="button" class="close" data-dismiss="modal">&times;</button></div>';
    $html .= '<div class="modal-body"><p>' . __('Would you want to activate ?') . '</p></div>';
    $html .= '<div class="modal-footer"><button type="button" class="btn btn-primary delete-btn" data-dismiss="modal">' . __("Close") . '</button>';
    $html .= '<a class="btn btn-primary delete-btn"href="' . route($route, encrypt($id)) . '">' . __('Confirm') . '</a>';
    $html .= '</div>';
    $html .= '</div>';
    $html .= '</div>';
    $html .= '</div>';
    return $html;
}

function block_html($route, $id) {
    $html = '<li class="deleteuser"><a href="#active_' . $id . '" data-toggle="modal"><i class="fa fa-lock"></i></a> <span>' . __('Block') . '</span></li>';
    $html .= '<div id="active_' . $id . '" class="modal fade delete" role="dialog">';
    $html .= '<div class="modal-dialog modal-sm">';
    $html .= '<div class="modal-content">';
    $html .= '<div class="modal-header"><h6 class="modal-title"><strong>' . __('Block User') . '</strong></h6><button type="button" class="close" data-dismiss="modal">&times;</button></div>';
    $html .= '<div class="modal-body"><p>' . __('Would you want to block this user ?') . '</p></div>';
    $html .= '<div class="modal-footer"><button type="button" class="btn btn-primary delete-btn" data-dismiss="modal">' . __("Close") . '</button>';
    $html .= '<a class="btn btn-primary delete-btn"href="' . route($route, encrypt($id)) . '">' . __('Confirm') . '</a>';
    $html .= '</div>';
    $html .= '</div>';
    $html .= '</div>';
    $html .= '</div>';
    return $html;
}

function email_verify_html($route, $id) {
    $html = '<li class="deleteuser"><a href="#verify_' . $id . '" data-toggle="modal"><i class="fa fa-envelope-o"></i></a> <span>' . __('Verify Email') . '</span></li>';
    $html .= '<div id="verify_' . $id . '" class="modal fade delete" role="dialog">';
    $html .= '<div class="modal-dialog modal-sm">';
    $html .= '<div class="modal-content">';
    $html .= '<div class="modal-header"><h6 class="modal-title"><strong>' . __('Verify Email') . '</strong></h6><button type="button" class="close" data-dismiss="modal">&times;</button></div>';
    $html .= '<div class="modal-body"><p>' . __('Would you want to verify email ?') . '</p></div>';
    $html .= '<div class="modal-footer"><button type="button" class="btn btn-primary delete-btn" data-dismiss="modal">' . __("Close") . '</button>';
    $html .= '<a class="btn btn-primary delete-btn"href="' . route($route, encrypt($id)) . '">' . __('Confirm') . '</a>';
    $html .= '</div>';
    $html .= '</div>';
    $html .= '</div>';
    $html .= '</div>';
    return $html;
}

function statusChange_html($route, $status, $id) {
    $icon = ($status != STATUS_SUCCESS) ? '<i class="fa fa-check"></i>' : '<i class="fa fa-close"></i>';
    $status_title = ($status != STATUS_SUCCESS) ? status(STATUS_SUCCESS) : status(STATUS_DEACTIVE);
    $html = '';
    $html .= '<li><a href="' . route($route, encrypt($id)) . '">' . $icon . '<span>' . $status_title . '</span></a> </li>';
    return $html;
}

function set_special_html($route, $status, $id) {
    $icon = ($status != IS_NOT_SPECIAL) ? '<i class="fa fa-check"></i>' : '<i class="fa fa-close"></i>';
    $status_title = ($status == IS_NOT_SPECIAL) ? check_special(IS_NOT_SPECIAL) : check_special(IS_SPECIAL);
    $html = '';
    $html .= '<li><a href="' . route($route, encrypt($id)) . '">' . $icon . '<span>' . $status_title . '</span></a> </li>';
    return $html;
}

// Discount amount
function discount_amount($discount, $discount_type) {
    $discount_amount = '';
    if (isset($discount_type) && $discount_type == DISCOUNT_TYPE_FIXED) {
        $discount_amount = currency_symbol(allSetting()['currency']) . $discount;
    } elseif (isset($discount_type) && $discount_type == DISCOUNT_TYPE_PERCENTAGE && $discount <= 100) {
        $discount_amount = $discount . ' %';
    }
    return $discount_amount;
}

function price($price, $discount, $discount_type) {
    if (isset($discount_type) && $discount_type == DISCOUNT_TYPE_FIXED) {
        $net_price = $price - $discount;
    } elseif (isset($discount_type) && $discount_type == DISCOUNT_TYPE_FIXED) {
        $net_price = $price - ($price * $discount) / 100;
    } else {
        $net_price = $price;
    }
    return $net_price;
}

function service_item($services) {
    $settings = allSetting();
    $html = '';
    foreach ($services as $service) {
        $html .= '<div class="col-lg-4 col-md-6 col-12 moreload">';
        $html .= '<div class="service-wrappper">';
        $html .= '<div class="service-img">';
        //        $html .= '<a class="wishlist" href="#"><i class="fa fa-heart"></i></a>';
        $html .= '<img src="';
        if (empty($service->images)) {
            $html .= asset('images/default.jpg');
        } else {
            $html .= asset(getImagePath('service') . $service->images);
        }
        $html .= ' " alt="">';
        //        $html .= '<div class="checkbox"><input type="checkbox" id="box-1"><label for="box-1"></label></div>';
        if ($service->discount > 0) $html .= '<span class="discount">' . discount_amount($service->discount, $service->discount_type) . '</span>';
        $html .= '</div>';
        $html .= '<div class="service-content">';
        $html .= '<h5>' . $settings['currency_symbol'] . price($service->price, $service->discount, $service->discount_type) . '<del>' . $settings['currency_symbol'] . $service->price . '</del></h5>';
        $html .= '<h3><a href="' . route('getServiceDetails', encrypt($service->id)) . '">' . $service->title . '</a></h3>';
        $html .= $service->description;
        $html .= '</div>';
        $html .= '</div>';
        $html .= '</div>';
    }
    return $html;
}

function reservation_service_item($services) {
    $settings = allSetting();
    $html = '';
    foreach ($services as $service) {
        $html .= '<div class="col-lg-4 col-md-6 col-12 moreload">';
        $html .= '<div class="service-wrappper">';
        $html .= '<div class="service-img">';
        //        $html .= '<a class="wishlist" href="#"><i class="fa fa-heart"></i></a>';
        $html .= '<img src="';
        if (empty($service->images)) {
            $html .= asset('images/default.jpg');
        } else {
            $html .= asset(getImagePath('service') . $service->images);
        }
        $html .= ' " alt="">';
        $html .= '<div class="checkbox"><input type="checkbox" name="service[]" value="' . $service->id . '" id="box-' . $service->id . '"><label for="box-' . $service->id . '"></label></div>';
        if ($service->discount > 0) $html .= '<span class="discount">' . discount_amount($service->discount, $service->discount_type) . '</span>';
        $html .= '</div>';
        $html .= '<div class="service-content">';
        $html .= '<h5>' . $settings['currency_symbol'] . price($service->price, $service->discount, $service->discount_type) . '<del>' . $settings['currency_symbol'] . $service->price . '</del></h5>';
        $html .= '<h3><a href="' . route('getServiceDetails', encrypt($service->id)) . '">' . $service->title . '</a></h3>';
        $html .= $service->description;
        $html .= '</div>';
        $html .= '</div>';
        $html .= '</div>';
    }
    return $html;
}

function status_search($keyword) {
    $st = [];
    if (strpos('_active', strtolower($keyword)) != false) {
        array_push($st, STATUS_ACTIVE);
    }
    if (strpos('_deactive', strtolower($keyword)) != false) {
        array_push($st, STATUS_DEACTIVE);
    }
    return $st;
}

function currency_symbol_text($a = null) {
    $output = [
        'AFN' => 'Afghani(؋)',
        'ANG' => 'Netherlands Antillian Guilder(ƒ)',
        'ARS' => 'Argentine Peso($)',
        'AUD' => 'Australian Dollar(A$)',
        'BRL' => 'Brazilian Real(R$)',
        'BSD' => 'Bahamian Dollar(B$)',
        'CAD' => 'Canadian Dollar($)',
        'CHF' => 'Swiss Franc(CHF)',
        'CLP' => 'CLF Chilean Peso Unidades de fomento($)',
        'CNY' => 'Yuan Renminbi(¥)',
        'COP' => 'COU Colombian Peso Unidad de Valor Real($)',
        'CZK' => 'Czech Koruna(Kč)',
        'DKK' => 'Danish Krone(kr)',
        'EUR' => 'Euro(€)',
        'FJD' => 'Fiji Dollar(FJ$)',
        'GBP' => 'Pound Sterling(£)',
        'GHS' => 'Ghanaian cedi(GH₵)',
        'GTQ' => 'Quetzal(Q)',
        'HKD' => 'Hong Kong Dollar($)',
        'HNL' => 'Lempira(L)',
        'HRK' => 'Croatian Kuna(kn)',
        'HUF' => 'Forint(Ft)',
        'IDR' => 'Rupiah(Rp)',
        'ILS' => 'New Israeli Sheqel(₪)',
        'INR' => 'Indian rupee(₹)',
        'ISK' => 'Iceland Krona(kr)',
        'JMD' => 'Jamaican Dollar(J$)',
        'JPY' => 'Yen(¥)',
        'KRW' => 'Won(₩)',
        'LKR' => 'Sri Lanka Rupee(₨)',
        'MAD' => 'Moroccan dirham(.د.م)',
        'MMK' => 'Myanmar kyat(K)',
        'MXN' => 'Mexican peso($)',
        'MYR' => 'Malaysian Ringgit(RM)',
        'NOK' => 'Norwegian Krone(kr)',
        'NZD' => 'New Zealand Dollar($)',
        'PAB' => 'USD Balboa US Dollar(B/.)',
        'PEN' => 'Nuevo Sol(S/.)',
        'PHP' => 'Philippine Peso(₱)',
        'PKR' => 'Pakistan Rupee(₨)',
        'PLN' => 'Zloty(zł)',
        'RON' => 'New Leu(lei)',
        'RSD' => 'Serbian Dinar(RSD)',
        'RUB' => 'Russian Ruble(руб)',
        'SEK' => 'Swedish Krona(kr)',
        'SGD' => 'Singapore Dollar(S$)',
        'THB' => 'Baht(฿)',
        'TND' => 'Tunisian dinar(DT)',
        'TRY' => 'Turkish lira(TL)',
        'TTD' => 'Trinidad and Tobago Dollar(TT$)',
        'TWD' => 'New Taiwan Dollar(NT$)',
        'USD' => 'US Dollar($)',
        'VEF' => 'Bolivar Fuerte(Bs)',
        'VND' => 'Dong(₫)',
        'XAF' => 'Central African CFA franc(FCFA)',
        'XCD' => 'East Caribbean Dollar($)',
        'XPF' => 'CFP franc(F)',
        'ZAR' => 'Rand(R)',
    ];
    if ($a == null) {
        return $output;
    } elseif (isset($output[$a])) {

        return $output[$a];
    } else {

        return $a;
    }
}

function currency_symbol($a = null) {
    $output = [
        'AFN' => '؋',
        'ANG' => 'ƒ',
        'ARS' => '$',
        'AUD' => 'A$',
        'BRL' => 'R$',
        'BSD' => 'B$',
        'CAD' => '$',
        'CHF' => 'CHF',
        'CLP' => '$',
        'CNY' => '¥',
        'COP' => '$',
        'CZK' => 'Kč',
        'DKK' => 'kr',
        'EUR' => '€',
        'FJD' => 'FJ$',
        'GBP' => '£',
        'GHS' => 'GH₵',
        'GTQ' => 'Q',
        'HKD' => '$',
        'HNL' => 'L',
        'HRK' => 'kn',
        'HUF' => 'Ft',
        'IDR' => 'Rp',
        'ILS' => '₪',
        'INR' => '₹',
        'ISK' => 'kr',
        'JMD' => 'J$',
        'JPY' => '¥',
        'KRW' => '₩',
        'LKR' => '₨',
        'MAD' => '.د.م',
        'MMK' => 'K',
        'MXN' => '$',
        'MYR' => 'RM',
        'NOK' => 'kr',
        'NZD' => '$',
        'PAB' => 'B/.',
        'PEN' => 'S/.',
        'PHP' => '₱',
        'PKR' => '₨',
        'PLN' => 'zł',
        'RON' => 'lei',
        'RSD' => 'RSD',
        'RUB' => 'руб',
        'SEK' => 'kr',
        'SGD' => 'S$',
        'THB' => '฿',
        'TND' => 'DT',
        'TRY' => 'TL',
        'TTD' => 'TT$',
        'TWD' => 'NT$',
        'USD' => '$',
        'VEF' => 'Bs',
        'VND' => '₫',
        'XAF' => 'FCFA',
        'XCD' => '$',
        'XPF' => 'F',
        'ZAR' => 'R',
    ];
    if ($a == null) {
        return $output;
    } elseif (isset($output[$a])) {

        return $output[$a];
    } else {

        return $a;
    }
}

if (!function_exists('payment_methods')) {
    function payment_methods($val = null) {
        $data = array(
            1 => __('Cash On Delivery'),
            2 => __('Credit Card'),
        );
        if ($val == null) {
            return $data;
        } else {
            return $data[$val];
        }
    }
}

if (!function_exists('product_is_wishlist')) {
    function product_is_wishlist($product_id) {
        $is_favorite = 0;
        $wishList = UserProduct::where(['user_id' => Auth::id(), 'product_id' => $product_id])->first();
        if ($wishList && ($wishList->is_wishlist == 1)) {
            $is_favorite = 1;
        }

        return $is_favorite;
    }
}


if (!function_exists('discounted_price')) {
    function discounted_price($price, $type, $discount) {
        $discounted_price = $price;
        if ($type == DISCOUNT_TYPE_PERCENTAGE) {
            if ($discount <= 100) {
                $discounted_price = $price - ($price * $discount) / 100;
            }
        } elseif ($type == DISCOUNT_TYPE_FIXED) {
            if ($price > $discount) {
                $discounted_price = $price - $discount;
            }
        }

        return $discounted_price;
    }
}
// get product base price
if (!function_exists('base_price')) {
    function base_price($product_id, $price) {
        $base_price = $price;
        $options = ProductOption::where('product_id', $product_id)->get();
        if (isset($options[0])) {
            $base_price = $options[0]->price;
        }

        return $base_price;
    }
}

// get product's selected base price
if (!function_exists('product_option_price')) {
    function product_option_price($product_id, $price, $option_id) {
        $base_price = base_price($product_id, $price);
        $option = ProductOption::where(['product_id' => $product_id, 'option_id' => $option_id])->first();
        if (isset($option)) {
            $base_price = $option->price;
        }

        return $base_price;
    }
}
if (!function_exists('discount_price')) {
    function discount_price($price, $type, $discount) {
        $discount_price = 0;
        if ($type == DISCOUNT_TYPE_PERCENTAGE) {
            if ($discount <= 100) {
                $discount_price = ($price * $discount) / 100;
            }
        } elseif ($type == DISCOUNT_TYPE_FIXED) {
            if ($price > $discount) {
                $discount_price = $discount;
            }
        }

        return $discount_price;
    }
}

// check is offer or not
function checkIsOffer($product_id) {
    return app(ProductRepository::class)->checkIsOffer($product_id);
}

// my cart list

// check is offer or not
function myCartList() {
    return app(CartRepository::class)->myCartList();
}

// offered price
function offered_price($product_id) {
    $price = 0;
    $offer = Offer::join('products', 'products.id', '=', 'offers.product_id')
        ->where(['offers.product_id' => $product_id])
        ->select('offers.*', 'products.price')
        ->first();
    if ($offer) {
        $price = discounted_price($offer->price, $offer->discount_type, $offer->discount);
    }

    return $price;
}

// product addons
function productAddons($type, $product_id) {
    return app(AddonRepository::class)->productAddons($type, $product_id);
}

// product options
function productOptions($product_id) {
    return app(OptionRepository::class)->productOptions($product_id);
}

// country list

function country($lang = null) {
    $output = [
        'AF' => __('Afghanistan'),
        'AL' => __('Albania'),
        'DZ' => __('Algeria'),
        'DS' => __('American Samoa'),
        'AD' => __('Andorra'),
        'AO' => __('Angola'),
        'AI' => __('Anguilla'),
        'AQ' => __('Antarctica'),
        'AG' => __('Antigua and Barbuda'),
        'AR' => __('Argentina'),
        'AM' => __('Armenia'),
        'AW' => __('Aruba'),
        'AU' => __('Australia'),
        'AT' => __('Austria'),
        'AZ' => __('Azerbaijan'),
        'BS' => __('Bahamas'),
        'BH' => __('Bahrain'),
        'BD' => __('Bangladesh'),
        'BB' => __('Barbados'),
        'BY' => __('Belarus'),
        'BE' => __('Belgium'),
        'BZ' => __('Belize'),
        'BJ' => __('Benin'),
        'BM' => __('Bermuda'),
        'BT' => __('Bhutan'),
        'BO' => __('Bolivia'),
        'BA' => __('Bosnia and Herzegovina'),
        'BW' => __('Botswana'),
        'BV' => __('Bouvet Island'),
        'BR' => __('Brazil'),
        'IO' => __('British Indian Ocean Territory'),
        'BN' => __('Brunei Darussalam'),
        'BG' => __('Bulgaria'),
        'BF' => __('Burkina Faso'),
        'BI' => __('Burundi'),
        'KH' => __('Cambodia'),
        'CM' => __('Cameroon'),
        'CA' => __('Canada'),
        'CV' => __('Cape Verde'),
        'KY' => __('Cayman Islands'),
        'CF' => __('Central African Republic'),
        'TD' => __('Chad'),
        'CL' => __('Chile'),
        'CN' => __('China'),
        'CX' => __('Christmas Island'),
        'CC' => __('Cocos (Keeling) Islands'),
        'CO' => __('Colombia'),
        'KM' => __('Comoros'),
        'CG' => __('Congo'),
        'CK' => __('Cook Islands'),
        'CR' => __('Costa Rica'),
        'HR' => __('Croatia (Hrvatska)'),
        'CU' => __('Cuba'),
        'CY' => __('Cyprus'),
        'CZ' => __('Czech Republic'),
        'DK' => __('Denmark'),
        'DJ' => __('Djibouti'),
        'DM' => __('Dominica'),
        'DO' => __('Dominican Republic'),
        'TP' => __('East Timor'),
        'EC' => __('Ecuador'),
        'EG' => __('Egypt'),
        'SV' => __('El Salvador'),
        'GQ' => __('Equatorial Guinea'),
        'ER' => __('Eritrea'),
        'EE' => __('Estonia'),
        'ET' => __('Ethiopia'),
        'FK' => __('Falkland Islands (Malvinas)'),
        'FO' => __('Faroe Islands'),
        'FJ' => __('Fiji'),
        'FI' => __('Finland'),
        'FR' => __('France'),
        'FX' => __('France, Metropolitan'),
        'GF' => __('French Guiana'),
        'PF' => __('French Polynesia'),
        'TF' => __('French Southern Territories'),
        'GA' => __('Gabon'),
        'GM' => __('Gambia'),
        'GE' => __('Georgia'),
        'DE' => __('Germany'),
        'GH' => __('Ghana'),
        'GI' => __('Gibraltar'),
        'GK' => __('Guernsey'),
        'GR' => __('Greece'),
        'GL' => __('Greenland'),
        'GD' => __('Grenada'),
        'GP' => __('Guadeloupe'),
        'GU' => __('Guam'),
        'GT' => __('Guatemala'),
        'GN' => __('Guinea'),
        'GW' => __('Guinea-Bissau'),
        'GY' => __('Guyana'),
        'HT' => __('Haiti'),
        'HM' => __('Heard and Mc Donald Islands'),
        'HN' => __('Honduras'),
        'HK' => __('Hong Kong'),
        'HU' => __('Hungary'),
        'IS' => __('Iceland'),
        'IN' => __('India'),
        'IM' => __('Isle of Man'),
        'ID' => __('Indonesia'),
        'IR' => __('Iran (Islamic Republic of)'),
        'IQ' => __('Iraq'),
        'IE' => __('Ireland'),
        'IL' => __('Israel'),
        'IT' => __('Italy'),
        'CI' => __('Ivory Coast'),
        'JE' => __('Jersey'),
        'JM' => __('Jamaica'),
        'JP' => __('Japan'),
        'JO' => __('Jordan'),
        'KZ' => __('Kazakhstan'),
        'KE' => __('Kenya'),
        'KI' => __('Kiribati'),
        'KP' => __('Korea, Democratic People\'s Republic of'),
        'KR' => __('Korea, Republic of'),
        'XK' => __('Kosovo'),
        'KW' => __('Kuwait'),
        'KG' => __('Kyrgyzstan'),
        'LA' => __('Lao People\'s Democratic Republic'),
        'LV' => __('Latvia'),
        'LB' => __('Lebanon'),
        'LS' => __('Lesotho'),
        'LR' => __('Liberia'),
        'LY' => __('Libyan Arab Jamahiriya'),
        'LI' => __('Liechtenstein'),
        'LT' => __('Lithuania'),
        'LU' => __('Luxembourg'),
        'MO' => __('Macau'),
        'MK' => __('Macedonia'),
        'MG' => __('Madagascar'),
        'MW' => __('Malawi'),
        'MY' => __('Malaysia'),
        'MV' => __('Maldives'),
        'ML' => __('Mali'),
        'MT' => __('Malta'),
        'MH' => __('Marshall Islands'),
        'MQ' => __('Martinique'),
        'MR' => __('Mauritania'),
        'MU' => __('Mauritius'),
        'TY' => __('Mayotte'),
        'MX' => __('Mexico'),
        'FM' => __('Micronesia, Federated States of'),
        'MD' => __('Moldova, Republic of'),
        'MC' => __('Monaco'),
        'MN' => __('Mongolia'),
        'ME' => __('Montenegro'),
        'MS' => __('Montserrat'),
        'MA' => __('Morocco'),
        'MZ' => __('Mozambique'),
        'MM' => __('Myanmar'),
        'NA' => __('Namibia'),
        'NR' => __('Nauru'),
        'NP' => __('Nepal'),
        'NL' => __('Netherlands'),
        'AN' => __('Netherlands Antilles'),
        'NC' => __('New Caledonia'),
        'NZ' => __('New Zealand'),
        'NI' => __('Nicaragua'),
        'NE' => __('Niger'),
        'NG' => __('Nigeria'),
        'NU' => __('Niue'),
        'NF' => __('Norfolk Island'),
        'MP' => __('Northern Mariana Islands'),
        'NO' => __('Norway'),
        'OM' => __('Oman'),
        'PK' => __('Pakistan'),
        'PW' => __('Palau'),
        'PS' => __('Palestine'),
        'PA' => __('Panama'),
        'PG' => __('Papua New Guinea'),
        'PY' => __('Paraguay'),
        'PE' => __('Peru'),
        'PH' => __('Philippines'),
        'PN' => __('Pitcairn'),
        'PL' => __('Poland'),
        'PT' => __('Portugal'),
        'PR' => __('Puerto Rico'),
        'QA' => __('Qatar'),
        'RE' => __('Reunion'),
        'RO' => __('Romania'),
        'RU' => __('Russian Federation'),
        'RW' => __('Rwanda'),
        'KN' => __('Saint Kitts and Nevis'),
        'LC' => __('Saint Lucia'),
        'VC' => __('Saint Vincent and the Grenadines'),
        'WS' => __('Samoa'),
        'SM' => __('San Marino'),
        'ST' => __('Sao Tome and Principe'),
        'SA' => __('Saudi Arabia'),
        'SN' => __('Senegal'),
        'RS' => __('Serbia'),
        'SC' => __('Seychelles'),
        'SL' => __('Sierra Leone'),
        'SG' => __('Singapore'),
        'SK' => __('Slovakia'),
        'SI' => __('Slovenia'),
        'SB' => __('Solomon Islands'),
        'SO' => __('Somalia'),
        'ZA' => __('South Africa'),
        'GS' => __('South Georgia South Sandwich Islands'),
        'ES' => __('Spain'),
        'LK' => __('Sri Lanka'),
        'SH' => __('St. Helena'),
        'PM' => __('St. Pierre and Miquelon'),
        'SD' => __('Sudan'),
        'SR' => __('Suriname'),
        'SJ' => __('Svalbard and Jan Mayen Islands'),
        'SZ' => __('Swaziland'),
        'SE' => __('Sweden'),
        'CH' => __('Switzerland'),
        'SY' => __('Syrian Arab Republic'),
        'TW' => __('Taiwan'),
        'TJ' => __('Tajikistan'),
        'TZ' => __('Tanzania, United Republic of'),
        'TH' => __('Thailand'),
        'TG' => __('Togo'),
        'TK' => __('Tokelau'),
        'TO' => __('Tonga'),
        'TT' => __('Trinidad and Tobago'),
        'TN' => __('Tunisia'),
        'TR' => __('Turkey'),
        'TM' => __('Turkmenistan'),
        'TC' => __('Turks and Caicos Islands'),
        'TV' => __('Tuvalu'),
        'UG' => __('Uganda'),
        'UA' => __('Ukraine'),
        'AE' => __('United Arab Emirates'),
        'UK' => __('United Kingdom'),
        'US' => __('United States'),
        'UM' => __('United States minor outlying islands'),
        'UY' => __('Uruguay'),
        'UZ' => __('Uzbekistan'),
        'VU' => __('Vanuatu'),
        'VA' => __('Vatican City State'),
        'VE' => __('Venezuela'),
        'VN' => __('Vietnam'),
        'VG' => __('Virgin Islands (British)'),
        'VI' => __('Virgin Islands (U.S.)'),
        'WF' => __('Wallis and Futuna Islands'),
        'EH' => __('Western Sahara'),
        'YE' => __('Yemen'),
        'ZR' => __('Zaire'),
        'ZM' => __('Zambia'),
        'ZW' => __('Zimbabwe')
    ];

    if ($lang == null) {
        return $output;
    } else if (is_array($lang)) {
        return array_intersect($output, $lang);
    } else {
        return isset($output[$lang]) ? $output[$lang] : $lang;
    }
}

function timezone($timezone = null) {
    $output = [
        'America/Adak' => '(GMT-10:00) America/Adak (Hawaii-Aleutian Standard Time)',
        'America/Atka' => '(GMT-10:00) America/Atka (Hawaii-Aleutian Standard Time)',
        'America/Anchorage' => '(GMT-9:00) America/Anchorage (Alaska Standard Time)',
        'America/Juneau' => '(GMT-9:00) America/Juneau (Alaska Standard Time)',
        'America/Nome' => '(GMT-9:00) America/Nome (Alaska Standard Time)',
        'America/Yakutat' => '(GMT-9:00) America/Yakutat (Alaska Standard Time)',
        'America/Dawson' => '(GMT-8:00) America/Dawson (Pacific Standard Time)',
        'America/Ensenada' => '(GMT-8:00) America/Ensenada (Pacific Standard Time)',
        'America/Los_Angeles' => '(GMT-8:00) America/Los_Angeles (Pacific Standard Time)',
        'America/Tijuana' => '(GMT-8:00) America/Tijuana (Pacific Standard Time)',
        'America/Vancouver' => '(GMT-8:00) America/Vancouver (Pacific Standard Time)',
        'America/Whitehorse' => '(GMT-8:00) America/Whitehorse (Pacific Standard Time)',
        'Canada/Pacific' => '(GMT-8:00) Canada/Pacific (Pacific Standard Time)',
        'Canada/Yukon' => '(GMT-8:00) Canada/Yukon (Pacific Standard Time)',
        'Mexico/BajaNorte' => '(GMT-8:00) Mexico/BajaNorte (Pacific Standard Time)',
        'America/Boise' => '(GMT-7:00) America/Boise (Mountain Standard Time)',
        'America/Cambridge_Bay' => '(GMT-7:00) America/Cambridge_Bay (Mountain Standard Time)',
        'America/Chihuahua' => '(GMT-7:00) America/Chihuahua (Mountain Standard Time)',
        'America/Dawson_Creek' => '(GMT-7:00) America/Dawson_Creek (Mountain Standard Time)',
        'America/Denver' => '(GMT-7:00) America/Denver (Mountain Standard Time)',
        'America/Edmonton' => '(GMT-7:00) America/Edmonton (Mountain Standard Time)',
        'America/Hermosillo' => '(GMT-7:00) America/Hermosillo (Mountain Standard Time)',
        'America/Inuvik' => '(GMT-7:00) America/Inuvik (Mountain Standard Time)',
        'America/Mazatlan' => '(GMT-7:00) America/Mazatlan (Mountain Standard Time)',
        'America/Phoenix' => '(GMT-7:00) America/Phoenix (Mountain Standard Time)',
        'America/Shiprock' => '(GMT-7:00) America/Shiprock (Mountain Standard Time)',
        'America/Yellowknife' => '(GMT-7:00) America/Yellowknife (Mountain Standard Time)',
        'Canada/Mountain' => '(GMT-7:00) Canada/Mountain (Mountain Standard Time)',
        'Mexico/BajaSur' => '(GMT-7:00) Mexico/BajaSur (Mountain Standard Time)',
        'America/Belize' => '(GMT-6:00) America/Belize (Central Standard Time)',
        'America/Cancun' => '(GMT-6:00) America/Cancun (Central Standard Time)',
        'America/Chicago' => '(GMT-6:00) America/Chicago (Central Standard Time)',
        'America/Costa_Rica' => '(GMT-6:00) America/Costa_Rica (Central Standard Time)',
        'America/El_Salvador' => '(GMT-6:00) America/El_Salvador (Central Standard Time)',
        'America/Guatemala' => '(GMT-6:00) America/Guatemala (Central Standard Time)',
        'America/Knox_IN' => '(GMT-6:00) America/Knox_IN (Central Standard Time)',
        'America/Managua' => '(GMT-6:00) America/Managua (Central Standard Time)',
        'America/Menominee' => '(GMT-6:00) America/Menominee (Central Standard Time)',
        'America/Merida' => '(GMT-6:00) America/Merida (Central Standard Time)',
        'America/Mexico_City' => '(GMT-6:00) America/Mexico_City (Central Standard Time)',
        'America/Monterrey' => '(GMT-6:00) America/Monterrey (Central Standard Time)',
        'America/Rainy_River' => '(GMT-6:00) America/Rainy_River (Central Standard Time)',
        'America/Rankin_Inlet' => '(GMT-6:00) America/Rankin_Inlet (Central Standard Time)',
        'America/Regina' => '(GMT-6:00) America/Regina (Central Standard Time)',
        'America/Swift_Current' => '(GMT-6:00) America/Swift_Current (Central Standard Time)',
        'America/Tegucigalpa' => '(GMT-6:00) America/Tegucigalpa (Central Standard Time)',
        'America/Winnipeg' => '(GMT-6:00) America/Winnipeg (Central Standard Time)',
        'Canada/Central' => '(GMT-6:00) Canada/Central (Central Standard Time)',
        'Canada/East-Saskatchewan' => '(GMT-6:00) Canada/East-Saskatchewan (Central Standard Time)',
        'Canada/Saskatchewan' => '(GMT-6:00) Canada/Saskatchewan (Central Standard Time)',
        'Chile/EasterIsland' => '(GMT-6:00) Chile/EasterIsland (Easter Is. Time)',
        'Mexico/General' => '(GMT-6:00) Mexico/General (Central Standard Time)',
        'America/Atikokan' => '(GMT-5:00) America/Atikokan (Eastern Standard Time)',
        'America/Bogota' => '(GMT-5:00) America/Bogota (Colombia Time)',
        'America/Cayman' => '(GMT-5:00) America/Cayman (Eastern Standard Time)',
        'America/Coral_Harbour' => '(GMT-5:00) America/Coral_Harbour (Eastern Standard Time)',
        'America/Detroit' => '(GMT-5:00) America/Detroit (Eastern Standard Time)',
        'America/Fort_Wayne' => '(GMT-5:00) America/Fort_Wayne (Eastern Standard Time)',
        'America/Grand_Turk' => '(GMT-5:00) America/Grand_Turk (Eastern Standard Time)',
        'America/Guayaquil' => '(GMT-5:00) America/Guayaquil (Ecuador Time)',
        'America/Havana' => '(GMT-5:00) America/Havana (Cuba Standard Time)',
        'America/Indianapolis' => '(GMT-5:00) America/Indianapolis (Eastern Standard Time)',
        'America/Iqaluit' => '(GMT-5:00) America/Iqaluit (Eastern Standard Time)',
        'America/Jamaica' => '(GMT-5:00) America/Jamaica (Eastern Standard Time)',
        'America/Lima' => '(GMT-5:00) America/Lima (Peru Time)',
        'America/Louisville' => '(GMT-5:00) America/Louisville (Eastern Standard Time)',
        'America/Montreal' => '(GMT-5:00) America/Montreal (Eastern Standard Time)',
        'America/Nassau' => '(GMT-5:00) America/Nassau (Eastern Standard Time)',
        'America/New_York' => '(GMT-5:00) America/New_York (Eastern Standard Time)',
        'America/Nipigon' => '(GMT-5:00) America/Nipigon (Eastern Standard Time)',
        'America/Panama' => '(GMT-5:00) America/Panama (Eastern Standard Time)',
        'America/Pangnirtung' => '(GMT-5:00) America/Pangnirtung (Eastern Standard Time)',
        'America/Port-au-Prince' => '(GMT-5:00) America/Port-au-Prince (Eastern Standard Time)',
        'America/Resolute' => '(GMT-5:00) America/Resolute (Eastern Standard Time)',
        'America/Thunder_Bay' => '(GMT-5:00) America/Thunder_Bay (Eastern Standard Time)',
        'America/Toronto' => '(GMT-5:00) America/Toronto (Eastern Standard Time)',
        'Canada/Eastern' => '(GMT-5:00) Canada/Eastern (Eastern Standard Time)',
        'America/Caracas' => '(GMT-4:-30) America/Caracas (Venezuela Time)',
        'America/Anguilla' => '(GMT-4:00) America/Anguilla (Atlantic Standard Time)',
        'America/Antigua' => '(GMT-4:00) America/Antigua (Atlantic Standard Time)',
        'America/Aruba' => '(GMT-4:00) America/Aruba (Atlantic Standard Time)',
        'America/Asuncion' => '(GMT-4:00) America/Asuncion (Paraguay Time)',
        'America/Barbados' => '(GMT-4:00) America/Barbados (Atlantic Standard Time)',
        'America/Blanc-Sablon' => '(GMT-4:00) America/Blanc-Sablon (Atlantic Standard Time)',
        'America/Boa_Vista' => '(GMT-4:00) America/Boa_Vista (Amazon Time)',
        'America/Campo_Grande' => '(GMT-4:00) America/Campo_Grande (Amazon Time)',
        'America/Cuiaba' => '(GMT-4:00) America/Cuiaba (Amazon Time)',
        'America/Curacao' => '(GMT-4:00) America/Curacao (Atlantic Standard Time)',
        'America/Dominica' => '(GMT-4:00) America/Dominica (Atlantic Standard Time)',
        'America/Eirunepe' => '(GMT-4:00) America/Eirunepe (Amazon Time)',
        'America/Glace_Bay' => '(GMT-4:00) America/Glace_Bay (Atlantic Standard Time)',
        'America/Goose_Bay' => '(GMT-4:00) America/Goose_Bay (Atlantic Standard Time)',
        'America/Grenada' => '(GMT-4:00) America/Grenada (Atlantic Standard Time)',
        'America/Guadeloupe' => '(GMT-4:00) America/Guadeloupe (Atlantic Standard Time)',
        'America/Guyana' => '(GMT-4:00) America/Guyana (Guyana Time)',
        'America/Halifax' => '(GMT-4:00) America/Halifax (Atlantic Standard Time)',
        'America/La_Paz' => '(GMT-4:00) America/La_Paz (Bolivia Time)',
        'America/Manaus' => '(GMT-4:00) America/Manaus (Amazon Time)',
        'America/Marigot' => '(GMT-4:00) America/Marigot (Atlantic Standard Time)',
        'America/Martinique' => '(GMT-4:00) America/Martinique (Atlantic Standard Time)',
        'America/Moncton' => '(GMT-4:00) America/Moncton (Atlantic Standard Time)',
        'America/Montserrat' => '(GMT-4:00) America/Montserrat (Atlantic Standard Time)',
        'America/Port_of_Spain' => '(GMT-4:00) America/Port_of_Spain (Atlantic Standard Time)',
        'America/Porto_Acre' => '(GMT-4:00) America/Porto_Acre (Amazon Time)',
        'America/Porto_Velho' => '(GMT-4:00) America/Porto_Velho (Amazon Time)',
        'America/Puerto_Rico' => '(GMT-4:00) America/Puerto_Rico (Atlantic Standard Time)',
        'America/Rio_Branco' => '(GMT-4:00) America/Rio_Branco (Amazon Time)',
        'America/Santiago' => '(GMT-4:00) America/Santiago (Chile Time)',
        'America/Santo_Domingo' => '(GMT-4:00) America/Santo_Domingo (Atlantic Standard Time)',
        'America/St_Barthelemy' => '(GMT-4:00) America/St_Barthelemy (Atlantic Standard Time)',
        'America/St_Kitts' => '(GMT-4:00) America/St_Kitts (Atlantic Standard Time)',
        'America/St_Lucia' => '(GMT-4:00) America/St_Lucia (Atlantic Standard Time)',
        'America/St_Thomas' => '(GMT-4:00) America/St_Thomas (Atlantic Standard Time)',
        'America/St_Vincent' => '(GMT-4:00) America/St_Vincent (Atlantic Standard Time)',
        'America/Thule' => '(GMT-4:00) America/Thule (Atlantic Standard Time)',
        'America/Tortola' => '(GMT-4:00) America/Tortola (Atlantic Standard Time)',
        'America/Virgin' => '(GMT-4:00) America/Virgin (Atlantic Standard Time)',
        'Antarctica/Palmer' => '(GMT-4:00) Antarctica/Palmer (Chile Time)',
        'Atlantic/Bermuda' => '(GMT-4:00) Atlantic/Bermuda (Atlantic Standard Time)',
        'Atlantic/Stanley' => '(GMT-4:00) Atlantic/Stanley (Falkland Is. Time)',
        'Brazil/Acre' => '(GMT-4:00) Brazil/Acre (Amazon Time)',
        'Brazil/West' => '(GMT-4:00) Brazil/West (Amazon Time)',
        'Canada/Atlantic' => '(GMT-4:00) Canada/Atlantic (Atlantic Standard Time)',
        'Chile/Continental' => '(GMT-4:00) Chile/Continental (Chile Time)',
        'America/St_Johns' => '(GMT-3:-30) America/St_Johns (Newfoundland Standard Time)',
        'Canada/Newfoundland' => '(GMT-3:-30) Canada/Newfoundland (Newfoundland Standard Time)',
        'America/Araguaina' => '(GMT-3:00) America/Araguaina (Brasilia Time)',
        'America/Bahia' => '(GMT-3:00) America/Bahia (Brasilia Time)',
        'America/Belem' => '(GMT-3:00) America/Belem (Brasilia Time)',
        'America/Buenos_Aires' => '(GMT-3:00) America/Buenos_Aires (Argentine Time)',
        'America/Catamarca' => '(GMT-3:00) America/Catamarca (Argentine Time)',
        'America/Cayenne' => '(GMT-3:00) America/Cayenne (French Guiana Time)',
        'America/Cordoba' => '(GMT-3:00) America/Cordoba (Argentine Time)',
        'America/Fortaleza' => '(GMT-3:00) America/Fortaleza (Brasilia Time)',
        'America/Godthab' => '(GMT-3:00) America/Godthab (Western Greenland Time)',
        'America/Jujuy' => '(GMT-3:00) America/Jujuy (Argentine Time)',
        'America/Maceio' => '(GMT-3:00) America/Maceio (Brasilia Time)',
        'America/Mendoza' => '(GMT-3:00) America/Mendoza (Argentine Time)',
        'America/Miquelon' => '(GMT-3:00) America/Miquelon (Pierre & Miquelon Standard Time)',
        'America/Montevideo' => '(GMT-3:00) America/Montevideo (Uruguay Time)',
        'America/Paramaribo' => '(GMT-3:00) America/Paramaribo (Suriname Time)',
        'America/Recife' => '(GMT-3:00) America/Recife (Brasilia Time)',
        'America/Rosario' => '(GMT-3:00) America/Rosario (Argentine Time)',
        'America/Santarem' => '(GMT-3:00) America/Santarem (Brasilia Time)',
        'America/Sao_Paulo' => '(GMT-3:00) America/Sao_Paulo (Brasilia Time)',
        'Antarctica/Rothera' => '(GMT-3:00) Antarctica/Rothera (Rothera Time)',
        'Brazil/East' => '(GMT-3:00) Brazil/East (Brasilia Time)',
        'America/Noronha' => '(GMT-2:00) America/Noronha (Fernando de Noronha Time)',
        'Atlantic/South_Georgia' => '(GMT-2:00) Atlantic/South_Georgia (South Georgia Standard Time)',
        'Brazil/DeNoronha' => '(GMT-2:00) Brazil/DeNoronha (Fernando de Noronha Time)',
        'America/Scoresbysund' => '(GMT-1:00) America/Scoresbysund (Eastern Greenland Time)',
        'Atlantic/Azores' => '(GMT-1:00) Atlantic/Azores (Azores Time)',
        'Atlantic/Cape_Verde' => '(GMT-1:00) Atlantic/Cape_Verde (Cape Verde Time)',
        'Africa/Abidjan' => '(GMT+0:00) Africa/Abidjan (Greenwich Mean Time)',
        'Africa/Accra' => '(GMT+0:00) Africa/Accra (Ghana Mean Time)',
        'Africa/Bamako' => '(GMT+0:00) Africa/Bamako (Greenwich Mean Time)',
        'Africa/Banjul' => '(GMT+0:00) Africa/Banjul (Greenwich Mean Time)',
        'Africa/Bissau' => '(GMT+0:00) Africa/Bissau (Greenwich Mean Time)',
        'Africa/Casablanca' => '(GMT+0:00) Africa/Casablanca (Western European Time)',
        'Africa/Conakry' => '(GMT+0:00) Africa/Conakry (Greenwich Mean Time)',
        'Africa/Dakar' => '(GMT+0:00) Africa/Dakar (Greenwich Mean Time)',
        'Africa/El_Aaiun' => '(GMT+0:00) Africa/El_Aaiun (Western European Time)',
        'Africa/Freetown' => '(GMT+0:00) Africa/Freetown (Greenwich Mean Time)',
        'Africa/Lome' => '(GMT+0:00) Africa/Lome (Greenwich Mean Time)',
        'Africa/Monrovia' => '(GMT+0:00) Africa/Monrovia (Greenwich Mean Time)',
        'Africa/Nouakchott' => '(GMT+0:00) Africa/Nouakchott (Greenwich Mean Time)',
        'Africa/Ouagadougou' => '(GMT+0:00) Africa/Ouagadougou (Greenwich Mean Time)',
        'Africa/Sao_Tome' => '(GMT+0:00) Africa/Sao_Tome (Greenwich Mean Time)',
        'Africa/Timbuktu' => '(GMT+0:00) Africa/Timbuktu (Greenwich Mean Time)',
        'America/Danmarkshavn' => '(GMT+0:00) America/Danmarkshavn (Greenwich Mean Time)',
        'Atlantic/Canary' => '(GMT+0:00) Atlantic/Canary (Western European Time)',
        'Atlantic/Faeroe' => '(GMT+0:00) Atlantic/Faeroe (Western European Time)',
        'Atlantic/Faroe' => '(GMT+0:00) Atlantic/Faroe (Western European Time)',
        'Atlantic/Madeira' => '(GMT+0:00) Atlantic/Madeira (Western European Time)',
        'Atlantic/Reykjavik' => '(GMT+0:00) Atlantic/Reykjavik (Greenwich Mean Time)',
        'Atlantic/St_Helena' => '(GMT+0:00) Atlantic/St_Helena (Greenwich Mean Time)',
        'Europe/Belfast' => '(GMT+0:00) Europe/Belfast (Greenwich Mean Time)',
        'Europe/Dublin' => '(GMT+0:00) Europe/Dublin (Greenwich Mean Time)',
        'Europe/Guernsey' => '(GMT+0:00) Europe/Guernsey (Greenwich Mean Time)',
        'Europe/Isle_of_Man' => '(GMT+0:00) Europe/Isle_of_Man (Greenwich Mean Time)',
        'Europe/Jersey' => '(GMT+0:00) Europe/Jersey (Greenwich Mean Time)',
        'Europe/Lisbon' => '(GMT+0:00) Europe/Lisbon (Western European Time)',
        'Europe/London' => '(GMT+0:00) Europe/London (Greenwich Mean Time)',
        'Africa/Algiers' => '(GMT+1:00) Africa/Algiers (Central European Time)',
        'Africa/Bangui' => '(GMT+1:00) Africa/Bangui (Western African Time)',
        'Africa/Brazzaville' => '(GMT+1:00) Africa/Brazzaville (Western African Time)',
        'Africa/Ceuta' => '(GMT+1:00) Africa/Ceuta (Central European Time)',
        'Africa/Douala' => '(GMT+1:00) Africa/Douala (Western African Time)',
        'Africa/Kinshasa' => '(GMT+1:00) Africa/Kinshasa (Western African Time)',
        'Africa/Lagos' => '(GMT+1:00) Africa/Lagos (Western African Time)',
        'Africa/Libreville' => '(GMT+1:00) Africa/Libreville (Western African Time)',
        'Africa/Luanda' => '(GMT+1:00) Africa/Luanda (Western African Time)',
        'Africa/Malabo' => '(GMT+1:00) Africa/Malabo (Western African Time)',
        'Africa/Ndjamena' => '(GMT+1:00) Africa/Ndjamena (Western African Time)',
        'Africa/Niamey' => '(GMT+1:00) Africa/Niamey (Western African Time)',
        'Africa/Porto-Novo' => '(GMT+1:00) Africa/Porto-Novo (Western African Time)',
        'Africa/Tunis' => '(GMT+1:00) Africa/Tunis (Central European Time)',
        'Africa/Windhoek' => '(GMT+1:00) Africa/Windhoek (Western African Time)',
        'Arctic/Longyearbyen' => '(GMT+1:00) Arctic/Longyearbyen (Central European Time)',
        'Atlantic/Jan_Mayen' => '(GMT+1:00) Atlantic/Jan_Mayen (Central European Time)',
        'Europe/Amsterdam' => '(GMT+1:00) Europe/Amsterdam (Central European Time)',
        'Europe/Andorra' => '(GMT+1:00) Europe/Andorra (Central European Time)',
        'Europe/Belgrade' => '(GMT+1:00) Europe/Belgrade (Central European Time)',
        'Europe/Berlin' => '(GMT+1:00) Europe/Berlin (Central European Time)',
        'Europe/Bratislava' => '(GMT+1:00) Europe/Bratislava (Central European Time)',
        'Europe/Brussels' => '(GMT+1:00) Europe/Brussels (Central European Time)',
        'Europe/Budapest' => '(GMT+1:00) Europe/Budapest (Central European Time)',
        'Europe/Copenhagen' => '(GMT+1:00) Europe/Copenhagen (Central European Time)',
        'Europe/Gibraltar' => '(GMT+1:00) Europe/Gibraltar (Central European Time)',
        'Europe/Ljubljana' => '(GMT+1:00) Europe/Ljubljana (Central European Time)',
        'Europe/Luxembourg' => '(GMT+1:00) Europe/Luxembourg (Central European Time)',
        'Europe/Madrid' => '(GMT+1:00) Europe/Madrid (Central European Time)',
        'Europe/Malta' => '(GMT+1:00) Europe/Malta (Central European Time)',
        'Europe/Monaco' => '(GMT+1:00) Europe/Monaco (Central European Time)',
        'Europe/Oslo' => '(GMT+1:00) Europe/Oslo (Central European Time)',
        'Europe/Paris' => '(GMT+1:00) Europe/Paris (Central European Time)',
        'Europe/Podgorica' => '(GMT+1:00) Europe/Podgorica (Central European Time)',
        'Europe/Prague' => '(GMT+1:00) Europe/Prague (Central European Time)',
        'Europe/Rome' => '(GMT+1:00) Europe/Rome (Central European Time)',
        'Europe/San_Marino' => '(GMT+1:00) Europe/San_Marino (Central European Time)',
        'Europe/Sarajevo' => '(GMT+1:00) Europe/Sarajevo (Central European Time)',
        'Europe/Skopje' => '(GMT+1:00) Europe/Skopje (Central European Time)',
        'Europe/Stockholm' => '(GMT+1:00) Europe/Stockholm (Central European Time)',
        'Europe/Tirane' => '(GMT+1:00) Europe/Tirane (Central European Time)',
        'Europe/Vaduz' => '(GMT+1:00) Europe/Vaduz (Central European Time)',
        'Europe/Vatican' => '(GMT+1:00) Europe/Vatican (Central European Time)',
        'Europe/Vienna' => '(GMT+1:00) Europe/Vienna (Central European Time)',
        'Europe/Warsaw' => '(GMT+1:00) Europe/Warsaw (Central European Time)',
        'Europe/Zagreb' => '(GMT+1:00) Europe/Zagreb (Central European Time)',
        'Europe/Zurich' => '(GMT+1:00) Europe/Zurich (Central European Time)',
        'Africa/Blantyre' => '(GMT+2:00) Africa/Blantyre (Central African Time)',
        'Africa/Bujumbura' => '(GMT+2:00) Africa/Bujumbura (Central African Time)',
        'Africa/Cairo' => '(GMT+2:00) Africa/Cairo (Eastern European Time)',
        'Africa/Gaborone' => '(GMT+2:00) Africa/Gaborone (Central African Time)',
        'Africa/Harare' => '(GMT+2:00) Africa/Harare (Central African Time)',
        'Africa/Johannesburg' => '(GMT+2:00) Africa/Johannesburg (South Africa Standard Time)',
        'Africa/Kigali' => '(GMT+2:00) Africa/Kigali (Central African Time)',
        'Africa/Lubumbashi' => '(GMT+2:00) Africa/Lubumbashi (Central African Time)',
        'Africa/Lusaka' => '(GMT+2:00) Africa/Lusaka (Central African Time)',
        'Africa/Maputo' => '(GMT+2:00) Africa/Maputo (Central African Time)',
        'Africa/Maseru' => '(GMT+2:00) Africa/Maseru (South Africa Standard Time)',
        'Africa/Mbabane' => '(GMT+2:00) Africa/Mbabane (South Africa Standard Time)',
        'Africa/Tripoli' => '(GMT+2:00) Africa/Tripoli (Eastern European Time)',
        'Asia/Amman' => '(GMT+2:00) Asia/Amman (Eastern European Time)',
        'Asia/Beirut' => '(GMT+2:00) Asia/Beirut (Eastern European Time)',
        'Asia/Damascus' => '(GMT+2:00) Asia/Damascus (Eastern European Time)',
        'Asia/Gaza' => '(GMT+2:00) Asia/Gaza (Eastern European Time)',
        'Asia/Istanbul' => '(GMT+2:00) Asia/Istanbul (Eastern European Time)',
        'Asia/Jerusalem' => '(GMT+2:00) Asia/Jerusalem (Israel Standard Time)',
        'Asia/Nicosia' => '(GMT+2:00) Asia/Nicosia (Eastern European Time)',
        'Asia/Tel_Aviv' => '(GMT+2:00) Asia/Tel_Aviv (Israel Standard Time)',
        'Europe/Athens' => '(GMT+2:00) Europe/Athens (Eastern European Time)',
        'Europe/Bucharest' => '(GMT+2:00) Europe/Bucharest (Eastern European Time)',
        'Europe/Chisinau' => '(GMT+2:00) Europe/Chisinau (Eastern European Time)',
        'Europe/Helsinki' => '(GMT+2:00) Europe/Helsinki (Eastern European Time)',
        'Europe/Istanbul' => '(GMT+2:00) Europe/Istanbul (Eastern European Time)',
        'Europe/Kaliningrad' => '(GMT+2:00) Europe/Kaliningrad (Eastern European Time)',
        'Europe/Kiev' => '(GMT+2:00) Europe/Kiev (Eastern European Time)',
        'Europe/Mariehamn' => '(GMT+2:00) Europe/Mariehamn (Eastern European Time)',
        'Europe/Minsk' => '(GMT+2:00) Europe/Minsk (Eastern European Time)',
        'Europe/Nicosia' => '(GMT+2:00) Europe/Nicosia (Eastern European Time)',
        'Europe/Riga' => '(GMT+2:00) Europe/Riga (Eastern European Time)',
        'Europe/Simferopol' => '(GMT+2:00) Europe/Simferopol (Eastern European Time)',
        'Europe/Sofia' => '(GMT+2:00) Europe/Sofia (Eastern European Time)',
        'Europe/Tallinn' => '(GMT+2:00) Europe/Tallinn (Eastern European Time)',
        'Europe/Tiraspol' => '(GMT+2:00) Europe/Tiraspol (Eastern European Time)',
        'Europe/Uzhgorod' => '(GMT+2:00) Europe/Uzhgorod (Eastern European Time)',
        'Europe/Vilnius' => '(GMT+2:00) Europe/Vilnius (Eastern European Time)',
        'Europe/Zaporozhye' => '(GMT+2:00) Europe/Zaporozhye (Eastern European Time)',
        'Africa/Addis_Ababa' => '(GMT+3:00) Africa/Addis_Ababa (Eastern African Time)',
        'Africa/Asmara' => '(GMT+3:00) Africa/Asmara (Eastern African Time)',
        'Africa/Asmera' => '(GMT+3:00) Africa/Asmera (Eastern African Time)',
        'Africa/Dar_es_Salaam' => '(GMT+3:00) Africa/Dar_es_Salaam (Eastern African Time)',
        'Africa/Djibouti' => '(GMT+3:00) Africa/Djibouti (Eastern African Time)',
        'Africa/Kampala' => '(GMT+3:00) Africa/Kampala (Eastern African Time)',
        'Africa/Khartoum' => '(GMT+3:00) Africa/Khartoum (Eastern African Time)',
        'Africa/Mogadishu' => '(GMT+3:00) Africa/Mogadishu (Eastern African Time)',
        'Africa/Nairobi' => '(GMT+3:00) Africa/Nairobi (Eastern African Time)',
        'Antarctica/Syowa' => '(GMT+3:00) Antarctica/Syowa (Syowa Time)',
        'Asia/Aden' => '(GMT+3:00) Asia/Aden (Arabia Standard Time)',
        'Asia/Baghdad' => '(GMT+3:00) Asia/Baghdad (Arabia Standard Time)',
        'Asia/Bahrain' => '(GMT+3:00) Asia/Bahrain (Arabia Standard Time)',
        'Asia/Kuwait' => '(GMT+3:00) Asia/Kuwait (Arabia Standard Time)',
        'Asia/Qatar' => '(GMT+3:00) Asia/Qatar (Arabia Standard Time)',
        'Europe/Moscow' => '(GMT+3:00) Europe/Moscow (Moscow Standard Time)',
        'Europe/Volgograd' => '(GMT+3:00) Europe/Volgograd (Volgograd Time)',
        'Indian/Antananarivo' => '(GMT+3:00) Indian/Antananarivo (Eastern African Time)',
        'Indian/Comoro' => '(GMT+3:00) Indian/Comoro (Eastern African Time)',
        'Indian/Mayotte' => '(GMT+3:00) Indian/Mayotte (Eastern African Time)',
        'Asia/Tehran' => '(GMT+3:30) Asia/Tehran (Iran Standard Time)',
        'Asia/Baku' => '(GMT+4:00) Asia/Baku (Azerbaijan Time)',
        'Asia/Dubai' => '(GMT+4:00) Asia/Dubai (Gulf Standard Time)',
        'Asia/Muscat' => '(GMT+4:00) Asia/Muscat (Gulf Standard Time)',
        'Asia/Tbilisi' => '(GMT+4:00) Asia/Tbilisi (Georgia Time)',
        'Asia/Yerevan' => '(GMT+4:00) Asia/Yerevan (Armenia Time)',
        'Europe/Samara' => '(GMT+4:00) Europe/Samara (Samara Time)',
        'Indian/Mahe' => '(GMT+4:00) Indian/Mahe (Seychelles Time)',
        'Indian/Mauritius' => '(GMT+4:00) Indian/Mauritius (Mauritius Time)',
        'Indian/Reunion' => '(GMT+4:00) Indian/Reunion (Reunion Time)',
        'Asia/Kabul' => '(GMT+4:30) Asia/Kabul (Afghanistan Time)',
        'Asia/Aqtau' => '(GMT+5:00) Asia/Aqtau (Aqtau Time)',
        'Asia/Aqtobe' => '(GMT+5:00) Asia/Aqtobe (Aqtobe Time)',
        'Asia/Ashgabat' => '(GMT+5:00) Asia/Ashgabat (Turkmenistan Time)',
        'Asia/Ashkhabad' => '(GMT+5:00) Asia/Ashkhabad (Turkmenistan Time)',
        'Asia/Dushanbe' => '(GMT+5:00) Asia/Dushanbe (Tajikistan Time)',
        'Asia/Karachi' => '(GMT+5:00) Asia/Karachi (Pakistan Time)',
        'Asia/Oral' => '(GMT+5:00) Asia/Oral (Oral Time)',
        'Asia/Samarkand' => '(GMT+5:00) Asia/Samarkand (Uzbekistan Time)',
        'Asia/Tashkent' => '(GMT+5:00) Asia/Tashkent (Uzbekistan Time)',
        'Asia/Yekaterinburg' => '(GMT+5:00) Asia/Yekaterinburg (Yekaterinburg Time)',
        'Indian/Kerguelen' => '(GMT+5:00) Indian/Kerguelen (French Southern & Antarctic Lands Time)',
        'Indian/Maldives' => '(GMT+5:00) Indian/Maldives (Maldives Time)',
        'Asia/Calcutta' => '(GMT+5:30) Asia/Calcutta (India Standard Time)',
        'Asia/Colombo' => '(GMT+5:30) Asia/Colombo (India Standard Time)',
        'Asia/Kolkata' => '(GMT+5:30) Asia/Kolkata (India Standard Time)',
        'Asia/Katmandu' => '(GMT+5:45) Asia/Katmandu (Nepal Time)',
        'Antarctica/Mawson' => '(GMT+6:00) Antarctica/Mawson (Mawson Time)',
        'Antarctica/Vostok' => '(GMT+6:00) Antarctica/Vostok (Vostok Time)',
        'Asia/Almaty' => '(GMT+6:00) Asia/Almaty (Alma-Ata Time)',
        'Asia/Bishkek' => '(GMT+6:00) Asia/Bishkek (Kirgizstan Time)',
        'Asia/Dacca' => '(GMT+6:00) Asia/Dacca (Bangladesh Time)',
        'Asia/Dhaka' => '(GMT+6:00) Asia/Dhaka (Bangladesh Time)',
        'Asia/Novosibirsk' => '(GMT+6:00) Asia/Novosibirsk (Novosibirsk Time)',
        'Asia/Omsk' => '(GMT+6:00) Asia/Omsk (Omsk Time)',
        'Asia/Qyzylorda' => '(GMT+6:00) Asia/Qyzylorda (Qyzylorda Time)',
        'Asia/Thimbu' => '(GMT+6:00) Asia/Thimbu (Bhutan Time)',
        'Asia/Thimphu' => '(GMT+6:00) Asia/Thimphu (Bhutan Time)',
        'Indian/Chagos' => '(GMT+6:00) Indian/Chagos (Indian Ocean Territory Time)',
        'Asia/Rangoon' => '(GMT+6:30) Asia/Rangoon (Myanmar Time)',
        'Indian/Cocos' => '(GMT+6:30) Indian/Cocos (Cocos Islands Time)',
        'Antarctica/Davis' => '(GMT+7:00) Antarctica/Davis (Davis Time)',
        'Asia/Bangkok' => '(GMT+7:00) Asia/Bangkok (Indochina Time)',
        'Asia/Ho_Chi_Minh' => '(GMT+7:00) Asia/Ho_Chi_Minh (Indochina Time)',
        'Asia/Hovd' => '(GMT+7:00) Asia/Hovd (Hovd Time)',
        'Asia/Jakarta' => '(GMT+7:00) Asia/Jakarta (West Indonesia Time)',
        'Asia/Krasnoyarsk' => '(GMT+7:00) Asia/Krasnoyarsk (Krasnoyarsk Time)',
        'Asia/Phnom_Penh' => '(GMT+7:00) Asia/Phnom_Penh (Indochina Time)',
        'Asia/Pontianak' => '(GMT+7:00) Asia/Pontianak (West Indonesia Time)',
        'Asia/Saigon' => '(GMT+7:00) Asia/Saigon (Indochina Time)',
        'Asia/Vientiane' => '(GMT+7:00) Asia/Vientiane (Indochina Time)',
        'Indian/Christmas' => '(GMT+7:00) Indian/Christmas (Christmas Island Time)',
        'Antarctica/Casey' => '(GMT+8:00) Antarctica/Casey (Western Standard Time (Australia))',
        'Asia/Brunei' => '(GMT+8:00) Asia/Brunei (Brunei Time)',
        'Asia/Choibalsan' => '(GMT+8:00) Asia/Choibalsan (Choibalsan Time)',
        'Asia/Chongqing' => '(GMT+8:00) Asia/Chongqing (China Standard Time)',
        'Asia/Chungking' => '(GMT+8:00) Asia/Chungking (China Standard Time)',
        'Asia/Harbin' => '(GMT+8:00) Asia/Harbin (China Standard Time)',
        'Asia/Hong_Kong' => '(GMT+8:00) Asia/Hong_Kong (Hong Kong Time)',
        'Asia/Irkutsk' => '(GMT+8:00) Asia/Irkutsk (Irkutsk Time)',
        'Asia/Kashgar' => '(GMT+8:00) Asia/Kashgar (China Standard Time)',
        'Asia/Kuala_Lumpur' => '(GMT+8:00) Asia/Kuala_Lumpur (Malaysia Time)',
        'Asia/Kuching' => '(GMT+8:00) Asia/Kuching (Malaysia Time)',
        'Asia/Macao' => '(GMT+8:00) Asia/Macao (China Standard Time)',
        'Asia/Macau' => '(GMT+8:00) Asia/Macau (China Standard Time)',
        'Asia/Makassar' => '(GMT+8:00) Asia/Makassar (Central Indonesia Time)',
        'Asia/Manila' => '(GMT+8:00) Asia/Manila (Philippines Time)',
        'Asia/Shanghai' => '(GMT+8:00) Asia/Shanghai (China Standard Time)',
        'Asia/Singapore' => '(GMT+8:00) Asia/Singapore (Singapore Time)',
        'Asia/Taipei' => '(GMT+8:00) Asia/Taipei (China Standard Time)',
        'Asia/Ujung_Pandang' => '(GMT+8:00) Asia/Ujung_Pandang (Central Indonesia Time)',
        'Asia/Ulaanbaatar' => '(GMT+8:00) Asia/Ulaanbaatar (Ulaanbaatar Time)',
        'Asia/Ulan_Bator' => '(GMT+8:00) Asia/Ulan_Bator (Ulaanbaatar Time)',
        'Asia/Urumqi' => '(GMT+8:00) Asia/Urumqi (China Standard Time)',
        'Australia/Perth' => '(GMT+8:00) Australia/Perth (Western Standard Time (Australia))',
        'Australia/West' => '(GMT+8:00) Australia/West (Western Standard Time (Australia))',
        'Australia/Eucla' => '(GMT+8:45) Australia/Eucla (Central Western Standard Time (Australia))',
        'Asia/Dili' => '(GMT+9:00) Asia/Dili (Timor-Leste Time)',
        'Asia/Jayapura' => '(GMT+9:00) Asia/Jayapura (East Indonesia Time)',
        'Asia/Pyongyang' => '(GMT+9:00) Asia/Pyongyang (Korea Standard Time)',
        'Asia/Seoul' => '(GMT+9:00) Asia/Seoul (Korea Standard Time)',
        'Asia/Tokyo' => '(GMT+9:00) Asia/Tokyo (Japan Standard Time)',
        'Asia/Yakutsk' => '(GMT+9:00) Asia/Yakutsk (Yakutsk Time)',
        'Australia/Adelaide' => '(GMT+9:30) Australia/Adelaide (Central Standard Time (South Australia))',
        'Australia/Broken_Hill' => '(GMT+9:30) Australia/Broken_Hill (Central Standard Time (South Australia/New South Wales))',
        'Australia/Darwin' => '(GMT+9:30) Australia/Darwin (Central Standard Time (Northern Territory))',
        'Australia/North' => '(GMT+9:30) Australia/North (Central Standard Time (Northern Territory))',
        'Australia/South' => '(GMT+9:30) Australia/South (Central Standard Time (South Australia))',
        'Australia/Yancowinna' => '(GMT+9:30) Australia/Yancowinna (Central Standard Time (South Australia/New South Wales))',
        'Antarctica/DumontDUrville' => '(GMT+10:00) Antarctica/DumontDUrville (Dumont-d\'Urville Time)',
        'Asia/Sakhalin' => '(GMT+10:00) Asia/Sakhalin (Sakhalin Time)',
        'Asia/Vladivostok' => '(GMT+10:00) Asia/Vladivostok (Vladivostok Time)',
        'Australia/ACT' => '(GMT+10:00) Australia/ACT (Eastern Standard Time (New South Wales))',
        'Australia/Brisbane' => '(GMT+10:00) Australia/Brisbane (Eastern Standard Time (Queensland))',
        'Australia/Canberra' => '(GMT+10:00) Australia/Canberra (Eastern Standard Time (New South Wales))',
        'Australia/Currie' => '(GMT+10:00) Australia/Currie (Eastern Standard Time (New South Wales))',
        'Australia/Hobart' => '(GMT+10:00) Australia/Hobart (Eastern Standard Time (Tasmania))',
        'Australia/Lindeman' => '(GMT+10:00) Australia/Lindeman (Eastern Standard Time (Queensland))',
        'Australia/Melbourne' => '(GMT+10:00) Australia/Melbourne (Eastern Standard Time (Victoria))',
        'Australia/NSW' => '(GMT+10:00) Australia/NSW (Eastern Standard Time (New South Wales))',
        'Australia/Queensland' => '(GMT+10:00) Australia/Queensland (Eastern Standard Time (Queensland))',
        'Australia/Sydney' => '(GMT+10:00) Australia/Sydney (Eastern Standard Time (New South Wales))',
        'Australia/Tasmania' => '(GMT+10:00) Australia/Tasmania (Eastern Standard Time (Tasmania))',
        'Australia/Victoria' => '(GMT+10:00) Australia/Victoria (Eastern Standard Time (Victoria))',
        'Australia/LHI' => '(GMT+10:30) Australia/LHI (Lord Howe Standard Time)',
        'Australia/Lord_Howe' => '(GMT+10:30) Australia/Lord_Howe (Lord Howe Standard Time)',
        'Asia/Magadan' => '(GMT+11:00) Asia/Magadan (Magadan Time)',
        'Antarctica/McMurdo' => '(GMT+12:00) Antarctica/McMurdo (New Zealand Standard Time)',
        'Antarctica/South_Pole' => '(GMT+12:00) Antarctica/South_Pole (New Zealand Standard Time)',
        'Asia/Anadyr' => '(GMT+12:00) Asia/Anadyr (Anadyr Time)',
        'Asia/Kamchatka' => '(GMT+12:00) Asia/Kamchatka (Petropavlovsk-Kamchatski Time)',
        'UTC' => 'UTC'
    ];
    if ($timezone == null) {
        return $output;
    } else if (is_array($timezone)) {
        return array_intersect($output, $timezone);
    } else {
        return isset($output[$timezone]) ? $output[$timezone] : $timezone;
    }
}

//get product shipping amount
function getProductShippingAmount($product_id, $shipping_id) {
    $price = 0;
    $productShipping = ProductShipping::where(['product_id' => $product_id, 'shipping_id' => $shipping_id])->first();
    if (isset($productShipping)) {
        $price = $productShipping->price;
    }
    return $price;
}


function shipping_address_details($order_id) {
    $data = [];
    $shipping = OrderShippingAddress::where('order_id', $order_id)->first();
    if (isset($shipping)) {
        $data = $shipping;
    }
    return $data;
}

function billing_address_details($order_id) {
    $data = [];
    $shipping = OrderBillingAddress::where('order_id', $order_id)->first();
    if (isset($shipping)) {
        $data = $shipping;
    }
    return $data;
}


// generate order id

function generate_order_id() {
    $orderId = 100000000;
    $order = Order::orderBy('id', 'desc')->first();
    if ($order) {
        $orderId = $order->order_id;
    }
    return $orderId;
}

// order option list
function get_order_option_list($sub_order_id) {
    $data = '';
    $items = OrderProductOption::where('sub_order_id', $sub_order_id)->first();
    if (isset($items)) {
        $data = $items;
    }
    return $data;
}

// order addons list
function get_order_addons_list($sub_order_id) {
    $data = [];
    $items = OrderProductAddon::where('sub_order_id', $sub_order_id)->get();
    if (isset($items[0])) {
        $data = $items;
    }
    return $data;
}

// order addons list
function get_order_supplements_list($sub_order_id) {
    $data = [];
    $items = OrderProductSupplement::where('sub_order_id', $sub_order_id)->get();
    if (isset($items[0])) {
        $data = $items;
    }
    return $data;
}

// count product by price
function product_count_by_price($min, $max) {
    $count = 0;
    $product = Product::where(['status' => STATUS_ACTIVE])
        ->whereBetween('price', [$min, $max])
        ->get();
    if (isset($product[0])) {
        $count = $product->count();
    }

    return $count;
}

// check offer
function check_offer($product_id) {
    $data['is_offer'] = 0;
    $offer = Offer::where(['product_id' => $product_id, 'status' => STATUS_ACTIVE])->first();
    if (isset($offer)) {
        $data = [
            'is_offer' => 1,
            'discount_type' => $offer->discount_type,
            'discount' => $offer->discount,
        ];
    }

    return $data;
}

function isMultiSelect($key, $obj) {
    foreach ($obj as $ob) {
        if ($key == $ob->country_key) {
            return 'selected';
        }
    }
    return '';
}

function isMultiSelectLicence($key, $obj) {

    foreach ($obj as $ob) {
        if ($key == $ob->licence_id) {
            return 'selected';
        }
    }
    return '';
}

function isSelect($key, $val) {
    if ($key == $val) {
        return 'selected';
    }
    return '';
}

function sendMail($email, $data, $subject = '') {
    Mail::to($email)->send(new SendEmail($subject, $data));
}

function sendForgotPassWordMail($email, $data, $subject = '')
{
    Mail::to($email)->send(new SendPassResetEmail($subject,$data));
}

function check_field() {

    return $check = \App\User::where('id', Auth::id())
        ->where('first_name', '!=', null)
        ->where('last_name', '!=', null)
        ->where('email', '!=', null)
        ->where('dob', '!=', null)
        ->where('phone', '!=', null)
        ->where('country', '!=', null)
        ->first();
}

function get_image_url($url) {
    if (!isValidURL($url)) {
        return asset(getImagePath('user') . $url);
    } else {
        return $url;
    }

}

function isValidURL($url) {
    return preg_match('|^http(s)?://[a-z0-9-]+(.[a-z0-9-]+)*(:[0-9]+)?(/.*)?$|i', $url);
}

